# To-do
# 1) Find side-chain backbone RCI uncertainties (- requires new leave one out optimization or/and multiple trajectories optimization of backbone RCI)
# 2) Develop sensitivity enchanced side-chain RCI with reduced floor or/and increased ceiling values. 
# 3) Develop side-chain only RCI and RCI combos for limited backbone (i+-1 or i+-2 or i+-3, etc.) + side-chain RCI
# 4) Test Random Coil Order Parameter on testing set and entripy examples
# 5) Optimize RCI to predict MD order parameter for the whole side-chain
# 6) Optimize RCI to predict experimental methyl order parameters
# 7) Output entropy values
# 8) Add parsers for:
#	a) Shifty
#   b) Sparky
#   c) NMRView
#   d) CARA
#   e) FASTA sequence file


#######################
# Defaults
#######################
mode="1" # If mode=1, RCI uses coefficients for complete assignment, if mode=2. RCI uses coefficients for assignments without sc protons
do_bb_rci=0# To-do
do_sc_rci=1
bb_rci_path=""# To-do
sc_rci_path=""# To-do
sc_rci_error=1 # If 1, find side-chain RCI undertainties
project=""
location=""
verbose=0
#side_chain_rci_dir="init"
pics=0
show_typical_sc_error=0
gnuplot=""
python_program="python"
always_run_rci=1# To-do
actually_run_rci=1# To-do
shift_file=""
sc_rci_mean=0  # To-do
bb_rci_mean=0 # To-do
gnuplop=""
sc_rci_options=""
exclude=""
#sc_rci_program="side_chain_rci_v2_7_24_13_A.py"
#sc_rci_program="side_chain_rci_v2_8_1_13_A.py"
#sc_rci_program="side_chain_rci_v2_8_1_13_B.py"
#sc_rci_program="side_chain_rci_v2_8_25_13_A_dev.py"
sc_rci_program="side_chain_rci_v2_9_16_13_B.py"


sc_coef_dict_file="4_26_13_power1_1_scaling1_4_floor1_0_5_ceil2_0_5_RCImethod_15_Wishart_CoefOnly_1_ResSense1_EXCLUDE0_spearman_All_Cases_THEBEST_rci_parameters_test1.py"
sc_coef_dict_file_asa="4_26_13_power1_1_scaling1_4_floor1_0_5_ceil2_0_5_RCImethod_15_Wishart_CoefOnly_1_ResSense1_EXCLUDE0_spearman_All_Cases_THEBEST_rci_parameters_test1.py"


sc_coef_dict_file_no_sc_protons="10_4_13_power1_1_scaling1_4_floor1_0_5_ceil_0_5_BB_coef_3_RCImethod_15_Wishart_CoefOnly_ResSense1_EXCLUDE0_ALLres_spearman_All_Cases_THEBEST_no_sc_h.py"
sc_coef_dict_file_asa_no_sc_protons="10_4_13_power1_1_scaling1_4_floor1_0_5_ceil_0_5_BB_coef_3_RCImethod_15_Wishart_CoefOnly_ResSense1_EXCLUDE0_ALLres_spearman_All_Cases_THEBEST_no_sc_h.py"




# To-do
sc_rci_result_dict={}
sc_rci_result_dict["Side_chain_RCI"]={"file":"Side_chain_RCI.txt","per_aa_dict":{}}
sc_rci_result_dict["Side_chain_NMR_RMSD"]={"file":"Side_chain_NMR_RMSD.txt","per_aa_dict":{}}
sc_rci_result_dict["Side_chain_MD_RMSD"]={"file":"Side_chain_MD_RMSD.txt","per_aa_dict":{}}
sc_rci_result_dict["ASAf"]={"file":"ASAf.txt","per_aa_dict":{}}







#print "sc_coef_dict_file= ", sc_coef_dict_file
#sys.exit()


sc_rci_coeff_dic={}
sc_rci_coeff_dic["All"]={}
sc_rci_coeff_dic["All"]["pdb"]="All"
sc_rci_coeff_dic["All"]["bmrb"]="All"
sc_rci_coeff_dic["All"]["file"]=sc_coef_dict_file




#########################################################
# Import global modules
#########################################################
import os, sys, time, stats, pstat,copy, random
from string import split, upper, lower
from os import linesep
#########################################################

#####################
# Standard  Functions:
#####################

def import_local_modules_old(l_module_file,l_script_location,l_prefix):
	"""
	Import Python dictionary
	"""
	verbose=1
	if verbose>=1: print "l_module_file= ", l_module_file
	l_module_base_file=os.path.basename(l_module_file)
	if len(l_module_base_file)>3 and l_module_base_file[-3:]==".py":
		l_module_file_base="%s_%s" % (l_prefix,l_module_base_file[:-3])
	else:
		l_module_file_base="%s_%s" %  (l_prefix,l_module_base_file)
	module_py="%s.py" % l_module_file_base
	module_pyc="%s.pyc" % l_module_file_base
	if  verbose>=1: print "module_py= ", module_py
	if  verbose>=1: print "module_pyc= ", module_pyc
	if file_exists(module_py): 
		if  verbose>=1: print "Removing module_py= ", module_py 
		os.system("rm %s " % module_py)
	if file_exists(module_pyc):
		if  verbose>=1: print "Removing module_pyc= ", module_pyc 
		os.system("rm %s " % module_pyc)

	module_local_filename,module_local_basename =copy_module_file(l_module_file, l_module_file_base,l_script_location)
	module_results = __import__(module_local_basename)

	reload(module_results)
	l_dic=copy.deepcopy(module_results.per_model_report_dict)	
	return l_dic, module_local_filename

def import_local_modules(l_module_file,l_script_location,l_prefix):
	"""
	Import Python dictionary
	"""
	verbose=1
	if verbose>=1: print "l_module_file= ", l_module_file
	l_module_base_file=os.path.basename(l_module_file)
	if len(l_module_base_file)>3 and l_module_base_file[-3:]==".py":
		l_module_file_base="%s_%s" % (l_prefix,l_module_base_file[:-3])
	else:
		l_module_file_base="%s_%s" %  (l_prefix,l_module_base_file)
	module_py="%s.py" % l_module_file_base
	module_pyc="%s.pyc" % l_module_file_base
	if  verbose>=1: print "module_py= ", module_py
	if  verbose>=1: 
		if  verbose>=1: print "Removing module_pyc= ", module_pyc 
		print "module_pyc= ", module_pyc
	if file_exists(module_py): 
		if  verbose>=1: print "Removing module_py= ", module_py 
		os.system("rm %s " % module_py)
	if file_exists(module_pyc): os.system("rm %s " % module_pyc)
	module_local_filename,module_local_basename =copy_module_file(l_module_file, l_module_file_base,l_script_location)
	module_results = __import__(module_local_basename)
	reload(module_results)
	l_dic=copy.deepcopy(module_results.report_dict)	
	return l_dic, module_local_filename


def copy_module_file(l_module_full_path, l_module_basename,l_script_location):
	"""
	Copy module file
	"""
	verbose=1
	local_module_name="%s.py" % (l_module_basename)
	local_module_basename="%s" % (l_module_basename)
	copy_command="cp %s %s/%s" % (l_module_full_path,l_script_location,local_module_name)
	if os.access("%sc" % local_module_name, os.F_OK)==1:
		os.system("rm %sc" % local_module_name)
	if os.access("%s" % local_module_name, os.F_OK)==1:
		os.system("rm %s" % local_module_name)
	if verbose>=11: 
		print copy_command
		print "local_module_name=",local_module_name
		print "local_module_basename=", local_module_basename
	os.system(copy_command)
	
	return local_module_name, local_module_basename



def make_dir(dir_name):
	"""
	Create directory if does not exist
	"""
	dir_exist=os.access(dir_name, os.F_OK)
	if dir_exist==0:
		os.system('mkdir %s' %  dir_name)
	else:
		os.system('rm -r -f %s' %  dir_name)	
		os.system('mkdir %s' %  dir_name)	
	return()	


def make_dir_no_remove(dir_name):
	"""
	Create directory if does not exist
	"""
	dir_exist=os.access(dir_name, os.F_OK)
	if dir_exist==0:
		os.system('mkdir %s' %  dir_name)
	return()

def is_number(l_entry):
	"""
	Function to determine if a string can be converted into a float. 
	Returns 1 if yes, 0 if no
	"""
	l_result=0
	try:
		a=float(l_entry)
		l_result=1
	except:
		l_result=0
	return(l_result)
	

def read_file(L_file_name):
	"""
	Read files, output lines
	"""
	done_input1_switch=0 
	while not done_input1_switch: 
		input_file0s=split(L_file_name) 
		input_file1=input_file0s[0] 
		input_file1_exist=os.access(input_file1, os.F_OK) 
		input_file1_read=os.access(input_file1, os.R_OK) 
		if input_file1_exist==0: 
			print ("""\n ERROR: File with  name %s does not exist. 
 			Please try again.\n: """ % input_file1) 
			sys.exit(1) 
		elif input_file1_read==0: 
			print("""\n ERROR: The program can not read the file.\n: """) 
			sys.exit(1)   
		else: 
			done_input1_switch=1    
	file_open=open(L_file_name,'r')
	file_lines = file_open.readlines()
	file_open.close()
	return(file_lines)	


def write_file(l_text,l_file_name):
	"""
	Write a file with results
	"""
	if l_text!="":
		l_file_name_full="%s" % (l_file_name)
		l_file_name_open=open(l_file_name_full,"w")
		l_file_name_open.write(l_text)
		l_file_name_open.close()		
	return		


def append_file(l_text,l_file_name):
	"""
	Write a file with results
	"""
	if l_text!="":
		l_file_name_full="%s" % (l_file_name)
		l_file_name_open=open(l_file_name_full,"a")
		l_file_name_open.write(l_text)
		l_file_name_open.close()		
	return		



def debug_exit(l_switch):
	"""
	Debugging exit
	"""
	if l_switch==1:
		print "Exiting...." 
		sys.exit()		
	return

def debug_print_exit(l_switch,l_text):
	"""
	Debugging exit
	"""
	if l_switch==1:
		print l_text
		print "Exiting...." 
		sys.exit()		
	return



def debug_print(l_switch,l_text):
	"""
	Debugging exit
	"""
	if l_switch==1:
		print l_text
	return

def file_exists(l_file):
	"""
	Return 1 if file exists and 0 if it does not 
	"""	
	exists=os.access(l_file, os.F_OK)
	return exists


#####################
# End of standard  Functions:
#####################



def run_sc_rci_4_dict(\
	l_script_location,\
	l_project_full_path,\
	l_do_sc_rci,\
	l_sc_rci_error,\
	l_sc_coef_dict_file,\
	l_sc_rci_coeff_dic,\
	l_python_program,\
	l_always_run_rci,\
	l_actually_run_rci,\
	l_shift_file,\
	l_sc_rci_program,\
	l_sc_rci_options,\
	l_sc_rci_result_dict,\
	):
	"""
	Run side-chain RCI

sc_rci_coeff_dic["yhgG"]={}
sc_rci_coeff_dic["yhgG"]["pdb"]="1XN7"
sc_rci_coeff_dic["yhgG"]["bmrb"]="6367"
sc_rci_coeff_dic["yhgG"]["file"]="no_yhgG_4_26_13_power1_1_scaling1_4_floor1_0_5_ceil_0_5_RCImethod_15_Wishart_CoefOnly_ResSense1_EXCLUDE0_ALLres_1_spearman_All_Cases_THEBEST_rci_parameters.py"

 python RCI_training_v1_9_2_13_A.py  -cluster jasper -submit 0 -p Training_set_SC_ASA_BB_Coef_1 -walltime 48 -gridsearch 0 -rmsf_log /media/backup/acer/manual/research/RCI/RMSF_data/2_26_13_B_MD_results/Ubiquitin_model_test_setup_info.txt_RESULTS_2_26_13_B_results_sc_only_rmsf_results.txt -l /media/sdb1/research/RCI2  -rmsf_cs  /home/markber/Dropbox/Research/RCI/init/bmr17769.str.corr   -bb_rci 1  -zodb 0  -miss_ass 0  -gap2 0  -smooth 0  -power1 1  -scaling1 4  -floor1 0.5  -ceil1 0.5 -run_rci 1 -stereomode 1 -nostereo 1  -rci_method 15  -res_sense 1 -coef_only 1  -bb_coef 1 -bb_smooth 3 -av 2 -rc_offset_grid 0   -no_outliers 0  -value_mode 1 -rc 1 -exclude 1  -rci_always 1  -zope 0 -run_rci 1 -rci_param ALL_4_26_13_power1_1_scaling1_4_floor1_0_5_ceil_0_5_RCImethod_15_Wishart_CoefOnly_ResSense1_EXCLUDE0_ALLres_1_spearman_All_Cases_THEBEST_rci_parameters.py  -test_dic  testing_dict_train1.py  -sc_asa 1  -rci_always 1  -do_ratio 1 -find_ratio 1


	
		
markber@markber-Aspire-E700:~/Dropbox/Research/RCI$ ls -lrt  /home/markber/Dropbox/Research/RCI/test/side_chain_rci/results/2K50_15819_csq_dir/rci_results/nogridsearch/pics/data
total 20
-rw-rw-r-- 1 markber markber 1409 Sep  5 04:07 Side_chain_RCI.txt
-rw-rw-r-- 1 markber markber 1357 Sep  5 04:07 Side_chain_NMR_RMSD.txt
-rw-rw-r-- 1 markber markber 1342 Sep  5 04:07 Side_chain_MD_RMSD.txt
-rw-rw-r-- 1 markber markber 1461 Sep  5 04:07 Side_chain_Backbone_RCI.txt
-rw-rw-r-- 1 markber markber 1407 Sep  5 04:07 ASAf.txt
		
	"""
	verbose=1
	if verbose>=1: 
		print "Running function run_sc_rci_4_dict()"
	l_dict={}
	#sys.exit()

	make_dir_no_remove(l_project_full_path)

	side_chain_rci_directory="%s/side_chain_rci" % (l_project_full_path)
	make_dir_no_remove(side_chain_rci_directory)

	side_chain_rci_results_directory="%s/results" % (side_chain_rci_directory)
	make_dir_no_remove(side_chain_rci_results_directory)


	side_chain_rci_reports_directory="%s/reports" % (side_chain_rci_directory)
	make_dir_no_remove(side_chain_rci_reports_directory)

	if l_do_sc_rci==1:
		if l_sc_rci_error==0:
			l_dict["ALL"]={}
			l_dict["ALL"]["pdb"]="ALL"
			l_dict["ALL"]["bmrb"]="ALL"
			l_dict["ALL"]["file"]=l_sc_coef_dict_file
		else:
			l_dict=l_sc_rci_coeff_dic

		for i in l_dict:
			pdb=l_dict[i]["pdb"]
			bmrb=l_dict[i]["bmrb"]
			l_sc_rci_param=l_dict[i]["file"]
			if verbose==1:
				print "i = ", i
				print "pdb = ", pdb
				print "bmrb = ", bmrb
				print "l_sc_rci_param = ", l_sc_rci_param
			sub_project = "%s_%s" % (pdb,bmrb)
			l_sc_rci_options+=" -l %s " % side_chain_rci_results_directory
			l_sc_rci_options+=" -p %s " % sub_project


			sc_rci_result_dir="%s/%s_csq_dir/rci_results/nogridsearch/pics/data" % (side_chain_rci_results_directory,sub_project)
			rci_result_file="%s/Side_chain_RCI.txt" % sc_rci_result_dir
			result_found=0
			if file_exists(rci_result_file):
				result_found=1

			if l_always_run_rci==1 or result_found==0:
				run_side_chain_rci(\
				l_python_program,\
				l_sc_rci_param,\
				l_actually_run_rci,\
				l_shift_file,\
				l_sc_rci_program,\
				l_sc_rci_options,\
				l_script_location,\
				l_sc_rci_result_dict,\
				)
			
			collect_sc_rci_results(sc_rci_result_dir,l_sc_rci_result_dict,sub_project)
	
			debug=0
			if debug==1:
				print "sc_rci_result_dir= ", sc_rci_result_dir
				print "Exiting..."
				sys.exit()
	

	find_sc_rci_mean_and_std(l_sc_rci_result_dict,side_chain_rci_reports_directory)

	debug=1
	if debug==1:
		print "Exiting after function  run_sc_rci()"
		sys.exit()

	return

def find_sc_rci_mean_and_std(l_sc_rci_result_dict,side_chain_rci_reports_directory):
	"""	
	Find SC RCI means and standard deviations

 79: {'res_name': 'Q', 'value_list': [5.33091425245], 'value_dict': {'2K50_15819': '5.33091425245'}}}, 'file': 'Side_chain_MD_RMSD.txt'}, 'Side_chain_NMR_RMSD': {'per_aa_dict': {}, 'file': 'Side_chain_NMR_RMSD.txt'}, 'Side_chain_RCI': {'per_aa_dict': {}, 'file': 'Side_chain_RCI.txt'}, 'ASAf': {'per_aa_dict': {}, 'file': 'ASAf.txt'}}
	"""
	verbose=1
	if verbose>=1: 
		print "Running function find_sc_rci_mean_and_std()"

	side_chain_rci_pics_dir="%s/pics" %  side_chain_rci_reports_directory
	make_dir_no_remove(side_chain_rci_pics_dir)


	side_chain_rci_images_dir="%s/images" %  side_chain_rci_pics_dir
	make_dir_no_remove(side_chain_rci_images_dir)


	side_chain_rci_data_dir="%s/data" %  side_chain_rci_pics_dir
	make_dir_no_remove(side_chain_rci_data_dir)

	for parameter in l_sc_rci_result_dict:
		per_aa_dict=l_sc_rci_result_dict[parameter]["per_aa_dict"]
		l_parameter_text=""
		l_parameter_file="%s/%s.txt" % (side_chain_rci_data_dir,parameter)
		for residue in per_aa_dict:
			res_name=per_aa_dict[residue]['res_name']
			value_list=per_aa_dict[residue]['value_list']
			sc_rci_mean_value=sc_rci_std_value="NONE"
			if len(value_list)>0:
				#print  parameter, residue, "value_list=", value_list
				sc_rci_mean_value=stats.lmean(value_list)
			if len(value_list)>1:
				sc_rci_std_value=stats.lstdev(value_list)
				print  parameter, residue, sc_rci_std_value, "value_list=", value_list

			#per_aa_dict['mean']=sc_rci_mean_value
			#per_aa_dict['std']=sc_rci_std_value

			l_parameter_text+="%s %s %s" % (residue,sc_rci_mean_value,sc_rci_std_value) + linesep

		print "Writing data file ", l_parameter_file
		write_file(l_parameter_text,l_parameter_file)

	debug=1
	if debug==1:
		print "Exiting after function  find_sc_rci_mean_and_std()"
		sys.exit()


	return
def collect_sc_rci_results(sc_rci_result_dir,l_sc_rci_result_dict,l_sub_project):
	"""
	
	Collect results of side-chain RCI

1 0.667819087839 M
2 0.502625885105 D
3 0.45466527386 R
4 0.243090297279 V
5 0.163206738742 L
6 0.131090389166 S
7 0.295245341028 R
8 0.146839535591 A
9 0.285170462425 D


sc_rci_result_dict={}
sc_rci_result_dict["Side_chain_RCI"]={"file":"Side_chain_RCI.txt","per_aa_dict":{}}
sc_rci_result_dict["Side_chain_NMR_RMSD"]={"file":"Side_chain_NMR_RMSD.txt","per_aa_dict":{}}
sc_rci_result_dict["Side_chain_MD_RMSD"]={"file":"Side_chain_MD_RMSD.txt","per_aa_dict":{}}
sc_rci_result_dict["ASAf"]={"file":"ASAf.txt","per_aa_dict":{}}

	"""
	verbose=1
	if verbose>=1: 
		print "Running function collect_sc_rci_results()"

	for parameter in l_sc_rci_result_dict:
		l_file=l_sc_rci_result_dict[parameter]["file"]
		per_aa_dict=l_sc_rci_result_dict[parameter]["per_aa_dict"]
		l_full_path="%s/%s" % (sc_rci_result_dir,l_file)
		l_file_lines=read_file(l_full_path)
		for i in l_file_lines:
			j=i.split()
			if len(j)==3:
				residue_number, rci_value, residue_name=int(j[0]),j[1],j[2]
				if verbose>=10: print residue_number, rci_value, residue_name
				if residue_number not in per_aa_dict:
					per_aa_dict[residue_number]={"res_name":residue_name,"value_list":[],"value_dict":{}}
				existing_residue_name=per_aa_dict[residue_number]["res_name"]
				if existing_residue_name!=residue_name:
					print "Warning!!! existing_residue_name %s !=residue_name %s for residue %s in %s %s " % (existing_residue_name,residue_name,residue_number,l_sub_project, parameter)
				else:
					if is_number(rci_value):
						per_aa_dict[residue_number]["value_list"]+=[float(rci_value)]
						if verbose>=10: print per_aa_dict[residue_number]["value_list"]
					else:
						print "Warning!!! RCI value for residue %s%s in case %s and file %s is not numerical and is " % (residue_name,residue_number,l_sub_project, parameter), [rci_value]
					per_aa_dict[residue_number]["value_dict"][l_sub_project]=rci_value	


		debug=0
		if debug==1:
			print "l_sc_rci_result_dict=", l_sc_rci_result_dict
			print "Exiting..."
			sys.exit()

	return 


def run_side_chain_rci(\
	l_python_program,\
	l_sc_rci_param,\
	l_actually_run_rci,\
	l_shift_file,\
	l_sc_rci_program,\
	l_sc_rci_options,\
	l_script_location,\
	l_sc_rci_result_dict,\
	):
	"""
	Run side chain RCI
	"""
	verbose=1
	if verbose>=1: 
		print "Running function run_side_chain_rci()"
	cur_dir=os.getcwd()
	rci_program_location="%s/init" % l_script_location


	if "/" not in shift_file:
		shift_file_full_path="%s/init/%s" % (l_script_location,cs_basename)
	else:
		shift_file_full_path=os.path.abspath(shift_file)

	sc_rci_command="%s %s -b %s -rci_param %s %s" % (l_python_program,l_sc_rci_program,shift_file_full_path,l_sc_rci_param,l_sc_rci_options)  #python  side_chain_rci_v2_8_25_13_A_dev.py -b 10147.txt   -i1 1 -bb_coef 0 -floor 0.1 -ceil1 2
	if verbose>=1: print "sc_rci_command = ", sc_rci_command
	if l_actually_run_rci:
		os.chdir(rci_program_location)
		os.system(sc_rci_command)	
		os.chdir(cur_dir)

		debug=0
		if debug==1:
			print "Exiting..."
			sys.exit()

	debug=0
	if debug==1:
		print "Exiting after function  run_side_chain_rci()"
		sys.exit()
	return

		

def main(\
	l_script_location,\
	l_sc_rci_coeff_dic,\
	l_project_full_path,\
	l_do_sc_rci,\
	l_do_bb_rci,\
	l_sc_rci_error,\
	l_sc_coef_dict_file,\
	l_python_program,\
	l_always_run_rci,\
	l_actually_run_rci,\
	l_shift_file,\
	l_sc_rci_program,\
	l_sc_rci_options,\
	l_sc_rci_result_dict,\
	l_sc_rci_mean,\
	l_bb_rci_mean,\
	l_pics,\
	l_show_typical_sc_error,\
	l_sc_coef_dict_file_asa,\
	):
	"""
	Main function
	"""
	if l_sc_rci_mean==0:
		run_sc_rci(\
			l_project_full_path,\
			l_do_sc_rci,\
			l_pics,\
			l_show_typical_sc_error,\
			l_shift_file,\
			l_python_program,\
			l_script_location,\
			l_sc_rci_program,\
			l_sc_rci_options,\
			l_sc_coef_dict_file,\
			l_sc_coef_dict_file_asa,\
			)
	else:
		run_sc_rci_4_dict(\
		l_script_location,\
		l_project_full_path,\
		l_do_sc_rci,\
		l_sc_rci_error,\
		l_sc_coef_dict_file,\
		l_sc_rci_coeff_dic,\
		l_python_program,\
		l_always_run_rci,\
		l_actually_run_rci,\
		l_shift_file,\
		l_sc_rci_program,\
		l_sc_rci_options,\
		l_sc_rci_result_dict,\
		)
	return

def run_sc_rci(\
			l_project_full_path,\
			l_do_sc_rci,\
			l_pics,\
			l_show_typical_sc_error,\
			l_shift_file,\
			l_python_program,\
			l_script_location,\
			l_sc_rci_program,\
			l_sc_rci_options,\
			l_rci_param,\
			l_rci_param_asa,\
			):
	"""
	Run side-chain RCI
	"""
	verbose=1
	if l_do_sc_rci==1:
		if verbose>=1: print "Running function run_sc_rci()"
		rci_command_without_asa="%s %s -b %s %s -asa 0 -rl %s -rci_param %s " % (l_python_program,l_sc_rci_program,l_shift_file,l_sc_rci_options,l_project_full_path,l_rci_param)
		if verbose>=1: print "rci_command_without_asa = ", rci_command_without_asa
		os.system(rci_command_without_asa)

		rci_command_with_asa="%s %s -b %s %s -asa 1 -rl %s  -rci_param %s " % (l_python_program,l_sc_rci_program,l_shift_file,l_sc_rci_options,l_project_full_path,l_rci_param_asa)
		if verbose>=1: print "rci_command_with_asa = ", rci_command_with_asa
		os.system(rci_command_with_asa)

	debug=0
	if debug==1:
		print "Exiting after function run_sc_rci()"
		sys.exit()
	return

########################################################
# Parsing arguments
########################################################	


arguments="python "
a=0
if len(sys.argv)>1:
	for item in sys.argv:
		arguments+=" %s " % item
		##############################
		# General options
		##############################
		if item == "-p":
			project=sys.argv[a+1]
		if item == "-l":
			location=os.path.abspath(sys.argv[a+1])
		if item in [ "-cs","-b"]:
			shift_file=os.path.abspath(sys.argv[a+1])
		if item == "-do_bb_rci":
			do_bb_rci=int(sys.argv[a+1])
		if item == "-do_sc_rci":# To-do
			do_sc_rci=int(sys.argv[a+1])
		if item == "-bb_rci":# To-do
			bb_rci_path=os.path.abspath(sys.argv[a+1])
		if item == "-sc_rci":# To-do
			sc_rci_path=os.path.abspath(sys.argv[a+1])
		if item == "-rci_always":# To-do
			always_run_rci=int(sys.argv[a+1])
		if item=="-sc_rci_mean": # To-do
			sc_rci_mean=int(sys.argv[a+1])
		if item=="-bb_rci_mean":# To-do
			bb_rci_mean=int(sys.argv[a+1])
		if item=="-pics":
			pics=int(sys.argv[a+1])
		if item=="-e":
			show_typical_sc_error=int(sys.argv[a+1])
		if item == "-gnuplot":
			gnuplot=os.path.abspath(sys.argv[a+1])

		if item=="-mode":# To-do
			mode=sys.argv[a+1]
		if item=="-exclude":# To-do
			exclude=int(sys.argv[a+1])
		#############


		a+=1
		

########################################################
# Processing arguments
########################################################	
this_script_full_path=os.path.abspath(sys.argv[0])
this_script_name=os.path.basename(sys.argv[0])
script_location=os.path.dirname(this_script_full_path)
if verbose>3:
	print "this_script_full_path=", this_script_full_path
	print "this_script_name=", this_script_name
	print "script_location=", script_location

cur_dir=os.getcwd()
if location=="":
	location=cur_dir

if location!="" and project!="":
	project_full_path="%s/%s" % (location,project)
elif location=="" and project!="":
	project_full_path="%s/%s" % (script_location,project)
else:
	project_full_path=script_location

if shift_file!="":
	sc_rci_options+=" -b %s " % shift_file

if gnuplot!="":
	sc_rci_options+=" -gnuplot %s " %  gnuplot

sc_rci_options+=" -pics %s " % pics
sc_rci_options+=" -e %s " % show_typical_sc_error


if mode in ["2","no_sc_protons", "no_sc_h"]:
	sc_coef_dict_file=sc_coef_dict_file_no_sc_protons
	sc_coef_dict_file_asa=sc_coef_dict_file_asa_no_sc_protons
	sc_rci_options+=" -exclude 0 "

if exclude!="":
	sc_rci_options+=" -exclude %s "	 % exclude

########################################################
# Main
########################################################	

main(\
	script_location,\
	sc_rci_coeff_dic,\
	project_full_path,\
	do_sc_rci,\
	do_bb_rci,\
	sc_rci_error,\
	sc_coef_dict_file,\
	python_program,\
	always_run_rci,\
	actually_run_rci,\
	shift_file,\
	sc_rci_program,\
	sc_rci_options,\
	sc_rci_result_dict,\
	sc_rci_mean,\
	bb_rci_mean,\
	pics,\
	show_typical_sc_error,\
	sc_coef_dict_file_asa,\
	)





