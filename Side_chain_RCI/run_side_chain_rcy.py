#!/usr/bin/python

from os import listdir, system
from os.path import exists, join

bmrbDir = "/Users/Noor/Documents/Research/CMPUT-605Dec12/CMPUT-605/testfiles/bmrb-addPDBresno"

bmrbFiles = [file for file in listdir(bmrbDir) if file.endswith(".str.corr.pdbresno")]

outputDir = "/Users/Noor/Documents/Research/FeatureGenerateMay08/FeatureGenerate/SideChain_RCI_flexibility"

scRCIdir = "/Users/Noor/Documents/Research/Side_chain_RCI"

#bmrbFiles = ["A001_bmr4032.str.corr.pdbresno"]
for bmrbFile in bmrbFiles:
    
    file_prefix = bmrbFile.split(".")[0]
    
    scRCIfile = file_prefix+'.Side_chain_RCI.txt'
    
    if not exists(join(outputDir, scRCIfile)):
        system("python side_chain_rci_v2_9_16_13_B.py -b %s/%s" %(bmrbDir, bmrbFile))
        system("mv %s/%s %s/%s" %(scRCIdir, bmrbFile+'.Side_chain_RCI.txt', outputDir, scRCIfile ))
        system("rm %s/%s*" %(scRCIdir, bmrbFile))