
shift_class_dict={}
shift_class_dict["H"]=["All", "Backbone","Main_chain","Protons","Backbone_protons","Main_chain_protons","H","RCI"]
shift_class_dict["H"]+=["All_ave", "Backbone_ave","Main_chain_ave","Protons_ave","Backbone_protons_ave","Main_chain_protons_ave"]

shift_class_dict["HN"]=["All", "Backbone","Main_chain","Protons","Backbone_protons","Main_chain_protons","H"]
shift_class_dict["HN"]+=["All_ave", "Backbone_ave","Main_chain_ave","Protons_ave","Backbone_protons_ave","Main_chain_protons_ave"]

shift_class_dict["NH"]=["All", "Backbone","Main_chain","Protons","Backbone_protons","Main_chain_protons","H"]
shift_class_dict["NH"]+=["All_ave", "Backbone_ave","Main_chain_ave","Protons_ave","Backbone_protons_ave","Main_chain_protons_ave"]


shift_class_dict["HAave"]=["All_ave","Backbone_ave","Main_chain_ave","Protons_ave","Backbone_protons_ave","Main_chain_protons_ave","HAave","RCI"]
shift_class_dict["HA"]=["All","Backbone","Main_chain","Protons","Backbone_protons","Main_chain_protons","HAall","HA"]
shift_class_dict["HA1"]=["All","Backbone","Main_chain","Protons","Backbone_protons","Main_chain_protons","HAall","HA1"]
shift_class_dict["HA2"]=["All","Backbone","Main_chain","Protons","Backbone_protons","Main_chain_protons","HAall","HA2"]
shift_class_dict["HA3"]=["All","Backbone","Main_chain","Protons","Backbone_protons","Main_chain_protons","HAall","HA3"]

shift_class_dict["HB"]=["All","Backbone","Side_chain","Protons","side_chain_protons","HB","HBall","No_Cbeta_side_chain"]
shift_class_dict["HBave"]=["All_ave","Backbone_ave","Side_chain_ave","Protons_ave","side_chain_protons_ave","HBave","No_Cbeta_side_chain_ave"]
shift_class_dict["HB1"]=["All","Backbone","Side_chain","Protons","side_chain_protons","HBall","HB1","No_Cbeta_side_chain"]
shift_class_dict["HB2"]=["All","Backbone","Side_chain","Protons","side_chain_protons","HBall","HB2","No_Cbeta_side_chain"]
shift_class_dict["HB3"]=["All","Backbone","Side_chain","Protons","side_chain_protons","HBall","HB3","No_Cbeta_side_chain"]

shift_class_dict["HGave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","No_Cbeta_side_chain_ave","Protons_ave","side_chain_protons_ave","No_beta_side_chain_protons_ave","HGave"]
shift_class_dict["HG"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HG","HGall"]
shift_class_dict["HG1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG1"]
shift_class_dict["HG2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG2"]
shift_class_dict["HG3"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG3"]
shift_class_dict["HG12"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG12"]
shift_class_dict["HG13"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG13"]
shift_class_dict["HG11"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG11"]
shift_class_dict["HG22"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG21"]
shift_class_dict["HG23"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG22"]
shift_class_dict["HG21"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HGall","HG23"]


shift_class_dict["HDave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","Protons_ave","side_chain_protons_ave","No_beta_side_chain_protons_ave","HDave"]
shift_class_dict["HD"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HD","HDall"]
shift_class_dict["HD1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD1"]
shift_class_dict["HD2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD2"]
shift_class_dict["HD3"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD3"]
shift_class_dict["HD21"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD21"]
shift_class_dict["HD22"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD22"]
shift_class_dict["HD23"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD23"]
shift_class_dict["HD11"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD11"]
shift_class_dict["HD12"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD12"]
shift_class_dict["HD13"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HDall","HD13"]

shift_class_dict["HEave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","Protons_ave","side_chain_protons_ave","No_beta_side_chain_protons_ave","HEave"]
shift_class_dict["HE"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HE","HEall"]
shift_class_dict["HE1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HEall","HE1"]
shift_class_dict["HE2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HEall","HE2"]
shift_class_dict["HE21"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HEall","HE21"]
shift_class_dict["HE22"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HEall","HE22"]
shift_class_dict["HE3"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HEall","HE3"]

shift_class_dict["HZave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","Protons_ave","side_chain_protons_ave","No_beta_side_chain_protons_ave","HZave"]
shift_class_dict["HZ"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HZ","HZall"]
shift_class_dict["HZ1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HZall","HZ1"]
shift_class_dict["HZ2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HZall","HZ2"]
shift_class_dict["HZ3"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HZall","HZ3"]

shift_class_dict["HHave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","Protons_ave","side_chain_protons_ave","No_beta_side_chain_protons_ave","HHave"]
shift_class_dict["HH"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HHall","HH"]
shift_class_dict["HH11"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HHall","HH11"]
shift_class_dict["HH12"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HHall","HH12"]
shift_class_dict["HH2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HHall","HH2"]
shift_class_dict["HH21"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HHall","HH21"]
shift_class_dict["HH22"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","Protons","side_chain_protons","No_beta_side_chain_protons","HHall","HH22"]

shift_class_dict["CGave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","heavy_atoms_ave","side_chain_heavy_atoms_ave","No_beta_side_chain_heavy_atoms_ave","CGave"]
shift_class_dict["CG"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CGall","CG"]
shift_class_dict["CG1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CGall","CG1"]
shift_class_dict["CG2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CGall","CG2"]

shift_class_dict["CDave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","heavy_atoms_ave","side_chain_heavy_atoms_ave","No_beta_side_chain_heavy_atoms_ave","CDave"]
shift_class_dict["CD"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CD","CDall"]
shift_class_dict["CD1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CDall","CD1"]
shift_class_dict["CD2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CDall","CD2"]

shift_class_dict["CEave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","heavy_atoms_ave","side_chain_heavy_atoms_ave","No_beta_side_chain_heavy_atoms_ave","CEave"]
shift_class_dict["CE"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CEall","CE"]
shift_class_dict["CE1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CEall","CE1"]
shift_class_dict["CE2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CEall","CE2"]
shift_class_dict["CE3"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CEall","CE3"]

shift_class_dict["CZave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","heavy_atoms_ave","side_chain_heavy_atoms_ave","No_beta_side_chain_heavy_atoms_ave","CZave"]
shift_class_dict["CZ"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CZall","CZ"]
shift_class_dict["CZ2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CZall","CZ2"]
shift_class_dict["CZ3"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CZall","CZ3"]

shift_class_dict["CH2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","CH2"]
shift_class_dict["CH2"]+=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","heavy_atoms_ave","side_chain_heavy_atoms_ave","No_beta_side_chain_heavy_atoms_ave","CH2"]

shift_class_dict["NEave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","heavy_atoms_ave","side_chain_heavy_atoms_ave","No_beta_side_chain_heavy_atoms_ave","NEave"]
shift_class_dict["NE"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","NEall","NE"]
shift_class_dict["NE1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","NEall","NE1"]
shift_class_dict["NE2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","NEall","NE2"]

shift_class_dict["NDave"]=["All_ave","Side_chain_ave","No_beta_side_chain_ave","No_Cbeta_side_chain_ave","heavy_atoms_ave","side_chain_heavy_atoms_ave","No_beta_side_chain_heavy_atoms_ave","NDave"]
shift_class_dict["ND"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","NDall","ND"]
shift_class_dict["ND1"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","NDall","ND1"]
shift_class_dict["ND2"]=["All","Side_chain","No_beta_side_chain","No_Cbeta_side_chain","heavy_atoms","side_chain_heavy_atoms","No_beta_side_chain_heavy_atoms","NDall","ND2"]

#shift_class_dict["NH_again"]=["H_again"]
#shift_class_dict["HA_again"]=["HA_again"]
							
shift_class_dict["N"]=["All","Backbone","Main_chain","heavy_atoms","Backbone_heavy_atoms","Main_chain_heavy_atoms","N","RCI"]
shift_class_dict["N"]+=["All_ave","Backbone_ave","Main_chain_ave","heavy_atoms_ave","Backbone_heavy_atoms_ave","Main_chain_heavy_atoms_ave"]

shift_class_dict["CA"]=["All","Backbone","Main_chain","heavy_atoms","Backbone_heavy_atoms","Main_chain_heavy_atoms","CA","RCI"]
shift_class_dict["CA"]+=["All_ave","Backbone_ave","Main_chain_ave","heavy_atoms_ave","Backbone_heavy_atoms_ave","Main_chain_heavy_atoms_ave"]

shift_class_dict["CB"]=["All","Backbone","Side_chain","heavy_atoms","Backbone_heavy_atoms","Main_chain_heavy_atoms","CB","RCI"]
shift_class_dict["CB"]+=["All_ave","Backbone_ave","Side_chain_ave","heavy_atoms_ave","Backbone_heavy_atoms_ave","Main_chain_heavy_atoms_ave"]

shift_class_dict["C"]=["All","Backbone","Main_chain","heavy_atoms","Backbone_heavy_atoms","Main_chain_heavy_atoms","C","RCI"]
shift_class_dict["C"]+=["All_ave","Backbone_ave","Main_chain_ave","heavy_atoms_ave","Backbone_heavy_atoms_ave","Main_chain_heavy_atoms_ave"]
#shift_class_dict["CO"]=["All","Backbone","Main_chain","heavy_atoms","Backbone_heavy_atoms","Main_chain_heavy_atoms","CO"]


