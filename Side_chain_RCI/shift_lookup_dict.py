dic={}
dic['HD22']= ['HD22']
dic['HD23']= ['HD23']
dic['HD21']= ['HD21']
dic['CEave']= ['CEave']
dic['CDave']= ['CDave']
dic['CZave']= ['CZave']
dic['HZave']= ['HZave']
dic['HDall']= ['HD22', 'HD23', 'HD21', 'HD', 'HD3', 'HD1', 'HD2', 'HD13', 'HD12', 'HD11']
dic['heavy_atoms']= [ 'CZ2', 'CZ3', 'C', 'CE3', 'CE2', 'CE1', 'N', 'CH2', 'CD1', 'CD2', 'ND', 'NE','ND2', 'CB', 'CA', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'NE2', 'NE1', 'CZ']
dic['heavy_atoms_ave']= ['CEave', 'CDave', 'CZave', 'NDave',  'C',  'CGave', 'N', 'CH2', 'NEave', 'CB', 'CA','CH2']
dic['HG12']= ['HG12']
dic['HG13']= ['HG13']
dic['HZ1']= ['HZ1']
dic['HZ3']= ['HZ3']
dic['HZ2']= ['HZ2']
dic['HA1']= ['HA1']
dic['HA2']= ['HA2']
dic['HA3']= ['HA3']
dic['HEall']= ['HE22', 'HE21', 'HE', 'HE1', 'HE2', 'HE3']
dic['HG11']= ['HG11']
dic['Main_chain_heavy_atoms']= ['C', 'N', 'CB', 'CA']
dic['H']= ['H', 'HN', 'NH']
# Original
dic['Side_chain']= ['HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2', 'HB1', 'HB3', 'HB2', 'HZ', 'CZ2', 'CZ3', 'HH', 'HB', 'HE22', 'HE21', 'HG', 'HD', 'HE', 'CE3', 'CE2', 'CE1', 'HD3', 'HD1', 'CH2', 'HH12', 'HH11', 'HG21', 'HG23', 'HG22', 'CD1', 'CD2', 'HE1', 'HE2', 'HE3', 'ND', 'NE', 'HH22', 'HH21',  'ND2', 'CB', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'HD2', 'NE2', 'NE1', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11', 'CZ']
dic['Side_chain_ave']= ['CEave', 'CDave', 'CZave', 'HZave',  'NDave', 'HGave', 'CGave',  'HHave', 'HBave',  'NEave','HDave', 'HEave',"CB",'CH2']

#dic['Side_chain']= ["HA","CA","C","N","H",'HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2', 'HB1', 'HB3', 'HB2', 'HZ', 'CZ2', 'CZ3', 'HH', 'HB', 'HE22', 'HE21', 'HG', 'HD', 'HE', 'CE3', 'CE2', 'CE1', 'HD3', 'HD1', 'CH2', 'HH12', 'HH11', 'HG21', 'HG23', 'HG22', 'CD1', 'CD2', 'HE1', 'HE2', 'HE3', 'ND', 'NE', 'HH22', 'HH21',  'ND2', 'CB', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'HD2', 'NE2', 'NE1', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11', 'CZ']
#dic['Side_chain_ave']= ["HA","CA","C","N","H",'CEave', 'CDave', 'CZave', 'HZave',  'NDave', 'HGave', 'CGave',  'HHave', 'HBave',  'NEave','HDave', 'HEave',"CB",'CH2']



dic['HZall']= [ 'HZ1', 'HZ3', 'HZ2', 'HZ']
dic['HGave']= ['HGave']
dic['HB1']= ['HB1']
dic['HB3']= ['HB3']
dic['HB2']= ['HB2']
dic['HZ']= ['HZ']
dic['Backbone_heavy_atoms']= ['C', 'N', 'CB', 'CA']
dic['CZ2']= ['CZ2']
dic['CZ3']= ['CZ3']
dic['HH']= ['HH']
dic['HB']= ['HB']
dic['HE22']= ['HE22']
dic['HE21']= ['HE21']
dic['HA']= ['HA']
dic['HG']= ['HG']
dic['HD']= ['HD']
dic['HE']= ['HE']

dic['No_beta_side_chain']= ['HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2',  'HZ', 'CZ2', 'CZ3', 'HH', 'HE22', 'HE21', 'HG', 'HD', 'HE', 'CE3', 'CE2', 'CE1', 'HD3', 'HD1', 'CH2', 'HH12', 'HH11', 'HG21', 'HG23', 'HG22', 'CD1', 'CD2', 'HE1', 'HE2', 'HE3', 'ND', 'NE', 'HH22', 'HH21',  'ND2', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'HD2', 'NE2', 'NE1', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11', 'CZ']
dic['No_beta_side_chain_ave']= ['CEave', 'CDave', 'CZave', 'HZave',  'NDave', 'HGave', 'CGave',  'HHave',   'NEave','HDave', 'HEave','CH2']

dic['No_Cbeta_side_chain']= ['HB1', 'HB3', 'HB2',"HB",'HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2',  'HZ', 'CZ2', 'CZ3', 'HH', 'HE22', 'HE21', 'HG', 'HD', 'HE', 'CE3', 'CE2', 'CE1', 'HD3', 'HD1', 'CH2', 'HH12', 'HH11', 'HG21', 'HG23', 'HG22', 'CD1', 'CD2', 'HE1', 'HE2', 'HE3', 'ND', 'NE', 'HH22', 'HH21',  'ND2', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'HD2', 'NE2', 'NE1', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11', 'CZ']
dic['No_Cbeta_side_chain_ave']= ['HBave', 'CEave', 'CDave', 'CZave', 'HZave',  'NDave', 'HGave', 'CGave',  'HHave',   'NEave','HDave', 'HEave','CH2']



dic['CEall']= ['CE3', 'CE2', 'CE1', 'CE']
dic['C']= ['C']
dic['HBall']= ['HB1', 'HB3', 'HB2', 'HB']
dic['NEall']= ['NE', 'NEave', 'NE2', 'NE1']
dic['CE3']= ['CE3']
dic['CE2']= ['CE2']
dic['CE1']= ['CE1']
dic['No_beta_side_chain_heavy_atoms']= [ 'CZ2', 'CZ3', 'CE3', 'CE2', 'CE1',  'CH2', 'CD1', 'CD2', 'ND', 'NE','ND2', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'NE2', 'NE1', 'CZ']
dic['No_beta_side_chain_heavy_atoms_ave']= ['CEave', 'CDave', 'CZave', 'NDave',  'CGave', 'NEave','CH2']
dic['HD3']= ['HD3']
dic['CGave']= ['CGave']
dic['HD1']= ['HD1']
dic['HE3']= ['HE3']
dic['HHave']= ['HHave']
dic['CH2']= ['CH2']
dic['HH12']= ['HH12']
dic['HH11']= ['HH11']
dic['HG21']= ['HG22']
dic['HG23']= ['HG21']
dic['HG22']= ['HG23']
dic['HBave']= ['HBave']
dic['CD1']= ['CD1']
dic['CD2']= ['CD2']
dic['HDave']= ['HDave']
dic['HE1']= ['HE1']
dic['HE2']= ['HE2']
dic['Backbone_protons']= ['HA1', 'HA2', 'HA3', 'H', 'HN', 'HA', 'NH']
dic['Backbone_protons_ave']= [ 'H', 'HN', 'NH', 'HAave']


dic['Backbone']= ['HA1', 'HA2', 'HA3', 'H', 'HB1', 'HB3', 'HB2', 'HN', 'HB', 'HA', 'C', 'N', 'NH', 'CB', 'CA']
dic['Backbone_ave']= [ 'H', 'HN', 'C', 'N', 'HBave', 'NH', 'HAave', 'CB', 'CA']


dic['RCI']= [ 'H',  'C', 'N',   'HAave', 'CB', 'CA']

dic['ND']= ['ND']
dic['NE']= ['NE']
dic['HH22']= ['HH22']
dic['HH21']= ['HH21']
dic['HHall']= ['HH', 'HH12', 'HH11', 'HH22', 'HH21', 'HH2']
dic['NEave']= ['NEave']
dic['HAave']= ['HAave']

dic['Main_chain']= ['HA1', 'HA2', 'HA3', 'H', 'HN', 'HA', 'C', 'N', 'NH', 'CA']
dic['Main_chain_ave']= [ 'H', 'HN',  'C', 'N', 'NH', 'HAave', 'CA']

dic['Main_chain_protons']= ['HA1', 'HA2', 'HA3', 'H', 'HN', 'HA', 'NH', ]
dic['Main_chain_protons_ave']= [ 'H', 'HN',  'NH', 'HAave']

dic['CB']= ['CB']
dic['CA']= ['CA']
dic['Protons']= ['HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2', 'HA1', 'HA2', 'HA3', 'H', 'HB1', 'HB3', 'HB2', 'HZ', 'HH', 'HN', 'HB', 'HE22', 'HE21', 'HA', 'HG', 'HD', 'HE', 'HD3', 'HD1',  'HH12', 'HH11', 'HG21', 'HG23', 'HG22',  'NH', 'HE1', 'HE2', 'HE3', 'HH22', 'HH21',  'HD2', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11']
dic['Protons_ave']= [ 'HZave',  'HGave', 'H', 'HN', 'NH','HHave',  'HBave',  'HAave','HDave', 'HEave']


dic['CE']= ['CE']
dic['N']= ['N']
dic['ND1']= ['ND1']
dic['CDall']= ['CD1', 'CD2', 'CD']
dic['CG1']= ['CG1']
dic['CG2']= ['CG2']
dic['HD2']= ['HD2']
dic['ND2']= ['ND2']
dic['NE2']= ['NE2']
dic['NE1']= ['NE1']
dic['HH2']= ['HH2']
dic['CGall']= ['CG', 'CG1', 'CG2']
dic['side_chain_protons']= ['HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2',  'HB1', 'HB3', 'HB2', 'HZ', 'HH', 'HB', 'HE22', 'HE21', 'HG', 'HD', 'HE', 'HD3', 'HD1',  'HH12', 'HH11', 'HG21', 'HG23', 'HG22', 'HE1', 'HE2', 'HE3', 'HH22', 'HH21', 'HD2', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11']
dic['side_chain_protons_ave']= [ 'HZave', 'HGave', 'HHave', 'HBave', 'HDave', 'HEave']

dic['NDave']= ['NDave']
dic['HGall']= ['HG11', 'HG12', 'HG13', 'HG', 'HG21', 'HG23', 'HG22', 'HG2', 'HG3', 'HG1']
dic['HG2']= ['HG2']
dic['HG3']= ['HG3']
dic['HG1']= ['HG1']
dic['HAall']= ['HA1', 'HA2', 'HA3', 'HA', 'HAave']
dic['HAall']= ['HA1', 'HA2', 'HA3', 'HA']


dic['No_beta_side_chain_protons']= ['HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2', 'HZ', 'HH', 'HE22', 'HE21', 'HG', 'HD', 'HE', 'HD3', 'HD1',  'HH12', 'HH11', 'HG21', 'HG23', 'HG22', 'HE1', 'HE2', 'HE3', 'HH22', 'HH21', 'HD2', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11']
dic['No_beta_side_chain_protons_ave']= ['HZave', 'HGave', 'HHave', 'HDave', 'HEave']


dic['side_chain_heavy_atoms']= ['CZ2', 'CZ3', 'CE3', 'CE2', 'CE1',  'CH2', 'CD1', 'CD2', 'ND', 'NE', 'ND2', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'NE2', 'NE1', 'CZ']
dic['side_chain_heavy_atoms_ave']= ['CEave', 'CDave', 'CZave', 'NDave',  'CGave', 'CH2',  'NEave']


dic['CG']= ['CG']
dic['CZall']= ['CZ2', 'CZ3', 'CZ']
dic['HD13']= ['HD13']
dic['HD12']= ['HD12']
dic['HD11']= ['HD11']
dic['CD']= ['CD']
dic['HEave']= ['HEave']

dic['All']= ['HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2', 'HA1', 'HA2', 'HA3', 'H',  'HB1', 'HB3', 'HB2', 'HZ', 'CZ2', 'CZ3', 'HH', 'HN', 'HB', 'HE22', 'HE21', 'HA', 'HG', 'HD', 'HE', 'C', 'CE3', 'CE2', 'CE1', 'HD3', 'HD1',  'N', 'CH2', 'HH12', 'HH11', 'HG21', 'HG23', 'HG22',  'CD1', 'CD2', 'NH', 'HE1', 'HE2', 'HE3', 'ND', 'NE', 'HH22', 'HH21',  'ND2', 'CB', 'CA', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'HD2', 'NE2', 'NE1', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11',  'CZ']
dic['All_ave']= ['CEave', 'CDave', 'CZave', 'HZave',  'NDave', 'H', 'HGave',  'C',  'CGave', 'HHave', 'N', 'CH2',  'HBave',  'NH', 'NEave', 'HAave',  'CB', 'CA',  'HDave', 'HEave',]

dic['CZ']= ['CZ']
dic['NDall']= [ 'ND', 'ND2', 'ND1']









