#####################
# Import global modules
#####################
import os, sys, time, stats, pstat,copy, random, math
from string import split, upper, lower
from os import linesep
#####################
# Import local modules
#####################
from shift_lookup_dict import dic as shift_class_lookup
from shift_classes_v2 import shift_class_dict as shift_class_dict
from shift_class_descriptions import shift_class_description_dict as shift_class_description_dict
from hertz_shift_classes import hertz_shift_classes as hertz_shift_classes

from backbone_random_coil_and_neighboring_corr import wang_preceed_nonextpro_0
from backbone_random_coil_and_neighboring_corr import wang_next_nonextpro_0
from backbone_random_coil_and_neighboring_corr import wang_preceed_nonextpro_1
from backbone_random_coil_and_neighboring_corr import wang_next_nonextpro_1
from backbone_random_coil_and_neighboring_corr import wang_preceed_nonextpro_2
from backbone_random_coil_and_neighboring_corr import wang_next_nonextpro_2
from backbone_random_coil_and_neighboring_corr import wang_preceed_nonextpro_3
from backbone_random_coil_and_neighboring_corr import wang_next_nonextpro_3
from backbone_random_coil_and_neighboring_corr import wright_preceed_nonextpro_0
from backbone_random_coil_and_neighboring_corr import wright_next_nonextpro_0
from backbone_random_coil_and_neighboring_corr import wright_preceed_nonextpro_1
from backbone_random_coil_and_neighboring_corr import wright_next_nonextpro_1
from backbone_random_coil_and_neighboring_corr import wright_preceed_nonextpro_2
from backbone_random_coil_and_neighboring_corr import wright_next_nonextpro_2
from backbone_random_coil_and_neighboring_corr import wright_preceed_nonextpro_3
from backbone_random_coil_and_neighboring_corr import wright_next_nonextpro_3
from backbone_random_coil_and_neighboring_corr import sw_preceed_nonextpro_0
from backbone_random_coil_and_neighboring_corr import sw_next_nonextpro_0
from backbone_random_coil_and_neighboring_corr import sw_preceed_nonextpro_1
from backbone_random_coil_and_neighboring_corr import sw_next_nonextpro_1
from backbone_random_coil_and_neighboring_corr import sw_preceed_nonextpro_2
from backbone_random_coil_and_neighboring_corr import sw_next_nonextpro_2
from backbone_random_coil_and_neighboring_corr import sw_preceed_nonextpro_3
from backbone_random_coil_and_neighboring_corr import sw_next_nonextpro_3
from backbone_random_coil_and_neighboring_corr import wright_preceed_nopreceedpro_1
from backbone_random_coil_and_neighboring_corr import wright_next_nopreceedpro_1

i_plus_minus_one_residue_correction_dict={}
i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_0"]=wang_preceed_nonextpro_0
i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_0"]=wang_next_nonextpro_0
i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_1"]=wang_preceed_nonextpro_1
i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_1"]=wang_next_nonextpro_1
i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_2"]=wang_preceed_nonextpro_2
i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_2"]=wang_next_nonextpro_2
i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_3"]=wang_preceed_nonextpro_3
i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_3"]=wang_next_nonextpro_3
i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_0"]=wright_preceed_nonextpro_0
i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_0"]=wright_next_nonextpro_0
i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_1"]=wright_preceed_nonextpro_1
i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_1"]=wright_next_nonextpro_1
i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_2"]=wright_preceed_nonextpro_2
i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_2"]=wright_next_nonextpro_2
i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_3"]=wright_preceed_nonextpro_3
i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_3"]=wright_next_nonextpro_3
i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_0"]=sw_preceed_nonextpro_0
i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_0"]=sw_next_nonextpro_0
i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_1"]=sw_preceed_nonextpro_1
i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_1"]=sw_next_nonextpro_1
i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_2"]=sw_preceed_nonextpro_2
i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_2"]=sw_next_nonextpro_2
i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_3"]=sw_preceed_nonextpro_3
i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_3"]=sw_next_nonextpro_3
i_plus_minus_one_residue_correction_dict["wright_preceed_nopreceedpro_1"]=wright_preceed_nopreceedpro_1
i_plus_minus_one_residue_correction_dict["wright_next_nopreceedpro_1"]=wright_next_nopreceedpro_1


from backbone_random_coil_and_neighboring_corr import preceed_preceed_res_effect
from backbone_random_coil_and_neighboring_corr import next_next_res_effect


i_plus_minus_two_residue_correction_dict={}
i_plus_minus_two_residue_correction_dict["preceed_preceed_res_effect"]=preceed_preceed_res_effect
i_plus_minus_two_residue_correction_dict["next_next_res_effect"]=next_next_res_effect


from  wishart_randomcoil import dic as wishart_side_chain_random_coil_dict
from  wright_randomcoil import dic as wright_side_chain_random_coil_dict

side_chain_random_coil_values={}
side_chain_random_coil_values["wishart_side_chain_random_coil_values"]=wishart_side_chain_random_coil_dict
side_chain_random_coil_values["wright_side_chain_random_coil_values"]=wright_side_chain_random_coil_dict

from backbone_random_coil_and_neighboring_corr import wishart_random_coil_dic
from backbone_random_coil_and_neighboring_corr import wang_random_coil_dic
from backbone_random_coil_and_neighboring_corr import lukin_random_coil_dic
from backbone_random_coil_and_neighboring_corr import wright_random_coil_dic
from backbone_random_coil_and_neighboring_corr import wright_wishart_random_coil_dic
from backbone_random_coil_and_neighboring_corr import wishart_wang_lukin_random_coil_dic
from backbone_random_coil_and_neighboring_corr import wang_wright_random_coil_dic
from backbone_random_coil_and_neighboring_corr import wright_lukin_random_coil_dic
from backbone_random_coil_and_neighboring_corr import mean_and_wright_random_coil_dic


backbone_random_coil_values_dict={}
backbone_random_coil_values_dict["wishart_random_coil_dic"]=wishart_random_coil_dic
backbone_random_coil_values_dict["wang_random_coil_dic"]=wang_random_coil_dic
backbone_random_coil_values_dict["lukin_random_coil_dic"]=lukin_random_coil_dic
backbone_random_coil_values_dict["wright_random_coil_dic"]=wright_random_coil_dic
backbone_random_coil_values_dict["wright_wishart_random_coil_dic"]=wright_wishart_random_coil_dic
backbone_random_coil_values_dict["wishart_wang_lukin_random_coil_dic"]=wishart_wang_lukin_random_coil_dic
backbone_random_coil_values_dict["wang_wright_random_coil_dic"]=wang_wright_random_coil_dic
backbone_random_coil_values_dict["wright_lukin_random_coil_dic"]=wright_lukin_random_coil_dic
backbone_random_coil_values_dict["mean_and_wright_random_coil_dic"]=mean_and_wright_random_coil_dic



from backbone_random_coil_and_neighboring_corr import coil_place
from backbone_random_coil_and_neighboring_corr import beta_place
from backbone_random_coil_and_neighboring_corr import helix_place

from backbone_random_coil_and_neighboring_corr import N_place
from backbone_random_coil_and_neighboring_corr import CO_place
from backbone_random_coil_and_neighboring_corr import CA_place
from backbone_random_coil_and_neighboring_corr import CB_place
from backbone_random_coil_and_neighboring_corr import NH_place
from backbone_random_coil_and_neighboring_corr import HA_place

from backbone_random_coil_and_neighboring_corr import place_dict

from  wishart_randomcoil import shiftx_rc_dic

random_coil_dic=wishart_random_coil_dic


from csq_atoms import atom_name_switch_dict
from csq_atoms import atom_dict
from csq_atoms import atom_list_for_ensemble as atom_list_for_ensemble_init
from csq_atoms import exp_assignment
from csq_atoms import shiftx_assignment
from csq_atoms import eligible_shifts
from csq_atoms import all_carbons
from csq_atoms import all_nitrogens
from csq_atoms import all_protons



files2copy={}

#files2copy["Side_chain_absolute_mean_B_factor.txt"]={"title":"Absolute mean side-chain B-factor", "y_label":"Absolute side-chain B-factort","base":"Absolute_mean_side_chain_B_factor","output_name":"Absolute_mean_side_chain_B_factor"}





######################################################
# Defaults
######################################################	
do_ensemble=1
g_debug=0
organize_dict={}
organize_dict["remove"]=[]
organize_dict["move"]={}
verbose=0
verbose2=0 # Only for command confirmation notice
non_split_dir="non_split_pdb_dir"
split_dir="split_pdb_dir"
plot_pict_dir="plot_images"
plot_data_dir="plot_data"
report_dir="reports"
ensemble_average_file_base_name="ensemble_averages"
make_pics=0
calc_offset=1 # Calculate offsets
max_offset=shifts_max_offset=camshift_max_offset=FourDspot_max_offset=shiftx2_max_offset=arshift_max_offset=ch3shift_max_offset=sparta_max_offset=meta_max_offset=rc_max_offset=""
predict_bmrb=0
zodb=0 # if 1, use Zope DB to store dictionaries
project="You_Must_Specify_Project_Name"
location=""
pdb=""
pdb_loc=""
pdb_abspath=""
pdb_dir=""
pdb_dir_abspath=""
bmrb_file_abspath=""
bmrb_file_loc=""
shifts_abspath=""
shifts_loc=""
shifts=""
shift_format=0 # 0 - BMRB, 1 - Shifty

chain="A"
strict=0 # If 1, only PDB files with .pdb extension will be used, else any files will be used
results_overwrite=0 # If 1, remove project directory with old results
all_pdbs_the_same=1 # If 0, treat PDB files as non_identical proteins
use_average_HA=1 # If 1, use aveage HA values for Gly
use_mean_or_median=0 # If 0, use mean, if 1, use median as a typical chemical shift
outlier_threashold=2 # number of standard deviation from the average to consider chemical shift an outlier
# Settings for calculating offset in residue numbering
strong_shift_limit=0.10
total_length_strong_offset=0 # If 1, strong offset flag depends on the ratio of the second offset residues vs total number of residues. If 0, the number of residues with max offset is used as the reference
make_training_files=1 # Make training files for ensemble building
ensemble_building_dir="ensemble_building"
matrix_filename="matrix.txt"
matrix_atom_list="" # ALL or a list of atom names 
weka_file_name="weka.arff"
csv_file_name="ensemble_prediction.dat"
#max_ensemble=20
max_ensemble=99999999999999999
find_bb_combos=1
file_of_combos_methods="combinations_of_predictors_methods.py"
do_pearson=1
do_spearman=1
do_kendall=1
verify_dict={}
author_cs=0
dict_with_common_atoms={}
dict_with_common_atoms_file=""
dict_with_common_atoms_offset=0
allow_mutations=1
stereo_matching_mode=0
make_error_pics=0
######################################################	



#############################
# Global options
#############################
global_always=""
global_actually=""
#############################

#############################
# Outliers options
#############################
outliers=0 # If 0, do not find outliers; If 1, find outliers for CSdiff
# To-do
global_outliers=0 # if 1, use per-protein standard deviations, 2, use per-residue standard deviations
outliers1_dict_file=""
outliers2_dict_file=""
fill_dict=1
#############################

#############################
# Output options
#############################
total_module_text_on=0 # Was 1
total_report_text_on=0 # Was 1
module_text_on=0
report_text_on=0
per_residue_on=0
legacy=1 # Output compatible with PROSESS server
#############################

#############################
# Averaging options
#############################
population_file=""
pop_type=1
population_dict={}
prefix="local"
#############################


########################## DSSP settings ###########
pdb_for_dssp=""
#dssp_program="/opt/markber/research/ShiftX_vs_Exp_diff/dsspcmbi"
dssp_program="/opt/markber/research/ShiftX_vs_Exp_diff/dsspcmbi"
dssp_results_dir="dssp_results"
dssp_always=1 # if 1, secondary structure will be always determined with DSSP even though a result file exists
#pdb_for_dssp_flag=1 # If 0, a user-defined PDB file (pdb_for_dssp) will be used to determine secondary structure
dssp_run_once=1
dssp_result_file_from_bias="" # To-do
######################################################



########################## Reduce settings ###########
#reduce_program="/opt/markber/software/gafolder/gafolder_v130_rmsd/gafolder_v130_rmsd_NMR_no_CCD/reduce"
reduce_program="/opt/markber/software/reduce/reduce.3.14.080821.linuxi386"
run_reduce=0 # Activate for ShiftS automatically
run_reduce1="" # Override Shifts options
######################################################


########################## Gnuplot settings ###########
gnuplot_program="gnuplot"
#gnuplot_program="/usr/scratch/prion/process/gnuplot/bin/gnuplot"
######################################################






########################## RCI settings ############## 
rc_offsets_dict_file=""
rci_dir=""
rci_program=""
redo_bb_rci=0 # If 1, calculate backbone RCI in this script
sc_rci=1 # If 1, calculate side chain RCI in this script
rci_results_dir="rci_results"
rci_result_file=""
run_rci=1
rci_easy=1
rci_score=0 # if 1, calculate CSI score
Random_coil_flag=1 # If 1, Wishart RC values will be used TO-DO: add other RC values
S2_program="" # Path to S2 program ???????????????????????????????
file_with_coefficients="Total_average_coeff_all"
miss_ass=0
find_rci=0
value_search=0 # Value search for gap,  if 1, search +1 and -1, if 2, search +1,+2,-1,-2
value_search2=0 # Value search for gap,  if 1, search +1 and -1, if 2, search +1,+2,-1,-2 - for the second gap filling
averaging=0
bb_averaging=3
make_rci_pics=2
make_rci_images=0
early_floor_value=0.1 #0.01 #0.1
floor_value1=0.5 # was 0.6 # 0.05 for HDall After end correction
#floor_value2=0.5 # Before end correction
floor_value2=0.6 # applied to sigma after End COrrection
#floor_value=0.5 # For HD all 12.0 #0.5#12.9 #0.5 # was 1.9, applied to sigma before End Correction	
scale=1 # If 1, scale RCI values if some shifts are missing
power1= 1.0 # was 1.5
scaling1=3 #4.0 # was 3.0
ceiling_value=0.5 #0.7 #0.5
rci_out_offset=0
rci_report_residues=""
make_rci_reports=1
make_secCS_reports=0
H_Hertz_corr=10.0
C_Hertz_corr=2.5
N_Hertz_corr=1.0
rci_method=15
find_value_mode=1 # Mean of chemical shifts is used to find RCI
no_outliers=0 # If 1, remove sec cs outliers per residue
use_unassigned=0 # If 0, excloude totally unassignmed residues TO-DO
runname_list=[]
rc_values=1 # IF 1, wishart, if 2 Dyson/Write
coef_only=1
exclude_atoms=1
sc_rci_weight=1.0
bb_rci_weight=3.0
sc_bb_combine=2
pause=0
rc_offset_gridsearch=0
# Temporary;  restore
rc_assign_dic={} 
backbone_random_coil_values=0 # If 0, Do not overwrite values from side-chain random coil sets
i_plus_minus_two_residue_correction=0 # If 0, do not use
i_plus_minus_one_residue_correction=0 # If 0, do not use

side_chain_H_correction=0
side_chain_C_correction=0
side_chain_N_correction=0

residue_type_sensitivity=1
detect_missing_atoms=0
hydro=0
hydro_div=4.0
hcoef=1.0
rc_residue_list=[]
rcoil_offset_dict={}
bb_end_correct=1
sc_end_correct=1


find_rc_order_parameter=0
find_MD_RMSF=1
find_NMR_RMSF=1
find_ASA=0
find_B_factor=1


bb_rci_opt=0
param_overwrite=1
rci_output_dict={}
rci_output_dict["Side_chain"]={'name':'Side_chain'}

pic_type="gif"
#pic_type="jpg"
#pic_type="png"
#pic_type="ps"
apache_flag=0


find_max=0
find_min=0
early_ceiling_value=99999999
normalize=0



rci_project=""
rci_location=""

######
# RCI grid search
#######
do_gridsearch1=0 # If 1, do gridsearch 1
remove_stereo=1
files4training=0
shiftclass4gridsearch_list=["Side_chain"]
gridsearch_coeff_list=[]
rc_increment_scale=1.0
rc_offsets={}
#rc_offset_dict={}

side_chain_protons_to_correct={}
side_chain_protons_to_correct[1]={}
side_chain_protons_to_correct[1]["atom_list"]=["HB1","HB2","HB3","HBave"]
side_chain_protons_to_correct[1]["H_reduction"]=2.0


side_chain_carbons_to_correct={}
side_chain_carbons_to_correct[1]={}
side_chain_carbons_to_correct[1]["atom_list"]=["CG","CG1","CG2","CGave"]
side_chain_carbons_to_correct[1]["C_reduction"]=2.0


side_chain_nitrogens_to_correct={}
side_chain_nitrogens_to_correct[1]={}
side_chain_nitrogens_to_correct[1]["atom_list"]=[]
side_chain_nitrogens_to_correct[1]["N_reduction"]=2.0



gridsearch_coeff_list1=[0.0,0.33,0.66,1.0]
gridsearch_coeff_list2=[0.0,0.49,1.0]
gridsearch_coeff_list3=[0.0,0.2,0.4,0.6,0.8,1.0]
gridsearch_coeff_list4=[0.49,1.0]
gridsearch_coeff_list5=[0.0,1.0]
gridsearch_coeff_list6=[0.3,1.0]
gridsearch_coeff_list7=[0.3,1.0]
gridsearch_coeff_list8=[1.0]
gridsearch_coeff_list9=[0.0,0.1,0.2,0.3,0.4,0.49,0.6,0.7,0.8,0.8,1.0]

res4gridsearch_list=[]
res4gridsearch_list+=["A"] #1
res4gridsearch_list+=["S"] #2
res4gridsearch_list+=["G"] #3
res4gridsearch_list+=["F"] #4
res4gridsearch_list+=["Q"] #5
res4gridsearch_list+=["W"] #6
res4gridsearch_list+=["E"] #7
res4gridsearch_list+=["R"] #8
res4gridsearch_list+=["T"] #9
res4gridsearch_list+=["Y"] #10
res4gridsearch_list+=["I"] #11
res4gridsearch_list+=["P"] #12
res4gridsearch_list+=["D"] #13
res4gridsearch_list+=["H"] #14
res4gridsearch_list+=["K"] #15
res4gridsearch_list+=["L"] #16
res4gridsearch_list+=["C"] #17
res4gridsearch_list+=["V"] #18
res4gridsearch_list+=["N"] #19
res4gridsearch_list+=["M"] #20


#http://www.sigmaaldrich.com/life-science/metabolomics/learning-center/amino-acid-reference-chart.html
hydrophob_dic={}
hydrophob_dic["ALA"]=41
hydrophob_dic["ARG"]=-14
hydrophob_dic["LEU"]=97
hydrophob_dic["LYS"]=-23
hydrophob_dic["MET"]=74
hydrophob_dic["GLN"]=-10
hydrophob_dic["ILE"]=99
hydrophob_dic["TRP"]=97
hydrophob_dic["TRP"]=97
hydrophob_dic["PHE"]=100
hydrophob_dic["TYR"]=63
hydrophob_dic["CYS"]=49
hydrophob_dic["PRO"]=-46
hydrophob_dic["VAL"]=76
hydrophob_dic["ASN"]=-28
hydrophob_dic["SER"]=-5
hydrophob_dic["HIS"]=8
hydrophob_dic["GLU"]=-31
hydrophob_dic["THR"]=13
hydrophob_dic["ASP"]=-55
hydrophob_dic["GLY"]=0


######################################################
# Averaging
######################################################

res4gridsearch_list_dict={}

#res4gridsearch_list_dict["A"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} # 36
#res4gridsearch_list_dict["S"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} # 216
#res4gridsearch_list_dict["T"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} # 256 for 1 and 1; 1296 for 3 and 1

# Deactivated for testing purposes
#res4gridsearch_list_dict["A"]={"coeff_set":gridsearch_coeff_list3, "stereo":1} # 36
res4gridsearch_list_dict["A"]={"coeff_set":gridsearch_coeff_list3, "stereo":1} # for List20 21 of 109 is accepted,  36
res4gridsearch_list_dict["S"]={"coeff_set":gridsearch_coeff_list3, "stereo":1} # was list 3, 216
res4gridsearch_list_dict["T"]={"coeff_set":gridsearch_coeff_list3, "stereo":1} # was list 3,; 256 for 1 and 1; 1296 for 3 and 1
res4gridsearch_list_dict["G"]={"coeff_set":gridsearch_coeff_list3, "stereo":1} # 0
res4gridsearch_list_dict["D"]={"coeff_set":gridsearch_coeff_list3, "stereo":1} #  L20_S1:  331 from 1315 ; L3_S1: 216 for 3 and 1
res4gridsearch_list_dict["C"]={"coeff_set":gridsearch_coeff_list3, "stereo":1} #  331 from 1315 for L20_S1; 91 from 211 for L3_S1, 
res4gridsearch_list_dict["N"]={"coeff_set":gridsearch_coeff_list1, "stereo":1} ## was 1, 4096 for 1 and 1,46656 for 3 and 1 (try list3 again)
res4gridsearch_list_dict["V"]={"coeff_set":gridsearch_coeff_list1, "stereo":1} ## L1_S1:  acepted combination is 3367 from 4093; L3_S1:  31031 from 46651 ; 4096 for 1 and 1 (6 hours), (46656 for List3 and Stereo1 - crashes  need  zope db)
res4gridsearch_list_dict["Q"]={"coeff_set":gridsearch_coeff_list2, "stereo":1} ## L1_S1 - takes more than 48 hours for some proteins. L1_S:  3367 from 4093 ; L3_S1: 31031 from 46651
res4gridsearch_list_dict["E"]={"coeff_set":gridsearch_coeff_list1, "stereo":1} ## L1_S1:  3367 from 4093; 4093 OK; L3_S1:  31031 from 46651

#res4gridsearch_list_dict["P"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} ## L7-S1: 511 from 511; from L2_S1:  19171 from 19681; L7_S0 3367 from 4093; was list 1 and stereo=0 4096 for 1 and 0, 262144 for 1 and 1
res4gridsearch_list_dict["P"]={"coeff_set":gridsearch_coeff_list1, "stereo":0} ## L1_S0: 3367 from 4093

#res4gridsearch_list_dict["Y"]={"coeff_set":gridsearch_coeff_list2, "stereo":1} ##  - memory error
res4gridsearch_list_dict["Y"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} ##  2047 from 2047; 
#res4gridsearch_list_dict["Y"]={"coeff_set":gridsearch_coeff_list1, "stereo":0} #  3367 from 4093 1

res4gridsearch_list_dict["H"]={"coeff_set":gridsearch_coeff_list2, "stereo":1} ## 2059 from 2185 ;  4096 for 1 and 0, 16384 for 1 and 1
#res4gridsearch_list_dict["H"]={"coeff_set":gridsearch_coeff_list1, "stereo":1} ##  14197 from 16381
#res4gridsearch_list_dict["H"]={"coeff_set":gridsearch_coeff_list3, "stereo":0} #31031 from 46651 

#res4gridsearch_list_dict["L"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} #  511 from 511 
#res4gridsearch_list_dict["L"]={"coeff_set":gridsearch_coeff_list2, "stereo":0} ##  665 from 727
#res4gridsearch_list_dict["L"]={"coeff_set":gridsearch_coeff_list2, "stereo":1} ##   19171 from 19681 - crashed
res4gridsearch_list_dict["L"]={"coeff_set":gridsearch_coeff_list1, "stereo":0} ##   3367 from 4093

#res4gridsearch_list_dict["M"]={"coeff_set":gridsearch_coeff_list2, "stereo":1} ##  change to L1_S0;  L2_S1: takes 10GB RAM ; 6305 from 6559; was list 1 and stereo=0  4096 for 1 and 0, 65536  for 1 and 1
res4gridsearch_list_dict["M"]={"coeff_set":gridsearch_coeff_list1, "stereo":0} ##  change to L1_S0;  L2_S1: takes 10GB RAM ; 6305 from 6559; was list 1 and stereo=0  4096 for 1 and 0, 65536  for 1 and 1

#res4gridsearch_list_dict["R"]={"coeff_set":gridsearch_coeff_list2, "stereo":0} ## 6305 from 6559 accepted; 6561 for 1 and 0, 65536 for 1 and 0,  4194304 for 1 and 1
res4gridsearch_list_dict["R"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} #   2047 from 2047

#res4gridsearch_list_dict["K"]={"coeff_set":gridsearch_coeff_list2, "stereo":0} #   6305 from 6559 : 6561 for 2 and 0,  65536 for 1 and 0
res4gridsearch_list_dict["K"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} #    4095 from 4095


res4gridsearch_list_dict["F"]={"coeff_set":gridsearch_coeff_list2, "stereo":0} ##  6305 from 6559 - crashes big proteins
# res4gridsearch_list_dict["F"]={"coeff_set":gridsearch_coeff_list7, "stereo":0} ##  

res4gridsearch_list_dict["W"]={"coeff_set":gridsearch_coeff_list7, "stereo":0} ##  2047 from 20472048 for 7 and 0, 177147 for 2 and 0, 4194304 for 0 and 1
#res4gridsearch_list_dict["W"]={"coeff_set":gridsearch_coeff_list2, "stereo":0} ##  177145 
##res4gridsearch_list_dict["W"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} ##   32767 from 32767

res4gridsearch_list_dict["I"]={"coeff_set":gridsearch_coeff_list1, "stereo":0} # 3367 from 4093
#res4gridsearch_list_dict["I"]={"coeff_set":gridsearch_coeff_list7, "stereo":1} #  511 from 511
#res4gridsearch_list_dict["I"]={"coeff_set":gridsearch_coeff_list2, "stereo":1} #  19681 of total 19681
######################################################



rc_offset_dict={}
rc_offset_dict["proton"]=0.05 # 0.5
rc_offset_dict["carbon"]=0.2 #2.0
rc_offset_dict["nitrogen"]=0.2 #2.0

rci_exclude_dic={}
rci_exclude_dic["Side_chain"]={}
rci_exclude_dic["Side_chain_ave"]={}
#1
rci_exclude_dic["Side_chain"]["R"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["R"]=["CB"]
#2
rci_exclude_dic["Side_chain"]["K"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["K"]=["CB"]
#3
rci_exclude_dic["Side_chain"]["E"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["E"]=["CB"]
#4
rci_exclude_dic["Side_chain"]["D"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["D"]=["CB"]
#5
rci_exclude_dic["Side_chain"]["P"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["P"]=["CB"]
#6
rci_exclude_dic["Side_chain"]["Q"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["Q"]=["CB"]
#7
rci_exclude_dic["Side_chain"]["H"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["H"]=["CB"]
#8
rci_exclude_dic["Side_chain"]["L"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["L"]=["CB"]

#9
#rci_exclude_dic["Side_chain"]["W"]=["CB"]
#rci_exclude_dic["Side_chain_ave"]["W"]=["CB"]

#10
#rci_exclude_dic["Side_chain"]["F"]=["CB"]
#rci_exclude_dic["Side_chain_ave"]["F"]=["CB"]

#11
#rci_exclude_dic["Side_chain"]["Y"]=["CB"]
#rci_exclude_dic["Side_chain_ave"]["Y"]=["CB"]

#12
#rci_exclude_dic["Side_chain"]["V"]=["CB"]
#rci_exclude_dic["Side_chain_ave"]["V"]=["CB"]

#13
#rci_exclude_dic["Side_chain"]["M"]=["CB"]
#rci_exclude_dic["Side_chain_ave"]["M"]=["CB"]

#14
#rci_exclude_dic["Side_chain"]["N"]=["CB"]
#rci_exclude_dic["Side_chain_ave"]["N"]=["CB"]

#15
#rci_exclude_dic["Side_chain"]["S"]=["CB"]
#rci_exclude_dic["Side_chain_ave"]["S"]=["CB"]

#16
#rci_exclude_dic["Side_chain"]["T"]=["CB"]
#rci_exclude_dic["Side_chain_ave"]["T"]=["CB"]

#17
# ALA

#18
# CYS

#19
# ILE

#20
# GLY


stereospecific_dictionary={}
stereospecific_dictionary["HAave"]={"list":["HA","HA1","HA2","HA3"]}
stereospecific_dictionary["HBave"]={"list":["HB","HB1","HB2","HB3","HB"]}
stereospecific_dictionary["HGave"]={"list":["HG","HG1","HG2","HG3","HG11","HG12","HG13","HG21","HG22","HG23"]}
stereospecific_dictionary["HDave"]={"list":["HD","HD1","HD2","HD3","HD11","HD12","HD13","HD21","HD22","HD23"]}
stereospecific_dictionary["HEave"]={"list":["HE","HE1","HE2","HE3","HE21","HE22"]}
stereospecific_dictionary["HHave"]={"list":["HH","HH11","HH12","HH2","HH21","HH22"]}
stereospecific_dictionary["HZave"]={"list":["HZ","HZ1","HZ2","HZ3"]}
stereospecific_dictionary["CGave"]={"list":["CG","CG1","CG2"]}
stereospecific_dictionary["CDave"]={"list":["CD","CD1","CD2"]}
stereospecific_dictionary["CEave"]={"list":["CE","CE1","CE2","CE3"]}
stereospecific_dictionary["CZave"]={"list":["CZ","CZ2","CZ3"]}
stereospecific_dictionary["NDave"]={"list":["ND","ND1","ND2"]}
stereospecific_dictionary["NEave"]={"list":["NE","NE1","NE2"]}




forbidden_atoms={}
forbidden_atoms["S"]=["HG"]
#forbidden_atoms["N"]=["CG"]
#forbidden_atoms["Q"]=["HE21"]
#forbidden_atoms["Q"]+=["HE22"]
forbidden_atoms["Q"]=["NE2"]



######################################################
# Databases
######################################################

rci_dict={}
rci_dict["RCI"]={}
rci_dict["RCI"]["shifts"]={}
rci_dict["RCI"]["shifts"]["CA"]={"weight":0.72}
rci_dict["RCI"]["shifts"]["HAave"]={"weight":0.85}
rci_dict["RCI"]["shifts"]["C"]={"weight":0.72}
rci_dict["RCI"]["shifts"]["N"]={"weight":0.539}
rci_dict["RCI"]["shifts"]["CB"]={"weight":0.15}
rci_dict["RCI"]["shifts"]["H"]={"weight":0.13}
rci_dict["RCI"]["missing_asignment"]=1

rci_parameter_dict={}
rci_parameter_dict_file="4_26_13_power1_1_scaling1_4_floor1_0_5_ceil2_0_5_RCImethod_15_Wishart_CoefOnly_1_ResSense1_EXCLUDE0_spearman_All_Cases_THEBEST_rci_parameters_test1.py"
#rci_parameter_dict_file=""

#Dictionary of amino acids
aa_names_full_all_CAP={"GLN":"Q","TRP":"W","GLU":"E","ARG":"R","THR":"T","TYR":"Y","ILE":"I","PRO":"P","ALA":"A","SER":"S","ASP":"D","PHE":"F","GLY":"G","HIS":"H","LYS":"K","LEU":"L","CYS":"C","VAL":"V","ASN":"N","MET":"M"}	
aa_names_1let_2_full_all_CAP={"Q":"GLN","W":"TRP","E":"GLU","R":"ARG","T":"THR","Y":"TYR","I":"ILE","P":"PRO","A":"ALA","S":"SER","D":"ASP","F":"PHE","G":"GLY","H":"HIS","K":"LYS","L":"LEU","C":"CYS","V":"VAL","N":"ASN","M":"MET","B":"CYS"}

	

######################################################
# Misc.
######################################################

usage_1="""

ERROR!!! You forgot to specify a file with chemical shifts


"""

usage_2="""


"""

usage="""



"""

######################################################
# Functions
######################################################

########################## Standard functions ###########


def file_exists(l_file):
	"""
	Return 1 if file exists and 0 if it does not 
	"""	
	exists=os.access(l_file, os.F_OK)
	return exists

def make_dir(dir_name):
	"""
	Create directory if does not exist
	"""
	dir_exist=os.access(dir_name, os.F_OK)
	if dir_exist==0:
		os.system('mkdir %s' %  dir_name)
	else:
		os.system('rm -r -f %s' %  dir_name)	
		os.system('mkdir %s' %  dir_name)	
	return()	


def make_dir_no_remove(dir_name):
	"""
	Create directory if does not exist
	"""
	dir_exist=os.access(dir_name, os.F_OK)
	if dir_exist==0:
		os.system('mkdir %s' %  dir_name)
	return()

def is_number(l_entry):
	"""
	Function to determine if a string can be converted into a float. 
	Returns 1 if yes, 0 if no
	"""
	l_result=0
	try:
		a=float(l_entry)
		l_result=1
	except:
		l_result=0
	return(l_result)
	

def read_file(L_file_name):
	"""
	Read files, output lines
	"""
	done_input1_switch=0 
	while not done_input1_switch: 
		input_file0s=split(L_file_name) 
		input_file1=input_file0s[0] 
		input_file1_exist=os.access(input_file1, os.F_OK) 
		input_file1_read=os.access(input_file1, os.R_OK) 
		if input_file1_exist==0: 
			print ("""\n ERROR: File with  name %s does not exist. 
 			Please try again.\n: """ % input_file1) 
			sys.exit(1) 
		elif input_file1_read==0: 
			print("""\n ERROR: The program can not read the file.\n: """) 
			sys.exit(1)   
		else: 
			done_input1_switch=1    
	file_open=open(L_file_name,'r')
	file_lines = file_open.readlines()
	file_open.close()
	return(file_lines)	


def write_file(l_text,l_file_name):
	"""
	Write a file with results
	"""
	if l_text!="":
		l_file_name_full="%s" % (l_file_name)
		l_file_name_open=open(l_file_name_full,"w")
		l_file_name_open.write(l_text)
		l_file_name_open.close()		
	return		

def append_file(l_text,l_file_name):
	"""
	Write a file with results
	"""
	if l_text!="":
		l_file_name_full="%s" % (l_file_name)
		l_file_name_open=open(l_file_name_full,"a")
		l_file_name_open.write(l_text)
		l_file_name_open.close()		
	return		




def import_local_modules(l_module_file,l_script_location,l_prefix):
	"""
	Import Python dictionary
	"""
	verbose=0
	if verbose>=1: 	print "Running function  import_local_modules()"
	if verbose>=1: print "l_module_file= ", l_module_file
	l_module_base_file=os.path.basename(l_module_file)
	if len(l_module_base_file)>3 and l_module_base_file[-3:]==".py":
		l_module_file_base="%s_%s" % (l_prefix,l_module_base_file[:-3])
	else:
		l_module_file_base="%s_%s" %  (l_prefix,l_module_base_file)
	module_py="%s.py" % l_module_file_base
	module_pyc="%s.pyc" % l_module_file_base
	if  verbose>=1: print "module_py= ", module_py
	if  verbose>=1: print "module_pyc= ", module_pyc
	if file_exists(module_py): os.system("rm %s " % module_py)
	if file_exists(module_pyc): os.system("rm %s " % module_pyc)

	module_local_filename,module_local_basename =copy_module_file(l_module_file, l_module_file_base,l_script_location)
	module_results = __import__(module_local_basename)

	reload(module_results)
	module_results_dic=copy.deepcopy(module_results.dic)	
	return module_results_dic



def copy_module_file(l_module_full_path, l_module_basename,l_script_location):
	"""
	Copy module file
	"""
	verbose=1
	local_module_name="%s.py" % (l_module_basename)
	local_module_basename="%s" % (l_module_basename)
	copy_command="cp %s %s/%s" % (l_module_full_path,l_script_location,local_module_name)
	if os.access("rm %sc " % local_module_name, os.F_OK)==1:
		os.system("rm %sc " % local_module_name)
	if os.access("rm %s " % local_module_name, os.F_OK)==1:
		os.system("rm %s " % local_module_name)
	if verbose>=11: 
		print copy_command
		print "local_module_name=",local_module_name
		print "local_module_basename=", local_module_basename
	os.system(copy_command)
	
	return local_module_name, local_module_basename




def calculate_average_abs_difference(l_list):
	"""
	Calculate average difference from mean and median
	"""
	l_average_diff_from_mean="NONE"
	l_average_diff_from_median="NONE"
	l_average_diff_from_mean_list=[]
	l_average_diff_from_median_list=[]	
	if len(l_list)>0:
		l_average_diff_from_mean=0
		l_average_diff_from_median=0
		l_mean=stats.lmean(l_list)
		l_median=stats.lmedianscore(l_list)
		if len(l_list)>1:
			for i in l_list:
				abs_diff_from_mean=abs(i-l_mean)
				abs_diff_from_median=abs(i-l_median)				
				l_average_diff_from_mean_list+=[abs_diff_from_mean]
				l_average_diff_from_median_list+=[abs_diff_from_median]
	if len(l_average_diff_from_mean_list)>0:
		l_average_diff_from_mean=stats.lmean(l_average_diff_from_mean_list)			
	if len(l_average_diff_from_median_list)>0:
		l_average_diff_from_median=stats.lmean(l_average_diff_from_median_list)			
	return l_average_diff_from_mean,l_average_diff_from_median


def create_project_dir(l_project,l_location):
	"""
	Create project directory
	"""
	if l_location=="":
		cur_dir=os.getcwd()
		l_full_project_dir="%s/%s_csq_dir" % (cur_dir,l_project)
	else:
		l_full_project_dir="%s/%s_csq_dir" % (l_location,l_project)
	make_dir_no_remove(l_full_project_dir)
	if verbose>=2: print "l_full_project_dir=", l_full_project_dir			
	return l_full_project_dir


def average_shifts(l_population_dict,l_pop_type,l_zodb,l_database_file,l_pdb_list,l_pdbdict,l_predictor,l_predictor_key):
	"""
	Average shifts

	"""
	verbose=0
	if verbose==0: print "Running function  average_shifts() for prdictor ", l_predictor
	combined_shift_dict={}	
	
	average_shift_dict={}		
	average_shift_dict["ensemble_mean_chemical_shifts"]={}
	average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key]={}	


	for i in l_pdb_list:
		weight=1.0
		if len(l_population_dict)>0:
			if i in l_population_dict:
			
				original_population=l_population_dict[i]["original_population"]
				nonzero_original_population=l_population_dict[i]["nonzero_original_population"]	
				scaled_population=l_population_dict[i]["scaled_population"]
				unscaled_population=l_population_dict[i]["unscaled_population"]	

				if l_pop_type==1:	
					weight=	float(original_population)
				elif  l_pop_type==2:
					weight=	float(nonzero_original_population)
				elif  l_pop_type==3:
					weight=	float(unscaled_population)
				elif  l_pop_type==4:
					weight=	float(scaled_population)


				if verbose>=0:
					print "Weight for structure %s is %s" % (i,weight)
			else:
				print "Warning!!! Case %s is not in population_dict" %  i
				print "The program will exit now"
				sys.exit()

		if verbose>=10: print "Processing case %s in function average_shifts()" %  i

		if l_zodb:
			storage = FileStorage(l_database_file)
			db = DB(storage)		
			connection = db.open()
			main_database = connection.root()
			l_pdb_dict=main_database["pdb_dict"]
		else:
			l_pdb_dict =  l_pdbdict
	
		shiftx_assign_dic=l_pdb_dict[i][l_predictor_key]
		if verbose>=1: print i#, shiftx_assign_dic
		for l_residue_number in shiftx_assign_dic:
			#print l_residue_number,shiftx_assign_dic[l_residue_number]
			l_residue_name=shiftx_assign_dic[l_residue_number][0]
			l_shifts_dict=shiftx_assign_dic[l_residue_number][1]
			if 	l_residue_number not in combined_shift_dict:
				combined_shift_dict[l_residue_number]={}
				combined_shift_dict[l_residue_number]["res_name"]=l_residue_name	
				combined_shift_dict[l_residue_number]["shift_lists"]={}																										
			for l_shift in l_shifts_dict:
				l_shift_value=l_shifts_dict[l_shift]
				if l_shift not in combined_shift_dict[l_residue_number]["shift_lists"]:
					combined_shift_dict[l_residue_number]["shift_lists"][l_shift]=[]
				if is_number(l_shift_value) and weight!=0.0:
					combined_shift_dict[l_residue_number]["shift_lists"][l_shift]+=[l_shift_value*weight]

				else:
					if  is_number(l_shift_value)==0:
						print "Warning!!!! Value of shift %s of %s%s in PDB %s is not numerical and is " % (l_shift,l_residue_number,l_residue_name,i), [l_shift_value] 

		if l_zodb:
			l_pdb_dict._p_changed = True
			transaction.savepoint(True)
			transaction.commit()
			transaction.get().commit() 
			connection.close()
			db.close()
			storage.close()
			l_pdb_dict={}



	for k_residue_number in combined_shift_dict:
		average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][k_residue_number]=[combined_shift_dict[k_residue_number]["res_name"],{},{},{},{},{},{}]
		for k_shift in 	combined_shift_dict[k_residue_number]["shift_lists"]:
			k_shift_list=combined_shift_dict[k_residue_number]["shift_lists"][k_shift]
			l_mean="NONE"
			l_median="NONE"
			l_stdev="NONE"	
			l_average_diff_from_mean="NONE"
			l_average_diff_from_median="NONE"
			if len(k_shift_list)>0 and len(l_population_dict)==0:
				l_mean=stats.lmean(k_shift_list)
				l_median=stats.lmedianscore(k_shift_list)	
				l_average_diff_from_mean,l_average_diff_from_median=calculate_average_abs_difference(k_shift_list)


				if verbose>=1: print k_residue_number, k_shift, k_shift_list, l_mean
							
			elif len(k_shift_list)>0 and len(l_population_dict)>0:
				l_mean=sum(k_shift_list)
				l_median=sum(k_shift_list)	
				l_average_diff_from_mean,l_average_diff_from_median=calculate_average_abs_difference(k_shift_list)
				
			if len(k_shift_list)>1 and len(l_population_dict)==0:
				l_stdev=stats.lstdev(k_shift_list)				
			if use_mean_or_median==0 and  l_mean!="NONE":
				average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][k_residue_number][1][k_shift]=l_mean
			elif use_mean_or_median==1 and  l_mean!="NONE":
				average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][k_residue_number][1][k_shift]=l_median				
						
			average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][k_residue_number][2][k_shift]=l_mean
			average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][k_residue_number][3][k_shift]=l_median
			average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][k_residue_number][4][k_shift]=l_stdev																					
			average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][k_residue_number][5][k_shift]=l_average_diff_from_mean	
			average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][k_residue_number][6][k_shift]=l_average_diff_from_median		


	debug=0
	if debug==1:
		print "Exiting after function average_shifts()"
		sys.exit()																																							

	return	average_shift_dict, combined_shift_dict

def write_average_shifts_bmrb(l_average_shift_dict,l_predict_bmrb,l_predictor_key,l_project_dir,l_report_dir,l_predictor,l_residue_list,l_list_4_offset):
	"""
	Write averaged shifts in BMRB NMR Star 2.1 format

	"""
	verbose=0
	if verbose>=1: print "write_average_shifts_bmrb()"
	if verbose>=1: print "l_predict_bmrb=", [l_predict_bmrb]
	if verbose>=10: print "l_average_shift_dict=", l_average_shift_dict
	if verbose>=1: print "l_residue_list=", [l_residue_list]
	if verbose>=1: print "l_list_4_offset=", [l_list_4_offset]




	l_report_dir_full_general="%s/%s" % (l_project_dir,l_report_dir)
	make_dir_no_remove(l_report_dir_full_general)

	l_report_dir_full="%s/%s" % (l_report_dir_full_general,l_predictor)
	make_dir_no_remove(l_report_dir_full)

	l_dir="%s/predicted_bmrbs" % (l_report_dir_full)
	make_dir_no_remove(l_dir)	
	l_file="%s/%s_mean_predicted_shifts_NMRStart21.str" % (l_dir,l_predictor)




	l_text=""
	l_3_letter_text="""

loop_
_Residue_seq_code
_Residue_label

"""


	l_text+="""
   _Residue_count                               %s
   _Mol_residue_sequence                       
;
""" % len(l_residue_list)

	l_assign_text=""

	counter=0
	three_residue_counter=0
	assign_count=1

	if l_predict_bmrb==1:
		if verbose>=0: print "Predicted mean chemical shifts will be in ", l_file

		l_residue_list.sort()
	
		for i in l_residue_list:
			res_name=i[1]
			res_number=i[0]
			res_number_found=0
			proceed=1
			if len(res_name)==1 and res_name in aa_names_1let_2_full_all_CAP:
				res_name_3_let=aa_names_1let_2_full_all_CAP[res_name]
			elif len(res_name)==3 and res_name in aa_names_full_all_CAP:
				res_name_3_let=res_name

			else:
				print "Warning!!! Residue %s%s was not found in aa_names_1let_2_full_all_CAP. The residue will be skiped" % (res_name,res_number)
				proceed=0
			for j in l_list_4_offset:
				#print j
				j_res_name=j[0]
				j_res_number=j[1]
				if j_res_number==res_number and proceed==1:

					res_number_found=1
					if res_name_3_let!=j_res_name:
						proceed=0
						print "Warning!!! Residue %s%s  in l_residue_list in is not the same as residue %s%s in l_list_4_offset. The residue will be skiped" % (res_name,res_number,j_res_name,j_res_number)	

					if "ensemble_mean_chemical_shifts" in l_average_shift_dict and l_predictor_key in l_average_shift_dict["ensemble_mean_chemical_shifts"]:
						if j_res_number in l_average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key]:
							if verbose>=10: print j_res_number, l_average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][j_res_number]
							if len(l_average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][j_res_number])>1:
								residue_name=l_average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][j_res_number][0]
								shift_dict=l_average_shift_dict["ensemble_mean_chemical_shifts"][l_predictor_key][j_res_number][1]	
								if verbose>=10: print "residue_number, residue_name, mean_shift_dict = ", [j_res_number,residue_name,mean_shift_dict]
								if res_name!=residue_name:
									proceed=0
									print "Warning!!! Residue %s%s  in l_residue_list in is not the same as residue %s%s in l_average_shift_dict. The residue will be skiped" % (res_name,res_number,residue_name,j_res_number)	

								if verbose>=10: 
									print [res_number],"res_name=",res_name 
									print [j_res_number],"j_res_name=",j_res_name 
									print [i],"residue_name=",residue_name

								if proceed==1:
									for shift in shift_dict:
										shift_value=shift_dict[shift]
										atom_type="NONE"
										if "H" in shift and "C" not in shift:
											atom_type="H"
										elif "H" not in shift and "C" in shift:
											atom_type="C"
										elif "H" not in shift and "N" in shift:
											atom_type="N"
										if atom_type=="NONE":
											print "Warning!!! Atom type is NONE",  shift_dict, shift_dict[shift]

										#print j_res_number, residue_name, shift, shift_value
										if is_number(shift_value) and float(shift_value)!=0.0 and "again" not in shift:
											l_assign_text+="%s %s %s %s %s %.2f . 1 " % (assign_count,j_res_number,j_res_name,shift,atom_type,shift_value) + linesep
											assign_count+=1
										elif 	is_number(shift_value) 	==0:
											print "Warning!!! Shift value is not  numerical  and is ", [shift_value] 	


						else:
							print "Warning!!! Residue %s%s is missing in  l_average_shift_dict" % (j_res_number,j_res_name)
					
						if three_residue_counter<20:
							three_residue_counter+=1
							l_3_letter_text+="%s %s " % (j_res_number,j_res_name)

								#print l_3_letter_text
						else:
							l_3_letter_text+=linesep
							l_3_letter_text+="%s %s " % (j_res_number,j_res_name)
							three_residue_counter=1				

			if res_number_found==0:
				print "Warning!!! Residue %s%s was not found in 	l_list_4_offset" % (res_name,res_number)
	
			#print res_name
			if counter<20:
				counter+=1
				l_text+=res_name
			else:
				l_text+=linesep
				l_text+=res_name
				counter=1
		l_text+=linesep	+";" + linesep + linesep
		l_text+=l_3_letter_text + linesep + "stop_" + linesep + linesep

		l_text+="""
   loop_
      _Atom_shift_assign_ID
      _Residue_seq_code
      _Residue_label
      _Atom_name
      _Atom_type
      _Chem_shift_value
      _Chem_shift_value_error
      _Chem_shift_ambiguity_code
	""" + linesep


	
		l_text+=l_assign_text

		l_text+="""
   stop_

save_
	"""
	#print l_text

		write_file(l_text,l_file)





	debug=0
	if debug==1:
		print "Exiting after function write_average_shifts_bmrb()"
		sys.exit()
	return
	

def detect_the_order1(l_bmrb_lines,aa_start_trigger1):
	"""
	Detect order residue numbering and author residue numbering
	"""
	#aa_start_trigger1="_Residue_label"
	seq_code_trigger="_Residue_seq_code"
	author_seq_code_trigger="_Residue_author_seq_code"
	verbose=0
	if verbose>=1: print "Running function detect_the_order()"
	aa_start_trigger_found=0
	seq_code_trigger_found=0
	author_seq_code_trigger_found=0
	detect=0
	seq_code_trigger_order=""
	author_seq_code_trigger_order=""
	for i in l_bmrb_lines:
		if (seq_code_trigger in i and seq_code_trigger_found==1) or  (author_seq_code_trigger in i and author_seq_code_trigger_found==1):
			detect=0
		if aa_start_trigger1 in i and detect==0:
			if verbose>=1: print "aa_start_trigger1 has been detected"
			aa_start_trigger_found=1
			detect=1

		if seq_code_trigger in i and detect==1 and seq_code_trigger_found==0 and aa_start_trigger_found==1:
			if verbose>=1: print "seq_code_trigger  has been detected"
			seq_code_trigger_found=1
			if author_seq_code_trigger_found==0:
				seq_code_trigger_order=1
			else:
				seq_code_trigger_order=2			


		if author_seq_code_trigger in i and detect==1 and author_seq_code_trigger_found==0 and aa_start_trigger_found==1:
			if verbose>=1: print "author_seq_code_trigger has been detected"
			author_seq_code_trigger_found=1
			if seq_code_trigger_found==0:
				author_seq_code_trigger_order=1
			else:
				author_seq_code_trigger_order=2							
	debug=0
	if debug==1:
		print "seq_code_trigger_order=", [seq_code_trigger_order]
		print "author_seq_code_trigger_order=", [author_seq_code_trigger_order]
		print "Exiting after function detect_the_order()"
		sys.exit()
	return seq_code_trigger_order, author_seq_code_trigger_order

def bmrb3_to_aa(l_bmrb_lines):
	
        """
        Create primary sequence from BMRB file

	loop_
       _Entity_comp_index.ID
       _Entity_comp_index.Auth_seq_ID
       _Entity_comp_index.Comp_ID
       _Entity_comp_index.Comp_label
       _Entity_comp_index.Entry_ID
       _Entity_comp_index.Entity_ID

        1 . MET . 4403 1 
        2 . ASP . 4403 1 
        3 . ARG . 4403 1 
        4 . VAL . 4403 1 

        """
        verbose=0
        if verbose>=1: print "Running function bmrb3_to_aa()"



        aa_start_trigger1="_Entity_comp_index.Entity_ID"
        #aa_stop_trigger1="_Atom_name"
        aa_stop_trigger1="stop_"
        #seq_code_trigger_order, author_seq_code_trigger_order=detect_the_order1(l_bmrb_lines,"_Mol_residue_sequence")
        seq_code_trigger_order = 1
        author_seq_code_trigger_order = 1

        start_flag=0
        l_run_once=0
        aa_res_name_num_list=[]

        first_list=[]
        second_list=[]

        l_bmrb_lines_new=[]
        for k in l_bmrb_lines:
                if "stop_" not in k:
                        l_bmrb_lines_new+=[k]
                else:
                        if verbose>=1: print "Separating stop_ in %s" % k
                        k=k.replace("stop_","")
                        l_bmrb_lines_new+=[k]
                        l_bmrb_lines_new+=["stop_"]



        for line in l_bmrb_lines_new:
                if verbose>=10: print line
                split_line=split(line)
                if len(split_line)>0:

                        if (split_line[0]==aa_stop_trigger1) and (l_run_once==0) and (start_flag==1):
                                start_flag=0
                                l_run_once=1
                                if verbose>=10: print "Stop trigger has been found"
                                #sys.exit()

                        if (split_line[0]==aa_start_trigger1) and (l_run_once==0):
                                start_flag=1

                        elif (len(split_line)>1) and (start_flag==1) and (split_line[0]!=aa_start_trigger1) and (l_run_once==0):
				res_number_found=0
                                res_name_found=0
                                if verbose>=1: print  split_line
                                res_num = int(split_line[0])
                                res_name = split_line[2]
                                aa_res_name_num_list_local=[res_name,res_num]
                                aa_res_name_num_list=aa_res_name_num_list+[aa_res_name_num_list_local]
                                first_list+=[[res_name,res_num]]
	if seq_code_trigger_order==1:
                seq_code_list=first_list
                author_seq_code_list=second_list
        elif seq_code_trigger_order==2:
                seq_code_list=second_list
                author_seq_code_list=first_list
        elif author_seq_code_trigger_order==2:
                seq_code_list=first_list
                author_seq_code_list=second_list
        elif author_seq_code_trigger_order==1:
                seq_code_list=second_list
                author_seq_code_list=first_list
        else:
                seq_code_list=first_list
                author_seq_code_list=second_list
        debug=0
        if debug==1:
                print "seq_code_trigger_order = ", seq_code_trigger_order
                print "author_seq_code_trigger_order = ", author_seq_code_trigger_order
                print "seq_code_list= ", seq_code_list
                print "author_seq_code_list= ", author_seq_code_list
                print "aa_res_name_num_list = " , aa_res_name_num_list
                print "Exiting after function bmrb3_to_aa()"
                sys.exit()
	#print(aa_res_name_num_list)	
        return(aa_res_name_num_list, seq_code_list, author_seq_code_list)			

def bmrb_to_aa(l_bmrb_lines): 
	"""
	Create primary sequence from BMRB file
	""" 
	verbose=0
	if verbose>=1: print "Running function bmrb_to_aa()"



	aa_start_trigger1="_Residue_label"
	aa_stop_trigger1="_Atom_name"     
	aa_stop_trigger2="stop_"
	seq_code_trigger_order, author_seq_code_trigger_order=detect_the_order1(l_bmrb_lines,"_Mol_residue_sequence")

	start_flag=0
	l_run_once=0
	aa_res_name_num_list=[]

	first_list=[]
	second_list=[]

	for line in l_bmrb_lines:
		split_line=split(line)
		if len(split_line)>0:

			if (split_line[0]==aa_stop_trigger1) or (split_line[0]==aa_stop_trigger2) and (l_run_once==0) and (start_flag==1):
				start_flag=0
				l_run_once=1

			if (split_line[0]==aa_start_trigger1) and (l_run_once==0):
				start_flag=1	
							
			elif (len(split_line)>1) and (start_flag==1) and (split_line[0]!=aa_start_trigger1) and (l_run_once==0):
				res_number_found=0
				res_name_found=0
				#print split_line
				for entry in split_line:
					if res_number_found==0:
						if is_number(entry)==1:	
							res_num=int(entry)
							res_num2=""
							res_number_found=1
							res_name_found=0
							#print res_num
					elif (res_number_found==1) and (res_name_found==0) and (len(entry)==3):
						try:  
							error_handle=float(entry)
							res_num2=int(entry)
						except ValueError:														
							res_name=entry
							aa_res_name_num_list_local=[res_name,res_num]
							aa_res_name_num_list=aa_res_name_num_list+[aa_res_name_num_list_local]
							aa_res_name_num_list_local=[]
							first_list+=[[res_name,res_num]]
							if res_num2!="":
								second_list+=[[res_name,res_num2]]
							res_num2=""

							res_name_found=1
							res_number_found=0				
	#print aa_res_name_num_list

	if seq_code_trigger_order==1:
		seq_code_list=first_list
		author_seq_code_list=second_list
	elif seq_code_trigger_order==2:
		seq_code_list=second_list
		author_seq_code_list=first_list
	elif author_seq_code_trigger_order==2:
		seq_code_list=first_list
		author_seq_code_list=second_list
	elif author_seq_code_trigger_order==1:
		seq_code_list=second_list
		author_seq_code_list=first_list
	else:
		seq_code_list=first_list
		author_seq_code_list=second_list
	debug=0
	if debug==1:
		print "seq_code_trigger_order=", seq_code_trigger_order
		print "author_seq_code_list=", author_seq_code_list
		print "seq_code_list= ", seq_code_list
		print "author_seq_code_list= ", author_seq_code_list
		print "Exiting after function bmrb_to_aa()"
		sys.exit()

	return(aa_res_name_num_list, seq_code_list, author_seq_code_list)

def extract_assignment(l_bmrb_lines):
	"""
	Extract chemical shift assignment from lines of a BMRB file
	"""
	verbose=0
	if verbose>=1: print "Running function extract_assignment()"

	header_end_trigger="_Chem_shift_ambiguity_code"

	seq_code_trigger_order, author_seq_code_trigger_order=detect_the_order1(l_bmrb_lines,"_Atom_shift_assign_ID")
	if verbose>=1: print "seq_code_trigger_order=", seq_code_trigger_order
	if verbose>=1: print " author_seq_code_trigger_order = ",  author_seq_code_trigger_order

	chemshift_end_trigger="stop_"
	start_trigger=0
	end_trigger=0
	assignment_list=[]
	for r in l_bmrb_lines:
		t=split(r)
		if len(t)>0:
			if (t[0]==chemshift_end_trigger) and (start_trigger==1):
				start_trigger=0
				end_trigger=1
			if t[0]==header_end_trigger:
				start_trigger=1			
			if (len(t)>3) and (start_trigger==1) and (t[0]!=header_end_trigger) and (end_trigger!=1):
			
				try:
					bmrb_format=2		
					res_num=int(t[2])
					res_name=t[3]
					bmrb_atom_name=t[4]
					bmrb_atom_type=t[5]	
					if is_number(t[6])==1 and aa_names_full_all_CAP.has_key(res_name)==1:								
						bmrb_shift=float(t[6])
						assignment_list+=[[res_name,res_num,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]
					else:
						float("N")	
				except:
					res_num=int(t[1])
					res_name=t[2]
					bmrb_atom_name=t[3]
					bmrb_atom_type=t[4]
					if is_number(t[5])==1 and aa_names_full_all_CAP.has_key(res_name)==1:					
						bmrb_shift=float(t[5])
						bmrb_format=1						
						assignment_list+=[[res_name,res_num,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]
					else:
						float("N")		
				
	debug=0
	if debug==1:				 
		print "Exiting after function extract_assignment()"
		sys.exit()
	#print assignment_list														
	return(assignment_list)

def extract_assignment_bmrb3(l_bmrb_lines):
	"""
	Parse assignment from bmrb3 formatted file
	"""
	verbose=0
        if verbose>=1: print "Running function extract_assignment_bmrb3()"
        header_end_trigger="_Atom_chem_shift.Assigned_chem_shift_list_ID"
        #seq_code_trigger_order, author_seq_code_trigger=detect_the_order1(l_bmrb_lines,"_Atom_chem_shift.ID", "_Atom_chem_shift.Seq_ID", "_Atom_chem_shift.Auth_seq_ID")
	seq_code_trigger_order = 1
        author_seq_code_trigger_order = 1
        if verbose>=1: print "seq_code_trigger_order=", seq_code_trigger_order
        if verbose>=1: print " author_seq_code_trigger_order = ",  author_seq_code_trigger_order
        debug=0
        if debug==1:
                print "Exiting...."
                sys.exit()
        chemshift_end_trigger="stop_"
        start_trigger=0
        end_trigger=0        
	assignment_list=[]
        first_list=[]
        second_list=[]
        B_Cys_list=[]

        atomSynonymDict={'VAL_HG11':'HG1', 'VAL_HG12':'HG1', 'VAL_HG13':'HG1', 'VAL_HG21':'HG2', 'VAL_HG22':'HG2', 'VAL_HG23':'HG2', 'LEU_HD11':'HD1', 'LEU_HD12':'HD1', 'LEU_HD13':'HD1', 'LEU_HD21':'HD2', 'LEU_HD22':'HD2', 'LEU_HD23':'HD2', 'ALA_HB1': 'HB', 'ALA_HB2': 'HB', 'ALA_HB3': 'HB', 'MET_HE1': 'HE', 'MET_HE2': 'HE', 'MET_HE3': 'HE','THR_HG21': 'HG2', 'THR_HG22': 'HG2', 'THR_HG23': 'HG2', 'ILE_HG21':'HG2', 'ILE_HG22':'HG2', 'ILE_HG23':'HG2', 'ILE_HD11':'HD1', 'ILE_HD12':'HD1', 'ILE_HD13':'HD1', 'LYS_HZ1': 'HZ', 'LYS_HZ2': 'HZ', 'LYS_HZ3': 'HZ'}
       
        l_bmrb_lines_new=[]
        for k in l_bmrb_lines:
                if "stop_" not in k:
                        l_bmrb_lines_new+=[k]
                else:
                        if verbose>=1: print "Separating stop_ in %s" % k
                        k=k.replace("stop_","")
                        l_bmrb_lines_new+=[k]
                        l_bmrb_lines_new+=["stop_"]


        for r in l_bmrb_lines_new:
                t=split(r)
                if verbose>=1: print t
                if "stop_" in t:
                        if verbose>=1: print 'chemshift_end_trigger="stop_" has been found in ', t
                if len(t)>0 and t[0]!="#":
                        if (t[0]==chemshift_end_trigger) and (start_trigger==1):
                                start_trigger=0
                                end_trigger=1
                                if verbose>=1: print "Terminating reading...."
                        if t[0]==header_end_trigger:
                                start_trigger=1
                                if verbose>=10: print "header_end_trigger was found"
                        if (len(t)==24) and (start_trigger==1) and (t[0]!=header_end_trigger) and (end_trigger!=1) and r[0]!="#":
                                if is_number(t[10])==1 and aa_names_full_all_CAP.has_key(upper(t[6]))==1:                                       
 
                                        bmrb_shift=float(t[10])
                                        bmrb_format=1
                                        res_num=int(t[5])
                                        res_name=upper(t[6])
					if t[6]+'_'+t[7] in atomSynonymDict:
						bmrb_atom_name=atomSynonymDict[t[6]+'_'+t[7]]  
					else:
						bmrb_atom_name=t[7]
                                        bmrb_atom_type=t[8]
                                        assignment_list+=[[res_name,res_num,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]
                                        first_list+=[[res_name,res_num,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]

                                        if (bmrb_atom_type=="CB") and (res_name=="CYS") and (bmrb_shift>35.0):
                                                B_Cys_list+=[residue_number]



	if seq_code_trigger_order==1:
                if verbose>=1: print "seq_code_trigger_order==1"
                assignment_list=assignment_seq_code_list=first_list
                assignment_author_seq_code_list=second_list
        elif seq_code_trigger_order==2:
                if verbose>=10:print "second_list = ", second_list
                if verbose>=1: print "seq_code_trigger_order==2"
                assignment_list=assignment_seq_code_list=second_list
                assignment_author_seq_code_list=first_list
        elif author_seq_code_trigger_order==2:
                if verbose>=1: print "author_seq_code_trigger_order==2"
                assignment_list=assignment_seq_code_list=first_list
                assignment_author_seq_code_list=second_list
        elif author_seq_code_trigger_order==1:
                if verbose>=1: print "author_seq_code_trigger_order==1"
                assignment_list=assignment_seq_code_list=second_list
                assignment_author_seq_code_list=first_list
        else:
                if verbose>=1: print "assignment_seq_code_list= ", assignment_seq_code_list
                assignment_list=assignment_seq_code_list=first_list
                assignment_author_seq_code_list=second_list

	debug_this_step=0
	if debug_this_step==1:
		#print assignment_list		
		print B_Cys_list
		print "Exiting after function extract_assignment_bmrb3()"
		sys.exit()
	return(assignment_list,assignment_seq_code_list,assignment_author_seq_code_list,B_Cys_list)



def extract_assignment3(l_bmrb_lines):
	"""
	Parse assignment
	"""
	verbose=0
	if verbose>=1: print "Running function extract_assignment3()"
	header_end_trigger="_Chem_shift_ambiguity_code"
	seq_code_trigger_order, author_seq_code_trigger_order=detect_the_order1(l_bmrb_lines,"_Atom_shift_assign_ID")
	if verbose>=10: print "seq_code_trigger_order=", seq_code_trigger_order
	if verbose>=10: print " author_seq_code_trigger_order = ",  author_seq_code_trigger_order
	chemshift_end_trigger="stop_"
	start_trigger=0
	end_trigger=0
	assignment_list=[]
	first_list=[]
	second_list=[]
	B_Cys_list=[]
	for r in l_bmrb_lines:
		t=split(r)
		#print t
		if len(t)>0:
			if (t[0]==chemshift_end_trigger) and (start_trigger==1):
				start_trigger=0
				end_trigger=1
			if t[0]==header_end_trigger:
				start_trigger=1			
			if (len(t)>3) and (start_trigger==1) and (t[0]!=header_end_trigger) and (end_trigger!=1) and r[0]!="#":
				if is_number(t[5])==1 and aa_names_full_all_CAP.has_key(t[2])==1:							
					bmrb_shift=float(t[5])
					bmrb_format=1
					res_num=int(t[1])
					res_name=t[2]
					bmrb_atom_name=t[3]
					bmrb_atom_type=t[4]	
					assignment_list+=[[res_name,res_num,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]
					first_list+=[[res_name,res_num,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]
					if (bmrb_atom_type=="CB") and (res_name=="C") and (bmrb_shift>35.0):
						B_Cys_list+=[res_num]
				elif is_number(t[6])==1 and aa_names_full_all_CAP.has_key(t[3])==1:
					bmrb_format=2
					res_num=int(t[2])
					res_num2=int(t[2])
					res_name=t[3]
					bmrb_atom_name=t[4]
					bmrb_atom_type=t[5]	
					bmrb_shift=float(t[6])					
					assignment_list+=[[res_name,res_num,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]
					first_list+=[[res_name,res_num,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]
					second_list+=[[res_name,res_num2,bmrb_atom_name,bmrb_atom_type,bmrb_shift]]
					if (bmrb_atom_type=="CB") and (res_name=="C") and (bmrb_shift>35.0):
						B_Cys_list+=[res_num]

	if seq_code_trigger_order==1:
		assignment_seq_code_list=first_list
		assignment_author_seq_code_list=second_list
	elif seq_code_trigger_order==2:
		assignment_seq_code_list=second_list
		assignment_author_seq_code_list=first_list
	elif author_seq_code_trigger_order==2:
		assignment_seq_code_list=first_list
		assignment_author_seq_code_list=second_list
	elif author_seq_code_trigger_order==1:
		assignment_seq_code_list=second_list
		assignment_author_seq_code_list=first_list
	else:
		assignment_seq_code_list=first_list
		assignment_author_seq_code_list=second_list

	debug_this_step=0
	if debug_this_step==1:
		#print assignment_list		
		print B_Cys_list
		print "Exiting after function extract_assignment3()"
		sys.exit()
	return(assignment_list,assignment_seq_code_list,assignment_author_seq_code_list,B_Cys_list)




def convert_assignment(l_verify_dict,l_assignment_list,l_protein_sequence_list,l_exp_assignement,l_dict_with_common_atoms,l_dict_with_common_atoms_offset,l_allow_mutations):
	"""
	Convert assignment list to be read by the comparison function

	"""
	verbose=0
	if verbose>=1:
		print "Runnig function  convert_assignment"
	final_list=[]
	not_found_list=[]
	not_found_file="missed_experimental_shifts.txt"
	not_found_list_text=""
	exp_assign_dict={}
	l_residue_numbers_list=[]
	for entry in l_protein_sequence_list:		
		l_HA="NONE"
		l_NH="NONE"
		l_CB="NONE"
		l_CO="NONE"
		l_CA="NONE"
		l_N="NONE"
		e_res_number,e_res_name=int(entry[1]),entry[0]


		if e_res_number not in exp_assign_dict:
			exp_assign_dict[e_res_number]={}
			exp_assign_dict[e_res_number]["shifts"]={}
			exp_assign_dict[e_res_number]["ha_list"]=[]
			exp_assign_dict[e_res_number]["hb_list"]=[]
			exp_assign_dict[e_res_number]["hg_list"]=[]
			exp_assign_dict[e_res_number]["hd_list"]=[]
			exp_assign_dict[e_res_number]["he_list"]=[]
			exp_assign_dict[e_res_number]["hz_list"]=[]
			exp_assign_dict[e_res_number]["hh_list"]=[]
			exp_assign_dict[e_res_number]["cg_list"]=[]
			exp_assign_dict[e_res_number]["cd_list"]=[]
			exp_assign_dict[e_res_number]["ce_list"]=[]
			exp_assign_dict[e_res_number]["cz_list"]=[]
			exp_assign_dict[e_res_number]["nd_list"]=[]
			exp_assign_dict[e_res_number]["ne_list"]=[]

			exp_assign_dict[e_res_number]["res_name"]=e_res_name

			l_residue_numbers_list+=[e_res_number]
		if verbose>=3:	print e_res_number,e_res_name
		l_HA_found,l_NH_found,l_CA_found,l_CB_found,l_CO_found,l_N_found=0,0,0,0,0,0
		ha_list=[]


		for item in l_assignment_list:
			n_res_number,n_res_name,n_atom,n_shift=int(item[1]),item[0],item[2],item[4]

			if len(l_verify_dict)>0:
				if n_res_name in aa_names_full_all_CAP:
					res_name_1_let=aa_names_full_all_CAP[n_res_name]
					if res_name_1_let in l_verify_dict:
						verify_atom_list=l_verify_dict[res_name_1_let]
						if n_atom not in verify_atom_list:
							print "Error!!!! Non-standard atom name %s was found for %s%s" % (n_atom,n_res_name,n_res_number)
							print "The program will exit now"
							sys.exit()

					else:
						print "Warning Residue name %s of residue number %s is not in l_verify_dict" % (n_res_name,n_res_number,)


				else:
					print "Warning!!!! Residue name %s of residue number %s is not in aa_names_full_all_CAP" % (n_res_name,n_res_number)
			proceed=1
			if len(l_dict_with_common_atoms)>0:
				dict_with_common_atoms_resnum=n_res_number+l_dict_with_common_atoms_offset
				if dict_with_common_atoms_resnum in l_dict_with_common_atoms:
					if 'residue_name' in l_dict_with_common_atoms[dict_with_common_atoms_resnum]:
						dict_with_common_atoms_resname=l_dict_with_common_atoms[dict_with_common_atoms_resnum]['residue_name']
						if (dict_with_common_atoms_resname==n_res_name) or l_allow_mutations:
							common_shifts=l_dict_with_common_atoms[dict_with_common_atoms_resnum]['common_shifts']
							if n_atom not in common_shifts:
								proceed=0	
								if verbose>=1: print "Warning!!! Excluding %s of %s%s (%s%s after offset %s) because it was not found among common atoms" % (n_atom,n_res_name,n_res_number,dict_with_common_atoms_resnum,dict_with_common_atoms_resname,l_dict_with_common_atoms_offset), common_shifts					
							

						elif (dict_with_common_atoms_resname!=n_res_name) and l_allow_mutations==0:
							if verbose>=1: print "Warning!!! Residue %s%s is %s%s in dict_with_common_atoms after applying offset %s" % (n_res_name,n_res_number,dict_with_common_atoms_resnum,dict_with_common_atoms_resname,l_dict_with_common_atoms_offset)
							proceed=0	

					else:
						if verbose>=1: print "Warning!!! Parameter residue_name was not found in l_dict_with_common_atoms for residue ", dict_with_common_atoms_resnum
						proceed=0	
				else:
					if verbose>=10: print "Warning!!!! Residue %s, which is %s after applying offset %s, is not found in l_dict_with_common_atoms" % (n_res_number,dict_with_common_atoms_resnum,l_dict_with_common_atoms_offset)
					proceed=0	



			if l_exp_assignement.has_key(n_atom)==0 and "H" in n_atom:
				if verbose>=0:
					print "Warning!!! Atom %s in %s%s is not found in atom dictionary" % (n_atom,n_res_name,n_res_number)
				not_found_list+=[n_res_name,n_res_number,n_atom]
				not_found_list_text+="%s %s %s" % (n_res_name,n_res_number,n_atom) + linesep
#						l_text+="dic[%s]={'residue_name':'%s','common_shifts':[]}" % (i,residue_name) + linesep

			elif l_exp_assignement.has_key(n_atom)==1 and proceed==1:
				l_exp_assignement[n_atom][n_res_number]={"res_name":n_res_name, "shift":float(n_shift)}
			#print n_res_number,n_res_name
			if (e_res_number==n_res_number) and (e_res_name==n_res_name) and proceed==1:

				debug=0
				if debug==1 and e_res_number==20:
					print e_res_number,e_res_name, n_atom, n_shift

				exp_assign_dict[e_res_number]["shifts"][n_atom]=n_shift


				if lower(n_atom) in ["ha","ha1","ha2","ha3"]:
					exp_assign_dict[e_res_number]["ha_list"]+=[float(n_shift)]
				if lower(n_atom) in ["hb","hb1","hb2","hb3"]:
					exp_assign_dict[e_res_number]["hb_list"]+=[float(n_shift)]
					if debug==10 and e_res_number==20:
						print exp_assign_dict[e_res_number]["hb_list"]
				if upper(n_atom) in ["HG","HG1","HG2","HG3","HG11","HG12","HG13","HG21","HG22","HG23"]:
					debug=0
					exp_assign_dict[e_res_number]["hg_list"]+=[float(n_shift)]
					if debug==1 and e_res_number==114:
						print exp_assign_dict[e_res_number]["hg_list"], "atom=", n_atom
				if upper(n_atom) in ["HD","HD1","HD2","HD3","HD11","HD12","HD13","HD21","HD22","HD23"]:
					exp_assign_dict[e_res_number]["hd_list"]+=[float(n_shift)]
				if upper(n_atom) in ["HE","HE1","HE2","HE3","HE21","HE22"]:
					exp_assign_dict[e_res_number]["he_list"]+=[float(n_shift)]
				if upper(n_atom) in ["HH","HH11","HH12","HH2","HH21","HH22"]:
					exp_assign_dict[e_res_number]["hh_list"]+=[float(n_shift)]

				if upper(n_atom) in ["HZ","HZ1","HZ2","HZ3"]:
					exp_assign_dict[e_res_number]["hz_list"]+=[float(n_shift)]

				if upper(n_atom) in ["CG","CG1","CG2"]:
					exp_assign_dict[e_res_number]["cg_list"]+=[float(n_shift)]
				if upper(n_atom) in ["CD","CD1","CD2"]:
					exp_assign_dict[e_res_number]["cd_list"]+=[float(n_shift)]
				if upper(n_atom) in ["CE","CE1","CE2","CE3"]:
					exp_assign_dict[e_res_number]["ce_list"]+=[float(n_shift)]
				if upper(n_atom) in ["CZ","CZ2","CZ3"]:
					exp_assign_dict[e_res_number]["cz_list"]+=[float(n_shift)]
				if upper(n_atom) in ["ND","ND1","ND2"]:
					exp_assign_dict[e_res_number]["nd_list"]+=[float(n_shift)]
				if upper(n_atom) in ["NE","NE1","NE2"]:
					exp_assign_dict[e_res_number]["ne_list"]+=[float(n_shift)]

				if "HA" in  n_atom:
					ha_list+=[float(n_shift)]
					l_HA_found=1
					if verbose>=3:
						print "HAs of %s %s is %s" % (n_res_name,n_res_number,ha_list)
				elif  n_atom=="H":
					l_NH=float(n_shift)
					l_NH_found=1	
				elif  n_atom=="CB":
					l_CB=float(n_shift)
					l_CB_found=1
				elif  n_atom=="C":
					l_CO=float(n_shift)
					l_CO_found=1	
				elif  n_atom=="N":
					l_N=float(n_shift)
					l_N_found=1				
				elif  n_atom=="CA":
					l_CA=float(n_shift)
					l_CA_found=1									
			elif (e_res_number==n_res_number) and (e_res_name!=n_res_name):		
				print "Error! Name of residue %s is %s in the residue list and %s in the shift list in the BMRB file" % (e_res_number,e_res_name,n_res_name) + linesep
				#l_log_name.write(log_out)

			elif (e_res_number==n_res_number) and (e_res_name==n_res_name) and proceed==0:
				if verbose>=10: print "Warning!!! Excluding %s of %s%s because proceed=0" % (n_atom,n_res_name,n_res_number)
				
		if len(ha_list)>0:
			l_HA=stats.lmean(ha_list)

		if len(exp_assign_dict[e_res_number]["ha_list"])>0:
			l_HA_ave=stats.lmean(exp_assign_dict[e_res_number]["ha_list"])
			if use_average_HA==1:
				exp_assign_dict[e_res_number]["shifts"]["HA"]=l_HA_ave
			exp_assign_dict[e_res_number]["shifts"]["HAave"]=l_HA_ave

		if len(exp_assign_dict[e_res_number]["hb_list"])>0:
			l_HB_ave=stats.lmean(exp_assign_dict[e_res_number]["hb_list"])
			if verbose>=10 and e_res_number==3:
				print "HB list=", e_res_name, e_res_number, l_HB_ave, exp_assign_dict[e_res_number]["hb_list"]
			exp_assign_dict[e_res_number]["shifts"]["HBave"]=l_HB_ave

		if len(exp_assign_dict[e_res_number]["hd_list"])>0:
			l_HD_ave=stats.lmean(exp_assign_dict[e_res_number]["hd_list"])
			exp_assign_dict[e_res_number]["shifts"]["HDave"]=l_HD_ave

		if len(exp_assign_dict[e_res_number]["hg_list"])>0:
			l_HG_ave=stats.lmean(exp_assign_dict[e_res_number]["hg_list"])
			exp_assign_dict[e_res_number]["shifts"]["HGave"]=l_HG_ave
			debug=0
			if debug==1 and e_res_number==114:
				print "HGave=",l_HG_ave

		if len(exp_assign_dict[e_res_number]["he_list"])>0:
			l_HE_ave=stats.lmean(exp_assign_dict[e_res_number]["he_list"])
			exp_assign_dict[e_res_number]["shifts"]["HEave"]=l_HE_ave

		if len(exp_assign_dict[e_res_number]["hh_list"])>0:
			l_HH_ave=stats.lmean(exp_assign_dict[e_res_number]["hh_list"])
			exp_assign_dict[e_res_number]["shifts"]["HHave"]=l_HH_ave

		if len(exp_assign_dict[e_res_number]["hz_list"])>0:
			l_HZ_ave=stats.lmean(exp_assign_dict[e_res_number]["hz_list"])
			exp_assign_dict[e_res_number]["shifts"]["HZave"]=l_HZ_ave



		if len(exp_assign_dict[e_res_number]["cg_list"])>0:
			l_CG_ave=stats.lmean(exp_assign_dict[e_res_number]["cg_list"])
			exp_assign_dict[e_res_number]["shifts"]["CGave"]=l_CG_ave


		if len(exp_assign_dict[e_res_number]["cd_list"])>0:
			l_CD_ave=stats.lmean(exp_assign_dict[e_res_number]["cd_list"])
			exp_assign_dict[e_res_number]["shifts"]["CDave"]=l_CD_ave


		if len(exp_assign_dict[e_res_number]["ce_list"])>0:
			l_CE_ave=stats.lmean(exp_assign_dict[e_res_number]["ce_list"])
			exp_assign_dict[e_res_number]["shifts"]["CEave"]=l_CE_ave

		if len(exp_assign_dict[e_res_number]["cz_list"])>0:
			l_CZ_ave=stats.lmean(exp_assign_dict[e_res_number]["cz_list"])
			exp_assign_dict[e_res_number]["shifts"]["CZave"]=l_CZ_ave

		if len(exp_assign_dict[e_res_number]["nd_list"])>0:
			l_ND_ave=stats.lmean(exp_assign_dict[e_res_number]["nd_list"])
			exp_assign_dict[e_res_number]["shifts"]["NDave"]=l_ND_ave

		if len(exp_assign_dict[e_res_number]["ne_list"])>0:
			l_NE_ave=stats.lmean(exp_assign_dict[e_res_number]["ne_list"])
			exp_assign_dict[e_res_number]["shifts"]["NEave"]=l_NE_ave

					
		final_list+=[[e_res_number,e_res_name,l_HA,l_NH,l_N,l_CA,l_CB,l_CO]]

		if verbose>=1:	
			if l_HA_found==0:
				print "HA of %s%s was not found  in residue" %  (e_res_number,e_res_name)
		
	if not_found_list_text!="":
		not_found_file_open=open(not_found_file,"w")
		not_found_file_open.write(not_found_list_text)
		not_found_file_open.close()

	debug=0
	if debug==1:
		print "Exiting after function convert_assignment()"
		sys.exit()
	return(final_list,l_exp_assignement,exp_assign_dict,l_residue_numbers_list)

def parse_chemical_shifts(l_verify_dict,l_shift_file_abspath,l_author_cs,l_dict_with_common_atoms,l_dict_with_common_atoms_offset,l_allow_mutations):
	"""
	Parse chemical shift file
	--both bmrb 2.1 and 3.1 file formats
	"""
	verbose=1
	if verbose==1: print "Running function parse_chemical_shifts()"
	l_bmrb_file_abspath=l_shift_file_abspath
	bmrb_lines=read_file(l_bmrb_file_abspath)
	if shift_format==0:
		protein_sequence_list,seq_code_list, author_seq_code_list=bmrb_to_aa(bmrb_lines)
		assignment,assignment_seq_code_list,assignment_author_seq_code_list,B_Cys_list=extract_assignment3(bmrb_lines)
	if shift_format==2:	
		protein_sequence_list,seq_code_list, author_seq_code_list=bmrb3_to_aa(bmrb_lines)
		assignment,assignment_seq_code_list,assignment_author_seq_code_list,B_Cys_list=extract_assignment_bmrb3(bmrb_lines)
	
	if l_author_cs==0 and len(seq_code_list)>0 and len(assignment_seq_code_list)>0:
		protein_sequence_list=seq_code_list
		assignment=assignment_seq_code_list
	elif  l_author_cs==1 and len(author_seq_code_list)>0 and len(assignment_author_seq_code_list)>0:
		protein_sequence_list=author_seq_code_list
		assignment=assignment_author_seq_code_list

	if verbose==10: print "protein_sequence_list = ", protein_sequence_list
	#sys.exit()
	final_assignment,exp_assignement,exp_assign_dict,l_residue_numbers_list=convert_assignment(l_verify_dict,assignment,protein_sequence_list,exp_assignment,l_dict_with_common_atoms,l_dict_with_common_atoms_offset,l_allow_mutations)
	if verbose==10: 
		print "assignment= ",assignment
	#print "final_assignment=", final_assignment
	#print exp_assignement
	#print exp_assign_dict
	debug_exit(0," Exiting after convert_assignment(() ...")	
	if verbose>=3: print "final_assignment=", final_assignment
	if verbose>=3: print "exp_assignement=", exp_assignement,exp_assign_dict
	l_residue_numbers_list.sort()

	debug_exit(0,"Exiting after function parse_chemical_shifts() ...")	

	debug=0
	if debug==1:
		for i in exp_assign_dict:
			if i==20:
				print i, exp_assign_dict[i]

		print "Exiting after function  parse_chemical_shifts()"
		sys.exit()
	return protein_sequence_list,final_assignment,exp_assignement,assignment, exp_assign_dict,l_residue_numbers_list,B_Cys_list





def non_stereospecific(l_stereospecific_dictionary,l_assign_dic):
	"""
	Find non-stereospecific assignments	
	"""
	verbose=0
	if verbose>=1: 
		print "Running function stereospecific_ambigous()"



	for res_num in l_assign_dic:
		if verbose>=1: 
			print res_num
		res_name=l_assign_dic[res_num][0]
		for averaged_shift in l_stereospecific_dictionary:
			if verbose>=1: print res_num,res_name,averaged_shift
			if averaged_shift not in  l_assign_dic[res_num][1]:
				atom_list=l_stereospecific_dictionary[averaged_shift]["list"]
				if verbose>=1: print res_num, res_name,averaged_shift, atom_list
				l_list=[]
				for atom  in l_assign_dic[res_num][1]:
					if verbose>=1: print res_num, res_name, atom
					if upper(atom) in  atom_list:
						if verbose>=1: print "FOUND"
						shift_value=l_assign_dic[res_num][1][atom]
						if is_number(shift_value):
							l_list+=[float(shift_value)]
						else:
							print "Warning!!! Value of shift %s for residue %s%s is not numerical and is " % (atom,res_name,res_num), [shift_value]				 
				if verbose>=1: print res_num, res_name,averaged_shift,l_list
				if len(l_list)>0:
					shift_mean=stats.lmean(l_list)
					if verbose>=10 and averaged_shift=="HGave":#"HGave":#"HAave": #"HBave":
						if verbose>=1: print "SUCESS", res_num, res_name, shift_mean, l_list, shift_mean

					l_assign_dic[res_num][1][averaged_shift]=shift_mean
	debug=0
	if debug==1:
		#print l_assign_dic
		print "Exiting function stereospecific_ambigous()"
		sys.exit()
	return




def find_random_coil_value(l_shift_type,l_res_name,l_single_letter_res_name):
	"""
	Find random coil value for a shift
	"""
	#shift_found=0
	verbose=0
	random_coil_value="NONE"
	if l_shift_type in shiftx_rc_dic and l_res_name in shiftx_rc_dic[l_shift_type]:
		random_coil_value=shiftx_rc_dic[l_shift_type][l_res_name]
	if random_coil_value=="NONE":
		for place in place_dict:
			if l_shift_type in  place_dict[place]:
				if l_single_letter_res_name in random_coil_dic:
					random_coil_value=random_coil_dic[l_single_letter_res_name][place]
				else:
					print "Error!!! Residue name %s was not found in  random_coil_dic" % l_single_letter_res_name


	if is_number(random_coil_value)==0 and verbose>=1:
		print "Error!!! Random coil value was not found for residue name %s and atom name %s" % (l_res_name,l_shift_type)
		print "Found Random coil value is ", [random_coil_value]
	return random_coil_value

def gnuplot_bars(l_data_file,l_title,l_Y_label,l_X_label,l_image_file,l_description):
	"""
	Create plots with vertical bars
	"""
	f=os.popen('%s' % gnuplot_program,'w')
	#print >>f, "set data style linespoints"
	print >>f, "set style data linespoints"
	print >>f, "set boxwidth 0.1"
	print >>f, "set offsets 0, 0, 1, 0"
	#print >>f, "set terminal postscript color"
	#print >>f, "set terminal postscript solid"	
	print >>f, "set terminal png  tiny size 640,480"
	print >>f, "set title '%s'" % l_title 
	print >>f, "set ylabel '%s'" % l_Y_label
	print >>f, "set xlabel '%s'" % l_X_label
	print >>f, "set grid"
	print >>f, "set xtics 5 out nomirror"
	print >>f, "set mxtics"
	print >>f, "set output '%s'" % (l_image_file)
	print >>f, "plot [] '%s' title '%s' with boxes lt 3 lw 4" % (l_data_file,l_description)
	print >>f, "exit"
	return()

def gnuplot_bars_mean_std(l_data_file,l_title,l_Y_label,l_X_label,l_image_file,l_description,l_mean,l_std,l_first_residue,l_last_residue):
	"""
	Create plots with vertical bars and horizontal lines for mean value
	"""

	mean_plus_2_std="NONE"
	if is_number(l_mean) and is_number(l_std):
		mean_plus_2_std=l_mean+(l_std*2.0)
		mean_plus_1_std=l_mean+(l_std*1.0)
	f=os.popen('%s' % gnuplot_program ,'w')
	print >>f, "set style data linespoints"
	print >>f, "set boxwidth 0.1"
	print >>f, "set offsets 0, 0, 1, 0"
	#print >>f, "set terminal postscript color"
	#print >>f, "set terminal postscript solid"	
	print >>f, "set terminal png  tiny size 640,480"
	print >>f, "set title '%s'" % l_title 
	print >>f, "set ylabel '%s'" % l_Y_label
	print >>f, "set xlabel '%s'" % l_X_label
	print >>f, "set xrange [%s:%s]" % (l_first_residue-1,l_last_residue+1)
	print >>f, "set grid"
	print >>f, "set xtics 5 out nomirror"
	print >>f, "set mxtics"
	print >>f, "set output '%s'" % (l_image_file)
	if is_number(l_mean)==0  and  is_number(l_std)==0: 
		print >>f, "plot [] '%s' title '%s' with boxes lt 3 lw 4" % (l_data_file,l_description)
	elif is_number(l_mean)==1  and  is_number(l_std)==0: 
		print >>f, "plot [] '%s' title '%s' with boxes lt 3 lw 4, %s title 'Mean' with lines lt 1" % (l_data_file,l_description,l_mean)
	elif is_number(l_mean)==1  and  is_number(mean_plus_2_std)==1: 
		print >>f, "plot [] '%s' title '%s' with boxes lt 3 lw 4, %s title 'Mean' with lines lt 2, %s title 'Mean + 1 std. deviation' with lines lt 4, %s title 'Mean + 2 std. deviations' with lines lt 1" % (l_data_file,l_description,l_mean,mean_plus_1_std,mean_plus_2_std)
	print >>f, "exit"
	return()



def scale_shift(l_predictor,l_abs_diff,l_res_name,l_shift_class,l_shift_type):
	"""
	Scale shift value
	"""
	verbose=0
	if verbose>=10: print "Running function scale_shift() for l_shift_class %s and l_shift_type %s " % (l_shift_class,l_shift_type)
	Hertz_correction_applied=1.0 #"NONE"
	if l_shift_class in hertz_shift_classes:
		if verbose>=10: print "Shift class %s was found in hertz_shift_classes, applying Hertz correlction to absolute shift diff of shift %s " % (l_shift_class,l_shift_type), [l_abs_diff]
		if "H" in l_shift_type and "C" not in l_shift_type:
			l_abs_diff=H_Hertz_corr*l_abs_diff
			Hertz_correction_applied=H_Hertz_corr
		elif "C" in l_shift_type:
			l_abs_diff=C_Hertz_corr*l_abs_diff
			Hertz_correction_applied=C_Hertz_corr
		elif "N" in l_shift_type and "H" not in l_shift_type:
			l_abs_diff=N_Hertz_corr*l_abs_diff	
			Hertz_correction_applied=N_Hertz_corr	
	if verbose>=10: print "Shift %s" % l_shift_class, ", shift: ", [l_shift_type], ", Corr: ", Hertz_correction_applied
	debug=0
	if debug==1:
		print "Exiting after function scale_shift()"
	return l_abs_diff, Hertz_correction_applied




def calc_cs_diff(stereo_matching_mode,l_mean_cs_mode,l_zodb,l_database_file,l_pdb_list,l_pdb_dict,exp_assign_dict,l_max_offset,l_predictor,l_predictor_key):	
	"""
	Calculate difference for each residue each NMR chemical shift
	
				l_assign_dic[res_num_1][1]["HG3"]=float(HG3)
			l_assign_dic[res_num_1][1]["HZ"]=float(HZ)



			shiftx_assignment["H"][res_num_1]=[[res_name_1, float(H), "H"]]
			shiftx_assignment["HA"][res_num_1]=[[res_name_1, float(HA), "HA"]]
			shiftx_assignment["HB"][res_num_1]=[[res_name_1, float(HB), "HB"]]
			
71 ['L', {'HZ': 0.0, 'NH_again': 8.4458000000000002, 'HD22': 0.0, 'HD21': 0.0, 'HB': 0.0, 'HE22': 0.0, 'HE21': 0.0, 'HA': 4.9400000000000004, 'HG': 1.55, 'HE': 0.0, 'HE1': 0.0, 'HE2': 0.0, 'HE3': 0.0, 'HG2': 0.0, 'HG3': 0.0, 'HG1': 0.0, 'HG12': 0.0, 'HG13': 0.0, 'H': 8.4499999999999993, 'CO': 176.3015, 'HA_again': 4.9359000000000002, 'CB': 43.240200000000002, 'CA': 53.589399999999998, 'N': 126.7392, 'HD3': 0.0, 'HD2': 0.83999999999999997, 'HD1': 0.97999999999999998, 'HB3': 1.5700000000000001, 'HB2': 1.55}]

					if n_res_number not in exp_assign_dict:
				exp_assign_dict[n_res_number]={}
				exp_assign_dict[n_res_number]["shifts"]={}
				exp_assign_dict[n_res_number]["res_name"]=n_res_name
			exp_assign_dict[n_res_number]["shifts"][n_atom]=n_shift
			
Residue number 76 was found in exp_assign_dict(). Content: {'res_name': 'GLY', 'shifts': {'H': 8.1600000000000001, 'CA': 46.200000000000003, 'HA3': 3.7000000000000002, 'HA2': 3.7799999999999998, 'N': 115.2}} 
HB3 0.0
Residue number 76 was found in exp_assign_dict(). Content: {'res_name': 'GLY', 'shifts': {'H': 8.1600000000000001, 'CA': 46.200000000000003, 'HA3': 3.7000000000000002, 'HA2': 3.7799999999999998, 'N': 115.2}} 
HB2 0.0
Residue number 76 was found in exp_assign_dict(). Content: {'res_name': 'GLY', 'shifts': {'H': 8.1600000000000001, 'CA': 46.200000000000003, 'HA3': 3.7000000000000002, 'HA2': 3.7799999999999998, 'N': 115.2}} 
			
shiftx_rc_dic={}
shiftx_rc_dic["H"]={}; shiftx_rc_dic["H"]["ALA"]= 8.24;	
	
	"""
	verbose=0
	if verbose>=0: print "Running function calc_cs_diff()"
	l_common_residue_numbers=[]	

	first_residue=last_residue="NONE"

	shift_classes={}
	

	#print "Before inserting dictionary into a database"
	#posts = db.posts
	#posts.insert(l_pdb_dict)
	#print "Exiting after inserting dictionary into a database"
	#sys.exit()
	#y="""
	#database_file= "%s/database.dat" % l_full_project_dir
	#for i in l_pdb_dict:

	if l_mean_cs_mode==1:
		l_pdb_list=["ensemble_mean_chemical_shifts"]

	for i in l_pdb_list:
		if verbose>=10: print "Processing case ", i
		if l_zodb and l_mean_cs_mode==0:		


			#print l_pdb_dict
			#persist_dictionary.PersistentDict.sync(l_pdb_dict)
			#persist_dictionary.PersistentDict.close(l_pdb_dict)
			#print l_pdb_dict
			#l_pdb_dict={}
			#l_pdb_dict =  persist_dictionary.PersistentDict(database_file, 'c', format='pickle')
			#print l_pdb_dict
			#sys.exit()
			#l_pdb_dict = shelve.open(database_file,writeback=True)
			#persist_dictionary.PersistentDict.close(database_file)
			#l_pdb_dict=persist_dictionary.PersistentDict.close(database_file)
	
				

			storage = FileStorage(l_database_file)

			#db=ZODB.DB(ZODB.FileStorage.FileStorage(database_file))
			db = DB(storage)	
			connection = db.open()
			#l_pdb_dict = connection.root()
			main_database = connection.root()
			#main_database["pdb_dict"]= OOBTree()
			l_pdb_dict=main_database["pdb_dict"]


			#l_pdb_dict=db.open().root()



			#print temp_dict
			#temp_dict
			#temp_dict=db.open().root()[i]		
			#if l_predictor not in temp_dict:
		if verbose>=0: print "Processing case  %s in function calc_cs_diff()" % i
		if l_predictor not in l_pdb_dict[i]:
			l_pdb_dict[i][l_predictor]={}
			#l_pdb_dict[i][l_predictor]= PersistentDict()
			#l_pdb_dict[i][l_predictor]= OOBTree()


		if l_zodb and l_mean_cs_mode==0:
			if l_predictor not in l_pdb_dict[i]:
				print """Warning!!!! Predictor %s is not in  l_pdb_dict[1]""" % l_predictor
				print "Program will exit now"
				sys.exit()
			#print "Adding predictor %s to temp_dict" %  l_predictor
			#if zodb:
				#db.open().root()[i][l_predictor]={}
				#temp_dict[l_predictor]={}
					#temp_dict._p_changed = True
				#l_pdb_dict._p_changed = True

				#db.open()._p_changed = True
				#transaction.savepoint(True)
				#transaction.commit()
				#get_transaction().commit()
				#print ""
		#if zodb:
			#db.close()
			#db=ZODB.DB(ZODB.FileStorage.FileStorage(database_file))
			#temp_dict=db.open().root()[i]
			#print temp_dict[l_predictor]
			#sys.exit()



			#print "Exiting..."
			#sys.exit()

			#db = DB(storage)	
			#issues=db.open().root()["issues"]
			#connection = db.open()
			#l_pdb_dict = connection.root()

	
			#transaction.commit()
			#db.close()

			#
	
			#print l_pdb_dict
			#sys.exit()
			#print ""






		#if verbose>=0: print "2) Processing case ", i
		#if l_predictor not in l_pdb_dict[i]:
		#	l_pdb_dict[i][l_predictor]={}
			#if zodb:
			#	l_pdb_dict[i][l_predictor]= OOBTree()
				#l_pdb_dict[i][l_predictor]= PersistentDict()
			#else:
			#	l_pdb_dict[i][l_predictor]={}


			#l_pdb_dict._p_changed = True
			#transaction.commit()
			#print "Adding predictor %s to l_pdb_dict[i]" %  l_predictor
		#sys.exit()

		
		#memory_mon = MemoryMonitor('markber')
		#used_memory = memory_mon.usage()
		#print "used_memory=", used_memory
						
		#shiftx_assignment=l_pdb_dict[i]["shiftx_assignment"]
		
		#shiftx_assign_dic=l_pdb_dict[i]["shiftx_assign_dic"]
		if l_predictor_key in l_pdb_dict[i]:
			pred_shift_assign_dic=l_pdb_dict[i][l_predictor_key]
			#print "len(pred_shift_assign_dic)=", len(pred_shift_assign_dic)
			#if len(pred_shift_assign_dic)<79:
			#	print "Error!!!! len(pred_shift_assign_dic)<79"
			if verbose==10:
				for w in 	pred_shift_assign_dic:
					print w, pred_shift_assign_dic[i]			
				
			#chain=l_pdb_dict[i]["chain"]
			#shiftx_list_4_offset=l_pdb_dict[i]["shiftx_list_4_offset"]						
			#pdb_dir=l_pdb_dict[i]["pdb_dir"]						
			#original_pdb=l_pdb_dict[i]["original_pdb"]						
			#pdb_full=l_pdb_dict[i]["pdb_full"]						
			#project_dir=l_pdb_dict[i]["project_dir"]						
			#shiftx_result=l_pdb_dict[i]["shiftx_result"]

			l_pdb_dict[i][l_predictor]["shift_diff_dict"]={}
			if zodb:
				l_pdb_dict[i][l_predictor]["protein_shift_classes"]={}
				#l_pdb_dict[i][l_predictor]["protein_shift_classes"]= OOBTree()
				#l_pdb_dict[i][l_predictor]["protein_shift_classes"]= PersistentDict()
			else:
				l_pdb_dict[i][l_predictor]["protein_shift_classes"]={}
			last_residue=-999999
			first_residue="NONE"
			
			for protein_shift_class in shift_class_description_dict:
				if verbose>=10: print "Processing protein_shift_class =", protein_shift_class 

				shift_classes[protein_shift_class]=[]	
				if zodb:
					l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]=OOBTree()
				else:
					l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]={}

				#if protein_shift_class not in l_pdb_dict[i][l_predictor]["protein_shift_classes"]:
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]={}
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]["true_diff_list"]=[]
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]["abs_diff_list"]=[]
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]["details"]=[]
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]["exp_list"]=[]
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]["pred_list"]=[]
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]["sec_exp_list"]=[]
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][protein_shift_class]["sec_sec_list"]=[]

				#{"true_diff_list":[],"abs_diff_list":[],"details":[],"exp_list":[],"pred_list":[],"sec_exp_list":[],"sec_sec_list":[]}		
			
			for residue_number in pred_shift_assign_dic:
				if verbose>=10: print "%s Processing residue number %s for structure %s " %  (l_predictor,residue_number, i)
				#l_pdb_dict[i]["shift_diff_dict"][residue_number]={"abs_diff":{},"true_diff":{}}

				if verbose>=10: print "l_max_offset=", [l_max_offset]
				if verbose>=10: print "residue_number=", [residue_number], linesep
				exp_resid_number=residue_number-l_max_offset
			

				l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]={}

				l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"]={}
				
				for residue_shift_class in shift_class_description_dict:
					l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][residue_shift_class]={"true_diff_list":[],"abs_diff_list":[],"details":{}}
						
				#y="""
				#print 	exp_resid_number, shiftx_assign_dic[exp_resid_number]
				res_name=pred_shift_assign_dic[residue_number][0]

				l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["res_name"]=res_name
				#shiftx_dict=shiftx_assign_dic[residue_number][1]
				l_single_letter_res_name="NONE"
				if len(res_name)==3:
					res_name_three_letter=res_name
					if res_name_three_letter not in aa_names_full_all_CAP:
						res_name_three_letter="NONE"
						print "Warning!!! Residue name %s of residue %s is not typical. The residue will not be processed. " % (res_name,residue_number)	
					else:
						l_single_letter_res_name=aa_names_full_all_CAP[res_name_three_letter]		
				else:
					if res_name in aa_names_1let_2_full_all_CAP :
						res_name_three_letter=aa_names_1let_2_full_all_CAP[res_name]
						l_single_letter_res_name=res_name
						if verbose>2: print "pred_shift:", residue_number,res_name, res_name_three_letter
					else:
						print "Warning!!! Residue name %s of residue %s is not typical. The residue will not be processed. " % (res_name,residue_number)	
	

				
				if exp_resid_number in exp_assign_dict and 'res_name' in exp_assign_dict[exp_resid_number] and res_name_three_letter==exp_assign_dict[exp_resid_number]['res_name']:
					if exp_resid_number not in l_common_residue_numbers:
						l_common_residue_numbers+=[exp_resid_number]
					if first_residue=="NONE":
						first_residue=exp_resid_number
					else:
						if exp_resid_number<first_residue:
							exp_resid_number=first_residue
					if exp_resid_number>last_residue:
						last_residue=exp_resid_number
						

					
					if "shifts" in exp_assign_dict[exp_resid_number]:
						exp_shift_dict=exp_assign_dict[exp_resid_number]["shifts"]

						if "reference_shifts" not in exp_assign_dict[exp_resid_number]:
							exp_assign_dict[exp_resid_number]["reference_shifts"]={}

						for exp_shift_type in exp_shift_dict:

							exp_shift_value=exp_shift_dict[exp_shift_type]

							alternative_exp_shift,alternative_exp_shift_value=find_alternative_shift_type(exp_shift_dict,exp_shift_type,l_single_letter_res_name)

							if verbose>=10: print exp_resid_number, res_name, exp_shift_type, "alternative_exp_shift=", alternative_exp_shift, "alternative_exp_shift_value=", alternative_exp_shift_value

							debug=0
							if debug==1 and exp_resid_number==4:
								print exp_shift_type, exp_shift_value, linesep

							if verbose>=10: 
								print "%s Processing Experimental Shift %s with value %s for residue number %s for structure %s " %  (l_predictor, exp_shift_type,exp_shift_value,residue_number, i)


							shift_type_found=0
							shift_with_values_found=0
							calc_diff=0	
							random_coil_value_found=0
						
					
							if is_number(exp_shift_value)==1:													
								if verbose>=2: print "Experimental:", exp_resid_number,res_name_three_letter,exp_shift_type,exp_shift_dict[exp_shift_type]
								calc_diff=1		
							else:
								print "Warning!!! Chemical shift value of atom %s of residue %s%s is not numerical and is " % (exp_shift_type,res_name_three_letter,exp_resid_number),[exp_shift_value] 
	
							if exp_shift_type not in l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]:
								l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][exp_shift_type]={}
							l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][exp_shift_type]["exp_value"]=exp_shift_value						
							
							for shift_type in pred_shift_assign_dic[residue_number][1]:

								if verbose>=10 and shift_type=="CA": 
									print "%s Processing Shift %s residue number %s for structure %s " %  (l_predictor,shift_type,residue_number, i)
							
								
								pred_shift_value=pred_shift_assign_dic[residue_number][1][shift_type]

								alternative_predicted_shift,alternative_predicted_shift_value=find_alternative_shift_type(pred_shift_assign_dic[residue_number][1],shift_type,l_single_letter_res_name)

								debug=0
								if debug==1 and residue_number==20:
									print shift_type, pred_shift_value, linesep

								if shift_type not in l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]:
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]={}							
								l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["pred_value"]=pred_shift_value

								if pred_shift_value!=0.0 and is_number(pred_shift_value)==1 and shift_type==exp_shift_type and calc_diff==1:

									random_coil_value=find_random_coil_value(shift_type,res_name_three_letter,l_single_letter_res_name)


								
									

									shift_type_found=1
									shift_with_values_found=1

									#matching_mode=0
									mean_abs_diff,abs_diff, true_diff,reference_shift_value_to_use,reference_shift_to_use=find_best_matching_reference_shift(exp_assign_dict[exp_resid_number]["reference_shifts"],stereo_matching_mode,shift_type,exp_shift_value,pred_shift_value,alternative_exp_shift,alternative_exp_shift_value,alternative_predicted_shift,alternative_predicted_shift_value)
								
									#true_diff=float(exp_shift_value)-float(pred_shift_value)
									#abs_diff=abs(true_diff)

	
									debug=0
									if debug==1 or g_debug==1:
									#if verbose>=10:
										#if residue_number==61 and shift_type in ["CGave", "CG1", "CG2"]:
										if residue_number in [12] :# and shift_class=="Side_chain": #shift_type in ["CGave", "CG1", "CG2"]:
											
											#print "shift_class", shift_class
											print "res_name=", res_name
											print "residue_number= ", residue_number
											print "shift_type= ", shift_type
											print "exp_shift_value= ", exp_shift_value
											print "pred_shift_value= ", pred_shift_value
											print "abs_diff= ", abs_diff, linesep, linesep

											#print "alternative_predicted_shift_value= ", alternative_predicted_shift_value
											#print "alt_abs_diff= ", alt_abs_diff, linesep, linesep
									debug=0

									sec_exp_shift_value="NONE"
									sec_pred_shift_value="NONE"
									if is_number(random_coil_value)==1:
										sec_exp_shift_value=float(exp_shift_value)-random_coil_value
										sec_pred_shift_value=float(pred_shift_value)-random_coil_value
									else:
										if verbose>=2: print "Warning!!!! Calculation of secondary chemical shift has failed for %s of %s%s, found rand coil is " % (shift_type,res_name_three_letter,exp_resid_number), [random_coil_value]

						
									if shift_type not in l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]:
										l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]={}

									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["true_diff"]=true_diff
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["abs_diff"]=abs_diff
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["exp_shift_value"]=float(exp_shift_value)
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["pred_value"]=float(pred_shift_value)
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["sec_exp_shift_value"]=sec_exp_shift_value
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["sec_pred_value"]=sec_pred_shift_value
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["res_name"]=res_name
				
									averaged_shift_type=""
					
									if len(shift_type)>1 and shift_type not in ["CB"]:
										averaged_shift_type="%save" % shift_type[:2]
										if averaged_shift_type in exp_shift_dict:
											if verbose>=10: print "average_shift_type %s was found in exp_shift_dict for shift_type %s " % (averaged_shift_type,shift_type)
									

											if averaged_shift_type not in l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]:
												l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][averaged_shift_type]={}
												if verbose>=10: print "Warning!!!! averaged_shift_type %s had to be added to l_pdb_dict with mean_abs_diff=%s and true_diff=%s for" % (averaged_shift_type,mean_abs_diff,true_diff), i,l_predictor,res_name,exp_resid_number

											else:
												if verbose>=1: print "FOUND averaged_shift_type %s in  l_pdb_dict for" % (averaged_shift_type), i,l_predictor,res_name,exp_resid_number		
											l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][averaged_shift_type]["true_diff"]=true_diff
											l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][averaged_shift_type]["abs_diff"]=mean_abs_diff
											l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][averaged_shift_type]["exp_shift_value"]=float(exp_shift_value)
											l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][averaged_shift_type]["pred_value"]=float(pred_shift_value)
											l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][averaged_shift_type]["sec_exp_shift_value"]=sec_exp_shift_value
											l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][averaged_shift_type]["sec_pred_value"]=sec_pred_shift_value
											l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][averaged_shift_type]["res_name"]=res_name




									#l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["res_name_three_letter"]=res_name_three_letter

				
									if shift_type in shift_class_dict:
										if verbose>=10:print "Processing shift_type %s with averaged_shift_type " % (shift_type), averaged_shift_type
										shift_classes=shift_class_dict[shift_type]
										averaged_shift_classes=[]
										if averaged_shift_type in  shift_class_dict:
											averaged_shift_classes=	shift_class_dict[averaged_shift_type]


										for shift_class in shift_classes:
											if verbose>=10:print "Processing shift_class=", shift_class

											if "ave" not in shift_class:
											#if 1==1:
												shift_class1="%s_ave" % shift_class
												shift_class2="%save" % shift_class
												#print "Place 1"
												shift_class_averaged=""
												if shift_class1 in averaged_shift_classes:
												#if shift_class1 in l_shift_class_lookup:
													shift_class_averaged=shift_class1
												elif shift_class2 in averaged_shift_classes:
												#elif shift_class2 in l_shift_class_lookup:
													shift_class_averaged=shift_class2
												else:
													shift_class_averaged=shift_class1

												if shift_class_averaged!="":
													if verbose>=10: print "shift_class_averaged %s was found for shift_class %s " % (shift_class_averaged,shift_class)


												if verbose>=10:
													if residue_number==3 and shift_class in ["Side_chain","Side_chain_ave"]:
														print "shift_class=", shift_class
														print "residue_number= ", residue_number
														print "shift_type= ", shift_type
														print "exp_shift_value= ", exp_shift_value
														print "pred_shift_value= ", pred_shift_value
														print "abs_diff= ", abs_diff, linesep, linesep


												scaled_abs_diff,Hertz_correction_applied=scale_shift(l_predictor,abs_diff,res_name,shift_class,shift_type)
											
												if verbose>=10: print exp_resid_number,res_name,shift_type, "Processing shift_class: ", shift_class, " shift_class_averaged=", shift_class_averaged, "  averaged_shift_type=", [averaged_shift_type]
												l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class]["abs_diff_list"]+=[scaled_abs_diff]
												l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class]["true_diff_list"]+=[true_diff]
												l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class]["details"][shift_type]={"scaled_abs_diff":scaled_abs_diff,"abs_diff":abs_diff,"true_diff":true_diff,"exp_shift_value":exp_shift_value,"pred_value":pred_shift_value}

												l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["abs_diff_list"]+=[scaled_abs_diff]
												l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["true_diff_list"]+=[true_diff]
												l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["details"]+=[{"scaled_abs_diff":scaled_abs_diff,"res_name":res_name,"residue_number":exp_resid_number,"shift_type":shift_type,"abs_diff":abs_diff,"true_diff":true_diff,"exp_shift_value":exp_shift_value,"pred_value":pred_shift_value}]
						
												l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["exp_list"]+=[exp_shift_value]
												l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["pred_list"]+=[pred_shift_value]

												if sec_exp_shift_value!="NONE":
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["sec_exp_list"]+=[sec_exp_shift_value]
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["sec_sec_list"]+=[sec_pred_shift_value]


												if shift_class_averaged!="" and averaged_shift_type!="" and shift_class_averaged in l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"]:
													if verbose>=10:print "1) Adding info to ", shift_class_averaged
													mean_scaled_abs_diff,Hertz_correction_applied=scale_shift(l_predictor,mean_abs_diff,res_name,shift_class,shift_type)
	
													if verbose>=10: print shift_class, "Processing shift_class_averaged: ", [shift_class_averaged]
													l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class_averaged]["abs_diff_list"]+=[mean_scaled_abs_diff]
													l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class_averaged]["true_diff_list"]+=[true_diff]
													l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class_averaged]["details"][averaged_shift_type]={"scaled_abs_diff":mean_scaled_abs_diff,"abs_diff":mean_abs_diff,"true_diff":true_diff,"exp_shift_value":exp_shift_value,"pred_value":pred_shift_value}



													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["abs_diff_list"]+=[mean_scaled_abs_diff]
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["true_diff_list"]+=[true_diff]
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["details"]+=[{"scaled_abs_diff":mean_scaled_abs_diff,"res_name":res_name,"residue_number":exp_resid_number,"shift_type":averaged_shift_type,"abs_diff":mean_abs_diff,"true_diff":true_diff,"exp_shift_value":exp_shift_value,"pred_value":pred_shift_value}]
						
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["exp_list"]+=[exp_shift_value]
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["pred_list"]+=[pred_shift_value]
	
													if sec_exp_shift_value!="NONE":
														l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["sec_exp_list"]+=[sec_exp_shift_value]
														l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["sec_sec_list"]+=[sec_pred_shift_value]


												elif shift_class_averaged!="" and averaged_shift_type=="" and shift_class_averaged in l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"]:
													if verbose>=10:print "2) Adding info to ", shift_class_averaged

	
													if verbose>=10: print shift_class, "Processing shift_class_averaged: ", [shift_class_averaged]
													l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class_averaged]["abs_diff_list"]+=[scaled_abs_diff]
													l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class_averaged]["true_diff_list"]+=[true_diff]
													l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number]["residue_shift_classes"][shift_class_averaged]["details"][shift_type]={"scaled_abs_diff":scaled_abs_diff,"abs_diff":abs_diff,"true_diff":true_diff,"exp_shift_value":exp_shift_value,"pred_value":pred_shift_value}


													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["abs_diff_list"]+=[scaled_abs_diff]
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["true_diff_list"]+=[true_diff]
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["details"]+=[{"scaled_abs_diff":scaled_abs_diff,"res_name":res_name,"residue_number":exp_resid_number,"shift_type":shift_type,"abs_diff":abs_diff,"true_diff":true_diff,"exp_shift_value":exp_shift_value,"pred_value":pred_shift_value}]
						
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["exp_list"]+=[exp_shift_value]
													l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["pred_list"]+=[pred_shift_value]
	
													if sec_exp_shift_value!="NONE":
														l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["sec_exp_list"]+=[sec_exp_shift_value]
														l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class_averaged]["sec_sec_list"]+=[sec_pred_shift_value]







									elif shift_type not in shift_class_dict:
										print "Warning!!! shift_type %s is not in shift_class_dict" % shift_type

								
								if shift_type==exp_shift_type and (pred_shift_value==0.0 or is_number(pred_shift_value)==0):
									shift_type_found=1
									if verbose>4: print "Warning!!! pred_shift value of atom %s of residue %s%s is non-sense and is " % (exp_shift_type,res_name_three_letter,exp_resid_number),[pred_shift_value]
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["no_shiftx_predictions"]=1
							if shift_type_found==0:
								if "H" in exp_shift_type and (use_average_HA==0 or (use_average_HA==1 and exp_shift_type not in ["HA1","HA2","HA3"])):
									if verbose>=0: print "Warning!!! pred_shift value for atom %s of residue %s%s was not found, but an experimental value exists " % (exp_shift_type,res_name_three_letter,exp_resid_number)													
								else:
									if verbose>=0: print "Warning!!! pred_shift value for atom %s of residue %s%s was not found at all but an experimental value exists " % (exp_shift_type,res_name_three_letter,exp_resid_number)																	
								debug=0
								if debug==1 and (shift_type not in ["NE2","NEave"] and res_name_three_letter!="TRP"):
									print "Exiting...."
									sys.exit()

								l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][exp_shift_type]["shiftx_not_found"]=1											
				#		print "Residue number %s was found in exp_assign_dict(). Content: %s " % (exp_resid_number,exp_assign_dict[exp_resid_number])
		

				#"""
		#l_pdb_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean"]=0.22


						#l_pdb_dict["%s" % pdb_counter]= PersistentDict()
						#l_pdb_dict["%s" % pdb_counter]= OOBTree()
		#print """l_pdb_dict[1]["shiftx"]["protein_shift_classes"]["HD22"]["mean"] = """, l_pdb_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean"]
		#sys.exit()
		if l_zodb and l_mean_cs_mode==0:
			l_pdb_dict._p_changed = True
			transaction.savepoint(True)
			transaction.commit()
			#transaction.get().commit() 
			connection.close()
			db.close()
			storage.close()
			l_pdb_dict={}
	

	

	debug=0
	if debug==1:
	#sys.exit()
		if l_zodb and l_mean_cs_mode==0:
			storage = FileStorage(l_database_file)
			db = DB(storage)		
			#transaction.commit()
			#db.close()
			connection = db.open()
			#l_pdb_dict={}
			main_database = connection.root()
			l_pdb_dict=main_database["pdb_dict"]
			#print """len(l_pdb_dict["1"][l_predictor]) =""", len(l_pdb_dict[1][l_predictor])

			print "Exiting..."
			sys.exit()


			#main_database["pdb_dict2"]= PersistentDict()
			#l_pdb_dict2=main_database["pdb_dict2"]
			#l_pdb_dict2['1']=l_pdb_dict['1']
			#l_temp_dict= OOBTree()
			#l_temp_dict['1']=l_pdb_dict['1']
			#print l_pdb_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]
			#l_pdb_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean"]=0.22
			#l_temp_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean"]=0.22
			#print """1) l_pdb_dict2[1]["shiftx"]["protein_shift_classes"]["HD22"]["mean"] = """, l_pdb_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean"]
			#print """1) l_temp_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean" = """, l_temp_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean"]
			#sys.exit()
			l_pdb_dict._p_changed = True
			transaction.savepoint(True)
			transaction.commit()
			#transaction.get().commit() 
			connection.close()
			db.close()
			storage.close()
			l_pdb_dict={}
	
			storage = FileStorage(l_database_file)
			db = DB(storage)		
			connection = db.open()
			main_database = connection.root()
			l_pdb_dict=main_database["pdb_dict"]
			#l_pdb_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean"]=0.22
			#print """2) l_pdb_dict2[1]["shiftx"]["protein_shift_classes"]["HD22"]["mean"] = """, l_pdb_dict['1']["shiftx"]["protein_shift_classes"]["HD22"]["mean"]




			#print l_pdb_dict
			#print "3) l_pdb_dict[i][l_predictor]=", l_pdb_dict[i][l_predictor]
			#db.close()

		#print "l_pdb_dict=", l_pdb_dict
		print "first_residue= ", first_residue
		print "last_residue= ", last_residue
		print "l_common_residue_numbers= ", l_common_residue_numbers
		print "Exiting after function calc_cs_diff()"
		sys.exit()

	return first_residue,last_residue,l_common_residue_numbers

def find_best_matching_reference_shift(exp_assign_dict_per_residue,matching_mode, shift_type,exp_shift_value,pred_shift_value,alternative_exp_shift,alternative_exp_shift_value,alternative_predicted_shift,alternative_predicted_shift_value):
	"""
	Find best matching reference shift

	exp_assign_dict[exp_resid_number]["reference_shifts"]
	"""
	verbose=0
	if verbose>=10: print "Running function find_best_matching_reference_shift()"

	reference_shift_value_to_use=pred_shift_value
	reference_shift_to_use=shift_type



	primary_minus_primary=float(exp_shift_value)-float(pred_shift_value)
	abs_primary_minus_primary=abs(primary_minus_primary)

	true_diff=primary_minus_primary
	abs_diff=abs_primary_minus_primary
	mean_abs_diff=abs_diff
	flip=0

	primary_minus_alternative=""

	alternative_minus_alternative=""

	alternative_minus_primary=""


	abs_primary_minus_alternative=""

	abs_alternative_minus_alternative=""

	abs_alternative_minus_primary=""

	if is_number(alternative_predicted_shift_value) and  is_number(alternative_exp_shift_value) and matching_mode!=0:
		primary_minus_alternative=float(exp_shift_value)-float(alternative_predicted_shift_value)

		alternative_minus_alternative=float(alternative_exp_shift_value)-float(alternative_predicted_shift_value)

		alternative_minus_primary=float(alternative_exp_shift_value)-float(pred_shift_value)

		abs_primary_minus_alternative=abs(primary_minus_alternative)

		abs_alternative_minus_alternative=abs(alternative_minus_alternative)

		abs_alternative_minus_primary=abs(alternative_minus_primary)


		mean_abs_diff=(abs_primary_minus_primary+abs_alternative_minus_alternative)/2.0

		if abs_primary_minus_primary>abs_primary_minus_alternative and abs_primary_minus_alternative<abs_alternative_minus_alternative:

			if abs_alternative_minus_primary<abs_alternative_minus_alternative and abs_alternative_minus_primary<abs_primary_minus_primary:
				flip=1
		
		if matching_mode==1:
			if flip==1:
				reference_shift_value_to_use=alternative_predicted_shift_value
				reference_shift_to_use=alternative_predicted_shift
				true_diff=primary_minus_alternative
				abs_diff=abs_primary_minus_alternative
		elif  matching_mode==2:
			if flip==0:		
				abs_diff=(abs_primary_minus_primary+abs_alternative_minus_alternative)/2.0
				if verbose>=1: print "abs_diff= ", abs_diff, "abs_primary_minus_primary=", abs_primary_minus_primary, "abs_alternative_minus_alternative=", abs_alternative_minus_alternative
				mean_abs_diff=abs_diff
			else:
				abs_diff=(abs_primary_minus_alternative+abs_alternative_minus_primary)/2.0	
				mean_abs_diff=abs_diff	
		elif  matching_mode==3:	
			if flip==0:		
				abs_diff=max([abs_primary_minus_primary,abs_alternative_minus_alternative])
				mean_abs_diff=abs_diff
			else:
				abs_diff=max([abs_primary_minus_alternative,abs_alternative_minus_primary])	
				mean_abs_diff=abs_diff
		elif  matching_mode==4:	
			if flip==0:		
				abs_diff=min([abs_primary_minus_primary,abs_alternative_minus_alternative])
				mean_abs_diff=abs_diff
			else:
				abs_diff=min([abs_primary_minus_alternative,abs_alternative_minus_primary])	
				mean_abs_diff=abs_diff
		if flip==1:
			if verbose>=1: print "Flipping....abs_primary_minus_primary=%s, abs_primary_minus_alternative=%s, abs_alternative_minus_alternative=%s, abs_alternative_minus_primary=%s" % (abs_primary_minus_primary,abs_primary_minus_alternative,abs_alternative_minus_alternative,abs_alternative_minus_primary)
	exp_assign_dict_per_residue[shift_type]={}
	exp_assign_dict_per_residue[shift_type]["flip"]=flip
	exp_assign_dict_per_residue[shift_type]["abs_diff"]=abs_diff
	exp_assign_dict_per_residue[shift_type]["true_diff"]=true_diff
	exp_assign_dict_per_residue[shift_type]["reference_shift_value"]=reference_shift_value_to_use
	exp_assign_dict_per_residue[shift_type]["reference_shift"]=reference_shift_to_use
	exp_assign_dict_per_residue[shift_type]["shift_type"]=shift_type

	exp_assign_dict_per_residue[shift_type]["primary_minus_primary"]=primary_minus_primary
	exp_assign_dict_per_residue[shift_type]["primary_minus_alternative"]=primary_minus_alternative
	exp_assign_dict_per_residue[shift_type]["alternative_minus_alternative"]=alternative_minus_alternative
	exp_assign_dict_per_residue[shift_type]["alternative_minus_primary"]=alternative_minus_primary


	debug=0
	if debug==1:
		if verbose>=1: print "Exiting after function find_best_matching_reference_shift()"
		sys.exit()
	return mean_abs_diff,abs_diff, true_diff,reference_shift_value_to_use,reference_shift_to_use

def find_alternative_shift_type(pred_shift_assign_dict,shift_type,l_single_letter_res_name):
	"""
	Find alternative shift type to remove stereo-specificity
	"""
	verbose=0
	if verbose>=10: print "Running function find_alternative_shift_type()"

	alternative_predicted_shift=alternative_predicted_shift_value=""

	if verbose>=10: print "Original shift type is ", shift_type
	if verbose>=10: print "Residue type is  ", l_single_letter_res_name
	if len(shift_type)>1 and is_number(shift_type[-1]):
		if verbose>=10: print "Stereospecific shift %s was found for residue type %s" % (shift_type,l_single_letter_res_name)
		for i in ["1","2","3"]:
			if i!=shift_type[-1]:
				alt_shift_type="%s%s" % (shift_type[:-1],i)	
				if verbose>=10: print "Testing ", alt_shift_type
				if alt_shift_type in pred_shift_assign_dict:
					alternative_predicted_shift=alt_shift_type
					alternative_predicted_shift_value=pred_shift_assign_dict[alt_shift_type]
					if verbose>=10: print "Alternative shift type %s was found for shift %s of residue type %s" % (alt_shift_type,shift_type,l_single_letter_res_name)
	return alternative_predicted_shift,alternative_predicted_shift_value


def calc_cs_diff_stats(l_zodb,l_database_file,l_pdb_list,l_fill_dict,l_total_module_text_on,l_total_report_text_on,l_module_text_on,l_report_text_on,l_per_residue_on, l_do_pearson, l_do_spearman, l_do_kendall, l_pdb_dict,l_project_dir,l_report_dir,l_mean_cs_mode,residue_numbers_list,l_predictor,l_find_outliers):
	"""
	Calculate Chemical Shift Difference
	"""

	verbose=0
	if verbose>=0: print "Running function calc_cs_diff_stats() for predictor ", l_predictor
	
	l_total_module_text="report_dict={}" + linesep
	l_total_report_text=""


	if l_report_text_on and l_module_text_on:
		l_report_dir_full_general="%s/%s" % (l_project_dir,l_report_dir)
		make_dir_no_remove(l_report_dir_full_general)

		l_report_dir_full="%s/%s" % (l_report_dir_full_general,l_predictor)
		make_dir_no_remove(l_report_dir_full)


	total_mean_atom_outlier_counter=0
	total_median_atom_outlier_counter=0

	total_mean_residue_outlier_counter=0
	total_median_residue_outlier_counter=0

	mean_per_residue_outlier_model_list=[]
	median_outlier_model_list=[]

	mean_per_atom_outlier_model_list=[]
	median_per_atom_outlier_model_list=[]

	counter=0

	#for i in l_pdb_dict:
	if l_mean_cs_mode==1:
		l_pdb_list=["ensemble_mean_chemical_shifts"]


	average_dict={}
	average_dict["protein_shift_classes"]={}

	for i in l_pdb_list:	
		print "Processing case %s in function calc_cs_diff_stats() for predictor %s" % (i,l_predictor)

		if l_zodb and l_mean_cs_mode==0:
			storage = FileStorage(l_database_file)
			db = DB(storage)		
			connection = db.open()
			main_database = connection.root()
			l_pdb_dict=main_database["pdb_dict"]

			#average_dict=main_database["average_dictionary"]

			#print "l_pdb_dict[i] =", l_pdb_dict[i] 
			#print l_pdb_dict[l_predictor]
			#sys.exit()




		per_residue_dict=l_pdb_dict[i][l_predictor]["shift_diff_dict"]
		if verbose>=10: print "i= ", i
		if verbose>=10: print i, "len(per_residue_dict= %s for predictor %s " % (len(per_residue_dict),l_predictor)
		
		#if 76 not in per_residue_dict:
		#	print "Exiting....no 76 in per_residue_dict" 
		#	sys.exit()

		if l_total_module_text_on:
			l_total_module_text+="report_dict['%s']={'shift_class':{}}" % i + linesep
		if l_total_report_text_on:
			l_total_report_text+="Model - %s" % i+ linesep+ linesep

		if l_module_text_on:
			l_module_text="per_model_report_dict={}"+ linesep
		if l_report_text_on:
			l_report_text="" + linesep


		l_total_mean_per_residue_outliers_text=""
		l_total_median_per_residue_outliers_text=""

		l_mean_per_residue_outliers_text=""
		l_median_per_residue_outliers_text=""


		l_total_mean_per_atom_outliers_text=""
		l_total_median_per_atom_outliers_text=""

		l_mean_per_atom_outliers_text=""
		l_median_per_atom_outliers_text=""


		mean_per_residue_outlier_shift_class_list=[]
		median_per_residue_outlier_shift_class_list=[]

		mean_per_atom_outlier_shift_class_list=[]
		median_per_atom_outlier_shift_class_list=[]
		if l_module_text_on: l_module_text+="per_model_report_dict['shift_class']={}" + linesep
		for shift_class in l_pdb_dict[i][l_predictor]["protein_shift_classes"]:
			if verbose>=10: print shift_class
			if l_total_module_text_on: l_total_module_text+="report_dict['%s']['shift_class']['%s']={}" % (i,shift_class) + linesep
			if l_total_report_text_on: l_total_report_text+="Chemical shift class - %s " % shift_class + linesep+ linesep


			if l_module_text_on: l_module_text+="per_model_report_dict['shift_class']['%s']={}" % shift_class + linesep
			if l_report_text_on: l_report_text+="Chemical shift class - %s " % shift_class + linesep+ linesep



			if shift_class not in average_dict["protein_shift_classes"]:
				average_dict["protein_shift_classes"][shift_class]={}
				average_dict["protein_shift_classes"][shift_class]["totals"]={}


				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]={}
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["mean"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["median"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["stdev"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["average_abs_diff_from_mean"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["average_abs_diff_from_median"]=[]


				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["pearson"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["spearman"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["kendall"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["sec_pearson"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["sec_spearman"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["sec_kendall"]=[]

				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["per_atom_mean_outliers"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["per_atom_median_outliers"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["per_residue_mean_outliers"]=[]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["per_residue_median_outliers"]=[]

				average_dict["protein_shift_classes"][shift_class]["per_residue"]={}
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]={}
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["mean"]=[]
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["median"]=[]
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["stdev"]=[]

				average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["mean_outlier"]=[]
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["median_outlier"]=[]	

				average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]={}
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_atom_outlier_dict"]={}
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_atom_outlier_dict"]={}							
	
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"]={}
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"]={}	




			abs_diff_list=l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["abs_diff_list"]
			exp_list=l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["exp_list"]
			pred_cs_list=l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["pred_list"]
			sec_exp_list=l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["sec_exp_list"]
			sec_pred_cs_list=l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["sec_sec_list"]

			l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["mean_outliers"]={}
			l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["median_outliers"]={}




			l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"]={}

			l_mean="NONE"
			l_median="NONE"
			l_std="NONE"
			pearson="NONE"
			spearman="NONE"
			kendalltau="NONE"

			sec_pearson="NONE"
			sec_spearman="NONE"
			sec_kendalltau="NONE"

			l_average_diff_from_mean="NONE"
			l_average_diff_from_median="NONE"


			if len(abs_diff_list)>0:
				l_mean=stats.lmean(abs_diff_list)
				l_median=stats.lmedianscore(abs_diff_list)
				l_average_diff_from_mean,l_average_diff_from_median=calculate_average_abs_difference(abs_diff_list)
				if shift_class=="CA":
					if verbose>=10: print "Mean %s for shift class %s was calcuated from abs_diff_list with length %s " % (l_mean,shift_class,len(abs_diff_list))
			if len(abs_diff_list)>1:
				l_std=stats.lstdev(abs_diff_list)

			#l_do_person, l_do_spearman, l_do_kendall, 

			pearson="NONE"
			if len(exp_list)>1:
				if l_do_pearson:
					try:
						pearson=1.0 - stats.lpearsonr(pred_cs_list,exp_list)[0]
					except:					
						if verbose>=2: print "Warning!!! Calculation of Pearson correlation coefficint for shift class %s has failed." %   shift_class
						if verbose>=2: print "pred_cs_list=", pred_cs_list
						if verbose>=2: print "exp_list=", exp_list, linesep

				spearman="NONE"
				if l_do_spearman:
					try:
						spearman=1.0 - stats.lspearmanr(pred_cs_list,exp_list)[0]
					except:
						if verbose>=2: print "Warning!!! Calculation of Spearman correlation coefficint for shift class %s has failed." %   shift_class
						if verbose>=2: print "pred_cs_list=", pred_cs_list
						if verbose>=2: print "exp_list=", exp_list, linesep	

				kendalltau="NONE"
				if l_do_kendall:
					try:
						kendalltau=1.0 - stats.lkendalltau(pred_cs_list,exp_list)[0]
					except:
						if verbose>=2: print "Warning!!! Calculation of Kendall Tau correlation coefficint for shift class %s has failed." %   shift_class
						if verbose>=2: print "pred_cs_list=", pred_cs_list
						if verbose>=2: print "exp_list=", exp_list, linesep								
			else:
				if verbose>=2: print "Warning!!! Lists of original chemical shifts are empty for shift class %s" % shift_class 


			if len(sec_exp_list)>1:
				sec_pearson="NONE"
				if l_do_pearson:
					try:
						sec_pearson=1.0 - stats.lpearsonr(sec_pred_cs_list,sec_exp_list)[0]
					except:
						if verbose>=2: print "Warning!!! Calculation of sec_Pearson correlation coefficint for shift class %s has failed." %   shift_class
						if verbose>=2: print "sec_pred_cs_list=", sec_pred_cs_list
						if verbose>=2: print "sec_exp_list=", sec_exp_list, linesep

				sec_spearman="NONE"
				if l_do_spearman:
					try:
						sec_spearman=1.0 - stats.lspearmanr(sec_pred_cs_list,sec_exp_list)[0]
					except:
						if verbose>=2: print "Warning!!! Calculation of sec_Spearman correlation coefficint for shift class %s has failed." %   shift_class
						if verbose>=2: print "sec_pred_cs_list=", sec_pred_cs_list
						if verbose>=2: print "sec_exp_list=", sec_exp_list, linesep	

				sec_kendalltau="NONE"
				if l_do_kendall:
					try:
						sec_kendalltau=1.0 - stats.lkendalltau(sec_pred_cs_list,sec_exp_list)[0]
					except:
						if verbose>=2: print "Warning!!! Calculation of sec_Kendall Tau correlation coefficint for shift class %s has failed." %   shift_class
						if verbose>=2: print "sec_pred_cs_list=", sec_pred_cs_list
						if verbose>=2: print "sec_exp_list=", sec_exp_list, linesep								

			else:
				if verbose>=2: print "Warning!!! Lists of secondary  chemical shifts are empty for shift class %s" % shift_class 



			if verbose>=20: print i,shift_class, "mean=", [l_mean], "median=",[l_median],"stdev=",[l_std], "pearson=",[pearson],"spearman=",[spearman], "kendalltau=",[kendalltau],"sec_pearson=",[sec_pearson],"sec_spearman=",[sec_spearman], "sec_kendalltau=",[sec_kendalltau], linesep, linesep   

			#debug=1



			if l_fill_dict==1:
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["mean"]=l_mean
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["median"]=l_median
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["stdev"]=l_std
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["average_abs_diff_from_mean"]=l_average_diff_from_mean
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["average_abs_diff_from_median"]=l_average_diff_from_median
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["pearson"]=pearson
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["spearman"]=spearman
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["kendall"]=kendalltau
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["sec_pearson"]=sec_pearson
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["sec_spearman"]=sec_spearman
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["sec_kendall"]=sec_kendalltau


			debug=0
			if debug==1:
		
				if l_zodb and l_mean_cs_mode==0:
					print """Debugging after filling l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]"""
					if shift_class=="HD22":
						print "Mean for HD22 is ", l_mean
						print """1) l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["mean"] =""", l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["mean"]
						l_pdb_dict._p_changed = True		
						transaction.savepoint(True)
						transaction.commit()
						#transaction.get().commit() 
						connection.close()
						db.close()
						storage.close()
						l_pdb_dict={}

						storage = FileStorage(l_database_file)
						db = DB(storage)		
						connection = db.open()
						main_database = connection.root()
						l_pdb_dict=main_database["pdb_dict"]

	

						#print """2) l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"] =""", l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]
						#print """2) l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]["mean"] =""", l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]["mean"]
						print "Exiting ...."
						sys.exit()


				print """l_pdb_dict[i][l_predictor]["protein_shift_classes"]=""", l_pdb_dict[i][l_predictor]["protein_shift_classes"]
				print "Exiting ...."
				sys.exit()


			if l_total_module_text_on:
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['mean']='%s'" % (i,shift_class,l_mean) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['median']='%s'" % (i,shift_class,l_median) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['stdev']='%s'" % (i,shift_class,l_std) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['average_abs_diff_from_mean']='%s'" % (i,shift_class,l_average_diff_from_mean) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['average_abs_diff_from_median']='%s'" % (i,shift_class,l_average_diff_from_median) + linesep

				l_total_module_text+="report_dict['%s']['shift_class']['%s']['pearson']='%s'" % (i,shift_class,pearson) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['spearman']='%s'" % (i,shift_class,spearman) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['kendall']='%s'" % (i,shift_class,kendalltau) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['sec_pearson']='%s'" % (i,shift_class,sec_pearson) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['sec_spearman']='%s'" % (i,shift_class,sec_spearman) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['sec_kendall']='%s'" % (i,shift_class,sec_kendalltau) + linesep

			if l_total_module_text_on:
				l_total_report_text+="%s, %s, Mean absolute difference,  %s " % (i, shift_class,l_mean) + linesep
				l_total_report_text+="%s, %s, Median absolute difference, %s " % (i, shift_class,l_median)  + linesep
				l_total_report_text+="%s, %s, Standard deviation, %s " % (i, shift_class,l_std )+ linesep 
				l_total_report_text+="%s, %s, Pearson correlation coefficient for regular chemical shifts  %s " % (i, shift_class,pearson)  + linesep
				l_total_report_text+="%s, %s, Spearman correlation coefficient for regular chemical shifts, %s " % (i, shift_class,spearman)  + linesep
				l_total_report_text+="%s, %s, Pearson correlation coefficient for secondary chemical shifts, %s " % (i, shift_class,sec_pearson)  + linesep
				l_total_report_text+="%s, %s, Spearman correlation coefficient for secondary chemical shifts, %s " % (i, shift_class,sec_spearman)  + linesep

			#l_module_text+="per_model_report_dict['shift_class']['%s']={}" % shift_class + linesep


			if l_module_text_on:
				l_module_text+="per_model_report_dict['shift_class']['%s']['mean']='%s'" % (shift_class,l_mean) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['median']='%s'" % (shift_class,l_median) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['stdev']='%s'" % (shift_class,l_std) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['average_abs_diff_from_mean']='%s'" % (shift_class,l_average_diff_from_mean) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['average_abs_diff_from_median']='%s'" % (shift_class,l_average_diff_from_median) + linesep

				l_module_text+="per_model_report_dict['shift_class']['%s']['pearson']='%s'" % (shift_class,pearson) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['spearman']='%s'" % (shift_class,spearman) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['kendall']='%s'" % (shift_class,kendalltau) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['sec_pearson']='%s'" % (shift_class,sec_pearson) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['sec_spearman']='%s'" % (shift_class,sec_spearman) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['sec_kendall']='%s'" % (shift_class,sec_kendalltau) + linesep

			if l_report_text_on:
				l_report_text+="%s, %s, Mean absolute difference, %s " % (i, shift_class,l_mean) + linesep
				l_report_text+="%s, %s, Median absolute difference, %s " % (i, shift_class,l_median) + linesep
				l_report_text+="%s, %s, Standard deviation, %s " % (i, shift_class,l_std) + linesep
				l_report_text+="%s, %s, Pearson correlation coefficient for regular chemical shifts, %s " % (i, shift_class,pearson) + linesep
				l_report_text+="%s, %s, Spearman correlation coefficient for regular chemical shifts, %s " % (i, shift_class,spearman) + linesep
				l_report_text+="%s, %s, Pearson correlation coefficient for secondary chemical shifts, %s " % (i, shift_class,sec_pearson) + linesep
				l_report_text+="%s, %s, Spearman correlation coefficient for secondary chemical shifts, %s " % (i, shift_class,sec_spearman) + linesep


			if l_fill_dict==1:
				if is_number(l_mean): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["mean"]+=[l_mean]
				if is_number(l_median): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["median"]+=[l_median]
				if is_number(l_std): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["stdev"]+=[l_std]

				if is_number(l_average_diff_from_mean): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["average_abs_diff_from_mean"]+=[l_average_diff_from_mean]
				if is_number(l_average_diff_from_median): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["average_abs_diff_from_median"]+=[l_average_diff_from_median]

				if is_number(pearson): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["pearson"]+=[pearson]
				if is_number(spearman): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["spearman"]+=[spearman]
				if is_number(kendalltau): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["kendall"]+=[kendalltau]
				if is_number(sec_pearson): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["sec_pearson"]+=[sec_pearson]
				if is_number(sec_spearman): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["sec_spearman"]+=[sec_spearman]
				if is_number(sec_kendalltau): average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["sec_kendall"]+=[sec_kendalltau]
			
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]={}
				#l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["nmrqplot_mean_text"]=""
				#l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["nmrqplot_median_text"]=""
				
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_mean_text_all"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_median_text_all"]=""			

				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_mean_text_good"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_median_text_good"]=""	

				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_mean_per_atom_outliers"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_median_per_atom_outliers"]=""
				
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_mean_text_unknown"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_median_text_unknown"]=""			
				
			
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_mean_text_all"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_median_text_all"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_std_text_all"]=""									

				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_mean_text_good"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_median_text_good"]=""	

				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_mean_text_outliers"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_median_text_outliers"]=""				
				
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_mean_text_unknown"]=""
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_median_text_unknown"]=""									

			number_of_mean_atom_outliers=0
			number_of_median_atom_outliers=0		

			local_mean_outliers_dict={}
			local_median_outliers_dict={}

			local_mean_outliers_list4text=[]
			local_median_outliers_list4text=[]

			if l_total_module_text_on:
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers']={}" % (i,shift_class) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers']={}" % (i,shift_class) + linesep

			if l_module_text_on:
				l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers']={}" % (shift_class) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers']={}" % (shift_class) + linesep

									
			
			for entry in l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["details"]:
				if verbose>=3: print entry
				res_name=entry["res_name"]
				residue_number=entry["residue_number"]
				abs_diff=entry["abs_diff"]
				
				exp_shift_value=entry["exp_shift_value"]
				shiftx_value=entry["pred_value"]
				shift_type=entry["shift_type"]		

		

				per_atom_mean_outlier="NONE"
				per_atom_median_outlier="NONE"
				

				if l_fill_dict==1:
					l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_mean_text_all"]+="%s %s " % (exp_shift_value,shiftx_value) + linesep 
					l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_median_text_all"]+="%s %s " % (exp_shift_value,shiftx_value) + linesep 
								
				if l_mean!="NONE" and l_std!="NONE" and l_find_outliers==1:
					per_atom_mean_outlier=0
					e_diff_of_diff_mean=abs_diff-l_mean								
					if e_diff_of_diff_mean>0 and e_diff_of_diff_mean>(l_std*outlier_threashold):
						per_atom_mean_outlier=1
						number_of_mean_atom_outliers+=1	
						total_mean_atom_outlier_counter+=1
						mean_diff_of_diff_dict={"diff_of_diff_mean":e_diff_of_diff_mean,"global_mean":l_mean,"std":l_std,"outlier_threashold":outlier_threashold}
						l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_mean_per_atom_outliers"]+="%s %s " % (exp_shift_value,shiftx_value) + linesep 						

						l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["mean_outliers"][number_of_mean_atom_outliers]=[entry,mean_diff_of_diff_dict]
						average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_atom_outlier_dict"][total_mean_atom_outlier_counter]=[entry,mean_diff_of_diff_dict,i]
						local_mean_outliers_dict[number_of_mean_atom_outliers]=[entry,mean_diff_of_diff_dict]

						local_mean_outliers_list4text+=[[residue_number,entry,mean_diff_of_diff_dict]]

						if l_total_module_text_on:
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]={}" % (i,shift_class,number_of_mean_atom_outliers) + linesep #
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['resid_name']='%s'" % (i,shift_class,number_of_mean_atom_outliers,res_name) + linesep#
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['residue_number']='%s'" % (i,shift_class,number_of_mean_atom_outliers,residue_number) + linesep#
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['abs_diff']='%s'" % (i,shift_class,number_of_mean_atom_outliers,abs_diff) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['exp_shift_value']='%s'" % (i,shift_class,number_of_mean_atom_outliers,exp_shift_value) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['shiftx_value']='%s'" % (i,shift_class,number_of_mean_atom_outliers,shiftx_value) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['shift_type']='%s'" % (i,shift_class,number_of_mean_atom_outliers,shift_type) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['global_mean']='%s'" % (i,shift_class,number_of_mean_atom_outliers,l_mean) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['diff_of_diff']='%s'" % (i,shift_class,number_of_mean_atom_outliers,e_diff_of_diff_mean) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['stdev']='%s'" % (i,shift_class,number_of_mean_atom_outliers,l_std) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_mean_outliers'][%s]['outlier_threashold']='%s'" % (i,shift_class,number_of_mean_atom_outliers,outlier_threashold) + linesep

						if l_module_text_on:
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]={}" % (shift_class,number_of_mean_atom_outliers) + linesep #
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['resid_name']='%s'" % (shift_class,number_of_mean_atom_outliers,res_name) + linesep#
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['residue_number']='%s'" % (shift_class,number_of_mean_atom_outliers,residue_number) + linesep#
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['abs_diff']='%s'" % (shift_class,number_of_mean_atom_outliers,abs_diff) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['exp_shift_value']='%s'" % (shift_class,number_of_mean_atom_outliers,exp_shift_value) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['shiftx_value']='%s'" % (shift_class,number_of_mean_atom_outliers,shiftx_value) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['shift_type']='%s'" % (shift_class,number_of_mean_atom_outliers,shift_type) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['global_mean']='%s'" % (shift_class,number_of_mean_atom_outliers,l_mean) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['diff_of_diff']='%s'" % (shift_class,number_of_mean_atom_outliers,e_diff_of_diff_mean) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['stdev']='%s'" % (shift_class,number_of_mean_atom_outliers,l_std) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_mean_outliers'][%s]['outlier_threashold']='%s'" % (shift_class,number_of_mean_atom_outliers,outlier_threashold) + linesep



						if i not in mean_per_atom_outlier_model_list:
							mean_per_atom_outlier_model_list+=[i]
							l_total_mean_per_atom_outliers_text+="Model - %s " % i + linesep + linesep	

						if shift_class in mean_per_atom_outlier_shift_class_list:
							mean_per_atom_outlier_shift_class_list+=[shift_class]
							l_total_mean_per_atom_outliers_text+="Shift Class - %s " % i + linesep + linesep	
							l_mean_per_atom_outliers_text+="Shift Class - %s " % i + linesep + linesep	



					elif e_diff_of_diff_mean<=(l_std*outlier_threashold):	
						per_atom_mean_outlier=0
						l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_mean_text_good"]+="%s %s " % (exp_shift_value,shiftx_value) + linesep 											
		


				
										
				if l_median!="NONE" and l_average_diff_from_median!="NONE" and l_find_outliers==1:
					per_atom_median_outlier=0
					e_diff_of_diff_median=abs_diff-l_median								
					if e_diff_of_diff_median>0 and e_diff_of_diff_median>(l_average_diff_from_median*outlier_threashold):
						per_atom_median_outlier=1
						number_of_median_atom_outliers+=1
						total_median_atom_outlier_counter+=1


						median_diff_of_diff_dict={"diff_of_diff_median":e_diff_of_diff_median,"global_median":l_median,"average_diff_from_median":l_average_diff_from_median,"outlier_threashold":outlier_threashold}

						l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_median_per_atom_outliers"]+="%s %s " % (exp_shift_value,shiftx_value) + linesep 												
						l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["median_outliers"][number_of_median_atom_outliers]=[entry,median_diff_of_diff_dict]
						average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_atom_outlier_dict"][total_median_atom_outlier_counter]=[entry,median_diff_of_diff_dict,i]
						local_median_outliers_dict[number_of_median_atom_outliers]=[entry,median_diff_of_diff_dict]

						local_median_outliers_list4text+=[[residue_number,entry,median_diff_of_diff_dict]]


						if l_total_module_text_on:
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]={}" % (i,shift_class,number_of_median_atom_outliers) + linesep #
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['resid_name']='%s'" % (i,shift_class,number_of_median_atom_outliers,res_name) + linesep#
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['residue_number']='%s'" % (i,shift_class,number_of_median_atom_outliers,residue_number) + linesep#
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['abs_diff']='%s'" % (i,shift_class,number_of_median_atom_outliers,abs_diff) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['exp_shift_value']='%s'" % (i,shift_class,number_of_median_atom_outliers,exp_shift_value) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['shiftx_value']='%s'" % (i,shift_class,number_of_median_atom_outliers,shiftx_value) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['shift_type']='%s'" % (i,shift_class,number_of_median_atom_outliers,shift_type) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['global_median']='%s'" % (i,shift_class,number_of_median_atom_outliers,l_median) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['diff_of_diff']='%s'" % (i,shift_class,number_of_median_atom_outliers,e_diff_of_diff_median) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['average_diff_from_median']='%s'" % (i,shift_class,number_of_median_atom_outliers,l_average_diff_from_median) + linesep
							l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_atom_median_outliers'][%s]['outlier_threashold']='%s'" % (i,shift_class,number_of_median_atom_outliers,outlier_threashold) + linesep


						if l_module_text_on:
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]={}" % (shift_class,number_of_median_atom_outliers) + linesep #
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['resid_name']='%s'" % (shift_class,number_of_median_atom_outliers,res_name) + linesep#
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['residue_number']='%s'" % (shift_class,number_of_median_atom_outliers,residue_number) + linesep#
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['abs_diff']='%s'" % (shift_class,number_of_median_atom_outliers,abs_diff) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['exp_shift_value']='%s'" % (shift_class,number_of_median_atom_outliers,exp_shift_value) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['shiftx_value']='%s'" % (shift_class,number_of_median_atom_outliers,shiftx_value) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['shift_type']='%s'" % (shift_class,number_of_median_atom_outliers,shift_type) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['global_median']='%s'" % (shift_class,number_of_median_atom_outliers,l_median) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['diff_of_diff']='%s'" % (shift_class,number_of_median_atom_outliers,e_diff_of_diff_median) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['average_diff_from_median']='%s'" % (shift_class,number_of_median_atom_outliers,l_average_diff_from_median) + linesep
							l_module_text+="per_model_report_dict['shift_class']['%s']['per_atom_median_outliers'][%s]['outlier_threashold']='%s'" % (shift_class,number_of_median_atom_outliers,outlier_threashold) + linesep


					
						if i not in median_per_atom_outlier_model_list:
							median_per_atom_outlier_model_list+=[i]
							l_total_median_per_atom_outliers_text+="Model - %s " % i + linesep + linesep	

						if shift_class in median_per_atom_outlier_shift_class_list:
							median_per_atom_outlier_shift_class_list+=[shift_class]
							l_total_median_per_atom_outliers_text+="Shift Class - %s " % i + linesep + linesep	
							l_median_per_atom_outliers_text+="Shift Class - %s " % i + linesep + linesep	




					elif  l_fill_dict==1 and (e_diff_of_diff_median<=(l_average_diff_from_median*outlier_threashold)):
						per_atom_median_outlier=0							
						l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_ShiftX_vs_Exp_median_text_good"]+="%s %s " % (exp_shift_value,shiftx_value) + linesep 																		
						
				if l_find_outliers==1:
					entry["mean_outlier"]=per_atom_mean_outlier		
					entry["median_outlier"]=per_atom_median_outlier


			if l_find_outliers==1:
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["per_atom_mean_outliers"]+=[number_of_mean_atom_outliers]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["per_atom_median_outliers"]+=[number_of_median_atom_outliers]

				local_mean_outliers_list4text.sort()
				for q in local_mean_outliers_list4text:
					entry=q[1]
					per_atom_mean_diff_of_diff_dict=q[2]
					resid_name=entry["res_name"]
					residue_number=entry["residue_number"]
					abs_diff=entry["abs_diff"]				
					exp_shift_value=entry["exp_shift_value"]
					shiftx_value=entry["pred_value"]
					shift_type=entry["shift_type"]		
					per_atom_diff_of_diff_mean=mean_diff_of_diff_dict["diff_of_diff_mean"]
					diff_of_diff_mean=mean_diff_of_diff_dict["diff_of_diff_mean"]
					global_mean=mean_diff_of_diff_dict["global_mean"]
					global_stdev=mean_diff_of_diff_dict["std"]

					l_total_mean_per_atom_outliers_text+="%s%s; Atom = %s; Exp. shift = %s; Shiftx shift = %s;   Abs. diff. = %s; Per-model abs. diff. = %s; Per-model st.dev. = %s" % (resid_name,residue_number,shift_type,exp_shift_value,shiftx_value,abs_diff,global_mean,global_stdev) + linesep
					l_mean_per_atom_outliers_text+="%s%s; Atom = %s; Exp. shift = %s; Shiftx shift = %s;   Abs. diff. = %s; Per-model abs. diff. = %s; Per-model st.dev. = %s" % (resid_name,residue_number,shift_type,exp_shift_value,shiftx_value,abs_diff,global_mean,global_stdev) + linesep	

				
				local_median_outliers_list4text.sort()
				for q in local_median_outliers_list4text:
					entry=q[1]
					per_atom_median_diff_of_diff_dict=q[2]
					resid_name=entry["res_name"]
					residue_number=entry["residue_number"]
					abs_diff=entry["abs_diff"]				
					exp_shift_value=entry["exp_shift_value"]
					shiftx_value=entry["pred_value"]
					shift_type=entry["shift_type"]		
					per_atom_diff_of_diff_median=median_diff_of_diff_dict["diff_of_diff_median"]
					diff_of_diff_median=median_diff_of_diff_dict["diff_of_diff_median"]
					global_median=median_diff_of_diff_dict["global_median"]
					global_average_diff_from_median=median_diff_of_diff_dict["average_diff_from_median"]
	
					l_total_median_per_atom_outliers_text+="%s%s; Atom = %s; Exp. shift = %s; Shiftx shift = %s;   Abs. diff. = %s; Per-model abs. diff. = %s; Average absolute difference from the median = %s" % (resid_name,residue_number,shift_type,exp_shift_value,shiftx_value,abs_diff,global_median,global_average_diff_from_median) + linesep
					l_median_per_atom_outliers_text+="%s%s; Atom = %s; Exp. shift = %s; Shiftx shift = %s;   Abs. diff. = %s; Per-model abs. diff. = %s; Average absolute difference from the median = %s" % (resid_name,residue_number,shift_type,exp_shift_value,shiftx_value,abs_diff,global_median,global_average_diff_from_median) + linesep	

				
						
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["number_of_mean_atom_outliers"]=number_of_mean_atom_outliers
				l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["number_of_median_atom_outliers"]=number_of_median_atom_outliers
				
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["mean_outlier"]+=[[number_of_mean_atom_outliers,local_mean_outliers_dict,i]]
				average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["median_outlier"]+=[[number_of_median_atom_outliers,local_median_outliers_dict,i]]	

			if l_total_module_text_on:
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_outliers']={}" % (i,shift_class) + linesep
				l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_outliers']={}" % (i,shift_class) + linesep

				if l_per_residue_on:	
					l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results']={}" % (i,shift_class) + linesep
					l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results']={}" % (i,shift_class) + linesep

			if l_module_text_on:
				l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_outliers']={}" % (shift_class) + linesep
				l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_outliers']={}" % (shift_class) + linesep

				if l_per_residue_on:	
					l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results']={}" % (shift_class) + linesep
					l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results']={}" % (shift_class) + linesep


			
			per_residue_mean_outlier_counter=0
			per_residue_median_outlier_counter=0
			for residue_number in residue_numbers_list:
				if verbose>=10: print "Residue number = ", residue_number 
				proceed=1
				if residue_number not in per_residue_dict:
					print "Warning!!! Residue number %s is not in per_residue_dict. It will be skipped. " % residue_number
					proceed=0
				else:
					residue_shift_classes=per_residue_dict[residue_number]["residue_shift_classes"]
					resid_name=per_residue_dict[residue_number]["res_name"]	
					#if "CA" in residue_shift_classes:
					#	if verbose>=0: print "Residue number = ", residue_number , "residue_shift_classes= ", residue_shift_classes["CA"]['details']	

					#per_residue_abs_diff=per_residue_dict[residue_number][shift_class]["abs_diff"]
					#per_residue_exp_shift_value=per_residue_dict[residue_number][shift_class]["exp_shift_value"]
					#per_residue_shiftx_value=per_residue_dict[residue_number][shift_class]["pred_value"]


					l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]={}

				if proceed==1 and shift_class in residue_shift_classes:
					l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["data"]=residue_shift_classes[shift_class]
						
					if "abs_diff_list" in residue_shift_classes[shift_class]:
					
						per_residue_abs_diff_list=residue_shift_classes[shift_class]["abs_diff_list"]

						l_per_residue_mean="NONE"
						l_per_residue_median="NONE"
						l_per_residue_std="NONE"
						if len(per_residue_abs_diff_list)>0:
						
							l_per_residue_mean=stats.lmean(per_residue_abs_diff_list)
							l_per_residue_median=stats.lmedianscore(per_residue_abs_diff_list)

							if l_fill_dict==1:
							
								l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_mean_text_all"]+="%s %s" % (residue_number,l_per_residue_mean) + linesep
								l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_median_text_all"]+="%s %s" % (residue_number,l_per_residue_median) + linesep

								l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["mean"]=l_per_residue_mean
								l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["median"]=l_per_residue_median
	
							average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["mean"]+=[l_per_residue_mean]
							average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["median"]+=[l_per_residue_median]

							l_mean_outlier="NONE"
							l_median_outlier="NONE"
							if l_mean!="NONE" and l_std!="NONE":
								l_mean_outlier=0
								diff_of_diff_mean=l_per_residue_mean-l_mean								
								if  l_find_outliers==1 and diff_of_diff_mean>0 and diff_of_diff_mean>(l_std*outlier_threashold):
									l_mean_outlier=1
									per_residue_mean_outlier_counter+=1
									total_mean_residue_outlier_counter+=1

									l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_mean_text_outliers"]+="%s %s" % (residue_number,l_per_residue_mean) + linesep									


									if i not in mean_per_residue_outlier_model_list:
										mean_per_residue_outlier_model_list+=[i]
										l_total_mean_per_residue_outliers_text+="Model - %s " % i + linesep + linesep	

									if shift_class in mean_per_residue_outlier_shift_class_list:
										mean_per_residue_outlier_shift_class_list+=[shift_class]
										l_total_mean_per_residue_outliers_text+="Shift Class - %s " % i + linesep + linesep	
										l_mean_per_residue_outliers_text+="Shift Class - %s " % i + linesep + linesep	

									l_total_mean_per_residue_outliers_text+="%s%s;  Per-residue abs. diff. = %s; Per-model abs. diff. = %s; Per-model st.dev. = %s" % (resid_name,residue_number,l_per_residue_mean,l_mean,l_std) + linesep
									l_mean_per_residue_outliers_text+="%s%s; Per-residue abs. diff. = %s; Per-model abs. diff. = %s; Per-model st.dev. = %s" % (resid_name,residue_number,l_per_residue_mean,l_mean,l_std) + linesep	

								
									if l_total_module_text_on:
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_outliers'][%s]={}" % (i,shift_class,per_residue_mean_outlier_counter) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_outliers'][%s]['resid_name']='%s'" % (i,shift_class,per_residue_mean_outlier_counter,resid_name) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_outliers'][%s]['residue_number']='%s'" % (i,shift_class,per_residue_mean_outlier_counter,residue_number) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_outliers'][%s]['per_residue_mean']='%s'" % (i,shift_class,per_residue_mean_outlier_counter,l_per_residue_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_outliers'][%s]['diff_of_diff_mean']='%s'" % (i,shift_class,per_residue_mean_outlier_counter,diff_of_diff_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_outliers'][%s]['global_mean']='%s'" % (i,shift_class,per_residue_mean_outlier_counter,l_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_outliers'][%s]['stdev']='%s'" % (i,shift_class,per_residue_mean_outlier_counter,l_std) + linesep

									if l_module_text_on:
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_outliers'][%s]={}" % (shift_class,per_residue_mean_outlier_counter) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_outliers'][%s]['resid_name']='%s'" % (shift_class,per_residue_mean_outlier_counter,resid_name) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_outliers'][%s]['residue_number']='%s'" % (shift_class,per_residue_mean_outlier_counter,residue_number) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_outliers'][%s]['per_residue_mean']='%s'" % (shift_class,per_residue_mean_outlier_counter,l_per_residue_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_outliers'][%s]['diff_of_diff_mean']='%s'" % (shift_class,per_residue_mean_outlier_counter,diff_of_diff_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_outliers'][%s]['global_mean']='%s'" % (shift_class,per_residue_mean_outlier_counter,l_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_outliers'][%s]['stdev']='%s'" % (shift_class,per_residue_mean_outlier_counter,l_std) + linesep


									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"][total_mean_residue_outlier_counter]={}
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"][total_mean_residue_outlier_counter]["model"]=i
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"][total_mean_residue_outlier_counter]['resid_name']=resid_name
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"][total_mean_residue_outlier_counter]['residue_number']=residue_number
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"][total_mean_residue_outlier_counter]['per_residue_mean']=l_per_residue_mean
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"][total_mean_residue_outlier_counter]['diff_of_diff_mean']=diff_of_diff_mean
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"][total_mean_residue_outlier_counter]['global_mean']=global_mean
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["mean_per_residue_outlier_dict"][total_mean_residue_outlier_counter]['stdev']=l_std

								elif diff_of_diff_mean<=(l_std*outlier_threashold) and  l_fill_dict==1:
									l_mean_outlier=0
									l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_mean_text_good"]+="%s %s" % (residue_number,l_per_residue_mean) + linesep																												
									
								if l_per_residue_on:
									if l_module_text_on:
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]={}" % (shift_class,residue_number) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['resid_name']='%s'" % (shift_class,residue_number,resid_name) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['residue_number']='%s'" % (shift_class,residue_number,residue_number) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['per_residue_mean']='%s'" % (shift_class,residue_number,l_per_residue_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['diff_of_diff_mean']='%s'" % (shift_class,residue_number,diff_of_diff_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['global_mean']='%s'" % (shift_class,residue_number,l_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['stdev']='%s'" % (shift_class,residue_number,l_std) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['outlier']='%s'" % (shift_class,residue_number,l_mean_outlier) + linesep

									if l_total_module_text_on:
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]={}" % (i,shift_class,residue_number) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['resid_name']='%s'" % (i,shift_class,residue_number,resid_name) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['residue_number']='%s'" % (i,shift_class,residue_number,residue_number) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['per_residue_mean']='%s'" % (i,shift_class,residue_number,l_per_residue_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['diff_of_diff_mean']='%s'" % (i,shift_class,residue_number,diff_of_diff_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['global_mean']='%s'" % (i,shift_class,residue_number,l_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['stdev']='%s'" % (i,shift_class,residue_number,l_std) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['outlier']='%s'" % (i,shift_class,residue_number,l_mean_outlier) + linesep




							if l_median!="NONE" and l_average_diff_from_median!="NONE" and l_find_outliers==1:
								l_median_outlier=0
								diff_of_diff_median=l_per_residue_median-l_median							
								if diff_of_diff_median>0 and diff_of_diff_median>(l_average_diff_from_median*outlier_threashold):

									l_median_outlier=1
									per_residue_median_outlier_counter+=1
									total_median_residue_outlier_counter+=1

									if l_fill_dict==1:
										l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_median_text_outliers"]+="%s %s" % (residue_number,l_per_residue_mean) + linesep																		

									if i not in median_outlier_model_list:
										median_outlier_model_list+=[i]
										l_total_median_per_residue_outliers_text+="Model - %s " % i + linesep + linesep	

									if shift_class in median_per_residue_outlier_shift_class_list:
										median_per_residue_outlier_shift_class_list+=[shift_class]
										l_total_median_per_residue_outliers_text+="Shift Class - %s " % i + linesep + linesep	
										l_median_per_residue_outliers_text+="Shift Class - %s " % i + linesep + linesep	

									l_total_median_per_residue_outliers_text+="%s%s;  Per-residue abs. diff. = %s; Per-model abs. diff. = %s; Average absolute difference from median = %s" % (resid_name,residue_number,l_per_residue_median,l_median,l_average_diff_from_median) + linesep
									l_median_per_residue_outliers_text+="%s%s; Per-residue abs. diff. = %s; Per-model abs. diff. = %s; Average absolute difference from median = %s" % (resid_name,residue_number,l_per_residue_median,l_median,l_average_diff_from_median) + linesep	

									if l_total_module_text_on:
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_outliers'][%s]={}" % (i,shift_class,per_residue_median_outlier_counter) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_outliers'][%s]['resid_name']='%s'" % (i,shift_class,per_residue_median_outlier_counter,resid_name) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_outliers'][%s]['residue_number']='%s'" % (i,shift_class,per_residue_median_outlier_counter,residue_number) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_outliers'][%s]['per_residue_median']='%s'" % (i,shift_class,per_residue_median_outlier_counter,l_per_residue_median) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_outliers'][%s]['diff_of_diff_median']='%s'" % (i,shift_class,per_residue_median_outlier_counter,diff_of_diff_median) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_outliers'][%s]['global_median']='%s'" % (i,shift_class,per_residue_median_outlier_counter,l_median) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_outliers'][%s]['average_diff_from_median']='%s'" % (i,shift_class,per_residue_median_outlier_counter,l_average_diff_from_median) + linesep

									if l_module_text_on:
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_outliers'][%s]={}" % (shift_class,per_residue_median_outlier_counter) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_outliers'][%s]['resid_name']='%s'" % (shift_class,per_residue_median_outlier_counter,resid_name) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_outliers'][%s]['residue_number']='%s'" % (shift_class,per_residue_median_outlier_counter,residue_number) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_outliers'][%s]['per_residue_median']='%s'" % (shift_class,per_residue_median_outlier_counter,l_per_residue_median) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_outliers'][%s]['diff_of_diff_median']='%s'" % (shift_class,per_residue_median_outlier_counter,diff_of_diff_median) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_outliers'][%s]['global_median']='%s'" % (shift_class,per_residue_median_outlier_counter,l_median) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_outliers'][%s]['average_diff_from_median']='%s'" % (shift_class,per_residue_median_outlier_counter,l_average_diff_from_median) + linesep


									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"][total_median_residue_outlier_counter]={}
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"][total_median_residue_outlier_counter]["model"]=i
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"][total_median_residue_outlier_counter]['resid_name']=resid_name
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"][total_median_residue_outlier_counter]['residue_number']=residue_number
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"][total_median_residue_outlier_counter]['per_residue_median']=l_per_residue_median
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"][total_median_residue_outlier_counter]['diff_of_diff_median']=diff_of_diff_median
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"][total_median_residue_outlier_counter]['global_median']=global_median
									average_dict["protein_shift_classes"][shift_class]["per_residue"]["dicts"]["median_per_residue_outlier_dict"][total_median_residue_outlier_counter]['average_diff_from_median']=l_average_diff_from_median


								elif  diff_of_diff_median<=(l_average_diff_from_median*outlier_threashold) and l_fill_dict==1:
									l_median_outlier=0
									l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_median_text_good"]+="%s %s" % (residue_number,l_per_residue_median) + linesep																												
																		
								if l_per_residue_on:
									if l_module_text_on:
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results'][%s]={}" % (shift_class,residue_number) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results'][%s]['resid_name']='%s'" % (shift_class,residue_number,resid_name) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results'][%s]['residue_number']='%s'" % (shift_class,residue_number,residue_number) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results'][%s]['per_residue_median']='%s'" % (shift_class,residue_number,l_per_residue_median) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results'][%s]['diff_of_diff_median']='%s'" % (shift_class,residue_number,diff_of_diff_median) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results'][%s]['global_median']='%s'" % (shift_class,residue_number,l_median) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results'][%s]['average_diff_from_median']='%s'" % (shift_class,residue_number,l_average_diff_from_median) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_median_results'][%s]['outlier']='%s'" % (shift_class,residue_number,l_median_outlier) + linesep
									if l_total_module_text_on:
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results'][%s]={}" % (i,shift_class,residue_number) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results'][%s]['resid_name']='%s'" % (i,shift_class,residue_number,resid_name) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results'][%s]['residue_number']='%s'" % (i,shift_class,residue_number,residue_number) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results'][%s]['per_residue_median']='%s'" % (i,shift_class,residue_number,l_per_residue_median) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results'][%s]['diff_of_diff_median']='%s'" % (i,shift_class,residue_number,diff_of_diff_median) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results'][%s]['global_median']='%s'" % (i,shift_class,residue_number,l_median) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results'][%s]['average_diff_from_median']='%s'" % (i,shift_class,residue_number,l_average_diff_from_median) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_median_results'][%s]['outlier']='%s'" % (i,shift_class,residue_number,l_median_outlier) + linesep



							
							l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["mean_outlier"]=l_mean_outlier
							l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["median_outlier"]=l_median_outlier									
									
						if len(per_residue_abs_diff_list)>1 and l_fill_dict==1:
							l_per_residue_std=stats.lstdev(per_residue_abs_diff_list)
							l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]["gnuplot_Per_Residue_std_text_all"]+="%s %s" % (residue_number,l_per_residue_std) + linesep
							l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["stdev"]=l_per_residue_std	

							average_dict["protein_shift_classes"][shift_class]["per_residue"]["lists"]["stdev"]+=[l_per_residue_std]
					else:
						print "Warning!!! abs_diff_list is not in residue_shift_classes[shift_class]"
				else:
					print "Warning!!! shift_class %s is not in residue_shift_classes" % shift_class

			if l_find_outliers==1:
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["per_residue_mean_outliers"]+=[per_residue_mean_outlier_counter]
				average_dict["protein_shift_classes"][shift_class]["totals"]["lists"]["per_residue_median_outliers"]+=[per_residue_median_outlier_counter]




		

		if l_find_outliers==1:
			if l_report_text_on:
				l_report_text+="%s" % l_mean_per_residue_outliers_text + l_mean_per_atom_outliers_text
			if l_total_report_text_on:
				l_total_report_text+="%s" % l_total_mean_per_residue_outliers_text + l_total_mean_per_atom_outliers_text


		#if l_mean_cs_mode==0:
		#print i
		#print l_pdb_dict[i]
		if "original_pdb_base_name" in l_pdb_dict[i]:
			model_name="%s_%s" % (l_pdb_dict[i]["original_pdb_base_name"], i)
			if verbose>=2: print "1) original_pdb_base_name = ", [model_name]
		else:
			model_name="MEAN"

		if l_report_text_on:
			l_per_model_report_file="%s/%s_report.txt" % (l_report_dir_full,model_name)
			write_file(l_report_text,l_per_model_report_file)

		if l_module_text_on:
			
			l_per_model_module_file="%s/%s_report.py" % (l_report_dir_full,model_name)
			write_file(l_module_text,l_per_model_module_file)
			print "Module for predictor %s is located in %s " % (l_predictor,l_per_model_module_file)


		debug=0
		if debug==1:
			print """Debugging..."""
			#print """1) l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]["mean"] =""", l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]["mean"]
			print "Exiting ...."
			sys.exit()


		if l_zodb and l_mean_cs_mode==0:
			l_pdb_dict._p_changed = True		
			transaction.savepoint(True)
			transaction.commit()
			#transaction.get().commit() 
			connection.close()
			db.close()
			storage.close()

			l_pdb_dict={}

			#sys.exit()



			debug=0
			if debug==1:
				print """Debugging..."""


				if l_zodb and l_mean_cs_mode==0:
					storage = FileStorage(l_database_file)
					db = DB(storage)		
					connection = db.open()
					main_database = connection.root()
					l_pdb_dict=main_database["pdb_dict"]

	

					#print """2) l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"] =""", l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]
					#print """2) l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]["mean"] =""", l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]["mean"]
					print "Exiting ...."
					sys.exit()


		else:
			if  l_fill_dict==0:
				l_pdb_dict[i][l_predictor]={}




	#if l_mean_cs_mode==0:
	if l_total_report_text_on:
		l_all_models_report_file="%s/all_models_report.txt" % (l_report_dir_full)
		write_file(l_total_report_text,l_all_models_report_file)

	if l_total_module_text_on:
		l_all_models_module_file="%s/all_models_report.py" % (l_report_dir_full)
		write_file(l_total_module_text,l_all_models_module_file)
		print "All-model module for predictor %s is located in %s " % (l_predictor,l_all_models_module_file)
	#print "original_pdb_base_name1= ", pdb_dict[1]["original_pdb_base_name"]

	debug=0
	if debug==1:

		print """Debugging..."""


		if l_zodb and l_mean_cs_mode==0:
			storage = FileStorage(l_database_file)
			db = DB(storage)		
			connection = db.open()
			main_database = connection.root()
			l_pdb_dict=main_database["pdb_dict"]

	

			#print """2) l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"] =""", l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]
			#print """2) l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]["mean"] =""", l_pdb_dict["1"][l_predictor]["protein_shift_classes"]["HD22"]["mean"]
			print "Exiting ...."
			sys.exit()



		print "Exiting after function calc_cs_diff_stats()..."
		sys.exit()
	return average_dict
	

def make_pictures(l_zodb,l_database_file,l_pdb_list,l_modeldict,l_pict_dict,l_project_dir,l_plot_data_dir,l_plot_pict_dir,l_first_residue,l_last_residue,l_mean_cs_mode,l_predictor):
	"""
	Make pictures
	l_pdb_dict[pdb_counter]["original_pdb_base_name"]=os.path.basename(l_pdb_file)	
	"""
	verbose=1
	if verbose>=1:
		print "Running function  make_pictures()"
	l_plot_data_dir_full="%s/%s" % (l_project_dir,l_plot_data_dir)
	l_plot_pict_dir_full="%s/%s" % (l_project_dir,l_plot_pict_dir)

	make_dir_no_remove(l_plot_data_dir_full)
	make_dir_no_remove(l_plot_pict_dir_full)	
	#print "l_mean_cs_mode=", l_mean_cs_mode
	#make_dir(l_plot_data_dir_full)
	#make_dir(l_plot_pict_dir_full)	

	if l_mean_cs_mode==1:
		l_model_list=["ensemble_mean_chemical_shifts"]
	else:
		l_model_list=l_pdb_list	


		#print "l_pdb_dict[i] =", l_pdb_dict[i] 
		#print l_pdb_dict[l_predictor]
		#sys.exit()


	for i in l_model_list:
	#for i in l_model_dict:
		if verbose>=1:
			print "Processing model ", i # l_model_dict[i]

		if l_zodb and  l_mean_cs_mode==0:
			storage = FileStorage(l_database_file)
			db = DB(storage)		
			connection = db.open()
			main_database = connection.root()
			l_model_dict=main_database["pdb_dict"]
		else:
			l_model_dict = l_modeldict



		if l_mean_cs_mode==0:
			model_name=l_model_dict[i]["original_pdb_base_name"]
		else:
			#model_name="mean_CS"
			model_name=i

		for shift_class in l_model_dict[i][l_predictor]["protein_shift_classes"]:
			if verbose>=11:print "Processing shift_class ", shift_class # l_model_dict[i]
			shift_plot_name=shift_class_description_dict[shift_class]
			if verbose>=10: print """l_model_dict[i][l_predictor]["protein_shift_classes"][shift_class]=""", l_model_dict[i][l_predictor]["protein_shift_classes"][shift_class]
			mean=l_model_dict[i][l_predictor]["protein_shift_classes"][shift_class]["mean"]
			median=l_model_dict[i][l_predictor]["protein_shift_classes"][shift_class]["median"]
			stdev=l_model_dict[i][l_predictor]["protein_shift_classes"][shift_class]["stdev"]

			for plot_type in pict_dict:
				title=pict_dict[plot_type]["title"] % shift_plot_name
				description=pict_dict[plot_type]["description"]
				Y_label=pict_dict[plot_type]["Y_label"]
				X_label=pict_dict[plot_type]["X_label"]
				plotting_function=pict_dict[plot_type]["plotting_function"]
				for j in pict_dict[plot_type]["data_files"]:
					plot_data=pict_dict[plot_type]["data_files"][j]
					if verbose>=3: print i,shift_class,plot_type, j, plot_data
					data_file_name_full="%s/%s_%s_%s.txt" % (l_plot_data_dir_full,model_name,shift_class,plot_data)
					image_file_name_full="%s/%s_%s_%s.png" % (l_plot_pict_dir_full,model_name,shift_class,plot_data)
					if plot_data in l_model_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"]:
						l_text=l_model_dict[i][l_predictor]["protein_shift_classes"][shift_class]["plot_data"][plot_data] 
						if verbose>=2: print shift_class, plot_data,l_model_dict[i]["protein_shift_classes"][shift_class]["plot_data"][plot_data] 	
						if verbose>=2: print "Writing file %s and creating gnuplot graph" % data_file_name_full
						if l_text!="":
							write_file(l_text,data_file_name_full)
							plotting_function(data_file_name_full,title,Y_label,X_label,image_file_name_full,description,mean,stdev,l_first_residue,l_last_residue)
					else:
						print """Error! Plot_data %s is not in 	l_model_dict[i][%s]["protein_shift_classes"][shift_class]["plot_data"]""" % (l_predictor,plot_data)

		if l_zodb:
			l_model_dict._p_changed = True
	
			transaction.savepoint(True)
			transaction.commit()
			transaction.get().commit() 
			connection.close()
			db.close()
			storage.close()
			l_model_dict={}

	debug=0
	if debug==1:
		print "Exiting after function  make_pictures()..."
		sys.exit()
	return


def write_bmrb(l_shifty_list,l_shifty_file,l_place_dic,l_full_project_dir,l_shifty_file_loc):
	"""
	Write a  BMRB file from Shifty file
	"""
	verbose=0
	if verbose>=1:
		print "Running function  write_bmrb()"
	l_bmrb_file_loc="%s_BMRB" % (l_shifty_file_loc)
	l_bmrb_file_abspath="%s/%s" % (l_full_project_dir,l_bmrb_file_loc)

	BMRB_file_open=open(l_bmrb_file_abspath,"w")
	BMRB_file_open.write(("loop_"+linesep))
	BMRB_file_open.write(("_Residue_seq_code"+linesep))
	BMRB_file_open.write(("_Residue_label"+linesep+linesep))	
	residue_name_list=[]
	for i in l_shifty_list:
		j=split(i)
		if j[0]!="#NUM" and len(j)>0:
			res_num=j[0]
			res_name=j[1]		
			res_name_three=aa_names_1let_2_full_all_CAP[res_name]
			if len(residue_name_list)<5:
				residue_name_list=residue_name_list+[[res_num,res_name_three]]
			if len(residue_name_list)==5:
				i_out=" "
				for i in  residue_name_list:
					i_out+="%s %s " % (i[0],i[1])		
				BMRB_file_open.write(i_out+linesep)
				residue_name_list=[]
					
					
	if len(residue_name_list)<5:
		i_out=" "
		for i in  residue_name_list:
			i_out+="%s %s " % (i[0],i[1])		
		BMRB_file_open.write(i_out+linesep)
		BMRB_file_open.write(linesep)
	BMRB_file_open.write((" stop_"+linesep))
	assignment_trigger= """  loop_
      _Atom_shift_assign_ID
      _Residue_seq_code
      _Residue_label
      _Atom_name
      _Atom_type
      _Chem_shift_value
      _Chem_shift_value_error
      _Chem_shift_ambiguity_code""" + linesep
	BMRB_file_open.write((assignment_trigger+linesep))
	counter=1
	for i in l_shifty_list:	
		j=split(i)
		if j[0]!="#NUM" and len(j)>0:
			res_num=j[0]
			res_name=j[1]
			res_name_three=aa_names_1let_2_full_all_CAP[res_name]
			for d in l_place_dic:
				atom_name=d
				shift=j[l_place_dic[d]]
				if atom_name=="N":
					nucleus="N"
				elif atom_name in ["CA","CB","C","CO"]:
					nucleus="C"				
 				elif atom_name in ["H","NH","HN","HA","HA1","HA2","HA3"]:
					nucleus="H"
 				if atom_name in ["NH","HN"]:
					atom_name="H"
 				if atom_name in ["CO"]:
					atom_name="C"
				if 	float(shift)!=0.0:
					shift_out=" %s %s %s %s %s %s . ." % (counter,res_num,res_name_three,atom_name,nucleus,shift)+linesep
					counter+=1
					BMRB_file_open.write(shift_out)
	BMRB_file_open.write(linesep)			
	end="""   stop_

save_"""
	BMRB_file_open.write(end)		
				
	BMRB_file_open.close()
	debug=0
	if debug==1:
		print "Exiting after function write_bmrb()..."
		sys.exit()

	return l_bmrb_file_abspath,l_bmrb_file_loc



def atom_place(l_shifty_list):
	place_dic={}		
	title_found=0
	for i in l_shifty_list:
		j=split(i)
		if j[0]=="#NUM":
			title_found=1
			counter=0
			for n in j:
				counter+=1
				if n not in ["#NUM","AA"]:
					place_dic[n]=counter-1
	if 	title_found==0:
		#print "ERROR: The program could not find names of the columns in the input file. The program will exit now" 
		sys.exit()		
	return(place_dic)	


def calc_cs_diff_averages_for_ensemble(l_legacy,l_average_dict,l_project_dir,l_report_dir,l_ensemble_average_file_base_name,l_predictor):
	"""
	Caluclate average correlation coefficeints and cs diffs for an ensemble
	"""
	verbose=0
	if verbose>=0:
		print "Running function calc_cs_diff_averages_for_ensemble()"

	l_report_text=""
	l_report_dict="report_dict={'shift_classes':{},'averages':{}}"+ linesep
	l_shift_classes=""
	l_shift_classes_list=[]

	total_dict={"lists":{},"shift_classes":{},"averages":{}}
	parameters_found=0

	for shift_class in l_average_dict['protein_shift_classes']:

		l_report_dict+="report_dict['shift_classes']['%s']={}" % (shift_class)+ linesep

		l_parameter_list=[]

		if shift_class not in l_shift_classes_list:
			l_shift_classes_list+=[shift_class]
			l_shift_classes+= linesep + linesep + linesep +"Shift class - %s: " % shift_class + linesep + linesep

		if shift_class not in total_dict["shift_classes"]:		
			total_dict["shift_classes"][shift_class]={}
		for parameter in l_average_dict['protein_shift_classes'][shift_class]['totals']['lists']:

			l_report_dict+="report_dict['shift_classes']['%s']['%s']={}" % (shift_class,parameter)+ linesep

			if parameter not in l_parameter_list:
				l_parameter_list+=[parameter]
				l_shift_classes+=linesep + linesep+linesep + "Parameter - %s: " % parameter + linesep + linesep


			if  parameter not in total_dict["lists"]:
				total_dict["lists"][parameter]={"mean":[],"median":[],"std_lists":[],"average_diff_from_mean":[],"average_diff_from_median":[]}

			value_list=l_average_dict['protein_shift_classes'][shift_class]['totals']['lists'][parameter]

			l_mean="NONE"
			l_median="NONE"
			l_stdev="NONE"
			average_diff_from_mean=l_average_diff_from_median="NONE"

			if len(value_list)>0:
				#print shift_class, parameter, value_list
				l_mean=stats.lmean(value_list)
				l_median=stats.lmedianscore(value_list)
				average_diff_from_mean,average_diff_from_median=calculate_average_abs_difference(value_list)
			if len(value_list)>1:
				l_stdev=stats.lstdev(value_list)
	
			if is_number(l_mean)==1:

				#print "Adding parameters"
				total_dict["lists"][parameter]["mean"]+=[l_mean]
				total_dict["lists"][parameter]["median"]+=[l_median]
				total_dict["lists"][parameter]["average_diff_from_mean"]+=[average_diff_from_mean]
				total_dict["lists"][parameter]["average_diff_from_median"]+=[average_diff_from_median]
				parameters_found=1
				l_report_dict+="report_dict['shift_classes']['%s']['%s']['mean']='%s'" % (shift_class,parameter,l_mean)+ linesep
				l_report_dict+="report_dict['shift_classes']['%s']['%s']['median']='%s'" % (shift_class,parameter,l_median)+ linesep
				l_report_dict+="report_dict['shift_classes']['%s']['%s']['average_diff_from_mean']='%s'" % (shift_class,parameter,average_diff_from_mean)+ linesep
				l_report_dict+="report_dict['shift_classes']['%s']['%s']['average_diff_from_median']='%s'" % (shift_class,parameter,average_diff_from_median)+ linesep
				l_shift_classes+="Mean absolute difference - %s " % l_mean + linesep 
				l_shift_classes+="Median absolute difference - %s " % l_median + linesep
				l_shift_classes+="Average absolute deviation from the mean absolute difference - %s " % average_diff_from_mean + linesep
				l_shift_classes+="Average absolute deviation from the median absolute difference - %s " % average_diff_from_median + linesep

			if is_number(l_stdev)==1:
				total_dict["lists"][parameter]["std_lists"]+=[l_stdev]
				l_report_dict+="report_dict['shift_classes']['%s']['%s']['standard_deviation']=%s " % (shift_class,parameter,l_stdev)+ linesep
				l_shift_classes+="Standard deviation - %s " % l_stdev + linesep
				


		######################
		# Do not remove!
		# More detailed info about outliers is available in dicts and lists below:
		#
		for j in l_average_dict['protein_shift_classes'][shift_class]['per_residue']["dicts"]:
			if verbose> 3 : print j		

		for k in l_average_dict['protein_shift_classes'][shift_class]['per_residue']["lists"]:
			if verbose> 3 : print k	
		######################################################################

	l_totals_text="Average values for the whole system and all chemical shifts: " + linesep + linesep + linesep
	#print l_shift_classes
	parameter_totals_text=""
	for parameter in total_dict["lists"]:
		total_dict["averages"][parameter]={}
		l_report_dict+="report_dict['averages']['%s']={}" %  parameter + linesep

		parameter_name_text=linesep + linesep+"Parameter - %s " % parameter + linesep + linesep
		#print parameter_name

		
		for list_name in total_dict["lists"][parameter]:

			#print parameter_name, list_name
			total_dict["averages"][parameter][list_name]={}
			p_list= total_dict["lists"][parameter][list_name]
			p_mean="NONE"
			p_median="NONE"
			p_stdev="NONE"
			p_average_diff_from_mean=p_l_average_diff_from_median="NONE"

			list_name_totals_text=""

			if len(p_list)>0:
				#print p_list
				p_mean=stats.lmean(p_list)
				p_median=stats.lmedianscore(p_list)
				p_average_diff_from_mean,p_average_diff_from_median=calculate_average_abs_difference(p_list)
			if len(p_list)>1:
				p_stdev=stats.lstdev(p_list)



			if is_number(p_mean)==1:
				total_dict["averages"][parameter][list_name]["mean"]=p_mean
				total_dict["averages"][parameter][list_name]["median"]=p_median
				total_dict["averages"][parameter][list_name]["average_diff_from_mean"]=p_average_diff_from_mean
				total_dict["averages"][parameter][list_name]["average_diff_from_median"]=p_average_diff_from_median

				l_report_dict+="report_dict['averages']['%s']['%s']= {} " %  (parameter,list_name) + linesep
				l_report_dict+="report_dict['averages']['%s']['%s']= {} " % (parameter,list_name)  + linesep

				l_report_dict+="report_dict['averages']['%s']['%s']['mean']= '%s' " %  (parameter,list_name,p_mean) + linesep
				l_report_dict+="report_dict['averages']['%s']['%s']['median']= '%s' " % (parameter,list_name,p_median)  + linesep
				l_report_dict+="report_dict['averages']['%s']['%s']['average_diff_from_mean']= '%s' " % (parameter,list_name,p_average_diff_from_mean)  + linesep
				l_report_dict+="report_dict['averages']['%s']['%s']['average_diff_from_median']= '%s' " % (parameter,list_name,p_average_diff_from_median)  + linesep

				list_name_totals_text+= linesep + linesep+"Sub-parameter: %s " %  list_name + linesep 
				list_name_totals_text+="Mean absolute difference - %s " % p_mean + linesep 
				list_name_totals_text+="Median absolute difference - %s " % p_median + linesep
				list_name_totals_text+="Average absolute deviation from the mean absolute difference - %s " % p_average_diff_from_mean + linesep
				list_name_totals_text+="Average absolute deviation from the median absolute difference - %s " % p_average_diff_from_median + linesep


			if is_number(p_stdev)==1:
				total_dict["averages"][parameter][list_name]["std"]=p_stdev
				l_report_dict+="report_dict['averages']['%s']['%s']['standard_deviation']= '%s' " % (parameter,list_name,p_stdev)  + linesep

		if list_name_totals_text!="":
			parameter_totals_text+=parameter_name_text	+ list_name_totals_text	

	l_report_dir_full="%s/%s" % (l_project_dir,l_report_dir)
	make_dir_no_remove(l_report_dir_full)

	# Legacy start
	if l_legacy:

		report_file="%s/%s.txt" % (l_report_dir_full,l_ensemble_average_file_base_name)
		report_module="%s/%s.py" % (l_report_dir_full,l_ensemble_average_file_base_name)

	# Legacy end

	l_report_dir_method_specific="%s/%s" % (l_report_dir_full,l_predictor)
	make_dir_no_remove(l_report_dir_method_specific)
	report_file_method_specific="%s/%s.txt" % (l_report_dir_method_specific,l_ensemble_average_file_base_name)
	report_module_method_specific="%s/%s.py" % (l_report_dir_method_specific,l_ensemble_average_file_base_name)


	#print l_shift_classes
	l_report_text=l_totals_text + parameter_totals_text + l_shift_classes
	if 	parameters_found==1: 
		# Legacy start
		if l_legacy:
			write_file(l_report_text,report_file)
			write_file(l_report_dict,report_module)

			if verbose>=0:
				print "Legacy module with average ensemble stats are located in ", report_module
				print "Non-Legacy module with average ensemble stats are located in ", report_module


		# Legacy end

		write_file(l_report_text,report_file_method_specific)
		write_file(l_report_dict,report_module_method_specific)



	debug=0
	if debug==1:
		print "Exiting after function calc_cs_diff_averages_for_ensemble()"
		sys.exit()
	return


def calc_offsets(l_pdb_1_list,l_pdb_2_list):
	"""
	Calculate offsets in residue numbering
	"""
	l_dic_of_offsets={}
	for j in range(0,len(l_pdb_1_list)-2): 
		l_pdb_1_res_name_1,l_pdb_1_res_num_1=l_pdb_1_list[j][0],l_pdb_1_list[j][1] 
		l_pdb_1_res_name_2,l_pdb_1_res_num_2=l_pdb_1_list[j+1][0],l_pdb_1_list[j+1][1] 
		l_pdb_1_res_name_3,l_pdb_1_res_num_3=l_pdb_1_list[j+2][0],l_pdb_1_list[j+2][1]		
		
		#print l_res_name_1,l_res_num_1,l_res_name_2,l_res_num_2,l_res_name_3,l_res_num_3  
		for j in range(0,len(l_pdb_2_list)-2): 
			l_pdb_2_res_name_1,l_pdb_2_res_num_1=l_pdb_2_list[j][0],l_pdb_2_list[j][1] 
			l_pdb_2_res_name_2,l_pdb_2_res_num_2=l_pdb_2_list[j+1][0],l_pdb_2_list[j+1][1] 
			l_pdb_2_res_name_3,l_pdb_2_res_num_3=l_pdb_2_list[j+2][0],l_pdb_2_list[j+2][1]
			#print 	"PDB:", l_pdb_res_name_1, l_pdb_res_name_2, l_pdb_res_name_3		
			if l_pdb_1_res_name_1==l_pdb_2_res_name_1 and l_pdb_1_res_name_2==l_pdb_2_res_name_2 and l_pdb_1_res_name_3==l_pdb_2_res_name_3:
				#print "KU"
				diff_1=l_pdb_1_res_num_1-l_pdb_2_res_num_1
				diff_2=l_pdb_1_res_num_2-l_pdb_2_res_num_2
				diff_3=l_pdb_1_res_num_3-l_pdb_2_res_num_3
				if diff_1==diff_2 and diff_3==diff_2 and diff_3==diff_1:
					if l_dic_of_offsets.has_key(diff_1)==0:
						l_dic_of_offsets[diff_1]=[1]
					else:
						l_dic_of_offsets[diff_1]+=[1]	
	#print "l_dic_of_offsets=",	l_dic_of_offsets		
	return (l_dic_of_offsets)



def calc_offsets2(l_pdb_1_list,l_pdb_2_list):
	"""
	Calculate offsets in residue numbering
	"""
	l_dic_of_offsets={}
	for j in range(0,len(l_pdb_1_list)-2): 
		l_pdb_1_res_name_1,l_pdb_1_res_num_1=l_pdb_1_list[j][1],l_pdb_1_list[j][0] 
		l_pdb_1_res_name_2,l_pdb_1_res_num_2=l_pdb_1_list[j+1][1],l_pdb_1_list[j+1][0] 
		l_pdb_1_res_name_3,l_pdb_1_res_num_3=l_pdb_1_list[j+2][1],l_pdb_1_list[j+2][0]		
		
		#print l_res_name_1,l_res_num_1,l_res_name_2,l_res_num_2,l_res_name_3,l_res_num_3  
		for j in range(0,len(l_pdb_2_list)-2): 
			l_pdb_2_res_name_1,l_pdb_2_res_num_1=l_pdb_2_list[j][1],l_pdb_2_list[j][0] 
			l_pdb_2_res_name_2,l_pdb_2_res_num_2=l_pdb_2_list[j+1][1],l_pdb_2_list[j+1][0] 
			l_pdb_2_res_name_3,l_pdb_2_res_num_3=l_pdb_2_list[j+2][1],l_pdb_2_list[j+2][0]
			#print 	"PDB:", l_pdb_res_name_1, l_pdb_res_name_2, l_pdb_res_name_3		
			if l_pdb_1_res_name_1==l_pdb_2_res_name_1 and l_pdb_1_res_name_2==l_pdb_2_res_name_2 and l_pdb_1_res_name_3==l_pdb_2_res_name_3:
				#print "KU"
				diff_1=l_pdb_1_res_num_1-l_pdb_2_res_num_1
				diff_2=l_pdb_1_res_num_2-l_pdb_2_res_num_2
				diff_3=l_pdb_1_res_num_3-l_pdb_2_res_num_3
				if diff_1==diff_2 and diff_3==diff_2 and diff_3==diff_1:
					if l_dic_of_offsets.has_key(diff_1)==0:
						l_dic_of_offsets[diff_1]=[1]
					else:
						l_dic_of_offsets[diff_1]+=[1]	
	#print "l_dic_of_offsets=",	l_dic_of_offsets		
	return (l_dic_of_offsets)

def calc_max_offset(l_dic_of_offsets,l_strong_shift_limit,l_total_length_strong_offset):
	"""
	Calculate maximal offset in residue numbering
	"""
	verbose=0
	if verbose>=1: print "Running function calc_max_offset()"
	if verbose>=1: print "l_dic_of_offsets=", l_dic_of_offsets
	l_max_offset=l_max_len=0
	offset_list=[]
	l_text=""
	total_length=0
	l_strong_shift=0
	l_second_offset_number=0
	l_second_offset=0
	l_second_ratio="NONE"
	for n in l_dic_of_offsets:
		if verbose>=10: print n, l_dic_of_offsets[n]
		n_len=len(l_dic_of_offsets[n])
		total_length+=n_len
		if n_len>l_max_len:
			l_max_offset=n
			l_max_len=n_len
		offset_list+=[[n_len,n]]
	offset_list.sort()
	offset_list.reverse()
	
	

	if len(offset_list)>1:
		l_first_offset_number=(offset_list[0][0])
		l_first_offset=(offset_list[0][1])
	
	
		l_second_offset_number=(offset_list[1][0])
		l_second_offset=(offset_list[1][1])
		
		if l_total_length_strong_offset==1:
			l_second_ratio=float(l_second_offset_number)/float(total_length)
		else:
			l_second_ratio=float(l_second_offset_number)/float(l_first_offset_number)	
		if l_second_ratio>l_strong_shift_limit:
			l_strong_shift=1
	if verbose>=1:
		print "l_strong_shift=", 		l_strong_shift, "l_second_ratio=", l_second_ratio, "l_second_offset_number=", l_second_offset_number, "l_second_offset=", l_second_offset

	debug_exit(0,"Exiting after function calc_max_offset()")
	return(l_max_offset,l_strong_shift,l_second_offset_number,l_second_offset,total_length,l_max_len)	


def read_shifts_result_file(l_verify_dict,l_shifts_C_N_result_file,l_shifts_NH_result_file,l_shifts_H_result_file):
	"""
	Read C_N_result_file from Shifts

#PDB cf residue atom     bb_p  bb_s  bb_f  chi_p chi_s  HB-D  HB-I  REF         pred

#---------------------------------------------------------------------------------------------

tmp. s LEU  71  N       -0.85  4.42 -0.39  3.92  1.02 -1.26 -0.54  118.68      124.99

tmp. s LEU  71  Ca       0.13  0.73  0.02  0.00  0.02  0.00  0.00   54.13       55.03

tmp. s LEU  71  Cb      -0.11 -2.37  0.11  0.00  0.02  0.00  0.00   46.19       43.83

tmp. s LEU  71  CO       0.02  0.33  0.13 -0.60  0.02  0.00  0.00  176.35      176.25

tmp. s ARG  72  N       -0.87  1.72  0.37  1.70  3.52  0.11 -4.97  118.95      120.53


			if res_num_1 not in l_residue_number_list:
				l_residue_number_list+=[res_num_1]
				l_list_4_offset+=[[aa_names_1let_2_full_all_CAP[res_name_1], res_num_1]]

			if res_num_1 not in l_assign_dic:
				l_assign_dic[res_num_1]=[res_name_1,{},{}]
			l_assign_dic[res_num_1][1]["H"]=float(H)
			l_assign_dic[res_num_1][1]["HA"]=float(HA)
			l_assign_dic[res_num_1][1]["HB"]=float(HB)

['=============================================================================']
['SHIFTS,', 'version', '4.1', '[X.-P.', 'Xu', 'and', 'D.A.', 'Case,', 'Mar.', '2002]']
['=============================================================================']
['shifts', 'will', 'be', 'computed', 'for', 'atoms', 'matching', '::H']
['found', '4', 'rings']
['ring', 'int.', 'center', 'anis.', 'iso.']
['PHE', '4', '1.000', '15.061', '20.105', '19.679', '-99.200', '-91.000']
['PHE', '45', '1.000', '10.032', '17.809', '8.855', '-99.200', '-91.000']
['TYR', '59', '0.840', '12.433', '19.651', '1.636', '-99.200', '-91.000']
['HIS', '68', '0.900', '14.327', '10.929', '12.442', '-99.200', '-91.000']
[]
['=============================================================================']
['Atom', 'RingCur', 'El', 'P_anis', 'Const', 'RC', 'pred', 'Obs']
['=============================================================================']
['tmp', 'GLN', '2', 'H', '-0.08', '0.04', '0.17', '-0.55', '8.47', '8.05']
['tmp', 'ILE', '3', 'H', '-0.28', '0.57', '0.39', '-0.55', '8.25', '8.38']
['tmp', 'PHE', '4', 'H', '0.03', '0.74', '0.32', '-0.55', '8.34', '8.88']
['tmp', 'VAL', '5', 'H', '0.07', '0.72', '0.30', '-0.55', '8.24', '8.79']
['tmp', 'LYS', '6', 'H', '-0.08', '0.69', '0.33', '-0.55', '8.42', '8.80']


['PHE', '45', '1.000', '7.048', '3.744', '8.339', '-99.200', '-91.000']
[]
['=============================================================================']
['Atom', 'RingCur', 'El', 'P_anis', 'Const', 'RC', 'pred', 'Obs']
['=============================================================================']
['tmp', 'ARG', '1', 'HA', '-0.16', '0.12', '0.25', '-0.75', '4.34', '3.79']
['tmp', 'ARG', '1', 'HB2', '-0.26', '0.05', '0.08', '-0.04', '1.90', '1.73']
['tmp', 'ARG', '1', 'HB3', '-0.16', '0.01', '0.01', '-0.04', '1.80', '1.61']

  _Chem_shift_ambiguity_code
    1       1      M   HA    H      3.88  .   1
    2       1      M   HB2   H      2.07  .   1
    3       1      M   HB3   H      2.01  .   1
    4       1      M   HG2   H      2.47  .   1
    5       1      M   HG3   H      1.97  .   1
    6       1      M   HE    H      1.68  .   1
    7       2      Q   H     H      8.05  .   1
    8       2      Q   HA    H      4.70  .   1
    9       2      Q   HB2   H      2.05  .   1
   10       2      Q   HB3   H      1.93  .   1
   11       2      Q   HG2   H      1.40  .   1
   12       2      Q   HG3   H      2.08  .   1
   13       2      Q   HE21  H      6.98  .   1
   14       2      Q   HE22  H      7.56  .   1
   15       3      I   H     H      8.38  .   1
   16       3      I   HA    H      4.05  .   1
   17       3      I   HB    H      1.83  .   1
   18       3      I   HG12  H      1.17  .   1


	"""
	verbose=0
	if verbose>=1: print "Running function read_shifts_result_file()"
	#sys.exit()
	l_assign_dic={}
	l_residue_number_list=[]
	l_list_4_offset=[]
	shifts_list=read_file(l_shifts_C_N_result_file)
	shifts_NH_list=read_file(l_shifts_NH_result_file)
	shifts_H_list=read_file(l_shifts_H_result_file)
	start_trigger=" _Chem_shift_ambiguity_code"
	stop_trigger="stop_"
	start_reading=0
	
	for i in shifts_list:
		if verbose>=10: print [i]
		j=i.split()
		if verbose>=10: print [j], linesep		


		if len(j)>12 and "tmp" in j[0]:
			if verbose>=11: print j
			try:
				res_name,res_num,atom_name,bb_p,bb_s,bb_f,chi_p,chi_s,HB_D,HB_I,REF,pred_shift=j[2],int(j[3]),j[4],j[5],j[6],j[7],j[8],j[9],j[10],j[11],j[12],j[13]
				res_name_1_let="NONE"
				if verbose>=10: print i


				if res_name in aa_names_full_all_CAP:
					res_name_1_let=aa_names_full_all_CAP[res_name]

					atom_name=rename_atoms(res_name_1_let,atom_name,atom_name_switch_dict,"shifts")

				if res_num not in l_residue_number_list:
					l_residue_number_list+=[res_num]
					if res_name in aa_names_full_all_CAP:
						l_list_4_offset+=[[res_name, res_num]]
					else:
						print "Warning!!! Nonstandard residue %s was found at position %s" % (res_name,res_num)
				if is_number(pred_shift):

					if res_num not in l_assign_dic:
						l_assign_dic[res_num]=[res_name_1_let,{},{}]
						l_assign_dic[res_num][2]["bb_p"]=bb_p
						l_assign_dic[res_num][2]["bb_s"]=bb_s
						l_assign_dic[res_num][2]["bb_f"]=bb_f
						l_assign_dic[res_num][2]["chi_p"]=chi_p
						l_assign_dic[res_num][2]["chi_s"]=chi_s
						l_assign_dic[res_num][2]["HB_D"]=HB_D
						l_assign_dic[res_num][2]["HB_I"]=HB_I
						l_assign_dic[res_num][2]["REF"]=REF
						if verbose>=11: print res_name_1_let

					if atom_name=="N":
						l_assign_dic[res_num][1]["N"]=float(pred_shift)
						shifts_assignment["N"][res_num]=[[res_name_1_let, float(pred_shift), "N"]]
					if atom_name=="Ca":
						l_assign_dic[res_num][1]["CA"]=float(pred_shift)
						shifts_assignment["CA"][res_num]=[[res_name_1_let, float(pred_shift), "CA"]]
					if atom_name=="Cb":
						l_assign_dic[res_num][1]["CB"]=float(pred_shift)
						shifts_assignment["CB"][res_num]=[[res_name_1_let,float(pred_shift), "CB"]]
					if atom_name=="CO":
						l_assign_dic[res_num][1]["C"]=float(pred_shift)
						shifts_assignment["C"][res_num]=[[res_name_1_let, float(pred_shift), "C"]]

				else:
					print "Warning!!! Predicted ShiftS chemical shift of %s for residue %s%s is not numerical and is " % (atom_name,res_name,res_num), [pred_shift]
			except:
				print "Warning!!! ShiftS outout is mis-formatted. The following line was ignored"
				print  [i], linesep


	#sys.exit()
	for k in shifts_NH_list:
		if verbose>=10: print k
		m=k.split()
		if verbose>=10: print m
		if len(m)>0 and "tmp" in m[0]:
			if verbose>=10: print m
			res_name,res_num,pred_shift_emp,HBD,HBWD,pred_shift=m[1],int(m[2]),m[4],m[5],m[6],m[7]
			if verbose>=10: print k
			res_name_1_let="NONE"
			if res_name in aa_names_full_all_CAP:
				res_name_1_let=aa_names_full_all_CAP[res_name]
			if is_number(pred_shift):

				if res_num not in l_assign_dic:
					l_assign_dic[res_num]=[res_name_1_let,{},{}]
				if verbose>=10: print res_name_1_let, res_name,res_num

				l_assign_dic[res_num][1]["HN_better"]=float(pred_shift)
				if "HN_better" not in shifts_assignment:
					shifts_assignment["HN_better"]={}
				shifts_assignment["HN_better"][res_num]=[[res_name_1_let, float(pred_shift), "HN_better"]]

				l_assign_dic[res_num][1]["HN_emp"]=float(pred_shift)
				if "HN_emp" not in shifts_assignment:
					shifts_assignment["HN_emp"]={}
				shifts_assignment["HN_emp"][res_num]=[[res_name_1_let, float(pred_shift), "HN_emp"]]
			else:
				print "Warning!!! Predicted ShiftS chemical shift of HNnew for residue %s%s is not numerical and is " % (res_name,res_num), [pred_shift]
	


	for n in shifts_H_list:
		if verbose>=10: print n
		f=n.split()
		if verbose>=10: print f

		if start_trigger in n:
			start_reading=1

		if stop_trigger in n:
			start_reading=0

		if len(f)>0 and is_number(f[0]) and start_reading==1:
			if verbose>=10: print f
			#res_name,res_num,atom,RingCur,El,P_anis,Const, RC, pred_shift=f[1],int(f[2]),f[3],f[4],f[5],f[6],f[7],f[8],f[9]

			res_num=int(f[1])			
			res_1_let_name=f[2]
			atom=f[3]
			pred_shift=f[5]
			if res_1_let_name in aa_names_1let_2_full_all_CAP:
				res_name=aa_names_1let_2_full_all_CAP[res_1_let_name]
			else:
				print "Warning!!! Nonstandard residue %s was found at position %s" % (res_1_let_name,res_num)				

			if atom=="HD" and res_name=="ILE":
				atom="HD1"


			if is_number(pred_shift):
				if res_num not in l_assign_dic:
					l_assign_dic[res_num]=[res_1_let_name,{},{}]
				l_assign_dic[res_num][1][atom]=float(pred_shift)

				if atom not in shifts_assignment:
					shifts_assignment[atom]={}
				if res_num not in shifts_assignment[atom]:
					shifts_assignment[atom][res_num]=[]
				shifts_assignment[atom][res_num]=[[res_name, float(pred_shift), atom]]				
				if "HA" in atom :
					#print "HA was detected" 
					if res_num not in shifts_assignment["HA"]:
						shifts_assignment["HA"][res_num]=[]
					shifts_assignment["HA"][res_num]+=[[res_name, float(pred_shift), atom]]
					#print "shifts_assignment=", shifts_assignment["HA"][res_num]
				if "HB" in  atom :
					if res_num not in shifts_assignment["HB"]:
						shifts_assignment["HB"][res_num]=[]
					shifts_assignment["HB"][res_num]+=[[res_name, float(pred_shift), atom]]
				if "HD" in  atom :
					if res_num not in shifts_assignment["HD"]:
						shifts_assignment["HD"][res_num]=[]
					shifts_assignment["HD"][res_num]+=[[res_name, float(pred_shift), atom]]
				if "HG" in  atom :
					if res_num not in shifts_assignment["HG"]:
						shifts_assignment["HG"][res_num]=[]
					shifts_assignment["HG"][res_num]+=[[res_name, float(pred_shift), atom]]
				if "HE" in  atom :
					if res_num not in shifts_assignment["HE"]:
						shifts_assignment["HE"][res_num]=[]
					shifts_assignment["HE"][res_num]+=[[res_name, float(pred_shift), atom]]
				if "HZ" in  atom :
					if res_num not in shifts_assignment["HZ"]:
						shifts_assignment["HZ"][res_num]=[]
					shifts_assignment["HZ"][res_num]+=[[res_name, float(pred_shift), atom]]
			else:
				print "Warning!!! Predicted ShiftS chemical shift of %s for residue %s%s is not numerical and is " % (atom,res_name,res_num), [pred_shift]


	for res_num in shifts_assignment["HA"]:
		HA_list=[]
		for g in shifts_assignment["HA"][res_num]:
			if g[1]!="HA":
				HA_list+=[g[1]]
		if len(HA_list)>0:
			HA_average=stats.lmean(HA_list)	
			l_assign_dic[res_num][1]["HAave"]=HA_average

	if 	len(l_verify_dict)>0:
		for res_num in l_assign_dic:
			res_name=l_assign_dic[res_num][0]
			for atom  in l_assign_dic[res_num][1]:
				if atom not in ["HA_again","NH_again","HAave","HN_emp","HN_better"]:
					if res_name in l_verify_dict:
						if atom not in l_verify_dict[res_name]:
							print "Error!!! Atom name %s of residue %s%s is not in  l_verify_dict" % (atom,res_name,res_num )
							print "The program will exit now"
							sys.exit()	

					else:
						print "Warning!!!! Residue name %s of residue %s is not in l_verify_dict" % (res_name,res_num)
	non_stereospecific(stereospecific_dictionary,l_assign_dic)
	
	debug_exit(0," Exiting function read_shifts_result_file() ...")			

	return l_assign_dic,l_list_4_offset



def debug_exit(debug,message):
	"""
	Print debug statment and exit
	"""
	if debug==1:
		print message
		sys.exit()
	return


def debug_print_list_exit(debug,message_list):
	"""
	Print debug statment and exit
	"""
	if debug==1:
		for i in message_list:
			print i
		sys.exit()
	return


	
def create_training_files_for_ensemble_building(pdb_list,l_zodb,l_database_file,l_pdbdict,l_atom_list_for_ensemble,l_full_project_dir,l_ensemble_building_dir,l_matrix_filename,l_weka_filename,l_project_name):
	"""
	l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["data"]=residue_shift_classes[shift_class

Running function create_training_files_for_ensemble()
i=  1 predictor=  chain
i=  1 predictor=  shiftx_assign_dic
i=  1 predictor=  shiftx_list_4_offset
i=  1 predictor=  pdb_dir
i=  1 predictor=  shiftx_residue_list
i=  1 predictor=  original_pdb
i=  1 predictor=  pdb_full
i=  1 predictor=  original_pdb_base_name
i=  1 predictor=  project_dir
i=  1 predictor=  shiftx_result
i=  1 predictor=  shiftx

1 protein_shift_classes
1 shift_diff_dict
2 protein_shift_classes
2 shift_diff_dict
End of CSQ
2 shift_diff_dict 76 CA {
'exp_value': 43.791, 
'exp_shift_value': 43.791, 
'sec_exp_shift_value': -1.3090000000000046, 
'true_diff': -1.8697000000000017, 
'sec_shiftx_value': 0.5606999999999971, 
'shiftx_value': 45.6607, 
'abs_diff': 1.8697000000000017}
'res_name': 'G', 


atom_list_for_ensemble+=["N"]
atom_list_for_ensemble+=["C","CO"]
atom_list_for_ensemble+=["CA"]
atom_list_for_ensemble+=["CB"]
atom_list_for_ensemble+=["H","HN","NH"]
atom_list_for_ensemble+=["HA","HA1","HA2","HA3"]


@RELATION "/apps/resprox/tmp_local/1317128273/m1Z/testset.txt.rama16.numres.bu95.holes.fil.ln"
@ATTRIBUTE bu_char_ob_perc NUMERIC
@ATTRIBUTE mean_hb_en_ob NUMERIC

@ATTRIBUTE label NUMERIC
@DATA
0,-2.000,-68.400,4.16666522380173,5.10533922956555,2.77258872223978,0,0,0,0.693147180559945,2.95848201777383,3.21767510429168,1.03549497935263,0,0.122217632724249,-1.19,0,0.0759859069779221,0,0,1.5993875765806,2.76000994003292,0.0121866456746508,20.000,0.613687,-1

1 protein_shift_classes
Residue number:  HZ
Residue number:  HB
Residue number:  HA
Residue number:  HG
Residue number:  HD
Residue number:  HE
Residue number:  Backbone_protons
Residue number:  Backbone
Residue number:  ND
Residue number:  NE
Residue number:  heavy_atoms
Residue number:  H
Residue number:  Main_chain
Residue number:  No_beta_side_chain
Residue number:  Main_chain_protons
Residue number:  Backbone_heavy_atoms
Residue number:  CO
Residue number:  side_chain_heavy_atoms
Residue number:  Main_chain_heavy_atoms
Residue number:  CB
Residue number:  CA
Residue number:  Protons
Residue number:  CE
Residue number:  N
Residue number:  CZ
Residue number:  CD
Residue number:  Side_chain
Residue number:  All
Residue number:  No_beta_side_chain_heavy_atoms
Residue number:  CG
Residue number:  side_chain_protons
Residue number:  No_beta_side_chain_protons


kendall
spearman
sec_spearman
sec_pearson
true_diff_list
pred_cs_list
exp_list
sec_pred_cs_list
median_outliers
details
sec_kendall
mean_outliers
number_of_median_atom_outliers
stdev
plot_data
pearson
abs_diff_list
median
per_residue_dict
average_abs_diff_from_mean
number_of_mean_atom_outliers
average_abs_diff_from_median
sec_exp_list
mean

	"""
	verbose=1
	if verbose>=1: print "Running function create_training_files_for_ensemble()"
	if verbose>=1: print "atom_list_for_ensemble=", l_atom_list_for_ensemble
	per_residue_dict={}
	#pdb_list=[]
	residue_number_list=[]
	global_parameter_list=[]



	if verbose>=10: print "Place 1"
	#for i in pdb_dict:
	#	pdb_list+=[[i,pdb_dict[i]]]
	pdb_list.sort()
	matrix_text=""
	if verbose>=10: print "Place 2"
	output_dir_full="%s/%s" % (l_full_project_dir,l_ensemble_building_dir)
	make_dir_no_remove(output_dir_full)

	matrix_filename_full="%s/%s_%s" % (output_dir_full,l_project_name,l_matrix_filename)
	weka_filename_full="%s/%s_%s" % (output_dir_full,l_project_name,l_weka_filename)
	#csv_file_name_full="%s/%s_%s" % (output_dir_full,l_project_name,l_csv_file_name)

	weka_text='@RELATION "%s"' % weka_filename_full + linesep
	#csv_text=""
	#allowed_shifts=["HA"]
	allowed_shifts=[]
	#per_residue_only=0
	#global_only=1

	#print "pdb_list=", pdb_list
	#sys.exit()
	for i in pdb_list:
		if verbose>=0: print "Processing case %s in function create_training_files_for_ensemble_building()" % i
		if l_zodb:
			storage = FileStorage(database_file)
			db = DB(storage)		
			connection = db.open()
			main_database = connection.root()
			l_pdb_dict=main_database["pdb_dict"]
			if verbose>=10: print "Place 0"
			if verbose>=10: print "l_pdb_dict =", l_pdb_dict 
			debug=0
			if debug==1:
				print "Exiting after reading database"
				sys.exit()

			#print "l_pdb_dict[i] =", l_pdb_dict[i] 
			#print l_pdb_dict[l_predictor]
			#sys.exit()
		else:
			l_pdb_dict = l_pdbdict




		#i=q[0]
		if verbose>=10: print "Place 3"
		if verbose>=10:
			print "i= ", i #, l_pdb_dict[i]
			#sys.exit()
			print "i= ", i, "chain= ", l_pdb_dict[i]["chain"]
			print "i= ", i, "pdb_dir= ", l_pdb_dict[i]["pdb_dir"]
			print "i= ", i, "shiftx_assign_dic= ", l_pdb_dict[i]["shiftx_assign_dic"]
			print "i= ", i, "shiftx_result= ", l_pdb_dict[i]["shiftx_result"]
			print "i= ", i, "shiftx= ", l_pdb_dict[i]["shiftx"]		
		for j in l_pdb_dict[i]["shiftx"]:
			if verbose>=10: print i, j
			if verbose>=10: print "Place 4"
			for residue_number in l_pdb_dict[i]["shiftx"][j]:
				if verbose>=10: print "Residue number: ",  residue_number
				#for r in l_pdb_dict[i]["shiftx"][j][residue_number]:
				#	print r
				#print "Exiting..."
				#sys.exit()
				if verbose>=10: print "Place 5"
				if is_number(residue_number) and residue_number not in residue_number_list:
					residue_number_list+=[residue_number]
				if j=="protein_shift_classes":
					global_parameter_list+=[residue_number]	


				if residue_number not in per_residue_dict:
					per_residue_dict[residue_number]={}

				if (j=="shift_diff_dict"): # and per_residue_only==1) or (global_only==1 and "protein_shift_classes") or (global_only==0 and  per_residue_only==0 and j in ["shift_diff_dict","protein_shift_classes"]):
					if verbose>=10: print i, j, residue_number 

					for shift in l_pdb_dict[i]["shiftx"]["shift_diff_dict"][residue_number ]:
						if verbose>=10: print "Place 6"
						if verbose>=10:print j, " Shift= ", shift
						proceed=0	
						for atom in l_atom_list_for_ensemble:
							if atom == shift:
								proceed=1
						if len(l_atom_list_for_ensemble)==0:
							proceed=1
						shift_dict=l_pdb_dict[i]["shiftx"]["shift_diff_dict"][residue_number][shift]
						if verbose>=10: print "Place 7"
						if 'res_name' not in shift_dict:
							proceed=0
						if len(allowed_shifts)>0 and shift not in allowed_shifts:
							proceed=0							
						if proceed==1:
							if shift not in per_residue_dict[residue_number]:
								per_residue_dict[residue_number][shift]={"pdbs":{},"use":1}
								if verbose>=10: print "Place 8"
							if verbose>=10: print i, j, residue_number, shift,  shift_dict
							res_name=shift_dict['res_name']
							exp_shift_value=shift_dict['exp_shift_value']
							shiftx_value=shift_dict['shiftx_value']
							if verbose>=10: print "Place 9"
							if is_number(shiftx_value)==0 or is_number(exp_shift_value)==0:
								per_residue_dict[residue_number][shift]["use"]=0
								if is_number(shiftx_value)==0:
									print "Warning!!! Shift %s for residue %s%s will not be used because shiftx_value is not numerical and is " % (shift,res_name,residue_number), [shiftx_value]
								if is_number(exp_shift_value)==0:
									print "Warning!!! Shift %s for residue %s%s will not be used because exp_shift_value is not numerical and is " % (shift,res_name,residue_number), [exp_shift_value]
							if "res_name" not in per_residue_dict[residue_number][shift]:
								per_residue_dict[residue_number][shift]["res_name"]=res_name
								if verbose>=10: print "Place 10"
							if i not in  per_residue_dict[residue_number][shift]["pdbs"]:
								per_residue_dict[residue_number][shift]["pdbs"][i]=shift_dict
								if verbose>=10: print "Place 11"
							if verbose>=10: print i,j,residue_number,shift,res_name
						
			#print "Exiting...."
			#sys.exit()

		if l_zodb:
			l_pdb_dict._p_changed = True
			transaction.savepoint(True)
			transaction.commit()
			transaction.get().commit() 
			connection.close()
			db.close()
			storage.close()
			l_pdb_dict={}

	residue_number_list.sort()
	shift_list=[]
	pdbs=[]

	row_counter=1
	refined_pdb_list=[]
	for w in pdb_list:
		if w not in refined_pdb_list:
			refined_pdb_list+=[w]
		else:
			print "Warning!!!! The pdb_list got messed up. Case %s is present more than ones in pdb_list" % w
			

	for residue_number in  residue_number_list:
		if verbose>=10: print "Place 12"
		if is_number(residue_number):
			if verbose>=10: print "residue_number=", residue_number
			for shift in per_residue_dict[residue_number]:
				if verbose>=10: print "Place 13"
				use=per_residue_dict[residue_number][shift]["use"]
				if verbose>=10: print "Place 14"
				if verbose>=10: print residue_number, shift, use, pdb_list
				proceed=0
				for atom in l_atom_list_for_ensemble:
					if atom == shift:
						proceed=1

				if use==1 and proceed==1:
					if verbose>=10: print residue_number, shift				
					if shift not in shift_list:
						shift_list+=[shift]
					pdb_counter=0
					for i in refined_pdb_list:
						shift_dict=per_residue_dict[residue_number][shift]["pdbs"][i]
						if verbose>=10: print "Place 15"
						exp_shift_value=shift_dict['exp_shift_value']
						shiftx_value=shift_dict['shiftx_value']
						if verbose>=10: print " residue_number= ", residue_number, " shift= ", shift,  "case= ",i, "shiftx_value= ", shiftx_value
						if i not in pdbs:
							pdbs+=[i]	
							weka_text+="@ATTRIBUTE %s NUMERIC" % 	i	+ linesep	

						if verbose>=10: print residue_number, shift, i
						matrix_text+="%s," % shiftx_value	
					matrix_text+="%s" % exp_shift_value + linesep
					row_counter+=1
					
	weka_text+="""@ATTRIBUTE label NUMERIC
@DATA
"""

			
	if verbose>=10: print 	matrix_text
	weka_text+=matrix_text
	if verbose>=10: print 	weka_text


	write_file(matrix_text,matrix_filename_full)
	write_file(weka_text,weka_filename_full)

	if verbose>=0: print "Weka file is located in ", weka_filename_full
	if verbose>=0: print "Matrix file is located in ", matrix_filename_full
	if verbose>=0: print "Total number of rows in the matrix is ", row_counter

	debug=0
	if debug:
		print "Exiting after function create_training_files_for_ensemble_building()"
		sys.exit()
	return 




def average_predictions(l_cluster_dir,l_cluster_program,l_combined_dictionary,l_full_project_dir,l_meta_results_dir,l_pdb,predictor_bb_combinations_dict):
	"""
	

						if resid_num not in l_arshift_residue_number_list:
							l_arshift_residue_number_list+=[resid_num]
							l_arshift_residue_list+=[[resid_num, '%s' % resid_name, [], [], [], [], [], []]]
							l_arshift_list_4_offset+=[[resname_3let,resid_num]]
					
						if is_number(shift_init):
						#	if resid_name=="I" and atom_name=="HG12" and shift_init=="0.00":
						#		shift_init=0.0001
						#	if shift_init=="-0.00":
						#		shift_init=0.0001

							if resid_num not in l_arshift_assign_dic:
								l_arshift_assign_dic[resid_num]=[resid_name,{},{}]
							l_arshift_assign_dic[resid_num][1][atom_name]=float(shift_init)

							l_arshift_assign_dic[resid_num][2]["atom_name"]=atom_name
							l_arshift_assign_dic[resid_num][2]["res_name"]=resid_name
							l_arshift_assign_dic[resid_num][2]["pred_shift"]=float(shift_init)
							l_arshift_assign_dic[resid_num][2]["secondary_shift"]=SEfit
							l_arshift_assign_dic[resid_num][2]["SDtrn"]=SDtrn
										combined_dictionary[ch3shift_resnum]={"residue_name":ch3shift_residname,"shifts":{}}
														combined_dictionary[ch3shift_resnum]["shifts"][atom]["predictors"]["ch3shift"]=float(shift_value)
														combined_dictionary[ch3shift_resnum]["shifts"][atom]["list"]+=[float(shift_value)]

aa_names_full_all_CAP={"GLN":"Q","TRP":"W","GLU":"E","ARG":"R","THR":"T","TYR":"Y","ILE":"I","PRO":"P","ALA":"A","SER":"S","ASP":"D","PHE":"F","GLY":"G","HIS":"H","LYS":"K","LEU":"L","CYS":"C","VAL":"V","ASN":"N","MET":"M"}	
aa_names_1let_2_full_all_CAP={"Q":"GLN","W":"TRP","E":"GLU","R":"ARG","T":"THR","Y":"TYR","I":"ILE","P":"PRO","A":"ALA","S":"SER","D":"ASP","F":"PHE","G":"GLY","H":"HIS","K":"LYS","L":"LEU","C":"CYS","V":"VAL","N":"ASN","M":"MET","B":"CYS"}

	
	"""
	verbose=1
	if verbose>=1: print "Running function average_predictions()"


	result_dict={}

	l_averaged_residue_list=[]
	l_averaged_assign_dic={}

	#l_clustering_1_mean_dic={}
	#l_clustering_2_mean_dic={}
	#l_clustering_3_mean_dic={}
	#l_clustering_4_mean_dic={}
	#l_clustering_5_mean_dic={}
	#l_clustering_6_mean_dic={}
	#l_clustering_7_mean_dic={}

	#l_clustering_1_median_dic={}
	#l_clustering_2_median_dic={}
	#l_clustering_3_median_dic={}
	#l_clustering_4_median_dic={}
	#l_clustering_5_median_dic={}
	#l_clustering_6_median_dic={}
	#l_clustering_7_median_dic={}


	l_2best_mean_dict={}
	l_3best_mean_dict={}
	l_4best_mean_dict={}
	l_5best_mean_dict={}


	l_2best_median_dict={}
	l_3best_median_dict={}
	l_4best_median_dict={}
	l_5best_median_dict={}


	l_1_std_mean_dict={}
	l_2_std_mean_dict={}
	l_3_std_mean_dict={}



	l_1_std_median_dict={}
	l_2_std_median_dict={}
	l_3_std_median_dict={}


	l_averaged_list_4_offset=[]
	l_averaged_residue_number_list=[]

	l_list_of_combos_methods=[]

	for res_num in l_combined_dictionary:
		residue_name=l_combined_dictionary[res_num]["residue_name"]
		averaged_residname_single_letter=aa_names_full_all_CAP[residue_name]
		if verbose>=1: print residue_name,res_num
		for atom  in l_combined_dictionary[res_num]["shifts"]:
			all_prediction_list=l_combined_dictionary[res_num]["shifts"][atom]["list"]
			all_prediction_dict=l_combined_dictionary[res_num]["shifts"][atom]["predictors"]
			if verbose>=10: print "prediction_dict=", prediction_dict
			if verbose>=10: print "Processing atom: ", atom
			for predictor_combo in predictor_bb_combinations_dict:
				prediction_list=[]
				prediction_dict={}
				proceed=0
				if verbose>=10: print "Processing predicor combo: ", predictor_combo
				list_of_predictors=predictor_bb_combinations_dict[predictor_combo]["list"]
				if verbose>10: print "list_of_predictors=", list_of_predictors
				proceed=check_predictors(list_of_predictors,eligible_shifts,atom,residue_name)

				for l_predictor  in list_of_predictors:
					if verbose>=10: print "l_predictor=", l_predictor
					if l_predictor in all_prediction_dict:
						shift_value=all_prediction_dict[l_predictor]['shift']
						if verbose>10: print "Predictor %s produce value %s for shift %s" % (l_predictor,shift_value,atom)
						if is_number(shift_value):
							prediction_list+=[float(shift_value)]
							prediction_dict[l_predictor]=float(shift_value)
						else:
							if verbose>0: print "Warning!!! Shift value for atom %s of %s%s is not numerical and is " % (atom,residue_name,res_num), prediction_list
					else:
						if verbose>10: print "Warning!!! Predictor %s is not in prediction_dict" % l_predictor
				if verbose>10: print "Shifts for %s of %s%s are " % (atom ,residue_name,res_num), prediction_list

				if res_num not in l_averaged_residue_number_list:
					l_averaged_residue_number_list+=[res_num]
					l_averaged_residue_list+=[[res_num, '%s' % averaged_residname_single_letter, [], [], [], [], [], []]]
					l_averaged_list_4_offset+=[[residue_name,res_num]]

				mean_shift=std_shift=median_shift="NONE"
				if len(prediction_list)>0:

					mean_shift=stats.lmean(prediction_list)
					median_shift=stats.lmedianscore(prediction_list)

					if verbose>=10:#10 and atom=="CA":
						print "Cacluate average shift %s from " % mean_shift, prediction_list

				if len(prediction_list)>1:
					std_shift=stats.lstdev(prediction_list)

				combined_dict=combining_method_1(l_cluster_dir,l_cluster_program,prediction_dict,mean_shift,median_shift,prediction_list,std_shift,l_full_project_dir,l_meta_results_dir,l_pdb)

				for l_method in combined_dict:						
					l_meta_shift= combined_dict[l_method]
					l_metaprediction=combined_dict[l_method]
					l_predictor_method_name="%s_%s_dic" % (predictor_combo,l_method)
					l_predictor_method_label="%s_%s" % (predictor_combo,l_method)
					#if verbose>=0: print "l_predictor_method_name=", l_predictor_method_name
					#sys.exit()
					l_predictor_method_list=[l_predictor_method_name,l_predictor_method_label,l_averaged_residue_list]
					if l_predictor_method_list not in l_list_of_combos_methods:
						l_list_of_combos_methods+=[l_predictor_method_list]
					if l_predictor_method_name not in result_dict:
						result_dict[l_predictor_method_name ]={}
					if res_num not in result_dict[l_predictor_method_name]:
						result_dict[l_predictor_method_name][res_num]=[residue_name,{},{}]
					if is_number(l_meta_shift)==0:
						if verbose>10: print "Warning!!! Meta shift from %s for %s of %s%s is not numeric and is " %  (l_predictor_method_name,atom,residue_name,res_num), [l_meta_shift]
					else:			
						result_dict[l_predictor_method_name][res_num][1][atom]=float(l_meta_shift)
					if "atom_name" not in result_dict[l_predictor_method_name][res_num][2]:
						result_dict[l_predictor_method_name][res_num][2]["atom_name"]=atom
					if "res_name" not in result_dict[l_predictor_method_name][res_num][2]:
						result_dict[l_predictor_method_name][res_num][2]["res_name"]=residue_name
					if "pred_shift" not in result_dict[l_predictor_method_name][res_num][2]:
						result_dict[l_predictor_method_name][res_num][2]["pred_shift"]=mean_shift
					if "stdev" not in result_dict[l_predictor_method_name][res_num][2]:
						result_dict[l_predictor_method_name][res_num][2]["stdev"]=std_shift



				#print "Exiting... "
				#sys.exit()




	print "Length of 	l_list_of_combos_methods is ", len(l_list_of_combos_methods)
	debug_exit(0,"Exiting after function average_predictions() ...")			
	return l_list_of_combos_methods, result_dict, l_averaged_list_4_offset, l_averaged_residue_list




def combining_method_1(l_cluster_dir,l_cluster_program,l_prediction_dict,l_mean,l_median,l_prediction_list,l_std_shift,l_full_project_dir,l_meta_results_dir,l_pdb):
	"""
	Combining method 1

l_prediction_dict= {'shiftx2': 175.0935, 'shiftx': 174.6956, 'fourdspot': 175.079, 'sparta': 176.305}


	"""
	verbose=0
	if verbose>=10: print "Running function average_predictions()"
	if verbose>=10: print "l_prediction_dict=", l_prediction_dict

	result_dict={}

	l_1_std_list_mean=[]
	l_2_std_list_mean=[]
	l_3_std_list_mean=[]

	l_1_std_list_median=[]
	l_2_std_list_median=[]
	l_3_std_list_median=[]


	if do_best:
		l_5best_list_mean=[]
		l_4best_list_mean=[]
		l_3best_list_mean=[]
		l_2best_list_mean=[]

	

		l_5best_list_median=[]
		l_4best_list_median=[]
		l_3best_list_median=[]
		l_2best_list_median=[]


		l_5best_mean="NONE"
		l_4best_mean="NONE"
		l_3best_mean="NONE"
		l_2best_mean="NONE"


		l_5best_median="NONE"
		l_4best_median="NONE"
		l_3best_median="NONE"
		l_2best_median="NONE"


	#l_1_ave_err_list_mean=[]
	#l_1_5_ave_err_list_mean=[]
	#l_2_ave_err_list_mean=[]





	#l_1_ave_err_list_median=[]
	#l_1_5_ave_err_list_median=[]
	#l_2_ave_err_list_median=[]


	l_list_mean=[]
	l_list_median=[]







	l_1_std_mean="NONE"
	l_2_std_mean="NONE"
	l_3_std_mean="NONE"


	l_1_std_median="NONE"
	l_2_std_median="NONE"
	l_3_std_median="NONE"

	#print  l_prediction_dict
	for predictor in l_prediction_dict:
		shift=l_prediction_dict[predictor]
		#print predictor, shift

		diff_mean=shift-l_mean
		abs_diff_mean=abs(diff_mean)
		l_list_mean+=[[abs_diff_mean,random.random(),predictor,shift]]


		diff_median=shift-l_median
		abs_diff_median=abs(diff_median)
		l_list_median+=[[abs_diff_median,random.random(),predictor,shift]]


	l_list_mean.sort()
	l_list_median.sort()

	counter=0
	for i in l_list_mean:
		l_abs_diff,predictor,shift=i[0],i[2],i[3]
		counter+=1		
		if do_best:
			if counter<=1 or counter<=2:
				l_5best_list_mean+=[shift]
				l_4best_list_mean+=[shift]
				l_3best_list_mean+=[shift]
				l_2best_list_mean+=[shift]
			elif counter<=3:
				l_5best_list_mean+=[shift]
				l_4best_list_mean+=[shift]
				l_3best_list_mean+=[shift]
			elif counter<=4:
				l_5best_list_mean+=[shift]
				l_4best_list_mean+=[shift]
			elif counter<=5:
				l_5best_list_mean+=[shift]
		if is_number(l_std_shift):
			if l_abs_diff<l_std_shift:
				l_1_std_list_mean+=[shift]
			if l_abs_diff<(l_std_shift*2.0):
				l_2_std_list_mean+=[shift]
			if l_abs_diff<(l_std_shift*3.0):
				l_3_std_list_mean+=[shift]


	counter=0
	for i in l_list_median:
		l_abs_diff,predictor,shift=i[0],i[2],i[3]
		counter+=1		

		if do_best:
			if counter<=1 or counter<=2:
				l_5best_list_median+=[shift]
				l_4best_list_median+=[shift]
				l_3best_list_median+=[shift]
				l_2best_list_median+=[shift]
			elif counter<=3:
				l_5best_list_median+=[shift]
				l_4best_list_median+=[shift]
				l_3best_list_median+=[shift]
			elif counter<=4:
				l_5best_list_median+=[shift]
				l_4best_list_median+=[shift]
			elif counter<=5:
				l_5best_list_median+=[shift]

		if is_number(l_std_shift):
			if l_abs_diff<l_std_shift:
				l_1_std_list_median+=[shift]
			if l_abs_diff<(l_std_shift*2.0):
				l_2_std_list_median+=[shift]
			if l_abs_diff<(l_std_shift*3.0):
				l_3_std_list_median+=[shift]
			

	if do_best:
		if len(l_5best_list_mean)>0:
			l_5best_mean=stats.lmean(l_5best_list_mean)
		if len(l_4best_list_mean)>0:
			l_4best_mean=stats.lmean(l_4best_list_mean)
		if len(l_3best_list_mean)>0:
			l_3best_mean=stats.lmean(l_3best_list_mean)
		if len(l_2best_list_mean)>0:
			l_2best_mean=stats.lmean(l_2best_list_mean)


		if len(l_5best_list_median)>0:
			l_5best_median=stats.lmean(l_5best_list_median)
		if len(l_4best_list_median)>0:
			l_4best_median=stats.lmean(l_4best_list_median)
		if len(l_3best_list_median)>0:
			l_3best_median=stats.lmean(l_3best_list_median)
		if len(l_2best_list_median)>0:
			l_2best_median=stats.lmean(l_2best_list_median)


	if len(l_1_std_list_mean)>0:
		l_1_std_mean=stats.lmean(l_1_std_list_mean)
	if len(l_2_std_list_mean)>0:
		l_2_std_mean=stats.lmean(l_2_std_list_mean)
	if len(l_3_std_list_mean)>0:
		l_3_std_mean=stats.lmean(l_3_std_list_mean)


	if len(l_1_std_list_median)>0:
		l_1_std_median=stats.lmedian(l_1_std_list_median)
	if len(l_2_std_list_median)>0:
		l_2_std_median=stats.lmedian(l_2_std_list_median)
	if len(l_3_std_list_median)>0:
		l_3_std_median=stats.lmedian(l_3_std_list_median)

	cluster_result_dict={}
	if len(l_prediction_list)>0:
		cluster_result_dict=clustering(l_cluster_dir,l_cluster_program,l_full_project_dir,l_meta_results_dir,l_pdb,l_prediction_list)
	for i in cluster_result_dict:
		result_dict[i]=cluster_result_dict[i]


	result_dict["mean"]=l_mean
	
	if do_best:
		result_dict["5best_mean"]=l_5best_mean
		result_dict["4best_mean"]=l_4best_mean
		result_dict["3best_mean"]=l_3best_mean
		result_dict["2best_mean"]=l_2best_mean

		result_dict["5best_median"]=l_5best_median
		result_dict["4best_median"]=l_4best_median
		result_dict["3best_median"]=l_3best_median
		result_dict["2best_median"]=l_2best_median


	result_dict["1_std_mean"]=l_1_std_mean
	result_dict["2_std_mean"]=l_2_std_mean
	result_dict["3_std_mean"]=l_3_std_mean

	result_dict["1_std_median"]=l_1_std_median
	result_dict["2_std_median"]=l_2_std_median
	result_dict["3_std_median"]=l_3_std_median

	#print result_dict

			
	debug_exit(0,"Exiting after function  combining_method_1() ...")
	return result_dict

def clustering(l_cluster_dir,l_cluster_program,l_full_project_dir,l_meta_results_dir,l_pdb,l_prediction_list):
	"""
#:/opt/markber/software/clustering/cluster/ClusterV2.0$ ./cluster -m 6  -l 2 -d number_lists.txt

Cluster V2.0 (c) 1995 Dr. Andrew C.R. Martin, UCL.

Usage: cluster [-t] [-d] [-l <nclus>] [-n <nclus>] [-m <type>] [infile [outfile]]
       -t Do not display cluster table
       -d Do not display dendogram
       -l Limit printing of clusters & dendogram to specified number
       -n Print near-median vector for specified number of clusters
       -m Cluster type. This may be a number or word as follows:
          1 Ward                Ward's minimum variance method (default)
          2 Single              Single linkage
          3 Complete            Complete linkage
          4 Average (or Group)  Group average
          5 McQuitty            McQuitty's method
          6 Median (or Gower)   Gower's median method
          7 Centroid            Centroid method

Performs cluster analysis on an arbitrary numerical dataset. Each vector
to be clustered must appear as a single record in the file. No description
of the file size is necessary and blank lines are ignored. Comments may
be placed in the file on lines starting with a ! or a #

As of V2.0, a label may be appended to each vector (row) in the data file.
This label must consist of a # character followed by an arbitrary string.
Labels will then be appended to the clustering table.

Clustering code based on code by F. Murtagh, ESA/ESO/STECF, Garching.
The original FORTRAN code is available from STATLIB FTP sites.


markber@markber-Aspire-E700:/opt/markber/software/clustering/cluster/ClusterV2.0$ ./cluster -m 6  -l 2 -d number_lists.txt 
     SEQ NOS 2CL 3CL 4CL 5CL 6CL 7CL 8CL 9CL
     ------- --- --- --- --- --- --- --- --- ----
          1   1
          2   1
          3   1
          4   1
          5   1
          6   2
          7   2


	"""
	verbose=0
	if verbose>=10: print "Running function clustering()"
	meta_dir="%s/%s" % (l_full_project_dir,l_meta_results_dir)	
	make_dir_no_remove(meta_dir)
	clustering_dir="%s/clustering" % (meta_dir)
	make_dir_no_remove(clustering_dir)
	l_file_name="%s/%s.input" % (clustering_dir,l_pdb)
	#clustering_methods=[1,2,3,4,5,6,7]
	clustering_methods=[1,2,3,5,7]
	#clustering_methods=[1]
	max_clusters=2
	l_text=""
	for i in l_prediction_list:
		l_text+="%s" % i + linesep
	write_file(l_text,l_file_name)
	cluster_result_dict={}
	#cur_dir=os.getcwd()
	#os.chdir(l_cluster_dir)
	for method in clustering_methods:
		method_name="clustering_%s" % method
		l_output="%s/%s_%s.output" % (clustering_dir,method,l_pdb)
		cluster_command="%s/%s -m %s -l %s -d %s %s" % (l_cluster_dir,l_cluster_program,method,max_clusters,l_file_name,l_output)
		if verbose>=10: print cluster_command
		os.system(cluster_command)
		file_lines=read_file(l_output)
		cluster_dict={}
		row_dict={}
		for j in file_lines:
			k=j.split()
			if len(k)>0 and is_number(k[0]):
				row_number,cluster_number=k[0],k[1]
				if is_number(row_number) and is_number(cluster_number):
					row_dict[int(row_number)]=int(cluster_number)
					if int(cluster_number) not in cluster_dict:
						cluster_dict[int(cluster_number)]={"list":[],"size":"NONE"}
					cluster_dict[int(cluster_number)]["list"]+=[int(row_number)]
				else:
					print "Warning!!! Either row_number %s or/and  cluster_number %s is not numerical" % (row_number,cluster_number)
			
		l_cluster_list=[]
		for cluster_number in cluster_dict:
			l_list=cluster_dict[cluster_number]["list"]
			l_size=len(l_list)
			cluster_dict[cluster_number]["size"]=l_size
			l_cluster_list+=[[l_size, random.random(),l_list]]

		l_cluster_list.sort()
		l_cluster_list.reverse()
		if len(l_cluster_list)>0:
			if len(l_cluster_list[0])>1:
				l_list_of_rows=l_cluster_list[0][2]
				#print l_cluster_list
				#print "l_list_of_rows=", l_list_of_rows
				good_prediction_list=[]
				for row in l_list_of_rows:
					index=row-1
					#print "row=", row
					#print "index=", index
					#print [l_text]
					#print "l_prediction_list=", l_prediction_list, len(l_prediction_list),linesep, linesep

					value=l_prediction_list[index]
					good_prediction_list+=[value]
			else:
				print "Warning!!! 1) Output of clustering method %s is misformatted at line " %   method, [l_cluster_list[0]]
		else:
			print "Warning!!! 2) Output of clustering method %s is misformatted at line " %   method, [l_cluster_list]


		#print "good_prediction_list=", good_prediction_list
		l_mean=l_median="NONE"
		if len(good_prediction_list)>0: 
			l_mean=stats.lmean(good_prediction_list)
			l_median=stats.lmedianscore(good_prediction_list)
		
		cluster_result_dict["%s_mean" % method_name]=l_mean
		cluster_result_dict["%s_median" % method_name]=l_median

	debug_exit(0,"Exiting after function  clustering() ...")
	return cluster_result_dict



def rename_atoms(l_residue_name,l_atom_name,l_atom_name_switch_dict,l_predictor):
	"""
	Rename atoms
	"""
	new_atom_name=l_atom_name
	if l_atom_name in l_atom_name_switch_dict["all_predictors"]:
		new_atom_name=l_atom_name_switch_dict["all_predictors"][l_atom_name]
	if l_predictor in l_atom_name_switch_dict:
		if l_residue_name in l_atom_name_switch_dict[l_predictor]:
			if l_atom_name in l_atom_name_switch_dict[l_predictor][l_residue_name]:
				new_atom_name=	l_atom_name_switch_dict[l_predictor][l_residue_name][l_atom_name]

	return new_atom_name



def predictor_combinations_backbone(l_predictor_combinations_4_meta_predictor_bb,l_shiftx2_meta_always):
	"""
	Find all combinations of predictors
	=[["shiftx","shiftx2","sparta","camshift","fourdspot","shifts"]]
	"""
	#l_two_predictors=[]
	#l_three_predictors=[]
	#l_four_predictors=[]
	#l_five_predictors=[]
	l_predictors=[]
	for i in l_predictor_combinations_4_meta_predictor_bb:
		#print i
		l_two_list_A=[i]
		l_three_list_A=[i]
		l_four_list_A=[i]
		l_five_list_A=[i]
		#print "l_two_list1=", l_two_list
		for j in l_predictor_combinations_4_meta_predictor_bb:
			l_three_list_B=[]
			l_four_list_B=[]
			l_five_list_B=[]

			if j not in l_three_list_A:
				l_three_list_B=[j]
			if j not in l_four_list_A:
				l_four_list_B=[j]
			if j not in l_five_list_A:
				l_five_list_B=[j]

			if j not in l_two_list_A:
				l_two_list=l_two_list_A+[j]
				#print "l_two_list2=", l_two_list
				l_two_list.sort()
				if l_two_list not in l_predictors and  len(l_two_list)==2:					
					l_predictors+=[l_two_list]
					#print "Adding...  ", l_two_list
					#print "Combined list:", l_predictors
					l_two_list=[]
				else:
					l_two_list=[]	
			
			for k in l_predictor_combinations_4_meta_predictor_bb:


				l_four_list_C=[]
				l_five_list_C=[]
				if k not in l_four_list_A and k not in l_four_list_B:
					l_four_list_C=[k]
				if k not in l_five_list_A and  k not in l_five_list_B :
					l_five_list_C=[k]

				if k not in l_three_list_A and k not in l_three_list_B and  l_three_list_B!=[]:
					l_three_list=[k]+l_three_list_A+l_three_list_B
					l_three_list.sort()
					if l_three_list not in l_predictors and len(l_three_list)==3:
						l_predictors+=[l_three_list]
						l_three_list=[]	
					else:
						l_three_list=[]					



				for m in l_predictor_combinations_4_meta_predictor_bb:
					#print m,  l_four_list_A, l_four_list_B, l_four_list_C
					l_five_list_D=[]
					if m not in l_five_list_A and  m not in l_five_list_B and m not in l_five_list_C:
						l_five_list_D=[m]

					if m not in l_four_list_A and m not in l_four_list_B and  m not in l_four_list_C  and l_four_list_B!=[] and l_four_list_C!=[]:
						l_four_list=[m]+l_four_list_A+l_four_list_B+l_four_list_C
						l_four_list.sort()
						if l_four_list not in l_predictors and len(l_four_list)==4:
							l_predictors+=[l_four_list]
							l_four_list=[]	
						else:
							l_four_list=[]		
				
					for n in l_predictor_combinations_4_meta_predictor_bb:

						if n not in l_five_list_A and n not in l_five_list_B and  n not in l_five_list_C  and n not in l_five_list_D and l_five_list_D!=[] and l_five_list_B!=[] and l_five_list_C!=[]:
							l_five_list=[n]+l_five_list_A+l_five_list_B+l_five_list_C+l_five_list_D
							l_five_list.sort()
							if l_five_list not in l_predictors and len(l_five_list)==5:
								l_predictors+=[l_five_list]
								l_five_list=[]	
							else:
								l_five_list=[]		



	l_predictors+=[l_predictor_combinations_4_meta_predictor_bb]	
	l_predictors.sort()
	refined_list=[]
	combinations_dict={}
	counter=0
	for x in l_predictors:
		x.sort()
		if x not in refined_list:
			#print x
			refined_list+=[x]
			l_combo_name=""
			for d in x:
				l_combo_name+="%s_" % d
			l_combo_name=l_combo_name[:-1]
			if l_shiftx2_meta_always==0 or (shiftx2_meta_always and "shiftx2" in l_combo_name):
				combinations_dict[l_combo_name]={"list":x}	
				#print 	l_combo_name
				counter+=1
	#print "Total number of combinations is ", counter
	#sys.exit()
	return combinations_dict






def create_new_ensemble(l_use_reference,l_population_dict,l_pop_type,l_pseudo_ensemble_dir,l_ensemble_size,l_concoord_ensemble_dir,l_dssp_path,l_concoord_dir,l_rsteps,l_bump, l_viol, l_i_steps,l_ff,l_bonds,l_keep_parent,l_new_ensemble_dir):
	"""
	Create pseudo ensemble for evaluation
	"""
	verbose=1
	if verbose>=1: print "Running function create_new_ensemble()"
	if verbose>=1: print "l_ensemble_size= " , l_ensemble_size
	#sys.exit()
	l_total=0
	make_dir(l_pseudo_ensemble_dir)
	concoord_conditions_list=[]
	concoord_conditions_checked=0
	new_ensemble_dict={}
	make_dir(l_new_ensemble_dir)
	for i in l_population_dict:
		l_population=l_population_dict[i]["population"]


		number_of_structures=int(round(l_population*l_ensemble_size))
		l_population_dict[i]["number_of_structures"]=number_of_structures

		if verbose>=10: print i, "population=", [l_population],  "number_of_structures=", number_of_structures
		l_total+=number_of_structures
		if number_of_structures>0:
			counter=1
			if "pdb_path" in l_population_dict[i]:
				pdb_path=l_population_dict[i]["pdb_path"]
				for j in range(0,int(number_of_structures)):
					if verbose>=1: print j, number_of_structures
					copy_command="cp %s %s/%s_%s.pdb" % (pdb_path,l_pseudo_ensemble_dir,i,counter)
					if verbose>=1: print copy_command
					os.system(copy_command)
					counter+=1
				# Create a Concoord ensemble
				concoord_structires=int(number_of_structures)-1
				if concoord_structires>0 or (l_keep_parent==1 and concoord_structires==0):
					concoord_dict=run_concoord(l_use_reference,i,pdb_path,l_dssp_path,l_concoord_dir,l_rsteps,l_bump, l_viol, number_of_structures, l_i_steps,l_concoord_ensemble_dir,l_ff,l_bonds)
					if concoord_conditions_checked==0:
						for concoord_condition in concoord_dict:
							concoord_conditions_list+=[concoord_condition]
						concoord_conditions_checked=1
					if verbose>=1:
						for concoord_condition in concoord_dict:
							concoord_condition_dir=concoord_dict[concoord_condition]["disco_dir"]
							print "concoord_condition=", concoord_condition
							print "concoord_condition_dir=", concoord_condition_dir
					l_population_dict[i]["concoord_dict"]=concoord_dict

			else:
				print "Error!!! pdb_path is missing"

	if len(concoord_conditions_list)>0:
		list_of_conditions=concoord_conditions_list
	else:
		list_of_conditions=["new_ensemble"]

	for condition in list_of_conditions:
		ensemble_dir="%s/%s" % (l_new_ensemble_dir,condition)
		new_ensemble_dict[condition]={"ensemble_dir":ensemble_dir}
		if verbose>=1: print "Creating ensemble directory: ", ensemble_dir
		make_dir(ensemble_dir)
		copy_counter=0
		for j in l_population_dict:
			number_of_structures=l_population_dict[j]["number_of_structures"]
			l_population=l_population_dict[j]["population"]

			if verbose>=1: print "j= ", j, "number_of_structures=", number_of_structures
			if number_of_structures>0:
				if "pdb_path" in l_population_dict[j]:
					pdb_path=l_population_dict[j]["pdb_path"]
					structure_has_been_added=0
					if l_keep_parent==1:
						copy_command="cp %s %s/%s.pdb" % (pdb_path,ensemble_dir,j)
						if verbose>=1: print "Copying...", 	copy_command
						os.system(copy_command)
						copy_counter+=1
						if verbose>=1: print "Place 1: copy_counter= ", copy_counter
						concoord_structires=int(number_of_structures)-1
					else:
						concoord_structires=int(number_of_structures)

					
					if verbose>=10: print "l_keep_parent= ", l_keep_parent
					if verbose>=10: print "number_of_structures= ", number_of_structures
					if verbose>=10: print "concoord_structires= ", concoord_structires
					if verbose>=10: print j, " population= ", l_population
					if concoord_structires>0:
						if "concoord_dict" in l_population_dict[j]:
							concoord_dict=l_population_dict[j]["concoord_dict"]
							#for i in concoord_dict:
							#	print "i= ", i, concoord_dict[i] 
							if condition in concoord_dict:
								if 'disco_dir' in concoord_dict[condition]:
									disco_dir=concoord_dict[condition]['disco_dir']
									list_of_files=os.listdir(disco_dir)
									shuffle(list_of_files)
									shuffle(list_of_files)
									if len(list_of_files)<concoord_structires:
										print "ERROR!!!! The number of concoord structures %s is smaller than required %s " % ( len(list_of_files),concoord_structires)
										print "Dico directory is " , disco_dir
										print "The program will exit now"
										sys.exit()
										#concoord_structires=len(list_of_files)
									for p in range(0,concoord_structires):
										filename=list_of_files[p]
										filename_full="%s/%s" % (disco_dir,filename)
										copy_command="cp %s %s/%s.pdb" % (filename_full,ensemble_dir,filename)
										if verbose>=1: print "Copying...", 	copy_command
										os.system(copy_command)
										copy_counter+=1
										if verbose>=1: print "Place 2: p= %s, copy_counter= ", (p,copy_counter)
				
										
	
							else:
								print "Error!!! Condition %s is not in concoord_dict in l_population_dict for %s " % (condition, j)

						else:
							print "Error!!! number_of_structures>1 but concoord_dict is not in l_population_dict"
						#else:
						#	print "Error!!! Condition %s is not in l_population_dict" % condition
					
	
				else:
					print "Error!  number_of_structures>0 but pdb_path is not in l_population_dict[j]"

		if l_ensemble_size!=copy_counter and condition!="new_ensemble" and concoord_structires>0:
			if verbose>10:
				print "Error!!!! The copy_counter %s is not equal to l_ensemble_size %s " % (copy_counter,l_ensemble_size)
				#print "The program will exit now"
				#sys.exit()
			
			

	if verbose>=1: print  "l_total=", l_total
	debug=0
	if debug==1:
		print "Exiting after function create_new_ensemble()"
		sys.exit()

	return new_ensemble_dict




def find_random_coil_value2(l_shift_type,l_single_letter_res_name,l_random_coil_dic,place_dict):
	"""
	Find random coil value for a shift
	"""
	verbose=0
	if verbose>=10: print "Running function find_random_coil_value2()"
	if verbose>=10: print "l_random_coil_dic=", l_random_coil_dic
	random_coil_value=""
	for place in place_dict:
		if l_shift_type in  place_dict[place]:
			if verbose>=10: print "Processing l_shift_type %s in place_dict for place %s" % (l_shift_type,place)
			if l_single_letter_res_name in l_random_coil_dic:
				random_coil_value=l_random_coil_dic[l_single_letter_res_name][place]
			else:
				if verbose>=10: print "Error!!! Residue name %s was not found in  l_random_coil_dic for atom %s" % (l_single_letter_res_name,l_shift_type)

	if is_number(random_coil_value)==0 and verbose>=1:
		print "Error!!! Random coil value was not found for residue name %s " % (l_single_letter_res_name)
		print "Found Random coil of %s value is " % l_shift_type, [random_coil_value]
	return random_coil_value




def hydrophobicity2flexibility(l_hydrophob_dic,l_res_name,l_hydro_coef):
	"""
	Convert hydrophobicity to flexibility
	"""
	verbose=0
	if verbose>=1: print "Running function hydrophobicity2flexibility()"
	hydroflex=""
	if l_res_name!="GLY":

		l_res_name_1_let=aa_names_1let_2_full_all_CAP[l_res_name]
		if l_res_name_1_let in l_hydrophob_dic:
			hydrophobicity=l_hydrophob_dic[l_res_name_1_let]
			if hydrophobicity!=0:
				a=float(hydrophobicity)+100.0
				b=a/200.0
				hydroflex=(1.0-b)/l_hydro_coef
		else:
			print "Error!!! Residue name %s is not l_hydrophob_dic()" % l_res_name_1_let
	debug=0
	if debug==1: 
		print "Flexibility of %s is " % l_res_name_1_let, [hydroflex]
		print "Exiting after function hydrophobicity2flexibility()"
		sys.exit()
	return hydroflex


def find_neighboring_correction(\
	l_shift_type,\
	l_single_letter_res_name,\
	l_neighbor_dic,\
	place_dict,\
	N_place,\
	CO_place,\
	CA_place,\
	CB_place,\
	NH_place,\
	HA_place,\
	side_chain_H_correction,\
	side_chain_C_correction,\
	side_chain_N_correction,\
	side_chain_protons_to_correct,\
	side_chain_carbons_to_correct,\
	side_chain_nitrogens_to_correct,\
	):
	"""
	Find random coil value for a shift
	"""
	verbose=0
	if verbose>=10: print "Running function find_neighboring_correction() for l_shift_type ",l_shift_type
	if verbose>=10: print "l_neighbor_dic=", l_neighbor_dic
	neighbor_value=0.0



	
	H_corr=""
	C_corr=""
	N_corr=""
	if l_neighbor_dic!={}:
		for place in place_dict:
			if l_shift_type in  place_dict[place]:
				if verbose>=10: print "Processing l_shift_type %s in place_dict for place %s" % (l_shift_type,place)
				if l_single_letter_res_name in l_neighbor_dic:
					neighbor_value=l_neighbor_dic[l_single_letter_res_name][place][1]
				else:
					if verbose>=10: print "Error!!! Residue name %s was not found in  l_neighbor_dic for atom %s" % (l_single_letter_res_name,l_shift_type)

		if l_shift_type[0]=="H" and l_shift_type not in ["HA","HA1","HA2","HA3","H","HN","HN"]:
			if verbose>=10:  print "Side-chain proton shift detected"

			if side_chain_H_correction==1:
				H_corr=l_neighbor_dic[l_single_letter_res_name][HA_place][1]
			elif side_chain_H_correction==2:
				H_corr=l_neighbor_dic[l_single_letter_res_name][NH_place][1]
			if H_corr!="":
				for i in side_chain_protons_to_correct:
					atom_list=side_chain_protons_to_correct[i]["atom_list"]
					H_reduction=side_chain_protons_to_correct[i]["H_reduction"]
					if l_shift_type in atom_list:
						neighbor_value=H_corr/H_reduction
						if verbose>=10: print "Applying HA correction of %s to " % neighbor_value, l_shift_type

		if l_shift_type[0]=="C" and l_shift_type not in ["CA","CB","C","CO"]:

			if side_chain_C_correction==1:
				C_corr=l_neighbor_dic[l_single_letter_res_name][CA_place][1]
			elif side_chain_C_correction==2:
				C_corr=l_neighbor_dic[l_single_letter_res_name][CB_place][1]
			elif side_chain_C_correction==3:
				C_corr=l_neighbor_dic[l_single_letter_res_name][CO_place][1]
			if verbose>=1: print "C_corr= ", [C_corr]


			if C_corr!="":
				for i in side_chain_carbons_to_correct:
					atom_list=side_chain_carbons_to_correct[i]["atom_list"]
					C_reduction=side_chain_carbons_to_correct[i]["C_reduction"]
					if l_shift_type in atom_list:
						neighbor_value=C_corr/C_reduction
						if verbose>=0: print "Applying C correction of %s to " % neighbor_value, l_shift_type

		if l_shift_type[0]=="N" and l_shift_type not in ["N"]:
			if side_chain_N_correction==1:
				N_corr=l_neighbor_dic[l_single_letter_res_name][N_place][1]

			if N_corr!="":
				for i in side_chain_nitrogens_to_correct:
					atom_list=side_chain_nitrogens_to_correct[i]["atom_list"]
					N_reduction=side_chain_nitrogens_to_correct[i]["N_reduction"]
					if l_shift_type in atom_list:
						neighbor_value=N_corr/N_reduction
						if verbose>=1: print "Applying N correction of %s to " % neighbor_value, l_shift_type


		if l_shift_type[0]=="C" and l_shift_type not in ["C","CO","CA","CB"]:
			if verbose>=10:  print "Side-chain carbon shift detected"

		if l_shift_type[0]=="N" and l_shift_type not in ["N"]:
			if verbose>=1:  print "Side-chain nitrogen shift detected"

		if is_number(neighbor_value)==0 and verbose>=1:
			print "Error!!! Neighboring Value was not found for residue name %s " % (l_single_letter_res_name)
			print "Found Neighboring Value of %s value is " % l_shift_type, [neighbor_value]
	return neighbor_value


def caclulate_random_coil_values(\
			backbone_random_coil_values,\
			i_plus_minus_two_residue_correction,\
			i_plus_minus_one_residue_correction,\
			backbone_random_coil_values_dict,\
			side_chain_random_coil_values,\
			i_plus_minus_two_residue_correction_dict,\
			i_plus_minus_one_residue_correction_dict,\
			B_Cys_list,\
			l_find_rci,\
			l_protein_sequence_list,\
			l_exp_assignment_dict,\
			l_pdb_dict,\
			l_rc_values,\
			place_dict,\
			l_rc_offsets,\
			l_stereospecific_dictionary,\
			l_rcoil_offset_dict,\
			):
	"""
	Find random coil values

76: {'ne_list': [], 'hz_list': [], 'hg_list': [], 'ce_list': [], 'cd_list': [], 'cz_list': [], 'res_name': 'GLY', 'shifts': {'HA': 3.74, 'H': 8.16, 'CA': 46.2, 'N': 115.2, 'HAave': 3.74, 'HA2': 3.78, 'HA3': 3.7}, 'hh_list': [], 'he_list': [], 'hd_list': [], 'cg_list': [], 'ha_list': [3.78, 3.7], 'nd_list': [], 'hb_list': []

					l_sparta_residue_list+=[[resid_num, '%s' % resid_name, [], [], [], [], [], []]]
					l_sparta_list_4_offset+=[[aa_names_1let_2_full_all_CAP[resid_name],resid_num]]
				
				if is_number(shift_init):
					if resid_name=="I" and atom_name=="HG12" and shift_init=="0.00":
						shift_init=0.0001
					if shift_init=="-0.00":
						shift_init=0.0001


					atom_name=rename_atoms(resid_name,atom_name,atom_name_switch_dict,"sparta")
					

					if resid_num not in l_sparta_assign_dic:
						l_sparta_assign_dic[resid_num]=[resid_name,{},{}]
					l_sparta_assign_dic[resid_num][1][atom_name]=float(shift_init)
					l_sparta_assign_dic[resid_num][2]["atom_name"]=atom_name
					l_sparta_assign_dic[resid_num][2]["res_name"]=resid_name
					l_sparta_assign_dic[resid_num][2]["pred_shift"]=float(shift_init)
					l_sparta_assign_dic[resid_num][2]["secondary_shift"]=secondary_shift
					l_sparta_assign_dic[resid_num][2]["rc_shift"]=rc_shift
					l_sparta_assign_dic[resid_num][2]["ring_current"]=ring_current
					l_sparta_assign_dic[resid_num][2]["electric"]=electric
					l_sparta_assign_dic[resid_num][2]["sigma_error"]=sigma_error




i_plus_minus_one_residue_correction_dict={}
i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_0"]=wang_preceed_nonextpro_0
i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_0"]=wang_next_nonextpro_0
i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_1"]=wang_preceed_nonextpro_1
i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_1"]=wang_next_nonextpro_1
i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_2"]=wang_preceed_nonextpro_2
i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_2"]=wang_next_nonextpro_2
i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_3"]=wang_preceed_nonextpro_3
i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_3"]=wang_next_nonextpro_3
i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_0"]=wright_preceed_nonextpro_0
i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_0"]=wright_next_nonextpro_0
i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_1"]=wright_preceed_nonextpro_1
i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_1"]=wright_next_nonextpro_1
i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_2"]=wright_preceed_nonextpro_2
i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_2"]=wright_next_nonextpro_2
i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_3"]=wright_preceed_nonextpro_3
i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_3"]=wright_next_nonextpro_3
i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_0"]=sw_preceed_nonextpro_0
i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_0"]=sw_next_nonextpro_0
i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_1"]=sw_preceed_nonextpro_1
i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_1"]=sw_next_nonextpro_1
i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_2"]=sw_preceed_nonextpro_2
i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_2"]=sw_next_nonextpro_2
i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_3"]=sw_preceed_nonextpro_3
i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_3"]=sw_next_nonextpro_3
i_plus_minus_one_residue_correction_dict["wright_preceed_nopreceedpro_1"]=wright_preceed_nopreceedpro_1
i_plus_minus_one_residue_correction_dict["wright_next_nopreceedpro_1"]=wright_next_nopreceedpro_1


from backbone_random_coil_and_neighboring_corr import preceed_preceed_res_effect
from backbone_random_coil_and_neighboring_corr import next_next_res_effect


i_plus_minus_two_residue_correction_dict={}
i_plus_minus_two_residue_correction_dict["preceed_preceed_res_effect"]=preceed_preceed_res_effect
i_plus_minus_two_residue_correction_dict["next_next_res_effect"]=next_next_res_effect



from backbone_random_coil_and_neighboring_corr import coil_place
from backbone_random_coil_and_neighboring_corr import beta_place
from backbone_random_coil_and_neighboring_corr import helix_place

from backbone_random_coil_and_neighboring_corr import N_place
from backbone_random_coil_and_neighboring_corr import CO_place
from backbone_random_coil_and_neighboring_corr import CA_place
from backbone_random_coil_and_neighboring_corr import CB_place
from backbone_random_coil_and_neighboring_corr import NH_place
from backbone_random_coil_and_neighboring_corr import HA_place

wright_next_nopreceedpro_1={'A':[[-0.33,-0.33,-0.33],[-0.77,-0.77,-0.77],[-0.17,-0.17,-0.17],[0.00,0.00,0.00],[-0.05,-0.05,-0.05],[-0.03,-0.03,-0.03]], \
	'R':[[-0.14,-0.14,-0.14],[-0.49,-0.49,-0.49],[-0.07,-0.07,-0.07],[0.00,0.00,0.00],[-0.02,-0.02,-0.02],[-0.02,-0.02,-0.02]], \
	'N':[[-0.26,-0.26,-0.26],[-0.66,-0.66,-0.66],[-0.03,-0.03,-0.03],[0.00,0.00,0.00],[-0.03,-0.03,-0.03],[-0.01,-0.01,-0.01]], \
	'D':[[-0.20,-0.20,-0.20],[-0.58,-0.58,-0.58],[0.00,0.00,0.00],[0.00,0.00,0.00],[-0.03,-0.03,-0.03],[-0.01,-0.01,-0.01]], \
	'C':[[-0.26,-0.26,-0.26],[-0.51,-0.51,-0.51],[-0.07,-0.07,-0.07],[0.00,0.00,0.00],[-0.02,-0.02,-0.02],[0.02,0.02,0.02]], \
	'B':[[-0.26,-0.26,-0.26],[-0.51,-0.51,-0.51],[-0.07,-0.07,-0.07],[0.00,0.00,0.00],[-0.02,-0.02,-0.02],[0.02,0.02,0.02]], \
	'Q':[[-0.14,-0.14,-0.14],[-0.48,-0.48,-0.48],[-0.06,-0.06,-0.06],[0.00,0.00,0.00],[-0.02,-0.02,-0.02],[-0.02,-0.02,-0.02]], \
	'E':[[-0.20,-0.20,-0.20],[-0.48,-0.48,-0.48],[-0.08,-0.08,-0.08],[0.00,0.00,0.00],[-0.03,-0.03,-0.03],[-0.02,-0.02,-0.02]], \
	'G':[[0.00,0.00,0.00],[0.00,0.00,0.00],[0.00,0.00,0.00],[0.00,0.00,0.00],[0.00,0.00,0.00],[0.00,0.00,0.00]], \
	'H':[[-0.55,-0.55,-0.55],[-0.65,-0.65,-0.65],[-0.09,-0.09,-0.09],[0.00,0.00,0.00],[-0.04,-0.04,-0.04],[-0.06, -0.06,-0.06]], \
	'I':[[-0.14,-0.14,-0.14],[-0.58,-0.58,-0.58],[0.20,0.20,0.20],[0.00,0.00,0.00],[-0.06,-0.06,-0.06],[-0.02,-0.02,-0.02]], \
	'L':[[-0.14,-0.14,-0.14],[-0.50,-0.50,-0.50],[-0.10,-0.10,-0.10],[0.00,0.00,0.00],[-0.03,-0.03,-0.03],[-0.03,-0.03,-0.03]], \
	'K':[[-0.20,-0.20,-0.20],[-0.50,-0.50,-0.50],[-0.11,-0.11,-0.11],[0.00,0.00,0.00],[-0.03,-0.03,-0.03],[-0.02,-0.02,-0.02]], \
	'M':[[-0.20,-0.20,-0.20],[-0.41,-0.41,-0.41],[0.10,0.10,0.10],[0.00,0.00,0.00],[-0.02,-0.02,-0.02],[-0.01,-0.01,-0.01]], \
	'F':[[-0.49,-0.49,-0.49],[-0.83,-0.83,-0.83],[-0.23,-0.23,-0.23],[0.00,0.00,0.00],[-0.12,-0.12,-0.12],[-0.09,-0.09,-0.09]], \
	'P':[[-0.32,-0.32,-0.32],[-2.84,-2.84,-2.84],[-2.00,-2.00,-2.00],[0.00,0.00,0.00],[-0.18,-0.18,-0.18],[0.11,0.11,0.11]], \
 	'S':[[-0.03,-0.03,-0.03],[-0.40,-0.40,-0.40],[-0.08,-0.08,-0.08],[0.00,0.00,0.00],[-0.03,-0.03,-0.03],[0.02,0.02,0.02]], \
	'T':[[-0.03,-0.03,-0.03],[-0.19,-0.19,-0.19],[-0.04,-0.04,-0.04],[0.00,0.00,0.00],[0.00,0.00,0.00],[0.05,0.05,0.05]], \
	'W':[[-0.26,-0.26,-0.26],[-0.85,-0.85,-0.85],[-0.17,-0.17,-0.17],[0.00,0.00,0.00],[-0.13,-0.13,-0.13],[-0.10,-0.10,-0.10]], \
	'Y':[[-0.43,-0.43,-0.43],[-0.85,-0.85,-0.85],[-0.22,-0.22,-0.22],[0.00,0.00,0.00],[-0.11,-0.11,-0.11],[-0.10,-0.10,-0.10]],\
	'V':[[-0.14,-0.14,-0.14],[-0.57,-0.57,-0.57],[-0.21,-0.21,-0.21],[0.00,0.00,0.00],[-0.05,-0.05,-0.05],[-0.01,-0.01,-0.01]]}
#N_place=0

place_dict={}
place_dict[0]=["N"]
place_dict[1]=["C","CO"]
place_dict[2]=["CA"]
place_dict[3]=["CB"]
place_dict[4]=["H","HN","NH"]
place_dict[5]=["HA","HA1","HA2","HA3"]

coil_place=1
beta_place=0
helix_place=2


stereospecific_dictionary={}
stereospecific_dictionary["HAave"]={"list":["HA","HA1","HA2","HA3"]}
			l_rc_offsets,\
			l_stereospecific_dictionary,\
			l_rcoil_offset_dict,\
	"""


	#TO-DO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# Apply neighboring residue correction
	# Apply disulphide bond correction

	verbose=0
	if verbose>=10: print "Running function caclulate_random_coil_values()"
	if verbose>=10: print "l_rc_offsets=", l_rc_offsets
	if verbose>=1: print "l_rcoil_offset_dict= ",  l_rcoil_offset_dict
	#sys.exit()
	l_rc_residue_list=[]
	l_rc_list_4_offset=[]
	l_rc_assign_dic={}
	side_chain_random_coil_dict={}
	backbone_random_coil_dict={}
	i_minus_one_residue_correction_dict={}
	i_plus_one_residue_correction_dict={}
	i_minus_two_residue_correction_dict={}
	i_plus_two_residue_correction_dict={}

	if len(l_rcoil_offset_dict)>0:
		rcoil_offset_dict_2_use=l_rcoil_offset_dict
	else:
		rcoil_offset_dict_2_use=l_rc_offsets	
	

	if l_find_rci:

		if l_rc_values==1:
			side_chain_random_coil_dict=side_chain_random_coil_values["wishart_side_chain_random_coil_values"]
		if l_rc_values==2:
			side_chain_random_coil_dict=side_chain_random_coil_values["wright_side_chain_random_coil_values"]


		if backbone_random_coil_values==1:
			backbone_random_coil_dict=backbone_random_coil_values_dict["wishart_random_coil_dic"]
		if backbone_random_coil_values==2:
			backbone_random_coil_dict=backbone_random_coil_values_dict["wang_random_coil_dic"]
		if backbone_random_coil_values==3:
			backbone_random_coil_dict=backbone_random_coil_values_dict["lukin_random_coil_dic"]
		if backbone_random_coil_values==4:
			backbone_random_coil_dict=backbone_random_coil_values_dict["wright_random_coil_dic"]
		if backbone_random_coil_values==5:
			backbone_random_coil_dict=backbone_random_coil_values_dict["wright_wishart_random_coil_dic"]
		if backbone_random_coil_values==6:
			backbone_random_coil_dict=backbone_random_coil_values_dict["wishart_wang_lukin_random_coil_dic"]
		if backbone_random_coil_values==7:
			backbone_random_coil_dict=backbone_random_coil_values_dict["wang_wright_random_coil_dic"]
		if backbone_random_coil_values==8:
			backbone_random_coil_dict=backbone_random_coil_values_dict["wright_lukin_random_coil_dic"]
		if backbone_random_coil_values==9:
			backbone_random_coil_dict=backbone_random_coil_values_dict["mean_and_wright_random_coil_dic"]


		if i_plus_minus_one_residue_correction==1:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_0"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_0"]

		if i_plus_minus_one_residue_correction==2:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_1"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_1"]

		if i_plus_minus_one_residue_correction==3:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_2"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_2"]

		if i_plus_minus_one_residue_correction==4:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wang_preceed_nonextpro_3"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wang_next_nonextpro_3"]

		if i_plus_minus_one_residue_correction==5:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_0"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_0"]

		if i_plus_minus_one_residue_correction==6:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_1"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_1"]

		if i_plus_minus_one_residue_correction==7:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_2"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_2"]


		if i_plus_minus_one_residue_correction==8:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_preceed_nonextpro_3"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_next_nonextpro_3"]


		if i_plus_minus_one_residue_correction==9:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_0"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_0"]


		if i_plus_minus_one_residue_correction==10:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_1"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_1"]

		if i_plus_minus_one_residue_correction==11:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_2"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_2"]

		if i_plus_minus_one_residue_correction==12:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["sw_preceed_nonextpro_3"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["sw_next_nonextpro_3"]

		if i_plus_minus_one_residue_correction==13:
			i_minus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_preceed_nopreceedpro_1"]
			i_plus_one_residue_correction_dict=i_plus_minus_one_residue_correction_dict["wright_next_nopreceedpro_1"]


		if i_plus_minus_two_residue_correction==1:
			i_minus_two_residue_correction_dict=i_plus_minus_two_residue_correction_dict["preceed_preceed_res_effect"]
			i_plus_two_residue_correction_dict=i_plus_minus_two_residue_correction_dict["next_next_res_effect"]


		for i in l_protein_sequence_list:
			residue_name, residue_number=i[0],i[1]

			if residue_name in aa_names_full_all_CAP:
				residue_name_1_let=aa_names_full_all_CAP[residue_name]
				residue_name_1_let_ori=residue_name_1_let
				if residue_number in B_Cys_list:
					residue_name_1_let="B"
				if residue_name_1_let in side_chain_random_coil_dict:
					for atom in side_chain_random_coil_dict[residue_name_1_let]:
						rc_value=""
						backbone_rc_value=find_random_coil_value2(atom,residue_name_1_let,backbone_random_coil_dict,place_dict)
						if backbone_rc_value!="" and is_number(backbone_rc_value):
							rc_value=backbone_rc_value
							if verbose>=1: print "Backbone RC %s was found for %s of  %s%s" % (rc_value,atom,residue_name,residue_number)
						if rc_value=="":
							rc_value=side_chain_random_coil_dict[residue_name_1_let][atom]
						if verbose>=10: print residue_name_1_let_ori, atom,  rc_value

						#i_minus_one_residue_correction=0.0
						i_minus_one_residue_correction=find_neighboring_correction(atom,residue_name_1_let,i_minus_one_residue_correction_dict,place_dict,\
																					N_place,\
																					CO_place,\
																					CA_place,\
																					CB_place,\
																					NH_place,\
																					HA_place,\
																					side_chain_H_correction,\
																					side_chain_C_correction,\
																					side_chain_N_correction,\
																					side_chain_protons_to_correct,\
																					side_chain_carbons_to_correct,\
																					side_chain_nitrogens_to_correct)
						if verbose>=10 and i_minus_one_residue_correction!="": print " i_minus_one_residue_correction %s was found for %s of  %s%s" % (i_minus_one_residue_correction,atom,residue_name,residue_number)						

						#i_plus_one_residue_correction=0.0
						i_plus_one_residue_correction=find_neighboring_correction(atom,residue_name_1_let,i_plus_one_residue_correction_dict,place_dict,\
																					N_place,\
																					CO_place,\
																					CA_place,\
																					CB_place,\
																					NH_place,\
																					HA_place,\
																					side_chain_H_correction,\
																					side_chain_C_correction,\
																					side_chain_N_correction,\
																					side_chain_protons_to_correct,\
																					side_chain_carbons_to_correct,\
																					side_chain_nitrogens_to_correct)
						if verbose>=10 and i_plus_one_residue_correction!="": print " i_plus_one_residue_correction %s was found for %s of  %s%s" % (i_plus_one_residue_correction,atom,residue_name,residue_number)						



						#i_minus_two_residue_correction=0.0
						i_minus_two_residue_correction=find_neighboring_correction(atom,residue_name_1_let,i_minus_two_residue_correction_dict,place_dict,\
																					N_place,\
																					CO_place,\
																					CA_place,\
																					CB_place,\
																					NH_place,\
																					HA_place,\
																					side_chain_H_correction,\
																					side_chain_C_correction,\
																					side_chain_N_correction,\
																					side_chain_protons_to_correct,\
																					side_chain_carbons_to_correct,\
																					side_chain_nitrogens_to_correct)
						if verbose>=10 and i_minus_two_residue_correction!="": print " i_minus_two_residue_correction %s was found for %s of  %s%s" % (i_minus_two_residue_correction,atom,residue_name,residue_number)						

						#i_plus_two_residue_correction=0.0
						i_plus_two_residue_correction=find_neighboring_correction(atom,residue_name_1_let,i_plus_two_residue_correction_dict,place_dict,\
																					N_place,\
																					CO_place,\
																					CA_place,\
																					CB_place,\
																					NH_place,\
																					HA_place,\
																					side_chain_H_correction,\
																					side_chain_C_correction,\
																					side_chain_N_correction,\
																					side_chain_protons_to_correct,\
																					side_chain_carbons_to_correct,\
																					side_chain_nitrogens_to_correct)
						if verbose>=10 and i_plus_two_residue_correction!="": print " i_plus_two_residue_correction %s was found for %s of  %s%s" % (i_plus_two_residue_correction,atom,residue_name,residue_number)						

						rc_value+=i_minus_one_residue_correction+i_plus_one_residue_correction+i_minus_two_residue_correction+i_plus_two_residue_correction
					
						if residue_number not in l_rc_assign_dic:
							l_rc_assign_dic[residue_number]=[residue_name_1_let_ori,{},{}]
							l_rc_residue_list+=[[residue_number, '%s' % residue_name_1_let_ori, [], [], [], [], [], []]]
							l_rc_list_4_offset+=[[residue_name,residue_number]]

						l_rc_assign_dic[residue_number][2]["res_name"]=residue_name_1_let_ori
						if verbose>=1: print residue_number, residue_name, residue_name_1_let_ori, atom, rc_value
						rc_offset=""
						#rc_offset_found=0
						if residue_name_1_let_ori in rcoil_offset_dict_2_use:
							if atom in rcoil_offset_dict_2_use[residue_name_1_let_ori]:
								rc_offset= rcoil_offset_dict_2_use[residue_name_1_let_ori][atom]
							else:
								non_stereospecific_atom=""
								for non_stereo_atom in l_stereospecific_dictionary:
									if verbose>=10: print "non_stereo_atom=", non_stereo_atom
									stereo_atom_list=l_stereospecific_dictionary[non_stereo_atom]["list"]
									for stereo_atom  in stereo_atom_list:
										if verbose>=10: print "stereo_atom=", stereo_atom, rcoil_offset_dict_2_use[residue_name_1_let_ori]
										if stereo_atom == atom and non_stereospecific_atom=="":
											if non_stereo_atom in rcoil_offset_dict_2_use[residue_name_1_let_ori]:
												rc_offset= rcoil_offset_dict_2_use[residue_name_1_let_ori][non_stereo_atom]
												non_stereospecific_atom=stereo_atom
											#elif stereo_atom in  rcoil_offset_dict_2_use[residue_name_1_let_ori] and non_stereospecific_atom!="":
											#	print "ERROR!!!  Something is wrong with stereo specific dictionary ", l_stereospecific_dictionary
								

						if is_number(rc_offset):
							rc_value+=float(rc_offset)
							#rc_offset_found=1
							if verbose>=10: print "RC offset %s was found for residue %s and atom %s " % (rc_offset,residue_name,atom)
						else:
							if verbose>=10: print "Error!!! RC offset was not found for residue %s and atom %s " % (residue_name,atom)

						l_rc_assign_dic[residue_number][1][atom]=rc_value							

				else:
					print "Warning!!! Residue name %s is not in wishart_randomcoil" % residue_name_1_let
			else:
				print "Warning!!! Residue name %s is not in aa_names_full_all_CAP" % residue_name
				residue_name_1_let="X"


		#for residue_name in side_chain_random_coil_dict:
		#	for shift in side_chain_random_coil_dict[residue_name]:
		#		if verbose>=10: print residue_name, shift, side_chain_random_coil_dict[residue_name][shift]
	#print l_protein_sequence_list

	#TO-DO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# Apply neighboring residue correction
	# Apply disulphide bond correction

	non_stereospecific(stereospecific_dictionary,l_rc_assign_dic)


	l_pdb_dict["rci"]={}
	l_pdb_dict["rci"]["rc_assign_dic"]=l_rc_assign_dic
	l_pdb_dict["rci"]["rc_list_4_offset"]=l_rc_list_4_offset	
	l_pdb_dict["rci"]["rc_residue_list"]=l_rc_residue_list
	l_pdb_dict["rci"]["chain"]="RCI"
	l_pdb_dict["rci"]["pdb_full"]="RCI"
	l_pdb_dict["rci"]["pdb_dir"]="RCI"
	l_pdb_dict["rci"]["project_dir"]="RCI"
	l_pdb_dict["rci"]["original_pdb"]="RCI"
	l_pdb_dict["rci"]["original_pdb_base_name"]="RCI"
	l_pdb_dict["rci"]["original_pdb_base_name2"]="RCI"
	l_pdb_list=["rci"]
	debug=0
	if debug==1:
		#print l_rc_assign_dic
		print "Exiting after function find_random_coil_values()"
		sys.exit()
	return l_rc_assign_dic,l_rc_list_4_offset, l_rc_residue_list,l_pdb_list


def mean_coef(l_coeff_dict):
	"""
	Function to calculate mean weighting coefficient
	"""
	all_atoms_trigger="111111"
	coef_diction=l_coeff_dict[all_atoms_trigger]
	l_atom_list=["CA","HA","CO","CB","N","H"]
	l_coef_list=[]
	for i in l_atom_list:
		coef=coef_diction[i]
		l_coef_list+=[coef]
	l_coef_mean=(sum(l_coef_list)/len(l_coef_list))	
	return(l_coef_mean)



def coef_dic(l_file_w_coef):
	"""
	Function to read file with coefficients and output dictionary with coefficients
	"""
	dic_coef={}
	l_file_w_coef_read=read_file(l_file_w_coef)
	for i in l_file_w_coef_read:
		j=split(i)
		CA_trig=j[2]
		CB_trig=j[4]
		CO_trig=j[6]
		N_trig=j[8]
		HA_trig=j[10]
		NH_trig=j[12]		
		coef_trig="%s%s%s%s%s%s" % (CA_trig,CB_trig,CO_trig,NH_trig,N_trig,HA_trig)	
		CA_coef=float(j[21])
		CO_coef=float(j[23])
		CB_coef=float(j[25])
		N_coef=float(j[27])
		HA_coef=float(j[29])
		NH_coef=float(j[31])
		dic_coef[coef_trig]={"CA":CA_coef,"CO":CO_coef,"C":CO_coef,"CB":CB_coef,"N":N_coef,"HA":HA_coef,"NH":NH_coef,"H":NH_coef,"HN":NH_coef}

	dic_coef['000000']={"CA":0.0,"CO":0.0,"C":0.0,"CB":0.0,"N":0.0,"HA":0.0,"NH":0.0,"H":0.0,"HN":0.0}
	#print dic_coef
	return(dic_coef)


def build_shift_class_lookup(l_shift_class_dict,l_shift_class_description_dict):
	"""
	Build look up dictionary for shift classes
	"""
	verbose=0
	if verbose>=0: print "Running function build_shift_class_lookup()"
	l_shift_class_lookup_dict={}
	for shift_class in shift_class_description_dict:
		l_found=0
		l_shift_class_lookup_dict[shift_class]=[]
		for shift in shift_class_dict:
			shift_list=shift_class_dict[shift ]
			if shift_class in shift_list:
				l_found=1
				l_shift_class_lookup_dict[shift_class]+=[shift]
		if l_found==0:
			print "Warning!!! Shift class %s was not found in  shift_class_dict" % shift_class

	for i in l_shift_class_lookup_dict:
		shift_list=l_shift_class_lookup_dict[i]
		print "dic['%s']=" % i,shift_list 
	debug=0
	if debug==1:
		print "Exiting after function build_shift_class_lookup()"
		sys.exit()

	return l_shift_class_lookup_dict



def make_rci_plots(\
		l_pdb_list,\
		l_pdb_dict,\
		l_zodb,\
		l_rci_result_dir_full,\
		l_gnuplot,\
		l_shift_class_lookup,\
		l_make_rci_pics,\
		l_make_rci_images,\
		l_database_file,\
		l_runname,\
		l_find_rc_order_parameter,\
		l_find_MD_RMSF,\
		l_find_NMR_RMSF,\
		l_find_ASA,\
		l_find_B_factor,\
		l_rci_output_dict,\
		):
	"""
	Averaging after combining shifts	
	"""
	verbose=0
	if verbose>=1: print "Running function make_rci_plots()"
	#l_rci_result_dir_full="%s/%s" % (l_project_dir_full_path,l_rci_result_dir)
	if verbose>=1: print "runname=", runname
	if l_make_rci_pics>0:
		make_dir_no_remove(l_rci_result_dir_full)
		pics_dir="%s/pics" % l_rci_result_dir_full
		make_dir_no_remove(pics_dir)
		data_dir="%s/data" % pics_dir
		make_dir_no_remove(data_dir)
		if l_make_rci_images:
			images_dir="%s/images" % pics_dir
			make_dir_no_remove(images_dir)

		if l_zodb:
			storage = FileStorage(l_database_file)
			db = DB(storage)		
			connection = db.open()
			main_database = connection.root()
			l_pdb_dict=main_database["pdb_dict"]

		#for shift_class in l_pdb_dict["rci"]["rc"]["protein_shift_classes"]:
		for shift_class in l_rci_output_dict:
			file_base_name=l_rci_output_dict[shift_class]["name"]
			#file_base_name= shift_class.replace(" ","_")
			data_file="%s/%s_RCI.txt" % (data_dir,file_base_name)
			bb_data_file="%s/%s_Backbone_RCI.txt" % (data_dir,file_base_name)
			bb_rmsd_file="%s/%s_Backbone_RMSD.txt" % (data_dir,file_base_name)
			if verbose>=1: print "Processing shift class", shift_class
			if "rci_text" in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][ shift_class]:
				rci_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["rci_text"]
				bb_rci_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["bb_rci_text"]	
				bb_rmsd_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["bb_rmsd_text"]	
				write_file(rci_text,data_file)
				write_file(bb_rci_text,bb_data_file)
				write_file(bb_rmsd_text,bb_rmsd_file)
				if verbose>=1: print "Writing RCI data file ", data_file
				if l_make_rci_images:
					l_image_file="%s/%s.ps" % (images_dir,file_base_name)
					l_title="%s vs residue number" % file_base_name
					l_Y_label="%s" % file_base_name
					l_X_label="Residue number"
					l_description="%s" % file_base_name
					gnuplot_bars(data_file,l_title,l_Y_label,l_X_label,l_image_file,l_description)
			else:
				if verbose>=1: print "Warning!!! RCI TEXT is not available for protein class ",  shift_class


			if l_find_rc_order_parameter==1 and "rc_order_parameter_text" in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][ shift_class]:
				rc_order_parameter_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["rc_order_parameter_text"]
				rc_order_parameter_data_file="%s/%s_RC_Order_Parameters.txt" % (data_dir,file_base_name)
				write_file(rc_order_parameter_text,rc_order_parameter_data_file)
				if verbose>=1: print "Writing rc_order_parameter data file ", rc_order_parameter_data_file
			else:
				if verbose>=1: print "Warning!!! RSS TEXT is not available for protein class ",  shift_class

			if l_find_MD_RMSF==1 and "MD_RMSF_text" in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][ shift_class]:
				MD_RMSF_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["MD_RMSF_text"]
				MD_RMSF_data_file="%s/%s_MD_RMSD.txt" % (data_dir,file_base_name)
				write_file(MD_RMSF_text,MD_RMSF_data_file)
				if verbose>=1: print "Writing MD_RMSF data file ", MD_RMSF_data_file
			else:
				if verbose>=1: print "Warning!!! RSS TEXT is not available for protein class ",  shift_class



			if l_find_NMR_RMSF==1 and "NMR_RMSF_text" in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][ shift_class]:
				NMR_RMSF_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["NMR_RMSF_text"]
				NMR_RMSF_data_file="%s/%s_NMR_RMSD.txt" % (data_dir,file_base_name)
				write_file(NMR_RMSF_text,NMR_RMSF_data_file)
				if verbose>=1: print "Writing NMR_RMSF data file ", NMR_RMSF_data_file
			else:
				if verbose>=1: print "Warning!!! RSS TEXT is not available for protein class ",  shift_class



			if l_find_ASA==1 and "ASA_text" in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][ shift_class]:
				ASA_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["ASA_text"]
				ASA_data_file="%s/ASAf.txt" % (data_dir)
				write_file(ASA_text,ASA_data_file)
				if verbose>=1: print "Writing ASA data file ", ASA_data_file
			else:
				if verbose>=1: print "Warning!!! RSS TEXT is not available for protein class ",  shift_class


			if l_find_B_factor==1 and "normalized_B_factor_text" in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][ shift_class]:
				normalized_B_factor_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["normalized_B_factor_text"]
				normalized_B_factor_data_file="%s/%s_normalized_mean_B_factor.txt" % (data_dir,file_base_name)

				write_file(normalized_B_factor_text,normalized_B_factor_data_file)
				#print "normalized_B_factor_data_file=", normalized_B_factor_data_file
				#sys.exit()
				if verbose>=1: print "Writing normalized_B_factor data file ", normalized_B_factor_data_file

			if l_find_B_factor==1 and "absolute_B_factor_text" in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][ shift_class]:
				absolute_B_factor_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["absolute_B_factor_text"]
				absolute_B_factor_data_file="%s/%s_absolute_mean_B_factor.txt" % (data_dir,file_base_name)
				write_file(absolute_B_factor_text,absolute_B_factor_data_file)
				if verbose>=1: print "Writing absolute_B_factor data file ", absolute_B_factor_data_file




			else:
				if verbose>=1: print "Warning!!! RSS TEXT is not available for protein class ",  shift_class

		if l_zodb:
			l_pdb_dict._p_changed = True		
			transaction.savepoint(True)
			transaction.commit()
			#transaction.get().commit() 
			connection.close()
			db.close()
			storage.close()
			l_pdb_dict={}
	debug=0
	if debug==1:
		print "Exiting after function make_rci_plots()"
		sys.exit()

	return

def final_smoothing(\
		l_pdb_list,\
		l_pdb_dict,\
		l_zodb,\
		l_averaging,\
		l_shift_class_lookup,\
		l_rc_residue_list,\
		l_rci_dict,\
		l_database_file,\
		l_rci_out_offset,\
		l_runname,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_runname_result_dict,\
		l_do_gridsearch1,\
		l_rci_method,\
		l_sc_bb_combine,\
		):
	"""
	Averaging after combining shifts	


	l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["rci_text"]+="%s %s" % (residue_number,double_smoothed_value) +linesep



					if l_rci_method not in [15,16,17,18,19,20,21,22,23,24,25,26,27,28]:
						l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][rci_shift_class_ori]["combined_abs_diff"]=value_abs
					else:
						bb_rci_value=value_abs
						l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][rci_shift_class_ori]["bb_rci_abs_diff"]=value_abs




	"""
	verbose=0
	if verbose>=1: print "Running function final_smoothing()"
	if verbose>=1: print "l_averaging=", 3
	#print "l_rci_dict=", l_rci_dict
	if l_zodb:
		storage = FileStorage(l_database_file)
		db = DB(storage)		
		connection = db.open()
		main_database = connection.root()
		l_pdb_dict=main_database["pdb_dict"]

	for shift_class in l_shift_class_lookup:
		if verbose>=10: print "shift_class =", shift_class , shift_list	
		shift_class_ori=shift_class
		for i in l_rc_residue_list:
			residue_number =i[0]
			residue_name=i[1]
			if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type


			if l_remove_stereo in [1,2]:
				if (l_remove_stereo==1 and residue_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and residue_name in l_res4gridsearch_list_dict and residue_name in l_res4gridsearch_list):
					stereo=l_res4gridsearch_list_dict[residue_name]["stereo"]
					if verbose>10: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (residue_name,residue_number,stereo)
				
					if stereo==0 and  shift_class in l_shiftclass4gridsearch_list:
						shift_class1="%s_ave" % shift_class
						shift_class2="%save" % shift_class
						#print "Place 1"
						if shift_class1 in l_shift_class_lookup:
							shift_class=shift_class1
						elif shift_class2 in l_shift_class_lookup:
							shift_class=shift_class2
						else: 	
							if verbose>=0: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)


			if  residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_class in l_rci_dict:

				if verbose>=10: print residue_number, shift_class

				if shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]:

					if "bb_rci_abs_diff" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]:

						combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["bb_rci_abs_diff"]

						if verbose>=10: print "final_smoothing(): shift_class= ", shift_class, "residue_number= ", residue_number, "combined_abs_diff= ", combined_abs_diff

						value_list=[combined_abs_diff]

						res_list=[]
						if l_averaging in [3,5]:
							res_minus_1=residue_number-1
							res_plus_1=residue_number+1
							res_list=[res_minus_1,res_plus_1]

						if l_averaging in  [5]:
							res_minus_2=residue_number-2
							res_plus_2=residue_number+2
							res_list+=[res_minus_1,res_plus_1]

						for neighboring_residue in res_list:
							if neighboring_residue in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"]:
								if "bb_rci_abs_diff" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]:
									combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]["bb_rci_abs_diff"]							
									if is_number(combined_abs_diff):
										value_list+=[combined_abs_diff]
								else:
									if verbose>=10: print "Warning!!! 	combined_abs_diff is not in l_pdb_dict for shift_class %s and residue_number %s " % (shift_class,neighboring_residue)

						if len(value_list)>0:
							if verbose>=10: print "Smoothing  for  shift_class= ", shift_class, "residue_number= ", residue_number, "value_list=", value_list
							double_smoothed_value=stats.lmean(value_list)
							if verbose>=10: print "Smoothed value =", double_smoothed_value, value_list, linesep

							if l_do_gridsearch1>0:
								if verbose>=10: print "Place 1"
								if residue_number not in l_runname_result_dict:
									l_runname_result_dict[residue_number]={"res_name":residue_name,"residue_shift_classes":{}}
								if shift_class_ori not in l_runname_result_dict[residue_number]["residue_shift_classes"]:
									l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]={"bb_rci_values":double_smoothed_value,"rci_values":double_smoothed_value}
									if verbose>=10: print residue_number, shift_class_ori, "bb_rci_values=", double_smoothed_value
								if l_rci_method in [15,16,17,18,19,20,21,22,23,24,25,26,27,28]:
									if "bb_rci_values" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["bb_rci_values"]={}
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["bb_rci_values"]=double_smoothed_value
									if verbose>=10: print residue_number, shift_class_ori, "bb_rci_values=", double_smoothed_value


									



							else:

								if "bb_rci_values" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["bb_rci_values"]={}
								l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["bb_rci_values"][l_runname]=double_smoothed_value
								if verbose>=10: print residue_number, shift_class_ori, "bb_rci_values=", double_smoothed_value

								if "rci_values" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["rci_values"]={}
								l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["rci_values"][l_runname]=double_smoothed_value



								#l_runname_result_dict
								if shift_class not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"]:
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]={}
								if "rci_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]:
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["rci_text"]=""

								l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["rci_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,double_smoothed_value,residue_name) +linesep

					else:
						if verbose>=10: print "Warning!!! combined_abs_diff is not in l_pdb_dict for RCI for shift class %s and residue %s " % (shift_class,residue_number)
				else:
					if verbose>=10: print "Warning!!! Shift class %s is not in 	l_pdb_dict for RCI for residue %s " % (shift_class,residue_number)

	if l_zodb:
		l_pdb_dict._p_changed = True		
		transaction.savepoint(True)
		transaction.commit()
		#transaction.get().commit() 
		connection.close()
		db.close()
		storage.close()
		l_pdb_dict={}

	debug=0
	if debug==1:
		print "Exiting after function final_smoothing()"
		sys.exit()

	return






def double_final_smoothing(\
		l_pdb_list,\
		l_pdb_dict,\
		l_zodb,\
		l_averaging,\
		l_shift_class_lookup,\
		l_rc_residue_list,\
		l_rci_dict,\
		l_database_file,\
		l_rci_out_offset,\
		l_runname,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_do_gridsearch1,\
		l_runname_result_dict,\
		l_rci_method,\
		l_sc_bb_combine,\
		l_sc_rci_weight,\
		l_bb_rci_weight,\
		find_rc_order_parameter,\
		find_MD_RMSF,\
		find_NMR_RMSF,\
		find_ASA,\
		find_B_factor,\
		):
	"""
	Averaging after combining shifts	

	l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["rci_text"]+="%s %s" % (residue_number,double_smoothed_value) +linesep
	"""
	verbose=0
	if verbose>=1: print "Running function double final_smoothing()"
	if l_zodb:
		storage = FileStorage(l_database_file)
		db = DB(storage)		
		connection = db.open()
		main_database = connection.root()
		l_pdb_dict=main_database["pdb_dict"]

	for shift_class in l_shift_class_lookup:
		if verbose>=10: print "shift_class =", shift_class , shift_list	
		shift_class_ori=shift_class
		for i in l_rc_residue_list:
			residue_number =i[0]
			residue_name =i[1]
			if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type



			if l_remove_stereo in [1,2] and l_do_gridsearch1>0:
				if (l_remove_stereo==1 and residue_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and residue_name in l_res4gridsearch_list_dict and residue_name in l_res4gridsearch_list):
					stereo=l_res4gridsearch_list_dict[residue_name]["stereo"]
					if verbose>=10: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (residue_name,residue_number,stereo)
					shift_class=shift_class_ori

					if stereo==0 and  shift_class in l_shiftclass4gridsearch_list:
						shift_class1="%s_ave" % shift_class
						shift_class2="%save" % shift_class
						#print "Place 1"
						if shift_class1 in l_shift_class_lookup:
							shift_class=shift_class1
						elif shift_class2 in l_shift_class_lookup:
							shift_class=shift_class2
						else: 	
							if verbose>=0: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)







			if  residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_class not in l_rci_dict:
				if shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]:
					if "combined_abs_diff" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]:
						combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["combined_abs_diff"]
						if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "combined_abs_diff= ", combined_abs_diff
						value_list=[combined_abs_diff]
						res_list=[]
						if l_averaging in [3,6,5,10]:
							res_minus_1=residue_number-1
							res_plus_1=residue_number+1
							res_list=[res_minus_1,res_plus_1]

						if l_averaging in  [5,10]:
							res_minus_2=residue_number-2
							res_plus_2=residue_number+2
							res_list+=[res_minus_2,res_plus_2]

						for neighboring_residue in res_list:
							if neighboring_residue in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"]:
								if "combined_abs_diff" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]:
									combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]["combined_abs_diff"]							
									if is_number(combined_abs_diff):
										value_list+=[combined_abs_diff]
								else:
									if verbose>=10: print "Warning!!! 	combined_abs_diff is not in l_pdb_dict for shift_class %s and residue_number %s " % (shift_class,neighboring_residue)

						if len(value_list)>0:
							if verbose>=10: print "Smoothing  for  shift_class= ", shift_class, "residue_number= ", residue_number, "value_list=", value_list
							smoothed_value=stats.lmean(value_list)
							
							if verbose>=10: 
								if shift_class=="Side_chain" and residue_number==25:
									print "Smoothed value =", smoothed_value, value_list, linesep
							#if shift_type not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"]:
							#	l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]={}
							l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["smoothed_value"]=smoothed_value

							#if shift_class not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"]:
							#	l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]={}
							#if "rci_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]:
							#	l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["rci_text"]=""

							#l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["rci_text"]+="%s %s" % (residue_number,smoothed_value) +linesep

					else:
						if verbose>=10: print "Warning!!! combined_abs_diff is not in l_pdb_dict for RCI for shift class %s and residue %s " % (shift_class,residue_number)
				else:
					if verbose>=10: print "Warning!!! Shift class %s is not in 	l_pdb_dict for RCI for residue %s " % (shift_class,residue_number)






	for shift_class in l_shift_class_lookup:
		shift_class_ori=shift_class
		if verbose>=10: print "shift_class =", shift_class , shift_list	
		for i in l_rc_residue_list:
			residue_number =i[0]
			residue_name=i[1]

			if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type

			if l_remove_stereo in [1,2] and l_do_gridsearch1>0:

				if (l_remove_stereo==1 and residue_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and residue_name in l_res4gridsearch_list_dict and residue_name in l_res4gridsearch_list):
					stereo=l_res4gridsearch_list_dict[residue_name]["stereo"]
					if verbose>=10: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (residue_name,residue_number,stereo)
					shift_class=shift_class_ori

					if stereo==0 and  shift_class in l_shiftclass4gridsearch_list:
						shift_class1="%s_ave" % shift_class
						shift_class2="%save" % shift_class
						#print "Place 1"
						if shift_class1 in l_shift_class_lookup:
							shift_class=shift_class1
						elif shift_class2 in l_shift_class_lookup:
							shift_class=shift_class2
						else: 	
							if verbose>=0: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)


			if  residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_class not in l_rci_dict:
				if shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]:
					if "smoothed_value" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]:
						d_smoothed_value=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["smoothed_value"]
						if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "combined_abs_diff= ", d_smoothed_value
						debug=0
						if debug==1:
							if residue_number==20 and shift_class_ori in ["Side_chain"]:
									print residue_number, "d_combined_abs_diff=", d_smoothed_value
						d_value_list=[d_smoothed_value]
						res_list=[]
						#l_averaging=5
						if l_averaging in [6,10]:
							res_minus_1=residue_number-1
							res_plus_1=residue_number+1
							res_list=[res_minus_1,res_plus_1]

						if l_averaging in  [10]:
							res_minus_2=residue_number-2
							res_plus_2=residue_number+2
							res_list+=[res_minus_1,res_plus_1]

						for neighboring_residue in res_list:
							if neighboring_residue in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"]:
								if "smoothed_value" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]:
									d_smoothed_value=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]["smoothed_value"]							
									if is_number(d_smoothed_value):
										d_value_list+=[d_smoothed_value]
								else:
									if verbose>=10 and shift_class=="Side_chain": 
										print "Warning!!! 	combined_abs_diff is not in l_pdb_dict for shift_class %s and residue %s " % (shift_class,neighboring_residue)


						if len(d_value_list)>0:
							if verbose>=10 and  shift_class=="Side_chain": 
								print "Smoothing  for  shift_class= ", shift_class, "residue_number= ", residue_number, residue_name, "d_value_list=", d_value_list

							double_smoothed_value=stats.lmean(d_value_list)
							hydroflex=hydrophobicity2flexibility(hydrophob_dic,residue_name,hydro_div)
							if hydro==2:
								if hydroflex!="" and (hydroflex*hcoef)>double_smoothed_value:
									print "Replacing RCI value %s with hydroflex % s for residue %s%s" % (double_smoothed_value,hydroflex,residue_name,residue_number)
									double_smoothed_value=hydroflex
							bb_rci_value=sc_rci_weight=bb_rci_weight=""

							if l_sc_bb_combine==2 and l_rci_method in [15,16,17,18,19,20,21,22,23,24,25,26,27,28]:
								#print "Place 3"
								if "RCI" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"] and "bb_rci_values" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]["RCI"]:

									if  l_do_gridsearch1>0:
										bb_rci_value=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]["RCI"]["bb_rci_values"]
									else:
										bb_rci_value=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]["RCI"]["bb_rci_values"][l_runname]



								elif  "RCI" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]:
									if verbose>=10: print "Place 5"
								elif "bb_rci_values" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]["RCI"]:
									if verbose>=10: print "Place 6", l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]["RCI"]
									if verbose>=1: print "WARNING: bb_rci_values are not available for  ", residue_number, l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]["RCI"]
									#sys.exit()
									

								if is_number(bb_rci_value):
									#print "Place 4"
									if "sc_rci_weight" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										sc_rci_weight=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["sc_rci_weight"]

									if is_number(sc_rci_weight)==0 and is_number(l_sc_rci_weight):
										sc_rci_weight=l_sc_rci_weight
									if is_number(sc_rci_weight)==0:
										print "Warning!!! sc_rci_weight is not numerical and is", [sc_rci_weight]


									if "bb_rci_weight" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										bb_rci_weight=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["bb_rci_weight"]

									if is_number(bb_rci_weight)==0 and is_number(l_bb_rci_weight):
										bb_rci_weight=l_bb_rci_weight
									if is_number(bb_rci_weight)==0:
										print "Warning!!! bb_rci_weight is not numerical and is", [bb_rci_weight]

								
									if is_number(bb_rci_weight) and is_number(sc_rci_weight):
										#l_bb_rci_weight=100.0
										coeff_ave=(l_sc_rci_weight+l_bb_rci_weight)/2.0
										bb_coeff_ave=(l_sc_rci_weight+100.0)/2.0
										if coeff_ave==0 or l_bb_rci_weight==0 or l_sc_rci_weight==0:
											coeff_ave=1

										#bb_rci_value2

										sc_rci_value=double_smoothed_value
										double_smoothed_value=((l_sc_rci_weight*sc_rci_value) + (l_bb_rci_weight*bb_rci_value))/coeff_ave
										#double_smoothed_value=((l_sc_rci_weight*sc_rci_value) + (l_bb_rci_weight*bb_rci_value))
	
										if hydro==1:
											double_smoothed_value=hydroflex	


										
										if find_rc_order_parameter:
											rc_order_parameter=get_rc_order_parameter(double_smoothed_value)
										if find_MD_RMSF:
											MD_RMSF=get_MD_RMSF(double_smoothed_value)
										if find_NMR_RMSF:
											NMR_RMSF=get_NMR_RMSF(double_smoothed_value)
										if find_ASA:
											ASA=get_ASA(double_smoothed_value)
										if find_B_factor:
											normalized_B_factor,absolute_B_factor=get_B_factor(double_smoothed_value)
										



										if verbose>=10: print "Combined RCI for residue %s and shift class %s is" % (residue_number,shift_class_ori),  double_smoothed_value
										if verbose>=10 and shift_class_ori=="Side_chain" and residue_number==73:
											print "residue_number=", residue_number
											print "l_sc_rci_weight=", l_sc_rci_weight
											print "sc_rci_value=", sc_rci_value
											print "l_bb_rci_weight=", l_bb_rci_weight
											print "bb_rci_value=", bb_rci_value
											print "coeff_ave=", coeff_ave
											print "RCI value, double_smoothed_value=", double_smoothed_value
											print linesep, linesep


								else:
									if verbose>=10: print "Warning!!!! bb_rci_values for residue %s and shift_class %s are not numerical and is  " % (residue_number,shift_class_ori), [bb_rci_values]
							


							if residue_number==20 and shift_class_ori in ["Side_chain"]:
								if verbose>=10: print residue_number, "Side_chain RCI=", double_smoothed_value
							debug=0
							if debug==1:
								if residue_number==24 and shift_class=="Side_chain":
									print "DEBUGGING: shift_class is %s, residue number is %s, runname is %s, rci value is %s" % (shift_class,residue_number,l_runname,double_smoothed_value)

							if verbose>=10: print "Smoothed value =", double_smoothed_value, d_value_list, linesep

							if l_do_gridsearch1>0:


								if residue_number not in l_runname_result_dict:
									l_runname_result_dict[residue_number]={"res_name":residue_name,"residue_shift_classes":{}}
								if shift_class_ori not in l_runname_result_dict[residue_number]["residue_shift_classes"]:
									l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]={"rci_values":double_smoothed_value}
									l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]={"bb_rci_values":bb_rci_value}
									if find_rc_order_parameter:
										l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]["rc_order_parameter"]=rc_order_parameter
									if find_MD_RMSF:
										l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]["MD_RMSF"]=MD_RMSF
									if find_NMR_RMSF:
										l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]["NMR_RMSF"]=NMR_RMSF
									if find_ASA:
										l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]["ASA"]=ASA
									if find_B_factor:
										l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]["normalized_B_factor"]=normalized_B_factor
										l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]["absolute_B_factor"]=absolute_B_factor

							else:



								if "rci_values" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["rci_values"]={}
								l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["rci_values"][l_runname]=double_smoothed_value
								#l_runname_result_dict

								if shift_class not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"]:
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]={}
								if "rci_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]:
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["rci_text"]=""
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["bb_rci_text"]=""
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["bb_rmsd_text"]=""
								l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["rci_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,double_smoothed_value,residue_name) +linesep

								#bb_rci_value2show=(bb_rci_value*100.0)/bb_coeff_ave
								#bb_rci_value2show=(bb_rci_value*l_bb_rci_weight)
								if is_number(bb_rci_value):
									bb_rci_value2show=(bb_rci_value*l_bb_rci_weight)/2.0
									bb_rmsd_value=bb_rci_value*28.3
								else:
									if shift_class_ori=="Side_chain":
										#print "l_runname = ", l_runname
										print "Warning!!! bb_rci_value is not numerical for residue %s likely due to missing backbone assignment and is " % (residue_number), [bb_rci_value]
										print "RCI prediction for this residue will be unreliable. You should not use incomplete assignments to calculate RCI"



								l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["bb_rci_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,bb_rci_value2show,residue_name) +linesep

								l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["bb_rmsd_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,bb_rmsd_value,residue_name) +linesep

								if find_rc_order_parameter:
									if "rc_order_parameter" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["rc_order_parameter"]={}
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["rc_order_parameter"][l_runname]=rc_order_parameter

									if "rc_order_parameter_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["rc_order_parameter_text"]=""
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["rc_order_parameter_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,rc_order_parameter,residue_name) +linesep


								if find_MD_RMSF:
									if "MD_RMSF" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["MD_RMSF"]={}
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["MD_RMSF"][l_runname]=MD_RMSF

									if "MD_RMSF_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["MD_RMSF_text"]=""
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["MD_RMSF_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,MD_RMSF,residue_name) +linesep



								if find_NMR_RMSF:
									if "NMR_RMSF" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["NMR_RMSF"]={}
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["NMR_RMSF"][l_runname]=NMR_RMSF

									if "NMR_RMSF_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["NMR_RMSF_text"]=""
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["NMR_RMSF_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,NMR_RMSF,residue_name) +linesep


								if find_ASA:
									if "ASA" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["ASA"]={}
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["ASA"][l_runname]=ASA

									if "ASA_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["ASA_text"]=""
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["ASA_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,ASA,residue_name) +linesep


								if find_B_factor:
									if "normalized_B_factor" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["normalized_B_factor"]={}
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["normalized_B_factor"][l_runname]=normalized_B_factor

									if "normalized_B_factor_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["normalized_B_factor_text"]=""
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["normalized_B_factor_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,normalized_B_factor,residue_name) +linesep



									#l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]["normalized_B_factor"]=normalized_B_factor




									if "absolute_B_factor" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["absolute_B_factor"]={}
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["absolute_B_factor"][l_runname]=absolute_B_factor

									if "absolute_B_factor_text" not in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]:
										l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["absolute_B_factor_text"]=""
									l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class_ori]["absolute_B_factor_text"]+="%s %s %s" % (residue_number+l_rci_out_offset,absolute_B_factor,residue_name) +linesep


									#l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class_ori]["absolute_B_factor"]=absolute_B_factor



					else:
						if verbose>=10: print "Warning!!! combined_abs_diff is not in l_pdb_dict for RCI for shift class %s and residue %s " % (shift_class,residue_number)
				else:
					if verbose>=10: print "Warning!!! Shift class %s is not in 	l_pdb_dict for RCI for residue %s " % (shift_class,residue_number)






	if l_zodb:
		l_pdb_dict._p_changed = True		
		transaction.savepoint(True)
		transaction.commit()
		#transaction.get().commit() 
		connection.close()
		db.close()
		storage.close()
		l_pdb_dict={}

	debug=0
	if debug==1:
		print "Exiting after function double smoothing()"
		sys.exit()

	return


def get_rc_order_parameter(RCI_value):
	"""
	Get RC order parameter
	"""
	RCI_value2use=RCI_value#*100.0
	max_RCI=1.0#*100.0
	min_RCI=0.001#*100.0
	#rc_order_parameter=math.log1p((RCI_value2use)/((max_RCI)-(min_RCI)))
	#rc_order_parameter=math.log1p(RCI_value)/(math.log1p(max_RCI)-math.log1p(min_RCI))

	#rc_order_parameter=math.log1p(RCI_value)/math.log1p(max_RCI-min_RCI)
	rc_order_parameter=1.443736737*math.log1p(RCI_value)
										

	#rc_order_parameter=(0.8*math.pow((4+math.log(RCI_value)),0.5))-0.5 #!!!!!


	#rc_order_parameter=math.exp(RCI_value)/math.exp(max_RCI-min_RCI)
	#rc_order_parameter=math.exp(RCI_value)/(math.exp(max_RCI)-math.exp(min_RCI))
	#rc_order_parameter=0.4*math.log1p(1 + 17.7*RCI_value) 
	#rc_order_parameter=math.log(RCI_value)/(math.log(max_RCI)-math.log(min_RCI))
	#rc_order_parameter=math.log(1+RCI_value)/math.log(1+max_RCI-min_RCI)

	#print "RCI_value=",RCI_value2use, "rc_order_parameter=",rc_order_parameter, "math.log1p(max_RCI-min_RCI)=", math.log1p(max_RCI-min_RCI), "math.log1p(RCI_value)=", math.log1p(RCI_value)
	return rc_order_parameter


def get_MD_RMSF(RCI_value):
	"""
	Get MD RMSF
	"""
	MD_RMSF=RCI_value*9.0
	return MD_RMSF



def get_NMR_RMSF(RCI_value):
	"""
	Get NMR RMSF
	"""
	NMR_RMSF=RCI_value*6.0
	return NMR_RMSF


def get_ASA(RCI_value):
	"""
	Get ASA
	"""
	ASA=RCI_value*1.5
	if ASA>1.0:
		ASA=1.0
	return ASA


def get_B_factor(RCI_value):
	"""
	Get B_factor
	"""
	normalized_B_factor=(RCI_value*100)+1.0
	absolute_B_factor=(math.pi**2.0)*8.0*((RCI_value*9.0)**2.0)
	#absolute_B_factor=((math.pi**2.0)*8.0*((RCI_value*9.0)**2.0))/3.0
	return normalized_B_factor,absolute_B_factor

def smoothing(\
		l_pdb_list,\
		l_pdb_dict,\
		l_zodb,\
		l_averaging,\
		l_shift_class_lookup,\
		l_rc_residue_list,\
		l_rci_dict,\
		l_database_file,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_rc_assign_dic,\
		):
	"""
	Three-residue averaging before combining shifts	
	"""	
	verbose=0
	if verbose>=1: print "Running function smoothing()"
	if verbose>=10: print "l_rc_assign_dic=", l_rc_assign_dic
	debug=0
	if debug==1:
		for i in l_rc_assign_dic:
			print i,  l_rc_assign_dic[i]

		sys.exit()
	if verbose>=10: print "l_rci_dict = ", l_rci_dict
	if l_zodb:
		storage = FileStorage(l_database_file)
		db = DB(storage)		
		connection = db.open()
		main_database = connection.root()
		l_pdb_dict=main_database["pdb_dict"]



	#for shift_class in l_shift_class_lookup:

	#	shift_list=l_shift_class_lookup[shift_class]
	#	if verbose>=10: print "shift_class =", shift_class , shift_list	

	#	for shift_type in shift_list:
	#		if verbose>=10: print "shift_type =", shift_type

	#		for i in l_rc_residue_list:
	#			residue_number =i[0]
	#			if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type






	for i in l_rc_residue_list:
		residue_number =i[0]
		res_name =i[1]
		if verbose>=1: print res_name,residue_number



		for shift_class in l_shift_class_lookup:
			
			shift_class_ori=shift_class
			if verbose>=10: print "shift_class =", shift_class

			if l_remove_stereo in [1,2]:
				if (l_remove_stereo==1 and res_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and res_name in l_res4gridsearch_list_dict and res_name in l_res4gridsearch_list):
					stereo=l_res4gridsearch_list_dict[res_name]["stereo"]
					if verbose>=1: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (res_name,residue_number,stereo)
				


					shift_class=shift_class_ori
					if stereo==0 and  shift_class in l_shiftclass4gridsearch_list:
						shift_class1="%s_ave" % shift_class
						shift_class2="%save" % shift_class
						if shift_class1 in l_shift_class_lookup:
							shift_class=shift_class1
						elif shift_class2 in l_shift_class_lookup:
							shift_class=shift_class2
						else: 	
							if verbose>=0: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)

			shift_list=l_shift_class_lookup[shift_class]			

			for shift_type in shift_list:
				if verbose>=1 and shift_class in ["Side_chain","Side_chain_ave"]: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type, "res_name=", res_name

				if shift_type in l_rc_assign_dic[residue_number][1] and shift_class in l_rci_dict:
					if verbose>=10: print "shift_type %s was found for residue %s" %  (shift_type,  residue_number)				
					if  residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
						if shift_type in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"]:
							details=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]
							if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type, "details=", details
							value_list=[]
							if len(details)>0:
								scaled_abs_diff=details['scaled_abs_diff']
								value_list+=[scaled_abs_diff]

							res_list=[]
							if l_averaging in [3,5]:
								res_minus_1=residue_number-1
								res_plus_1=residue_number+1
								res_list=[res_minus_1,res_plus_1]

							if l_averaging in  [5]:
								res_minus_2=residue_number-2
								res_plus_2=residue_number+2
								res_list+=[res_minus_1,res_plus_1]

							for neighboring_residue in res_list:
								if neighboring_residue in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_type in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]["details"]:
									neighboring_residue_details=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]["details"][shift_type]								
									if len(neighboring_residue_details)>0:
										neighboring_residue_scaled_abs_diff=neighboring_residue_details['scaled_abs_diff']
										value_list+=[neighboring_residue_scaled_abs_diff]

							if len(value_list)>0:
								if verbose>=10: print "Smoothing  for  shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type, "value_list=", value_list
								smoothed_value=stats.lmean(value_list)
								if verbose>=10: print "Smoothed value =", mean_scaled_value, value_list, linesep
								if verbose>=10: print "l_rc_assign_dic[residue_number][1]=", l_rc_assign_dic[residue_number][1]
								if shift_type not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"]:
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]={}
								l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]['smoothed_abs_diff']=smoothed_value


	if l_zodb:
		l_pdb_dict._p_changed = True		
		transaction.savepoint(True)
		transaction.commit()
		#transaction.get().commit() 
		connection.close()
		db.close()
		storage.close()
		l_pdb_dict={}
	debug=0
	if debug==1:
		print "Exiting after function smoothing()"
		sys.exit()

	return







def second_gap_filling(\
		l_pdb_list,\
		l_pdb_dict,\
		l_zodb,\
		l_search,\
		l_shift_class_lookup,\
		l_rc_residue_list,\
		l_rci_dict,\
		l_database_file,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		):
	"""
	Gap filling after combining shifts	
	"""
	verbose=0
	if verbose>=1: print "Running function second_gap_filling()"
	#print "l_rci_dict=", l_rci_dict
	if l_zodb:
		storage = FileStorage(l_database_file)
		db = DB(storage)		
		connection = db.open()
		main_database = connection.root()
		l_pdb_dict=main_database["pdb_dict"]

	for shift_class in l_shift_class_lookup:
		if verbose>=10: print "shift_class =", shift_class
		shift_class_ori=shift_class
		for i in l_rc_residue_list:
			residue_number =i[0]
			res_name=i[1]
			if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type
			found=0



			if l_remove_stereo in [1,2]:
				if (l_remove_stereo==1 and res_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and res_name in l_res4gridsearch_list_dict and res_name in l_res4gridsearch_list):
					stereo=l_res4gridsearch_list_dict[res_name]["stereo"]
					if verbose>=10: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (res_name,residue_number,stereo)
				
					shift_class=shift_class_ori
					if stereo==0 and  shift_class in l_shiftclass4gridsearch_list:
						shift_class1="%s_ave" % shift_class
						shift_class2="%save" % shift_class
						#print "Place 1"
						if shift_class1 in l_shift_class_lookup:
							shift_class=shift_class1
						elif shift_class2 in l_shift_class_lookup:
							shift_class=shift_class2
						else: 	
							if verbose>=0: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)






			if  residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_class not in l_rci_dict:
				if verbose>=10: print residue_number, shift_class
				if shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]:
					if "combined_abs_diff" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]:
						combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["combined_abs_diff"]
						if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "combined_abs_diff= ", combined_abs_diff
						if is_number(combined_abs_diff):
							found=1
							if verbose>=10: print "Combined value has been found", linesep
			if found==0 and l_search!=0 and shift_class not in l_rci_dict:
				if l_search in [1,2]:
					res_minus_1=residue_number-1
					res_plus_1=residue_number+1
					res_list=[res_minus_1,res_plus_1]
				if l_search==2:
					res_minus_2=residue_number-2
					res_plus_2=residue_number+2
					res_list+=[res_minus_2,res_plus_2]
				if verbose>=10:# and  shift_class=="Side-chain":
					print "MISSING: ", "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", linesep
				value_list=[]
				for neighboring_residue in res_list:
					if neighboring_residue in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"]:
						if "combined_abs_diff" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]:
							combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]["combined_abs_diff"]							
							if is_number(combined_abs_diff):
									value_list+=[combined_abs_diff]
						else:
							if verbose>=10: print "Warning!!! 	combined_abs_diff is not in l_pdb_dict for shift_class %s and residue_number %s " % (shift_class,neighboring_residue)

				if len(value_list)>0:
					if verbose>=10: print "Second gap filling for  shift_class= ", shift_class, "residue_number= ", residue_number, "value_list=", value_list
					gapped_filled_value=stats.lmean(value_list)
					if verbose>=10: print "Smoothed value =", gapped_filled_value, value_list, linesep
					l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["combined_abs_diff"]=gapped_filled_value



	if l_zodb:
		l_pdb_dict._p_changed = True		
		transaction.savepoint(True)
		transaction.commit()
		#transaction.get().commit() 
		connection.close()
		db.close()
		storage.close()
		l_pdb_dict={}

	debug=0
	if debug==1:
		print "Exiting after function second_gap_filling()"
		sys.exit()

	return



def gap_filling(\
		l_pdb_list,\
		l_pdb_dict,\
		l_zodb,\
		early_floor_value,\
		l_rc_residue_list,\
		l_shift_class_description_dict,\
		l_shift_class_lookup,\
		l_search,\
		l_database_file,\
		l_rci_dict,\
		l_parameter_dict,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		):
	"""
	Fill gaps	
	"""	
	verbose=0
	if verbose>=1: print "Running function gap_filling()"
	l_rc_residue_list.sort()

	if l_zodb:
		storage = FileStorage(l_database_file)
		db = DB(storage)		
		connection = db.open()
		main_database = connection.root()
		l_pdb_dict=main_database["pdb_dict"]
	rc_assign_dic=l_pdb_dict["rci"]["rc_assign_dic"]
	if verbose>=20: print "rc_assign_dic=", rc_assign_dic		
	l_early_floor_value=early_floor_value		


	for i in l_rc_residue_list:
		residue_number =i[0]
		res_name =i[1]
		if verbose>=1: print res_name,residue_number

		for shift_class in l_shift_class_lookup:
			shift_class_ori=shift_class

			if verbose>=10: print "shift_class =", shift_class

			if l_remove_stereo in [1,2]:
				if (l_remove_stereo==1 and res_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and res_name in l_res4gridsearch_list_dict and res_name in l_res4gridsearch_list):
					stereo=l_res4gridsearch_list_dict[res_name]["stereo"]
					if verbose>=1: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (res_name,residue_number,stereo)
				

					shift_class=shift_class_ori
					if stereo==0 and  shift_class in l_shiftclass4gridsearch_list:
						shift_class1="%s_ave" % shift_class
						shift_class2="%save" % shift_class
						if shift_class1 in l_shift_class_lookup:
							shift_class=shift_class1
						elif shift_class2 in l_shift_class_lookup:
							shift_class=shift_class2
						else: 	
							if verbose>=0: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)

			shift_list=l_shift_class_lookup[shift_class]			






			for shift_type in shift_list:
				#if verbose>=1: print "shift_type =", shift_type

				if verbose>=1 and shift_class in ["Side_chain","Side_chain_ave"]: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type, "res_name=", res_name
				#sys.exit()	
				found=0
					#	print "residue_number=", residue_number

				if shift_type in rc_assign_dic[residue_number][1] and shift_class in l_rci_dict:

					if shift_class in l_parameter_dict and res_name in  l_parameter_dict[shift_class] and "early_floor_value" in l_parameter_dict[shift_class][res_name]:
						l_early_floor_value=rci_dict[shift_class][res_name]["early_floor_value"]


					if verbose>=10: print "%s: shift_type %s was found for residue %s" %  (shift_class, shift_type,  residue_number)				
					if  residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
						if shift_type in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"]:
							details=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]
							if verbose>=10: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type, "details=", details
							if len(details)>0:
								scaled_abs_diff=details['scaled_abs_diff']
								if scaled_abs_diff<l_early_floor_value:
									scaled_abs_diff=l_early_floor_value
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]['scaled_abs_diff']=scaled_abs_diff
								found=1
								if verbose>=10: print "Scaled value has been found", linesep
					if found==0 and l_search!=0:
						if l_search in [1,2]:
							res_minus_1=residue_number-1
							res_plus_1=residue_number+1
							res_list=[res_minus_1,res_plus_1]
						if l_search==2:
							res_minus_2=residue_number-2
							res_plus_2=residue_number+2
							res_list+=[res_minus_2,res_plus_2]
						if verbose>=10: print "MISSING: ", "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type, linesep
						value_list=[]
						for neighboring_residue in res_list:
							if neighboring_residue in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and shift_type in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]["details"]:
								neighboring_residue_details=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][neighboring_residue]["residue_shift_classes"][shift_class]["details"][shift_type]								
								if len(neighboring_residue_details)>0:
									neighboring_residue_scaled_abs_diff=neighboring_residue_details['scaled_abs_diff']
									value_list+=[neighboring_residue_scaled_abs_diff]

						if len(value_list)>0:
							if verbose>=10: print "Filling gap for  shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type
							mean_scaled_value=stats.lmean(value_list)
							if mean_scaled_value<l_early_floor_value:
								mean_scaled_value=l_early_floor_value
							if verbose>=10: print "Gap value =", mean_scaled_value, value_list
							if verbose>=10: print "rc_assign_dic[residue_number][1]=", rc_assign_dic[residue_number][1]
							if shift_type not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"]:
								l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]={}
							l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]['scaled_abs_diff']=mean_scaled_value


	if l_zodb:
		l_pdb_dict._p_changed = True		
		transaction.savepoint(True)
		transaction.commit()
		#transaction.get().commit() 
		connection.close()
		db.close()
		storage.close()
		l_pdb_dict={}

	debug=0
	if debug==1:
		print "Exiting after function gap_filling()"
		sys.exit()
	return

def remove_outliers(l_list):
	"""
	Calculate cluster average and median
	"""
	verbose=0
	if verbose>=10: print "Running function remove_outliers()"
	clust_dict={}
	clust_list=[]
	max_diff=""
	final_list=[]
	output_list=[]
	for i in l_list:
		i_diff_list=[]
		for j in l_list:
			diff=abs(i-j)
			i_diff_list+=[diff]
		#mean_diff=stats.lmean(i_diff_list)
		mean_diff=stats.lmedianscore(i_diff_list)
		if mean_diff!=0.0:
			clust_list+=[[mean_diff,random.random(),i]]	
		clust_dict[i]={"mean_diff":mean_diff,"diff_list":i_diff_list}

	clust_list.sort()
	clust_list.reverse()
	for n in clust_list:
		n_mean_diff,n_shift=n[0],n[2]
		if verbose>=1: print "n_shift=", n_shift, "n_mean_diff=", n_mean_diff
	if len(clust_list)>0:
		if verbose>=1: print "The largest difference is for ", clust_list[0][2]
		max_diff=clust_list[0][0]

	if len(l_list)>1:
		mean=stats.lmean(l_list)
		stdev=stats.lstdev(l_list)
		if 	len(l_list)>2:
			if 	max_diff!="":
				#if max_diff>2.0*stdev:
				if max_diff>2.0*stdev:
					final_list=clust_list[1:]

	if final_list==[]:
		output_list=l_list
	else:
		if verbose>=1:print "final_list=", final_list
		for m in final_list:
			#print "m=", m
			output_list+=[m[2]]

	debug=0
	if debug==1:
		print "Exiting after function remove_outliers()"
		sys.exit()
	return output_list



def cleanup(l_full_project_dir,gridsearch_dir,project):
	"""
	Archive directory with grid search results to dicrease the number of files

R1_A_S_csq_dir/rci_results/gridsearch/A_shifts_HB_0.3_CB_1.0_floor1_0.6_ceil1_0.5_power1_1.5_scale1_3.0/pics/data

	"""
	verbose=0
	if verbose>=1: print "Running function cleanup()"
	curdir=os.getcwd()
	gridsearch_dir_full="%s/rci_results/%s" % (l_full_project_dir,gridsearch_dir)
	if verbose>=1: 	print "gridsearch_dir_full=", gridsearch_dir_full
	rci_result_dir="%s/rci_results" %  (l_full_project_dir)
	if verbose>=1: print "rci_result_dir=", rci_result_dir

	gridsearch_dir_archive="%s/rci_results/%s.tar.gz" % (l_full_project_dir,gridsearch_dir)
	tar_command="tar cvfz %s %s > %s.tarlog" % (gridsearch_dir_archive,gridsearch_dir,gridsearch_dir)
	if  file_exists(rci_result_dir):
		os.chdir(rci_result_dir)
		if file_exists(gridsearch_dir):
			os.system(tar_command)
		else:
			print "2_ Warning!!!! Directory %s does not exists. Can't tar" % gridsearch_dir
	else:
		print "1) Warning!!!! Directory %s does not exists. Can't tar" % rci_result_dir
	if verbose>=1: print tar_command
	remove_command="rm -rf %s " % gridsearch_dir_full
	if verbose>=1: print remove_command
	if file_exists(gridsearch_dir_full):
		os.system(remove_command)
	else:
		print "3) Warning!!!! Directory %s does not exists. Can't remove it" % gridsearch_dir_full
	os.chdir(curdir)
	debug=0
	if debug==1:
		print "Exiting after function cleanup()"
		sys.exit()
	return


def calculate_average_weighting_coefficient(l_coef_dict):
	"""
	Calculate average weighting coefficient

l_coef_dict=  {'CB': {'weight': 1.0}, 'CG': {'weight': 1.0}, 'HG3': {'weight': 1.0}, 'CE': {'weight': 1.0}, 'HG2': {'weight': 1.0}, 'HE': {'weight': 1.0}, 'HB3': {'weight': 1.0}, 'HB2': {'weight': 1.0}}

	"""
	verbose=0
	if verbose>=1: print "Running function calculate_average_weighting_coefficient()"
	if verbose>=10: print "l_coef_dict= ",l_coef_dict
	l_coeff_list=[]
	l_mean_weight=""
	expected_atom_string=""
	l_number_of_atoms=""
	for atom in l_coef_dict:
		weight=l_coef_dict[atom]['weight']
		if is_number(weight):
			l_coeff_list+=[weight]
			expected_atom_string+="%s_" % atom
		else:
			print "Warning!!! Weight %s is not numerical" % (weight)
	if len(l_coeff_list)>0:
		l_mean_weight=stats.lmean(l_coeff_list)
		l_number_of_atoms=len(l_coeff_list)
	
	debug=0
	if debug==1:
		print "l_mean_weight=", l_mean_weight
		print "Exiting after function calculate_average_weighting_coefficient()"
		sys.exit()
	return l_mean_weight,l_number_of_atoms,expected_atom_string


def combine(\
		l_pdb_list,\
		l_pdb_dict,\
		l_zodb,\
		l_database_file,\
		l_rci_dict,\
		l_shift_class_description_dict,\
		l_coef_mean,\
		l_miss_ass,\
		l_H_Hertz_corr,\
		l_C_Hertz_corr,\
		l_N_Hertz_corr,\
		floor_value1,\
		l_early_floor_value,\
		l_scale,\
		ceiling_value,\
		l_shift_class_lookup,\
		l_parameter_dict,\
		l_rci_method,\
		l_find_value_mode,\
		l_no_outliers,\
		l_power1,\
		l_scaling1,\
		l_runname_full_path,\
		l_runname,\
		residue_numbers_list,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_files4training,\
		l_do_gridsearch1,\
		l_rci_parameter_dict_file,\
		l_coef_only,\
		l_completeness_found,\
		l_forbidden_atoms,\
		l_sc_rci_weight,\
		l_bb_rci_weight,\
		l_sc_bb_combine,\
		l_detect_missing_atoms,\
		l_rc_offset_gridsearch,\
		l_bb_end_correct,\
		l_sc_end_correct,\
		l_exclude_atoms,\
		l_bb_rci_opt,\
		):
	"""
	Combine secondary chemical shifts

dic['Side_chain']={}
dic['Side_chain']['S']={'shifts':{}}
dic['Side_chain']['S']['early_floor_value']=0.1
dic['Side_chain']['S']['shifts']['CB']={}
dic['Side_chain']['S']['shifts']['CB']['weight']=0
dic['Side_chain']['S']['shifts']['HG']={}
dic['Side_chain']['S']['shifts']['HG']['weight']=0
dic['Side_chain']['S']['shifts']['HB3']={}
dic['Side_chain']['S']['shifts']['HB3']['weight']=0
dic['Side_chain']['S']['shifts']['HB2']={}
dic['Side_chain']['S']['shifts']['HB2']['weight']=0.33
dic['Side_chain']['S']['floor_value1']=0.6
dic['Side_chain']['S']['ceiling_value']=0.5
dic['Side_chain']['S']['scaling1']=3.0
~                                           

	"""
	verbose=0
	if verbose>=1: print "Running function combine()"

	dict_4_training_text="dic={}" + linesep
	coeff1=1.5
	coeff2=1.0
	rci_residue_numbers_list=[]


	if l_detect_missing_atoms==1 and l_completeness_found==0:
		make_dir_no_remove(l_runname_full_path)
		missing_atom_dir="%s/missing_atoms_reports" % l_runname_full_path
		make_dir_no_remove(missing_atom_dir)

	if l_zodb:
		storage = FileStorage(l_database_file)
		db = DB(storage)		
		connection = db.open()
		main_database = connection.root()
		l_pdb_dict=main_database["pdb_dict"]






	y="""
	for i in l_rc_residue_list:
		residue_number =i[0]
		res_name =i[1]
		if verbose>=1: print res_name,residue_number


		if l_remove_stereo in [1,2]:
			if (l_remove_stereo==1 and res_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and res_name in l_res4gridsearch_list_dict and res_name in l_res4gridsearch_list):
				stereo=l_res4gridsearch_list_dict[res_name]["stereo"]
				if verbose>=1: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (res_name,residue_number,stereo)
				

		for shift_class in l_shift_class_lookup:
			

			if verbose>=10: print "shift_class =", shift_class

			if stereo==0 and  shift_class in l_shiftclass4gridsearch_list:
				shift_class1="%s_ave" % shift_class
				shift_class2="%save" % shift_class
				if shift_class1 in l_shift_class_lookup:
					shift_class=shift_class1
				elif shift_class2 in l_shift_class_lookup:
					shift_class=shift_class2
				else: 	
					if verbose>=0: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)

			shift_list=l_shift_class_lookup[shift_class]			

			for shift_type in shift_list:
				if verbose>=1 and shift_class in ["Side_chain","Side_chain_ave"]: print "shift_class= ", shift_class, "residue_number= ", residue_number, "shift_type= ", shift_type, "res_name=", res_name

	"""

	for shift_class in l_shift_class_lookup:
		if verbose>=10: print "Processing shift_class ", shift_class
		include=1
		rci_param_dict={}
		l_coef_dict={}
		shift_class_ori=shift_class_combo=shift_class
		shift_type_list=l_shift_class_lookup[shift_class]
		averaged_shift_class=""
		scaling1=l_scaling1
		power1=l_power1
		l_floor_value1=floor_value1
		l_ceiling_value=ceiling_value
		sc_rci_weight=l_sc_rci_weight
		bb_rci_weight=l_bb_rci_weight

		dict_4_training_text+="dic['%s']={}" %  shift_class + linesep
		residue_counter=0
		if l_detect_missing_atoms==1 and l_completeness_found==0 and shift_class_ori in l_parameter_dict:
			shift_class_missing_atom_dir="%s/%s" % (missing_atom_dir,shift_class)
			make_dir_no_remove(shift_class_missing_atom_dir)
			l_missing_atoms_text=""
			l_missing_atoms_file="%s/missing_or_excessive_atoms.txt" % shift_class_missing_atom_dir

			l_completeness_per_residue_text=""
			l_completeness_per_residue_file="%s/assignment_completeness_per_residue.txt" % shift_class_missing_atom_dir

			l_completeness_report_text=""
			l_completeness_report_file="%s/assignment_completeness_report.txt" % shift_class_missing_atom_dir


			residues_with_missing_atoms_counter=0
			residues_with_excessive_atoms_counter=0
			completeness_list=[]
			missing_completeness_list=[]
			excessive_completeness_list=[]




		for residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
			res_name=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["res_name"]

			if verbose>=10 and shift_class_ori=="SideChain": 
				print shift_class_ori, "Processing residue ", res_name,residue_number, " in function combine()"
			sc_rci_value=bb_rci_value=""

			if shift_class not in l_rci_dict:

				dict_4_training_text+="dic['%s'][%s]={}" %  (shift_class,residue_number) + linesep


				dict_4_training_text+="dic['%s'][%s]['res_name']='%s'" %  (shift_class,residue_number,res_name) + linesep
				dict_4_training_text+="dic['%s'][%s]['shifts']={}" %  (shift_class,residue_number) + linesep


				debug=0
				if debug==1:
					if shift_class_ori=="Side_chain" and residue_number==3:
						if verbose>=0: print "Processing residue ", res_name,residue_number, " in function combine()"
				debug=0

				l_list=[]
				l_alt_list=[]

				if l_detect_missing_atoms==1  and l_completeness_found==0 and shift_class_ori in l_parameter_dict:				
					residue_counter+=1


				if l_remove_stereo in [1,2,3,4,5,6]:
					if (l_remove_stereo in [1,3,5] and res_name in l_res4gridsearch_list_dict) or (l_remove_stereo in [2,4,6] and res_name in l_res4gridsearch_list_dict and res_name in l_res4gridsearch_list):
						stereo=l_res4gridsearch_list_dict[res_name]["stereo"]
						if verbose>=10: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (res_name,residue_number,stereo)
						shift_class=shift_class_ori
						#print "Place 0"
						
						if (stereo==0 and  shift_class in l_shiftclass4gridsearch_list) or l_remove_stereo in [5,6]:
							shift_class1="%s_ave" % shift_class
							shift_class2="%save" % shift_class
							#print "Place 1", shift_class, residue_number
							if shift_class1 in l_shift_class_lookup:
								averaged_shift_class=shift_class1
								if l_remove_stereo in [1,2]:
									shift_class=shift_class1
							elif shift_class2 in l_shift_class_lookup:
								averaged_shift_class=shift_class2
								if l_remove_stereo in [1,2]:
									shift_class=shift_class2
							else: 	
								if verbose>=1: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)

				#dic['Side_chain']['W']['shifts']['HEave']={}
				#dic['Side_chain']['W']['shifts']['HEave']['weight']=1.0


				debug=0
				if debug==1:
					if shift_class_ori=="Side_chain" and residue_number==3:
						if verbose>=0: print "New shift class for residue ", res_name,residue_number, " in function combine() is ", shift_class
				debug=0


				l_coef_dict={}
				l_non_bb_rci_dict={}
				l_expected_number_of_atoms=""
				l_global_mean_coeff=""
				expected_atom_string=""
				actual_atom_string=""

				if (l_do_gridsearch1>0 and shift_class in l_parameter_dict) or (l_do_gridsearch1==0 and shift_class_ori in l_parameter_dict):

					shift_class2use=shift_class
					if (l_do_gridsearch1>0 ) and shift_class in l_parameter_dict:
						shift_class2use=shift_class
					elif l_do_gridsearch1==0 and shift_class_ori in l_parameter_dict:
						shift_class2use=shift_class_ori
					elif l_do_gridsearch1==0 and shift_class in l_parameter_dict:
						shift_class2use=shift_class
						
					if res_name in  l_parameter_dict[shift_class2use]:
						#print "Place 2"
						debug=0
						if debug==1:
							if residue_number==24 and shift_class_ori=="Side_chain": 
								print "shift_class2use %s was found for residue %s%s" % (shift_class2use, res_name, residue_number) 
 						if "floor_value1" in l_parameter_dict[shift_class2use][res_name]:
							l_floor_value1=l_parameter_dict[shift_class2use][res_name]["floor_value1"]
						if "scaling1" in l_parameter_dict[shift_class2use][res_name]:
							scaling1=l_parameter_dict[shift_class2use][res_name]["scaling1"]
						if "power1" in l_parameter_dict[shift_class2use][res_name]:
							power1=l_parameter_dict[shift_class2use][res_name]["power1"]
						if "ceiling_value" in l_parameter_dict[shift_class2use][res_name]:
							l_ceiling_value=l_parameter_dict[shift_class2use][res_name]["ceiling_value"]
						if "sc_rci_weight" in l_parameter_dict[shift_class2use][res_name]:
							l_sc_rci_weight=l_parameter_dict[shift_class2use][res_name]["sc_rci_weight"]
						if "bb_rci_weight" in l_parameter_dict[shift_class2use][res_name]:
							l_bb_rci_weight=l_parameter_dict[shift_class2use][res_name]["bb_rci_weight"]

						if "shifts" in  l_parameter_dict[shift_class2use][res_name]:
							l_coef_dict = l_parameter_dict[shift_class2use][res_name]["shifts"]
							l_non_bb_rci_dict=l_coef_dict
							if l_detect_missing_atoms==1 and l_completeness_found==0 and shift_class_ori in l_parameter_dict:
								l_global_mean_coeff,l_expected_number_of_atoms,expected_atom_string=calculate_average_weighting_coefficient(l_coef_dict)


				if verbose>=10 and shift_class_ori in ["Side_chain"]: print "1) shift_class_ori=",shift_class_ori, " shift_class= ", shift_class, "residue_number= ", residue_number, "res_name= ", res_name
				#one_found=0
				l_coef_list=[]
				coef_offset=1.0
				local_atom_list=[]

				for shift_type in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"]:

					l_forbidden_atom=0
					if res_name in l_forbidden_atoms and shift_type in l_forbidden_atoms[res_name]:
						l_forbidden_atom=1
						if verbose>=10: print "Forbidden shift %s was found for %s%s and shift_class %s " % (shift_type,res_name,residue_number,shift_class)

					details=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]
					scaled_abs_diff=details["scaled_abs_diff"]
					scaled_abs_diff_ori=details["scaled_abs_diff"]
					local_atom_list+=[shift_type]
					actual_atom_string+="%s_" % shift_type

					if l_remove_stereo in [3,4,5,6]:
						if shift_type not in ["H","C","N"]:
							shift_type_averaged="%save" % (shift_type[:2])
							if averaged_shift_class!="" and averaged_shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"]:
								if shift_type_averaged in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][averaged_shift_class]["details"]:
									averaged_details=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][averaged_shift_class]["details"][shift_type_averaged]
									scaled_abs_diff=averaged_details["scaled_abs_diff"]
									#print "Averaged value of %s of %s was found for residue %s%s and shift %s with original value %s" % (scaled_abs_diff,shift_type_averaged,residue_number,res_name,shift_type,scaled_abs_diff_ori)
					
					l_coef=1.0
					l_abs_diff_offset=0.0
					if l_do_gridsearch1==0 and l_coef_only and shift_class_ori in l_parameter_dict:
						l_coef=""
						l_abs_diff_offset=""

					if l_forbidden_atom==1:
						l_coef=""
						l_abs_diff_offset=""
					
					#if exclude_atoms==1 and shift_class in rci_exclude_dic: print "exclude_atoms==1"
					#if exclude_atoms==0: print "exclude_atoms==0"
					if l_exclude_atoms==1 and shift_class in rci_exclude_dic and res_name in rci_exclude_dic[shift_class]:
						#print "Place 1"
						for exlcuded_atom in rci_exclude_dic[shift_class][res_name]:
							#print res_name, "exlcuded_atom =", exlcuded_atom 
							if exlcuded_atom==shift_type:
								l_coef=""	
								l_abs_diff_offset=""	
								
								if verbose>=10: print " EXCLUDING: Excluded atom %s found in residue %s%s for shift_class %s" % (exlcuded_atom,residue_number,res_name,shift_class)

					debug=0
					if debug==1:
						if residue_number==24 and shift_class_ori=="Side_chain":
							print residue_number,res_name,shift_class,shift_type 




					if (l_do_gridsearch1>0 and shift_class in l_parameter_dict) or (l_do_gridsearch1==0 and shift_class_ori in l_parameter_dict):
						debug=0
						if debug==1:
							if residue_number==24 and shift_class_ori=="Side_chain":
								print "l_parameter_dict=", l_parameter_dict
								#for i in l_parameter_dict[shift_class]:
								#	print i
						#print "Place 1",  shift_class, "res_name=",res_name ,l_parameter_dict[ shift_class]


						if shift_type in l_coef_dict:
							#print "Place 3"
							#if "res_name" in l_coef_dict[shift_type]:
							#	print "Place 4"
								#coeff_res_name=l_coef_dict[shift_type]["res_name"]
								#print "coeff_res_name=", coeff_res_name
								#print 'l_coef_dict[shift_type]["res_name"]', l_coef_dict[shift_type]["res_name"]
								#if coeff_res_name==res_name:
							if "weight" in l_coef_dict[shift_type]:
								l_coef=l_coef_dict[shift_type]["weight"]
							if "abs_diff_offset" in l_coef_dict[shift_type] and l_rc_offset_gridsearch==2:
								l_abs_diff_offset=l_coef_dict[shift_type]["abs_diff_offset"]

							if is_number(l_abs_diff_offset) and is_number(l_coef)==0:
								l_coef=1.0

							if is_number(l_abs_diff_offset)==0 and is_number(l_coef):
								l_abs_diff_offset=0.0

							#if l_coef==1.0:
							#	one_found=1
							#print "Place 5"
							if verbose>=10: print "Weight %s for atom %s and residue %s%s was found in  l_coef_dict" % (l_coef,shift_type,res_name,residue_number)  
							if verbose>=10: print "l_abs_diff_offset %s for atom %s and residue %s%s was found in  l_coef_dict" % (l_abs_diff_offset,shift_type,res_name,residue_number)  
						#else:
						#	l_coef=l_coef_dict[shift_type]["weight"]


					debug=0
					if debug==1 or g_debug==1:
						if shift_class_ori=="Side_chain" and res_name=="T": #residue_number in [7,21,29]:
							if verbose>=0: print "Processing shift_type %s for residue " % shift_type , res_name,residue_number, " and shift class %s in function combine()" % shift_class, "scaled_abs_diff= ", scaled_abs_diff, " coeff= ", [l_coef]
					debug=0


						
					value_for_training=""
					sc_rci_value=bb_rci_value=""
					#if verbose>=0 and shift_class_ori=="Side_chain" and  residue_number in [5]:#[16,18,34,46,60]:
					if verbose>=10 and shift_class_ori=="Side_chain_ave" and  residue_number in [5]:#[16,18,34,46,60]:
						print shift_class, residue_number, shift_type, scaled_abs_diff#, l_coef # details						
					#print "scaled_abs_diff= ", scaled_abs_diff
					#l_floor_value1=0.1

					if is_number(l_abs_diff_offset):
						scaled_abs_diff=scaled_abs_diff+l_abs_diff_offset

					if find_max==1:
						scaled_abs_diff=0
					elif find_min==1:
						scaled_abs_diff=99999

					if 	scaled_abs_diff<l_floor_value1:
					#if 1==1:
						scaled_abs_diff=l_floor_value1

					if 	scaled_abs_diff>early_ceiling_value:
						scaled_abs_diff=early_ceiling_value


					if is_number(l_coef)==0:
						if verbose>0: print "Warning!!!! Weight was not found for %s of %s%s" % (shift_type,residue_number,res_name)

					if l_rci_method in [1,14,15,27] and is_number(l_coef):
						debug=0
						if debug==1 or g_debug==1:
							if shift_class=="Side_chain" and residue_number in [12]: print shift_class, residue_number, shift_type, "scaled_abs_diff=", scaled_abs_diff, "scaling1=", scaling1, "l_coef=", l_coef
						debug=0
						l_list+=[scaled_abs_diff*scaling1*l_coef]
						l_coef_list+=[l_coef]
						debug=0
						if debug==1:
							if shift_class_ori=="Side_chain" and residue_number==2:
								if verbose>=0: print "scaled_abs_diff=", scaled_abs_diff , " has been added"
						debug=0


					if l_rci_method in [2,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,22,23,24,25,26,27,28]:
						power10=power1

						if l_rci_method in [2,5,8,12,14,16,21,23,25]:
							#if l_rci_method!=14:
							scaled_scaled_abs_diff=scaled_abs_diff*scaling1
							powered_scaled_scaled_abs_diff=scaled_scaled_abs_diff**power10
							#else:
							#alt_scaled_scaled_abs_diff=scaled_abs_diff*scaling1
							#alt_powered_scaled_scaled_abs_diff=alt_scaled_scaled_abs_diff**power10								


						elif  l_rci_method in [4,17]:						
							scaled_scaled_abs_diff=scaled_abs_diff**power10
							powered_scaled_scaled_abs_diff=scaled_scaled_abs_diff*scaling1
							#l_ceiling_value=100.0
						elif l_rci_method in [6,7,18,22]:
							scaled_scaled_abs_diff=scaled_abs_diff
							powered_scaled_scaled_abs_diff=scaling1*math.exp(scaled_scaled_abs_diff)
							if verbose>=10: print "Taking exponent before inversion ", powered_scaled_scaled_abs_diff

						elif l_rci_method in [9,11,19,24]:
							scaled_scaled_abs_diff=scaled_abs_diff
							powered_scaled_scaled_abs_diff=scaling1*math.log((1.0+scaled_scaled_abs_diff),10)
							if verbose>=10: print "Taking logarithm before inversion ", powered_scaled_scaled_abs_diff
							#l_ceiling_value=100.0

						elif l_rci_method in [10,13,20,26]:
							scaled_scaled_abs_diff=scaled_abs_diff
							powered_scaled_scaled_abs_diff=scaling1*math.log1p(scaled_scaled_abs_diff)
							if verbose>=10: print "Taking natural logarithm before inversion ", powered_scaled_scaled_abs_diff
							#l_ceiling_value=100.0
						#print powered_scaled_scaled_abs_diff
						#if 	powered_scaled_scaled_abs_diff<5.0:
						#	powered_scaled_scaled_abs_diff=5.0		

					
						inversed_powered_scaled_scaled_abs_diff=1.0/powered_scaled_scaled_abs_diff

						if l_rci_method in [5,7,21,22]:
							inversed_powered_scaled_scaled_abs_diff=math.exp(inversed_powered_scaled_scaled_abs_diff)
							if verbose>=10: print "Taking exponent after inversion ", inversed_powered_scaled_scaled_abs_diff
							#l_ceiling_value=100.0

						if l_rci_method in [8,11,23,24]:
							inversed_powered_scaled_scaled_abs_diff=math.log(inversed_powered_scaled_scaled_abs_diff,10)
							if verbose>=10: print "Taking logarithm after inversion ", inversed_powered_scaled_scaled_abs_diff	

						if l_rci_method in [12,13,25,26]:
							inversed_powered_scaled_scaled_abs_diff=math.log1p(inversed_powered_scaled_scaled_abs_diff)
							if verbose>=10: print "Taking natural logarithm after inversion ", inversed_powered_scaled_scaled_abs_diff	


				
						#print inversed_powered_scaled_scaled_abs_diff
						#if inversed_powered_scaled_scaled_abs_diff > 0.2:
						#	inversed_powered_scaled_scaled_abs_diff=0.2

						#l_list+=[inversed_powered_scaled_scaled_abs_diff*l_coef]
						if l_rci_method in [14,27] and is_number(l_coef):
							l_alt_list+=[inversed_powered_scaled_scaled_abs_diff*l_coef]
						elif  l_rci_method not in [14,27] and is_number(l_coef):
							l_list+=[inversed_powered_scaled_scaled_abs_diff*l_coef]
							l_coef_list+=[l_coef]							

						value_for_training=inversed_powered_scaled_scaled_abs_diff


					elif l_rci_method==3  and is_number(l_coef):
						scaled_scaled_abs_diff=scaled_abs_diff*scaling1
						inversed_scaled_scaled_abs_diff=1.0/scaled_scaled_abs_diff
						l_list+=[inversed_scaled_scaled_abs_diff*l_coef]
						l_coef_list+=[l_coef]
						value_for_training=inversed_scaled_scaled_abs_diff


					if l_rci_method not in [1,15,16,17,18,19,20,21,22,23,24,25,26,27,28] and l_files4training:
						
						#print res_name, residue_number, "shift_class_ori=", shift_class_ori,  "shift_class=", shift_class,  "shift_type=", shift_type#, " l_parameter_dict=",  l_parameter_dict
						if "inversed_scaled_abs_diff" not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]:
							l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]["inversed_scaled_abs_diff"]={}
						if l_runname not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]["inversed_scaled_abs_diff"]:
							l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]["inversed_scaled_abs_diff"][l_runname]={}
						l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["details"][shift_type]["inversed_scaled_abs_diff"][l_runname]=value_for_training

						dict_4_training_text+="dic['%s'][%s]['shifts']['%s']={} " %  (shift_class_ori,residue_number,shift_type) + linesep
						dict_4_training_text+="dic['%s'][%s]['shifts']['%s']['abs_diff']='%s'" %  (shift_class_ori,residue_number,shift_type,scaled_abs_diff) + linesep
						dict_4_training_text+="dic['%s'][%s]['shifts']['%s']['value_for_training']='%s'" %  (shift_class_ori,residue_number,shift_type,value_for_training) + linesep

				combined_abs_diff=alt_combined_abs_diff="NONE"
				#print "l_list=", l_list
				if len(l_list)>0:
					#l_accepted_grid_points+=1
					if shift_class=="No_beta_side_chain_protons":

						if verbose>=10: print "No_beta_side_chain_protons: residue_number=",  residue_number, "l_list=", l_list

					if l_no_outliers:
						
						if l_rci_method in [14,27]:
							l_new_alt_list=remove_outliers(l_alt_list)
						#else:
						l_new_list=remove_outliers(l_list)
					else:
						l_new_list=	l_list
						if l_rci_method in [14,27]:
							l_new_alt_list=l_alt_list

					if l_find_value_mode==1:
						debug=0
						if debug==1:
							if shift_class=="Side_chain" and residue_number in [12]:
								if verbose>=0: print "Side_chain, residue number %s , List for averaging= " % residue_number, l_new_list
							if shift_class=="Side_chain_ave" and residue_number in [12]:
								if verbose>=0: print "Side_chain_ave,residue number %s ,  List for averaging= " % residue_number , l_new_list
						debug=0

						if len(l_new_list)>0:
							combined_abs_diff=stats.lmean(l_new_list)

						debug=0
						if debug==1 or g_debug==1:
							if residue_number in [12] and shift_class_ori=="Side_chain" :
								print "residue_number=", residue_number, "combined_abs_diff=", combined_abs_diff, "l_remove_stereo=", l_remove_stereo, "l_new_list=", l_new_list
						debug=0

						if l_rci_method in [14,27]:
							if len(l_new_alt_list)>0:
								alt_combined_abs_diff=stats.lmean(l_new_alt_list)


					elif l_find_value_mode==2:
						combined_abs_diff=stats.lmedianscore(l_new_list)
						if l_rci_method in [14,27]:
							alt_combined_abs_diff=stats.lmedianscore(l_new_alt_list)
					elif l_find_value_mode==3:
						combined_abs_diff=min(l_new_list)
						if l_rci_method in [14,27]:
							alt_combined_abs_diff=stats.min(l_new_alt_list)
					elif l_find_value_mode==4:
						combined_abs_diff=max(l_new_list)
						if l_rci_method in [14,27]:
							alt_combined_abs_diff=stats.max(l_new_alt_list)
						#l_ceiling_value=100.0

					if verbose>=10 and shift_class=="Side_chain" and  residue_number in [26,27]:
						print shift_class, residue_number, combined_abs_diff, 	l_new_list			
					#combined_abs_diff=stats.lmedianscore(l_new_list)

					if combined_abs_diff not in [0.0,"NONE"]:
						#print "PLace 1"
						if l_detect_missing_atoms==1 and l_completeness_found==0 and shift_class_ori in l_parameter_dict:

							if l_global_mean_coeff!="" and  l_scale==1 and len(l_coef_list)>0:
								l_local_mean_coeff=stats.lmean(l_coef_list)
								l_real_number_of_atoms=len(l_coef_list)
								coef_offset=l_global_mean_coeff/l_local_mean_coeff
								if verbose>=10 and coef_offset!=1.0:
									print "Warning!!!! Incomplete side-chain assignment for %s%s with coef_offset = %s; Detected = " % (residue_number,res_name,coef_offset), local_atom_list, "Expected = ", l_coef_dict
									residues_with_missing_atoms_counter+=1
								if is_number(l_expected_number_of_atoms):
									percentage_of_atoms_with_coeff=(float(l_real_number_of_atoms)/float(l_expected_number_of_atoms))*100.0
									completeness_list+=[percentage_of_atoms_with_coeff]

									l_completeness_per_residue_text+="%s %s %s" % (residue_number,res_name,percentage_of_atoms_with_coeff) + linesep

									if percentage_of_atoms_with_coeff!=100.0:
										l_missing_atoms_text+="%s %s %s Actual = %s , Expected = %s" % (residue_number,res_name,percentage_of_atoms_with_coeff,actual_atom_string,expected_atom_string)+ linesep

									if  percentage_of_atoms_with_coeff>100.0:
										residues_with_excessive_atoms_counter+=1
										excessive_completeness_list+=[percentage_of_atoms_with_coeff]

									elif  percentage_of_atoms_with_coeff<100.0:
										residues_with_missing_atoms_counter+=1
										missing_completeness_list+=[percentage_of_atoms_with_coeff]



						l_coeff_mean=stats.lmean(l_coef_list)
						if l_coeff_mean<=0:
							l_coeff_mean=1.0

						if normalize==0:
							combined_abs_diff=combined_abs_diff*coef_offset
						else:
							combined_abs_diff=(combined_abs_diff*coef_offset)/l_coeff_mean



						if  l_rci_method in [14,27]:
							alt_combined_abs_diff=alt_combined_abs_diff*coef_offset

						if l_rci_method in [1,15]:
							inverse_combined_abs_diff=1.0/(combined_abs_diff**power1)
							if verbose>10: print residue_number,"inverse_combined_abs_diff=", inverse_combined_abs_diff
						elif l_rci_method in [2,4,5,6,7,8,9,10,11,12,13,16,17,18,19,20,21,22,23,24,25,26]:
							inverse_combined_abs_diff=combined_abs_diff
						elif l_rci_method in [3,28]:
							inverse_combined_abs_diff=combined_abs_diff**power1
						elif  l_rci_method in [14,27]:
							#print (coeff1/(combined_abs_diff**power1)), (alt_combined_abs_diff*coeff2)
							inverse_combined_abs_diff=((coeff1/(combined_abs_diff**power1))+(alt_combined_abs_diff*coeff2))/2.0


						if verbose>=10 and shift_class=="Side_chain" and  residue_number in [26,27]:
							print shift_class, residue_number, combined_abs_diff, "inverse_combined_abs_diff=", inverse_combined_abs_diff

						#inverse_combined_abs_diff=1.0/(combined_abs_diff)

						if inverse_combined_abs_diff>l_ceiling_value:
							inverse_combined_abs_diff=l_ceiling_value

						#l_sc_rci_weight=l_parameter_dict[shift_class2use][res_name]["sc_rci_weight"]
						#l_bb_rci_weight=l_parameter_dict[shift_class2use][res_name]["bb_rci_weight"]


						if l_rci_method not in [15,16,17,18,19,20,21,22,23,24,25,26,27,28]:
							l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["combined_abs_diff"]=inverse_combined_abs_diff
						else:
							if l_sc_bb_combine==1:
								sc_rci_value=inverse_combined_abs_diff
							else:
								l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["combined_abs_diff"]=inverse_combined_abs_diff

								if is_number(l_sc_rci_weight):
									l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["sc_rci_weight"]=l_sc_rci_weight

						if residue_number not in rci_residue_numbers_list:
							rci_residue_numbers_list+=[residue_number]


						debug=0
						if debug==1:
							if residue_number==24 and shift_class_ori=="Side_chain":
								print "DEBUGGING: shift_class is %s, residue number is %s, runname is %s, combined_abs_diff is %s" % (shift_class,residue_number,l_runname,inverse_combined_abs_diff)



			if shift_class in l_rci_dict or l_rci_method in [15,16,17,18,19,20,21,22,23,24,25,26,27,28]:
			#if shift_class in l_rci_dict:
				rci_shift_class=rci_shift_class_ori="RCI"
				if verbose>=10: print "Shift class %s was found in rci_dict" % rci_shift_class
				#print l_rci_dict[rci_shift_class]

				rci_param_dict=l_rci_dict[rci_shift_class]
				include=0
				missing_asignment=rci_param_dict["missing_asignment"]
			#for residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
				res_name=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["res_name"]

				if l_remove_stereo in [1,2]:
					if (l_remove_stereo==1 and res_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and res_name in l_res4gridsearch_list_dict and res_name in l_res4gridsearch_list):
						stereo=l_res4gridsearch_list_dict[res_name]["stereo"]
						if verbose>=10: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (res_name,residue_number,stereo)
				
						rci_shift_class= rci_shift_class_ori
						if stereo==0 and  rci_shift_class in l_shiftclass4gridsearch_list:
							rci_shift_class1="%s_ave" % rci_shift_class
							rci_shift_class2="%save" % rci_shift_class
							#print "Place 1"
							if rci_shift_class1 in l_rci_shift_class_lookup:
								rci_shift_class=rci_shift_class1
							elif rci_shift_class2 in l_rci_shift_class_lookup:
								rci_shift_class=rci_shift_class2
							else: 	
								if verbose>=0: print "Warning!!!! Neither 	rci_shift_class1 %s and rci_shift_class2 %s were found in l_rci_shift_class_lookup" % (rci_shift_class1,rci_shift_class2)



				coef_list=[]
				valueabs_list=[]
				#for rci_shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_rci_shift_classes"]:
					#if rci_shift_class in l_rci_dict:
						#print "Shift class %s was found in rci_dict" % rci_shift_class
				CA_found=CB_found=CO_found=NH_found=N_found=HA_found=0
				#Hertz_correction_applied="NONE"

				l_coef_dict={} 
				if rci_shift_class in l_parameter_dict:
					if res_name in  l_parameter_dict[rci_shift_class]:
					#print "Place 2"
						if "floor_value1" in l_parameter_dict[rci_shift_class][res_name]:
							l_floor_value1=l_parameter_dict[rci_shift_class][res_name]["floor_value1"]
						if "scaling1" in l_parameter_dict[rci_shift_class][res_name]:
							scaling1=l_parameter_dict[rci_shift_class][res_name]["scaling1"]
						if "power1" in l_parameter_dict[rci_shift_class][res_name]:
							power1=l_parameter_dict[rci_shift_class][res_name]["power1"]
						if "ceiling_value" in l_parameter_dict[rci_shift_class][res_name]:
							l_ceiling_value=l_parameter_dict[rci_shift_class][res_name]["ceiling_value"]
						if "shifts" in  l_parameter_dict[rci_shift_class][res_name]:
							l_coef_dict = l_parameter_dict[rci_shift_class][res_name]["shifts"]





				for shift_type in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][rci_shift_class]["details"]:
					if verbose>=10: print "shift_type=", shift_type
					if "CA" in shift_type:
						CA_found=1
					if "HA" in shift_type:
						HA_found=1
					if shift_type=="C":
						CO_found=1
					if "CB" in shift_type:
						CB_found=1
					if "H" in shift_type:
						NH_found=1
					if "N" in shift_type:
						N_found=1
				coef_trigger="%s%s%s%s%s%s" % (CA_found,CB_found,CO_found,NH_found,N_found,HA_found)

				for shift_type in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][rci_shift_class]["details"]:
					if verbose>=10: print "shift_type=", shift_type
					details=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][rci_shift_class]["details"][shift_type]
					l_smoothed_abs_diff=details["smoothed_abs_diff"]



					abs_diff_offset=0.0
					if len(rci_param_dict)>0 and shift_type in rci_param_dict["shifts"]:

						if l_miss_ass==1 and missing_asignment==1:

							#print "Place 1"
							if "CA" in shift_type:
								atomabs="CA"
							elif "HA" in shift_type:
								atomabs="HA"
							elif shift_type=="C":
								atomabs="CO"
							elif "CB" in shift_type:
								atomabs="CB"
							elif "H" in shift_type:
								atomabs="H"
							elif "N" in shift_type:
								atomabs="N"
							coeffabs=coeff_dict[coef_trigger][atomabs]

							if verbose>=10: print "Coefficient for shift %s and trigger %s was found in coeff_dict and is " % (shift_type,coef_trigger), coeffabs
						elif l_coef_dict!={}:

							#print "Place 2"
							coeffabs=l_coef_dict[shift_type]["weight"]
							if "abs_diff_offset" in l_coef_dict[shift_type]:
								abs_diff_offset=l_coef_dict[shift_type]["abs_diff_offset"]

						else:
							#print "Place 3"
							coeffabs=rci_param_dict["shifts"][shift_type]["weight"]
							if verbose>=10: print "Shift %s was found in  rci_dict with coeffabs" % shift_type, coeffabs

					else:
						coeffabs=1.0



					if l_bb_rci_opt==1 and l_non_bb_rci_dict!={}:
							if shift_type in l_non_bb_rci_dict:
								if "weight" in l_non_bb_rci_dict[shift_type]:
									coeffabs=l_non_bb_rci_dict[shift_type]["weight"]
									if verbose>=10 and shift_class=="Side_chain": print "FOUND", "residue_number=", residue_number, "shift_class= ", shift_class, "shift_type=", shift_type, "coeffabs=", coeffabs
								if "abs_diff_offset" in l_non_bb_rci_dict[shift_type]:
									abs_diff_offset=l_non_bb_rci_dict[shift_type]["abs_diff_offset"]

					if shift_class=="Side_chain" and verbose>=10 : print "residue_number=", residue_number, "shift_class= ", shift_class, "shift_type=", shift_type, "coeffabs=", coeffabs
						#print "Exiting..."
						#sys.exit()

					if is_number(abs_diff_offset):
						l_smoothed_abs_diff+=abs_diff_offset



					if find_max==1:
						l_smoothed_abs_diff=0
					elif find_min==1:
						l_smoothed_abs_diff=99999


					if 	l_smoothed_abs_diff<l_floor_value1:
					#if 1==1:
						l_smoothed_abs_diff=l_floor_value1



					if 	l_smoothed_abs_diff>early_ceiling_value:
						l_smoothed_abs_diff=early_ceiling_value


					coef_list+=[coeffabs]
					valueabs_list+=[l_smoothed_abs_diff*coeffabs*5.0]

					#if residue_number==75 and rci_shift_class=="Backbone":
					#	print residue_number, rci_shift_class, shift_type, details


				if 	len(valueabs_list)>0:
					if verbose>=10: print "coef_list=", coef_list
					coef_local_mean=stats.lmean(coef_list)
					coef_offset=coef_mean/coef_local_mean
					valueabs_mean=stats.lmean(valueabs_list)
					if l_scale==1:
						valueabs_mean=valueabs_mean*coef_offset
					if valueabs_mean!=0:
						value_abs=(1.0/(abs(valueabs_mean)**1.5))
					if value_abs>l_ceiling_value:
						value_abs=l_ceiling_value

					l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][rci_shift_class_ori]["bb_rci_abs_diff"]=value_abs

					if verbose>=10: print "bb_rci_abs_diff", residue_number, value_abs

					if l_rci_method in [15,16,17,18,19,20,21,22,23,24,25,26,27,28]:
						bb_rci_value=value_abs

						if is_number(l_bb_rci_weight) and l_sc_bb_combine!=1:
							l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_ori]["bb_rci_weight"]=l_bb_rci_weight



					if residue_number not in rci_residue_numbers_list:
						rci_residue_numbers_list+=[residue_number]

				else: 	
					value_abs="NONE"




			if l_rci_method in [15,16,17,18,19,20,21,22,23,24,25,26,27,28] and l_sc_bb_combine==1:
				#print "Place 3"
				if is_number(bb_rci_value) and is_number(sc_rci_value):
					combined_rci=(l_sc_rci_weight*sc_rci_value) + (l_bb_rci_weight*bb_rci_value)
					l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class_combo]["combined_abs_diff"]=combined_rci
					if shift_class_combo=="Side_chain" and verbose>=10:
						print residue_number, "combined_rci=", combined_rci, "sc_rci_value=", sc_rci_value, "bb_rci_value=", bb_rci_value
			debug=0
			if debug==1:
				if shift_class_combo in ["Side_chain","Side_chain_ave"] and verbose>=0:
					print "Before end correction: ", shift_class_combo, residue_number, "inverse_combined_abs_diff=", inverse_combined_abs_diff, "bb_rci_value=", bb_rci_value
			debug=0

		if l_sc_end_correct:
			corr_mode="sc"
			end_correction(corr_mode,rci_residue_numbers_list,l_pdb_dict,shift_class_ori,l_ceiling_value,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,l_shiftclass4gridsearch_list,l_shift_class_lookup)
			end_correction(corr_mode,rci_residue_numbers_list,l_pdb_dict,shift_class_ori,l_ceiling_value,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,l_shiftclass4gridsearch_list,l_shift_class_lookup)

		if shift_class in l_rci_dict or l_rci_method in [15,16,17,18,19,20,21,22,23,24,25,26,27,28]:
			corr_mode="bb"
			rci_shift_class=rci_shift_class_ori="RCI"

			if l_bb_end_correct:
				end_correction(corr_mode,rci_residue_numbers_list,l_pdb_dict,rci_shift_class,l_ceiling_value,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,l_shiftclass4gridsearch_list,l_shift_class_lookup)
				end_correction(corr_mode,rci_residue_numbers_list,l_pdb_dict,rci_shift_class,l_ceiling_value,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,l_shiftclass4gridsearch_list,l_shift_class_lookup)

			#print "Smoothed value for residue 2 shift_class %s is " % shift_class, l_pdb_dict["rci"]["rc"]["shift_diff_dict"][2]["residue_shift_classes"]["RCI"]["bb_rci_abs_diff"]


		if residue_counter==0:
			if verbose>0: print "Warning!!! residue_counter is 0 for class %s " % shift_class, "l_detect_missing_atoms= ", l_detect_missing_atoms, "l_completeness_found= ", l_completeness_found

		elif l_detect_missing_atoms==1 and l_completeness_found==0 and residue_counter>0 and shift_class_ori in l_parameter_dict:


			percentage_residues_with_missing_atoms=(float(residues_with_missing_atoms_counter)/float(residue_counter))*100.0

			percentage_residues_with_excessive_atoms=(float(residues_with_excessive_atoms_counter)/float(residue_counter))*100.0

			average_completeness=average_missing_completeness=average_excessive_completeness="NONE"

			if len(completeness_list)>0:
				average_completeness=stats.lmean(completeness_list)

			if len(missing_completeness_list)>0:
				average_missing_completeness=stats.lmean(missing_completeness_list)

			if len(excessive_completeness_list)>0:
				average_excessive_completeness=stats.lmean(excessive_completeness_list)


			l_completeness_report_text+="Number of residues with missing atoms is %s from  %s (%s%s)" % (residues_with_missing_atoms_counter,residue_counter,percentage_residues_with_missing_atoms,"%") + linesep
			l_completeness_report_text+="Number of residues with excessive atoms is %s from  %s (%s%s)" % (residues_with_excessive_atoms_counter,residue_counter,percentage_residues_with_excessive_atoms,"%") + linesep
			l_completeness_report_text+="Average Completeness = %s%s" % (average_completeness,"%") + linesep
			l_completeness_report_text+="Average Missing Completeness = %s%s" % (average_missing_completeness,"%")  + linesep
			l_completeness_report_text+="Average Excessive Completeness = %s%s" % (average_excessive_completeness,"%")  + linesep


			write_file(l_missing_atoms_text,l_missing_atoms_file)
			write_file(l_completeness_per_residue_text,l_completeness_per_residue_file)
			write_file(l_completeness_report_text,l_completeness_report_file)
			if verbose>=0: print "Completeness report is in ", l_completeness_report_file


	if l_zodb:
		l_pdb_dict._p_changed = True		
		transaction.savepoint(True)
		transaction.commit()
		#transaction.get().commit() 
		connection.close()
		db.close()
		storage.close()

		l_pdb_dict={}


	if l_files4training:
		make_dir_no_remove(runname_full_path)
		file_4_training="%s/%s_training.py" % (l_runname_full_path,l_runname)
		write_file(dict_4_training_text,file_4_training)


	debug=0
	if debug==1:
		print "Exiting after function combine()"
		sys.exit()
	return 

def remove_stereospecificity(shift_class,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,res_name,residue_number,l_shiftclass4gridsearch_list,l_shift_class_lookup):
	"""
	Removing stereospecificity
	"""
	verbose=0
	if verbose>=1: print "Running function remove_stereospecificity()"
	l_shift_class_new=shift_class
	shift_class_ori=shift_class
	if l_remove_stereo!=0:
		if (l_remove_stereo==1 and res_name in l_res4gridsearch_list_dict) or (l_remove_stereo==2 and res_name in l_res4gridsearch_list_dict and res_name in l_res4gridsearch_list):
			stereo=l_res4gridsearch_list_dict[res_name]["stereo"]
			if verbose>=10: print "Residue %s%s was found in  l_res4gridsearch_list_dict with stereo= %s" % (res_name,residue_number,stereo)				
			shift_class= shift_class_ori
			if stereo==0 and  shift_class in l_shiftclass4gridsearch_list:
				shift_class1="%s_ave" % shift_class
				shift_class2="%save" % shift_class
				#print "Place 1"
				if shift_class1 in l_shift_class_lookup:
					shift_class=shift_class1
				elif shift_class2 in l_shift_class_lookup:
					shift_class=shift_class2
				else: 	
					if verbose>=0: print "Warning!!!! Neither 	shift_class1 %s and shift_class2 %s were found in l_shift_class_lookup" % (shift_class1,shift_class2)
	debug=0
	if debug==1:
		print "Exiting after function remove_stereospecificity()"
		sys.exit()
	return l_shift_class_new

def end_correction(mode,residue_numbers_list,l_pdb_dict,shift_class,l_ceiling_value,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,l_shiftclass4gridsearch_list,l_shift_class_lookup):
	"""
	Applying end correction to RCI

l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["combined_abs_diff"]=value_abs
l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["combined_abs_diff"]=inverse_combined_abs_diff
				res_name=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["res_name"]
	"""
	verbose=0
	#if shift_class=="Side_Chain": verbose=1
	if verbose>=1: print "Running function end_correction()"
	c_terminal_end_corr_limit=-3 #-5 # was -3 was -6
	n_terminal_end_corr_limit=4  # was 5
	#print "Exiting...."
	#sys.exit()
	residue_numbers_list.sort()
	max_value=""
	max_res_number=""
	shift_class_ori=shift_class
	if verbose>=10: 
		if mode=="bb":
			print "residue_numbers_list=", residue_numbers_list

	if mode=="sc":
		dict_key="combined_abs_diff"
	elif  mode=="bb":
		dict_key="bb_rci_abs_diff"

		#print "residue_numbers_list= ",residue_numbers_list 
		#print "Exiting...."
		#sys.exit()
	#print "dict_key=", dict_key
	if verbose>=1: 
		if shift_class in ["RCI", "Side_chain"]:
			print "Looking for maximal value at N-terminus"
	for res_num in residue_numbers_list[:n_terminal_end_corr_limit]:
		if verbose>=1: 
			if shift_class in ["RCI", "Side_chain"]:

				print "res_num=", res_num, "shift_class=", shift_class
		if res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:

			res_name=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["res_name"]
			shift_class=shift_class_ori
			shift_class=remove_stereospecificity(shift_class,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,res_name,res_num,l_shiftclass4gridsearch_list,l_shift_class_lookup)
			#print "Place 1", "shift_class_ori= ", shift_class_ori, "shift_class= ", shift_class
			if shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"]:
				#print "Place 2"
				if dict_key in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class]:
					#print "Place 3"
					combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class][dict_key]
					if verbose>=1: 
						if shift_class in ["RCI", "Side_chain"]:
							print "res_num=", res_num, "shift_class=", shift_class	, 		"combined_abs_diff=", combined_abs_diff			
					if is_number(combined_abs_diff):
						if max_value=="" or float(combined_abs_diff)>max_value:
							max_value=float(combined_abs_diff)
							max_res_number=res_num	
					debug=0
					if debug==1:
						#if shift_class_ori in ["Side_chain","Side_chain_ave"]:
						if shift_class_ori in ["RCI",  "Side_chain"]:
						#if res_num==2:
							print "Before N_terminal end correction: res_num =",res_num , "combined_abs_diff=", combined_abs_diff, "max_res_number=", max_res_number, "shift_class_ori=",shift_class_ori, "shift_class=", shift_class






				else:
					if verbose>=10: print "1) Warning!!!! combined_abs_diff is not in l_pdb_dict  for rci rc and shift_diff_dict keys and residue %s and shift_class %s" % (res_num,shift_class )

			else:
				if verbose>=10: print "1) Warning!!! shift_class %s is not in l_pdb_dict  for rci rc and shift_diff_dict keys and residue %s" % (shift_class,res_num)


		else:
			if res_num not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
				if verbose>=10: print "1) Warning!!! Residue number %s is not in  l_pdb_dict for rci rc and shift_diff_dict keys and shift_class" % (res_num,shift_class)
			elif res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num<=max_res_number:
				if verbose>=10: print "1) Residue number %s is <=max_res_number %s " % (res_num,max_res_number)
			elif res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num>max_res_number and max_value=="":
				if verbose>=10: print "1) max_value is empty for residue number %s and class %s" % (res_num,shift_class)


	debug=0
	if debug==1:
		#if shift_class_ori in ["Side_chain","Side_chain_ave","RCI"]:
		if shift_class_ori in ["RCI", "Side_chain"]:
			print "max_res_number=", max_res_number, "max_value=", max_value, "shift_class_ori= ", shift_class_ori, "shift_class= ", shift_class
			#print "Exiting..."
			#sys.exit()

	#sys.exit()
	for res_num in residue_numbers_list[:n_terminal_end_corr_limit]:
		res_name=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["res_name"]
		if res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num<max_res_number and max_value!="":
			shift_class=shift_class_ori
			shift_class=remove_stereospecificity(shift_class,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,res_name,res_num,l_shiftclass4gridsearch_list,l_shift_class_lookup)
			if shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"]:
				if dict_key in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class]:
					combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class][dict_key]
					if is_number(combined_abs_diff) and float(combined_abs_diff)<max_value:
						l_diff=abs(max_value-float(combined_abs_diff))
						combined_abs_diff_new=combined_abs_diff+(l_diff*2.0)
						if combined_abs_diff_new>l_ceiling_value*2.0:
							combined_abs_diff_new=l_ceiling_value

						l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class_ori][dict_key]=combined_abs_diff_new


						#combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["bb_rci_abs_diff"]

						#if verbose>=0: print "final_smoothing(): shift_class= ", shift_class, "residue_number= ", residue_number, "combined_abs_diff= ", combined_abs_diff


						debug=0
						if debug==1:
							#if shift_class_ori in ["Side_chain","Side_chain_ave"]:
							if shift_class_ori in ["RCI", "Side_chain"]:
								print "After N-terminal correction: res_num =",res_num , "dict_key=", dict_key, "combined_abs_diff=", combined_abs_diff, "l_diff*2.0=", (l_diff*2.0), "combined_abs_diff_new=", combined_abs_diff_new, "shift_class_ori=",shift_class_ori, "shift_class=", shift_class
								#print "Smoothed value for residue 2 is ", l_pdb_dict["rci"]["rc"]["shift_diff_dict"][2]["residue_shift_classes"]["RCI"]["bb_rci_abs_diff"]


				else:
					if verbose>=1: print "2) Warning!!!! combined_abs_diff is not in l_pdb_dict  for rci rc and shift_diff_dict keys and residue %s and shift_class %s" % (res_num,shift_class )

			else:
				if verbose>=1: print "2) Warning!!! shift_class %s is not in l_pdb_dict  for rci rc and shift_diff_dict keys and residue %s" % (shift_class,res_num)


		else:
			if res_num not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
				if verbose>=1: print "2) Warning!!! Residue number %s is not in  l_pdb_dict for rci rc and shift_diff_dict keys and shift_class" % (res_num,shift_class)
			elif res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num<=max_res_number:
				if verbose>=1: print "2) Residue number %s is <=max_res_number %s " % (res_num,max_res_number)
			elif res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num>max_res_number and max_value=="":
				if verbose>=1: print "2) max_value is empty for residue number %s and class %s" % (res_num,shift_class)


	if verbose>=1: 
		if shift_class in ["RCI", "Side_chain"]:
			print "Looking for maximal value at C-terminus"


	max_value=""
	max_res_number=""
	for res_num  in residue_numbers_list[c_terminal_end_corr_limit:]:
		res_name=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["res_name"]
		if verbose>=1: 
			if shift_class in ["RCI", "Side_chain","Side_chain_ave"] or  shift_class_ori in ["RCI", "Side_chain","Side_chain_ave"]:

				print "res_num=", res_num, "shift_class=", shift_class, "shift_class_ori=", shift_class_ori
		if res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
			shift_class=shift_class_ori
			shift_class=remove_stereospecificity(shift_class,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,res_name,res_num,l_shiftclass4gridsearch_list,l_shift_class_lookup)
			if shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"]:
				if dict_key in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class]:
					combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class][dict_key]
					if verbose>=1: 
						if shift_class in ["RCI", "Side_chain","Side_chain_ave"] or  shift_class_ori in ["RCI", "Side_chain","Side_chain_ave"]:
							print "Before C_terminal end correction: ", "res_num=", res_num, "shift_class=", shift_class	, 		"combined_abs_diff=", combined_abs_diff		
					if is_number(combined_abs_diff):
						if max_value=="" or float(combined_abs_diff)>max_value:
							max_value=float(combined_abs_diff)
							max_res_number=res_num	
				else:
					if verbose>=10: print "3) Warning!!!! combined_abs_diff is not in l_pdb_dict  for rci rc and shift_diff_dict keys and residue %s and shift_class %s" % (res_num,shift_class )

			else:
				if verbose>=10: print "3) Warning!!! shift_class %s is not in l_pdb_dict  for rci rc and shift_diff_dict keys and residue %s" % (shift_class,res_num)



		else:
			if res_num not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
				if verbose>=10: print "3) Warning!!! Residue number %s is not in  l_pdb_dict for rci rc and shift_diff_dict keys and shift_class" % (res_num,shift_class)
			elif res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num<=max_res_number:
				if verbose>=1: print "3) Residue number %s is <=max_res_number %s " % (res_num,max_res_number)
			elif res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num>max_res_number and max_value=="":
				if verbose>=1: print "3) max_value is empty for residue number %s and class %s" % (res_num,shift_class)

	debug=0
	if debug==1:
		#if shift_class_ori in ["Side_chain","Side_chain_ave","RCI"]:
		if shift_class in ["RCI", "Side_chain","Side_chain_ave"] or  shift_class_ori in ["RCI", "Side_chain","Side_chain_ave"]:
			print "C-terminal end correction: max_res_number=", max_res_number, "max_value=", max_value, "shift_class_ori= ", shift_class_ori, "shift_class= ", shift_class
			#print "Exiting..."
			#sys.exit()

	for res_num  in residue_numbers_list[c_terminal_end_corr_limit:]:
		res_name=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["res_name"]
		if res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num>max_res_number and max_value!="":
			shift_class=shift_class_ori
			shift_class=remove_stereospecificity(shift_class,l_remove_stereo,l_res4gridsearch_list_dict,l_res4gridsearch_list,res_name,res_num,l_shiftclass4gridsearch_list,l_shift_class_lookup)
			if shift_class in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"]:
				if dict_key in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class]:
					combined_abs_diff=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class][dict_key]
					if is_number(combined_abs_diff) and float(combined_abs_diff)<max_value:
						l_diff=abs(max_value-float(combined_abs_diff))
						combined_abs_diff_new=combined_abs_diff+(l_diff*2.0)
						if combined_abs_diff_new>l_ceiling_value*2.0:
							combined_abs_diff_new=l_ceiling_value
						l_pdb_dict["rci"]["rc"]["shift_diff_dict"][res_num]["residue_shift_classes"][shift_class_ori][dict_key]=combined_abs_diff_new
						debug=0
						if debug==1:
							if shift_class_ori in ["Side_chain","RCI","Side_chain_ave"]:
								#if res_num in [78]:
								print "After C-terminal end correction, residue number= %s, dict_key= %s, shift_class_ori=%s, combined_abs_diff_new= %s  " % (res_num, dict_key,shift_class_ori,combined_abs_diff_new)
						debug=0

				else:
					if verbose>=10: print "2) Warning!!!! combined_abs_diff is not in l_pdb_dict  for rci rc and shift_diff_dict keys and residue %s and shift_class %s" % (res_num,shift_class )

			else:
				if verbose>=10: print "2) Warning!!! shift_class %s is not in l_pdb_dict  for rci rc and shift_diff_dict keys and residue %s" % (shift_class,res_num)


		else:
			if res_num not in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
				if verbose>=10: print "4) Warning!!! Residue number %s is not in  l_pdb_dict for rci rc and shift_diff_dict keys and shift_class" % (res_num,shift_class)
			elif res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num<=max_res_number:
				if verbose>=1: print "4) Residue number %s is <=max_res_number %s " % (res_num,max_res_number)
			elif res_num in l_pdb_dict["rci"]["rc"]["shift_diff_dict"] and res_num>max_res_number and max_value=="":
				if verbose>=1: print "4) max_value is empty for residue number %s and class %s" % (res_num,shift_class)


	if verbose>=10: print "function end_correction() is finished"
	debug=0
	if debug==1:
		print "Exiting after function end_correction()"
		sys.exit()
	return

def build_rci_report_residues(rci_report_residues):
	"""
	Build dictionary to report average RCI and secCS values for protein segments and individual residues
	"""
	verbose=0
	if verbose>=0: print "Running function build_rci_report_residues()"
	counter=1
	l_dict={}
	for i in rci_report_residues.split(":"):
		print i
		name_text=i.replace(",","_")
		l_list=[]		
		for j in i.split(","):		
			if "-"not in j:
				if len(j.split())>0:
					h=j.split()[0]
					if is_number(h):
						l_list+=[ int(h)]
					else:
						print "1) rci_report_residues got misformatted", [h]
			else:
				k_list=j.split("-")
				if len(k_list)>1:
					l_start_init,l_end_init=k_list[0],k_list[1]
					if is_number(l_start_init):
						if is_number(l_end_init):
							for m in range(int(l_start_init),int(l_end_init)+1):
								l_list+=[ int(m)]		
						else:
							print "4) rci_report_residues got misformatted", [l_end_init]	
					else:
						print "3) rci_report_residues got misformatted", [l_start_init]
				else:
					print "2) rci_report_residues got misformatted", [j]
						
		l_dict[counter]={"name":name_text,"residues":l_list}
		counter+=1
	debug=0
	if debug==1:
		print "l_dic=", l_dict
		print "Exiting after function build_rci_report_residues()"
		sys.exit()
	return l_dict



def calculate_rci(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		rc_residue_list,\
		early_floor_value,\
		shift_class_description_dict,\
		shift_class_lookup,\
		value_search,\
		database_file,\
		rci_dict,\
		parameter_dict,\
		averaging,\
		bb_averaging,\
		coef_mean,\
		miss_ass,\
		H_Hertz_corr,\
		C_Hertz_corr,\
		N_Hertz_corr,\
		floor_value1,\
		scale,\
		ceiling_value,\
		rci_method,\
		find_value_mode,\
		no_outliers,\
		power1,\
		scaling1,\
		rci_out_offset,\
		value_search2,\
		full_project_dir,\
		rci_results_dir,\
		gnuplot_program,\
		make_rci_pics,\
		make_rci_images,\
		make_rci_reports,\
		make_secCS_reports,\
		rci_report_residues_dict,\
		l_runname,\
		l_gridsearch_dir,\
		l_protein_sequence_list,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_do_gridsearch1,\
		l_files4training,\
		rci_parameter_dict_file,\
		l_runname_result_dict,\
		l_coef_only,\
		l_completeness_found,\
		l_forbidden_atoms,\
		l_sc_rci_weight,\
		l_bb_rci_weight,\
		l_sc_bb_combine,\
		l_detect_missing_atoms,\
		l_rc_offset_gridsearch,\
		l_rc_assign_dic,\
		l_bb_end_correct,\
		l_sc_end_correct,\
		find_rc_order_parameter,\
		find_MD_RMSF,\
		find_NMR_RMSF,\
		find_ASA,\
		find_B_factor,\
		l_exclude_atoms,\
		l_bb_rci_opt,\
		):
	"""
	Caclulate RCI
	"""
	verbose=0
	if verbose>=1: print "Running function calculate_rci()"
	if verbose>=1: print "l_rc_assign_dic= ", l_rc_assign_dic
	#sys.exit()
	rci_result_full_path="%s/%s" % (full_project_dir,rci_results_dir)
	if l_runname==l_gridsearch_dir and l_gridsearch_dir=="nogridsearch":
		runname_full_path="%s/%s" % (rci_result_full_path,l_runname)
		gridsearch_full_path="%s/%s" % (rci_result_full_path,l_gridsearch_dir)

		if l_detect_missing_atoms or l_files4training or make_rci_pics>0:
			make_dir_no_remove(rci_result_full_path)
			make_dir_no_remove(gridsearch_full_path)
	else:
		gridsearch_full_path="%s/%s" % (rci_result_full_path,l_gridsearch_dir)
		runname_full_path="%s/%s" % (gridsearch_full_path,l_runname)

		if l_detect_missing_atoms or l_files4training or make_rci_pics>0:
			make_dir_no_remove(gridsearch_full_path)
			make_dir_no_remove(rci_result_full_path)



		#print "Making ", gridsearch_full_path, l_detect_missing_atoms, l_files4training, make_rci_pics
		#sys.exit()

	gap_filling(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		early_floor_value,\
		rc_residue_list,\
		shift_class_description_dict,\
		shift_class_lookup,\
		value_search,\
		database_file,\
		rci_dict,\
		parameter_dict,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		)

	smoothing(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		bb_averaging,\
		shift_class_lookup,\
		rc_residue_list,\
		rci_dict,\
		database_file,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_rc_assign_dic,\
		)

	combine(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		database_file,\
		rci_dict,\
		shift_class_description_dict,\
		coef_mean,\
		miss_ass,\
		H_Hertz_corr,\
		C_Hertz_corr,\
		N_Hertz_corr,\
		floor_value1,\
		early_floor_value,\
		scale,\
		ceiling_value,\
		shift_class_lookup,\
		parameter_dict,\
		rci_method,\
		find_value_mode,\
		no_outliers,\
		power1,\
		scaling1,\
		runname_full_path,\
		l_runname,\
		l_protein_sequence_list,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_files4training,\
		l_do_gridsearch1,\
		rci_parameter_dict_file,\
		l_coef_only,\
		l_completeness_found,\
		l_forbidden_atoms,\
		l_sc_rci_weight,\
		l_bb_rci_weight,\
		l_sc_bb_combine,\
		l_detect_missing_atoms,\
		l_rc_offset_gridsearch,\
		l_bb_end_correct,\
		l_sc_end_correct,\
		l_exclude_atoms,\
		l_bb_rci_opt,\
		)

	final_smoothing(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		bb_averaging,\
		shift_class_lookup,\
		rc_residue_list,\
		rci_dict,\
		database_file,\
		rci_out_offset,\
		l_runname,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_runname_result_dict,\
		l_do_gridsearch1,\
		rci_method,\
		l_sc_bb_combine,\
		)



	second_gap_filling(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		value_search2,\
		shift_class_lookup,\
		rc_residue_list,\
		rci_dict,\
		database_file,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		)


	#averaging=5
	double_final_smoothing(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		averaging,\
		shift_class_lookup,\
		rc_residue_list,\
		rci_dict,\
		database_file,\
		rci_out_offset,\
		l_runname,\
		l_res4gridsearch_list,\
		l_res4gridsearch_list_dict,\
		l_remove_stereo,\
		l_shiftclass4gridsearch_list,\
		l_do_gridsearch1,\
		l_runname_result_dict,\
		rci_method,\
		l_sc_bb_combine,\
		l_sc_rci_weight,\
		l_bb_rci_weight,\
		find_rc_order_parameter,\
		find_MD_RMSF,\
		find_NMR_RMSF,\
		find_ASA,\
		find_B_factor,\
		)

	if l_do_gridsearch1==0 or make_rci_pics==2:
		if verbose>=1: print "Creating RCI pictures"
		make_rci_plots(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		runname_full_path,\
		gnuplot_program,\
		shift_class_lookup,\
		make_rci_pics,\
		make_rci_images,\
		database_file,\
		l_runname,\
		find_rc_order_parameter,\
		find_MD_RMSF,\
		find_NMR_RMSF,\
		find_ASA,\
		find_B_factor,\
		rci_output_dict,\
		)

	return





def write_rci_reports(\
		l_pdb_list,\
		l_pdb_dict,\
		l_zodb,\
		l_database_file,\
		l_rci_result_dir_full,\
		l_make_rci_reports,\
		l_make_secCS_reports,\
		l_rci_report_residues_dict,\
		l_runname_list,\
		l_shiftclass4gridsearch_list,\
		l_do_gridsearch1,\
		l_runname_dict,\
		l_rc_offset_gridsearch,\
		):
	"""
	Write reports with RCI and sec CS values + averages

	for shift_class in l_pdb_dict["rci"]["rc"]["protein_shift_classes"]:
		file_base_name= shift_class.replace(" ","_")
		data_file="%s/%s" % (data_dir,file_base_name)
		if verbose>=10: print "Processing shift class", shift_class
		if "rci_text" in l_pdb_dict["rci"]["rc"]["protein_shift_classes"][ shift_class]:
			rci_text=l_pdb_dict["rci"]["rc"]["protein_shift_classes"][shift_class]["rci_text"]
			write_file(rci_text,data_file)

									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["true_diff"]=true_diff
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["abs_diff"]=abs_diff
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["exp_shift_value"]=float(exp_shift_value)
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["pred_value"]=float(pred_shift_value)
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["sec_exp_shift_value"]=sec_exp_shift_value
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["sec_pred_value"]=sec_pred_shift_value
									l_pdb_dict[i][l_predictor]["shift_diff_dict"][exp_resid_number][shift_type]["res_name"]=res_name



								l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["mean"]=l_per_residue_mean
								l_pdb_dict[i][l_predictor]["protein_shift_classes"][shift_class]["per_residue_dict"][residue_number]["median"]=l_per_residue_median
	

						l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["combined_abs_diff"]=inverse_combined_abs_diff


										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]={}" % (shift_class,residue_number) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['resid_name']='%s'" % (shift_class,residue_number,resid_name) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['residue_number']='%s'" % (shift_class,residue_number,residue_number) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['per_residue_mean']='%s'" % (shift_class,residue_number,l_per_residue_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['diff_of_diff_mean']='%s'" % (shift_class,residue_number,diff_of_diff_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['global_mean']='%s'" % (shift_class,residue_number,l_mean) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['stdev']='%s'" % (shift_class,residue_number,l_std) + linesep
										l_module_text+="per_model_report_dict['shift_class']['%s']['per_residue_mean_results'][%s]['outlier']='%s'" % (shift_class,residue_number,l_mean_outlier) + linesep

									if l_total_module_text_on:
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]={}" % (i,shift_class,residue_number) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['resid_name']='%s'" % (i,shift_class,residue_number,resid_name) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['residue_number']='%s'" % (i,shift_class,residue_number,residue_number) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['per_residue_mean']='%s'" % (i,shift_class,residue_number,l_per_residue_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['diff_of_diff_mean']='%s'" % (i,shift_class,residue_number,diff_of_diff_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['global_mean']='%s'" % (i,shift_class,residue_number,l_mean) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['stdev']='%s'" % (i,shift_class,residue_number,l_std) + linesep
										l_total_module_text+="report_dict['%s']['shift_class']['%s']['per_residue_mean_results'][%s]['outlier']='%s'" % (i,shift_class,residue_number,l_mean_outlier) + linesep


l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["rci_values"]=double_smoothed_value

					l_parameter_dict={}
					l_parameter_dict[shiftclass]={}
					l_parameter_dict[shiftclass]["missing_asignment"]=0
					l_parameter_dict[shiftclass]["early_floor_value"]=early_floor_value
					l_parameter_dict[shiftclass]["floor_value1"]=floor_value1
					l_parameter_dict[shiftclass]["ceiling_value"]=ceiling_value
					l_parameter_dict[shiftclass]["power1"]=power1
					l_parameter_dict[shiftclass]["scaling1"]=scaling1
					l_parameter_dict[shiftclass]["res_type"]=aa
					for combination in l_final_grid_list:
						if verbose>=1: print "Processing combination ", combination
						l_parameter_dict[shiftclass]["shifts"]={}
						total_coeff=0
						runname_suffx=""
						for i in  combination:
							if verbose>=10: print "i= ", i
							atomname,atom_coef=i[0],i[1]
							l_parameter_dict[shiftclass]["shifts"][atomname]={"weight":atom_coef,"res_name":aa}
							total_coeff+=atom_coef
							runname_suffx+="_%s_%s" % (atomname,atom_coef)

	"""
	verbose=0
	if verbose>=1: print "Running function write_rci_reports()"

	#l_rci_result_dir_full="%s/%s" % (l_full_project_dir,l_rci_results_dir)
	make_dir_no_remove(l_rci_result_dir_full)

	rci_result_dict_text="dic={}" + linesep

	#if l_make_rci_reports:
	rci_reports_dir_full="%s/RCI_reports" % (l_rci_result_dir_full)
	make_dir_no_remove(rci_reports_dir_full)
	rci_result_dict_file="%s/rci_results.py" % rci_reports_dir_full


	if l_make_secCS_reports:
		secCS_reports_dir_full="%s/secCS_reports" % (l_rci_result_dir_full)
		make_dir_no_remove(secCS_reports_dir_full)

	if l_zodb:
		storage = FileStorage(l_database_file)
		db = DB(storage)		
		connection = db.open()
		main_database = connection.root()
		l_pdb_dict=main_database["pdb_dict"]

	for runname in l_runname_list:
		rci_result_dict_text+="dic['%s']={}" % runname + linesep
		if verbose>=1:print "Processing runname ", runname
		for shift_class in l_pdb_dict["rci"]["rc"]["protein_shift_classes"]:
			if l_do_gridsearch1==0 or (l_do_gridsearch1>0 and shift_class in l_shiftclass4gridsearch_list):
				runname_parameters={}
				if shift_class in l_runname_dict:
					if runname in l_runname_dict[shift_class]:
						runname_parameters=l_runname_dict[shift_class][runname]
					else:	
						if  l_do_gridsearch1>0:
							print "Warning!!! Runname %s was not found in l_runname_dict for shift_class %s " % (runname,shift_class)
				else:
					if l_do_gridsearch1>0:
						print "Warning!!!! Shift class %s was not found in l_runname_dict" % shift_class

				if verbose>=1:print "Processing shift_class ", shift_class
				rci_result_dict_text+="dic['%s']['%s']={'residue_info':{},'run_info':{}}" % (runname,shift_class) + linesep


				if runname_parameters!={}:
					rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']={}" % (runname,shift_class) + linesep		
					done=0																			
					for residue_type  in runname_parameters:
						if done==0:
							for parameter in runname_parameters[residue_type ]:
								parameter_value=runname_parameters[residue_type ][parameter]
								if parameter!="shifts":
									rci_result_dict_text+="dic['%s']['%s']['run_info']['%s']='%s'" % (runname,shift_class,parameter,parameter_value) + linesep
								else:
									rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']={}" % (runname,shift_class) + linesep	
									for atomname in runname_parameters[residue_type ]["shifts"]:
										weight=runname_parameters[residue_type ]["shifts"][atomname]['weight']
										abs_diff_offset=0.0
										rc_offset=0.0
										if 'abs_diff_offset' in runname_parameters[residue_type ]["shifts"][atomname]:
											abs_diff_offset=runname_parameters[residue_type ]["shifts"][atomname]['abs_diff_offset']
											if l_rc_offset_gridsearch in [1]:
												rc_offset=abs_diff_offset
												abs_diff_offset=0.0

										rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']['%s']={'weight':%s}" % (runname,shift_class,atomname,weight) + linesep										
										rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']['%s']['abs_diff_offset']=%s" % (runname,shift_class,atomname,abs_diff_offset) + linesep										
										rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']['%s']['rc_offset']=%s" % (runname,shift_class,atomname,rc_offset) + linesep										
							done=1

				for residue_number in l_pdb_dict["rci"]["rc"]["shift_diff_dict"]:
					res_name=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["res_name"]

					rci_result_dict_text+="dic['%s']['%s']['residue_info'][%s]={}" % (runname,shift_class,residue_number) + linesep
					rci_result_dict_text+="dic['%s']['%s']['residue_info'][%s]['res_name']='%s'" % (runname,shift_class,residue_number,res_name) + linesep

					#l_runname_result_dict
					if "rci_values" in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]:
						if runname in l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["rci_values"]:
							rci_value=l_pdb_dict["rci"]["rc"]["shift_diff_dict"][residue_number]["residue_shift_classes"][shift_class]["rci_values"][runname]
							rci_result_dict_text+="dic['%s']['%s']['residue_info'][%s]['rci_value']='%s'" % (runname,shift_class,residue_number,rci_value) + linesep

						else:
							print "Warning!!! Runname %s is not in l_pdb_dict for shift_class %s  and residue  %s%s " % (runname,shift_class,res_name,residue_number)



	if l_zodb:
		l_pdb_dict._p_changed = True		
		transaction.savepoint(True)
		transaction.commit()
		#transaction.get().commit() 
		connection.close()
		db.close()
		storage.close()

		l_pdb_dict={}

	write_file(rci_result_dict_text,rci_result_dict_file)
	if verbose>=1: print "Dictionary with RCI results is located at ", rci_result_dict_file

	debug=0
	if debug==1:
		print "Exiting after function write_rci_reports()"
		sys.exit()
	return


def write_gridsearch_dict(l_rc_offset_gridsearch,l_runname_dict,l_database_file,l_pdb_list,l_zodb,rci_result_dict_file,l_runname_result_dict,runname,l_rci_result_dict_text,l_pdb_dict,l_shiftclass4gridsearch_list,l_do_gridsearch1):
	"""
	Write gridsearch result file
					if residue_number not in l_runname_result_dict:
						l_runname_result_dict[residue_number]={"res_name":res_name,"residue_shift_classes":{}}
					if shift_class not in l_runname_result_dict[residue_number]["residue_shift_classes"]:
						l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class]={}
	"""
	verbose=0
	if verbose>=1: print "Running function write_gridsearch_dict()"


	if l_zodb:
		storage = FileStorage(l_database_file)
		db = DB(storage)		
		connection = db.open()
		main_database = connection.root()
		l_pdb_dict=main_database["pdb_dict"]

	l_rci_result_dict_text="dic['%s']={}" % runname + linesep


	if 1==1: # Reserved
		for shift_class in l_pdb_dict["rci"]["rc"]["protein_shift_classes"]:
			if l_do_gridsearch1==0 or (l_do_gridsearch1>0 and shift_class in l_shiftclass4gridsearch_list):
				runname_parameters={}
				if verbose>=1:print "Processing shift_class ", shift_class
				l_rci_result_dict_text+="dic['%s']['%s']={'residue_info':{},'run_info':{}}" % (runname,shift_class) + linesep

				if shift_class in l_runname_dict:
					if runname in l_runname_dict[shift_class]:
						runname_parameters=l_runname_dict[shift_class][runname]
					else:	
						if  l_do_gridsearch1>0:
							print "Warning!!! Runname %s was not found in l_runname_dict for shift_class %s " % (runname,shift_class)
				else:
					if l_do_gridsearch1>0:
						print "Warning!!!! Shift class %s was not found in l_runname_dict" % shift_class

				if runname_parameters!={}:
					l_rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']={}" % (runname,shift_class) + linesep		
					done=0	

					for residue_type  in runname_parameters:
						if done==0:
							for parameter in runname_parameters[residue_type ]:
								parameter_value=runname_parameters[residue_type ][parameter]
								if parameter!="shifts":
									l_rci_result_dict_text+="dic['%s']['%s']['run_info']['%s']='%s'" % (runname,shift_class,parameter,parameter_value) + linesep
								else:
									l_rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']={}" % (runname,shift_class) + linesep	
									for atomname in runname_parameters[residue_type ]["shifts"]:
										weight=runname_parameters[residue_type ]["shifts"][atomname]['weight']

										abs_diff_offset=0.0
										rc_offset=0.0
										if 'abs_diff_offset' in runname_parameters[residue_type ]["shifts"][atomname]:
											abs_diff_offset=runname_parameters[residue_type ]["shifts"][atomname]['abs_diff_offset']
											if l_rc_offset_gridsearch==1:
												rc_offset=	abs_diff_offset
												abs_diff_offset=0.0

										l_rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']['%s']={'weight':%s}" % (runname,shift_class,atomname,weight) + linesep										
										l_rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']['%s']['abs_diff_offset']=%s" % (runname,shift_class,atomname,abs_diff_offset) + linesep										
										l_rci_result_dict_text+="dic['%s']['%s']['run_info']['coeff']['%s']['rc_offset']=%s" % (runname,shift_class,atomname,rc_offset) + linesep										
							done=1




				for residue_number in l_runname_result_dict:
					if verbose>=10: print residue_number, l_runname_result_dict[residue_number]
					res_name=l_runname_result_dict[residue_number]["res_name"]

					l_rci_result_dict_text+="dic['%s']['%s']['residue_info'][%s]={}" % (runname,shift_class,residue_number) + linesep
					l_rci_result_dict_text+="dic['%s']['%s']['residue_info'][%s]['res_name']='%s'" % (runname,shift_class,residue_number,res_name) + linesep

					if shift_class in l_runname_result_dict[residue_number]["residue_shift_classes"]:

						if "rci_values"  in l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class]:
							rci_value=l_runname_result_dict[residue_number]["residue_shift_classes"][shift_class]["rci_values"]
							l_rci_result_dict_text+="dic['%s']['%s']['residue_info'][%s]['rci_value']='%s'" % (runname,shift_class,residue_number,rci_value) + linesep
						else:
							print "Warning!!! rci_values is not in  l_runname_result_dict for residue %s and shift class %s and runname %s" % (residue_number,shift_class,runname)

					else:
						if verbose>10: print "Warning!!! Shift class %s is not in  l_runname_result_dict for residue %s and runname %s" % (shift_class,residue_number,runname)




	if l_zodb:
		l_pdb_dict._p_changed = True		
		transaction.savepoint(True)
		transaction.commit()
		#transaction.get().commit() 
		connection.close()
		db.close()
		storage.close()

		l_pdb_dict={}


	append_file(l_rci_result_dict_text,rci_result_dict_file)
	if verbose>=1: print "Runname is %s. Dictionary with RCI results was appended at " % (runname, rci_result_dict_file)

	debug=0
	if debug==1:
		print "Exiting after function write_gridsearch_dict()"
		sys.exit()
	return


def generate_grid(l_exclude_atoms,l_rc_increment_scale,l_residue_sensitivity, l_rc_offset_gridsearch,l_rc_offset_dict,l_shift_class_lookup,l_aa,l_rci_exclude_dic,l_shiftclass,l_aa_atom_list,shiftclass_list_of_atoms,gridsearch_coeff_list2use,res4gridsearch_list_dict):
	"""
	Generate grid for grid search

dic['Side_chain']= ['HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2', 'HB1', 'HB3', 'HB2', 'HZ', 'CZ2', 'CZ3', 'HH', 'HB', 'HE22', 'HE21', 'HG', 'HD', 'HE', 'CE3', 'CE2', 'CE1', 'HD3', 'HD1', 'CH2', 'HH12', 'HH11', 'HG21', 'HG23', 'HG22', 'CD1', 'CD2', 'HE1', 'HE2', 'HE3', 'ND', 'NE', 'HH22', 'HH21',  'ND2', 'CB', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'HD2', 'NE2', 'NE1', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11', 'CZ']

bmrb_atom_dictionary["D"]=[]
bmrb_atom_dictionary["D"]		+=["H"] #		H	HN	HN	HN	H		HN
bmrb_atom_dictionary["D"]		+=["HA"] #		HA	HA	HA	HA	HA		HA
bmrb_atom_dictionary["D"]		+=["HB2"] #	pro-S	1HB	HB1	HB1	HB2	HB2		HB2
bmrb_atom_dictionary["D"]		+=["HB3"] #	pro-R	2HB	HB2	HB2	HB1	HB1		HB3
bmrb_atom_dictionary["D"]		+=["HD2"] #				HD2				HD2
bmrb_atom_dictionary["D"]		+=["C"] #		C		C	C	C	C	C
bmrb_atom_dictionary["D"]		+=["CA"] #		CA		CA	CA	CA	CA	CA
bmrb_atom_dictionary["D"]		+=["CB"] #		CB		CB	CB	CB	CB	CB
bmrb_atom_dictionary["D"]		+=["CG"] #		CG		CG	CG	CG	CG	CG
bmrb_atom_dictionary["D"]		+=["N"] #		N		N	N	N	N	N
bmrb_atom_dictionary["D"]		+=["O"] #		O		O	O	O	O	O
bmrb_atom_dictionary["D"]		+=["OD1"] #		OD1		OD1	OD1	OD1	OD1	OD1
bmrb_atom_dictionary["D"]		+=["OD2"] #		OD2		OD2	OD2	OD2	OD2	OD2

rci_exclude_dic["Side_chain"]["L"]=["CB"]
rci_exclude_dic["Side_chain_ave"]["L"]=["CB"]

dic['heavy_atoms']= [ 'CZ2', 'CZ3', 'C', 'CE3', 'CE2', 'CE1', 'N', 'CH2', 'CD1', 'CD2', 'ND', 'NE','ND2', 'CB', 'CA', 'CG', 'CE', 'CD', 'ND1', 'CG1', 'CG2', 'NE2', 'NE1', 'CZ']
dic['heavy_atoms_ave']= ['CEave', 'CDave', 'CZave', 'NDave',  'C',  'CGave', 'N', 'CH2', 'NEave', 'CB', 'CA','CH2']
dic['HG12']= ['HG12']

dic['Protons']= ['HD22', 'HD23', 'HD21', 'HG11', 'HG12', 'HG13', 'HZ1', 'HZ3', 'HZ2', 'HA1', 'HA2', 'HA3', 'H', 'HB1', 'HB3', 'HB2', 'HZ', 'HH', 'HN', 'HB', 'HE22', 'HE21', 'HA', 'HG', 'HD', 'HE', 'HD3', 'HD1',  'HH12', 'HH11', 'HG21', 'HG23', 'HG22',  'NH', 'HE1', 'HE2', 'HE3', 'HH22', 'HH21',  'HD2', 'HH2', 'HG2', 'HG3', 'HG1', 'HD13', 'HD12', 'HD11']
dic['Protons_ave']= [ 'HZave',  'HGave', 'H', 'HN', 'NH','HHave',  'HBave',  'HAave','HDave', 'HEave']

	"""
	verbose=0
	if verbose>=1: print "Running function generate_grid1() for shift class ", l_shiftclass
	counter=0
	rc_counter=0
	l_coeff_dict={}
	l_rc_offsets_dict={}
	l_gridsearch_list=[]
	l_offset_list=[]
	l_eligible_atoms=[]
	list_of_excluded_atoms=[]
	if verbose>=10: print "shiftclass_list_of_atoms=", shiftclass_list_of_atoms
	if verbose>=10: print "Len(gridsearch_coeff_list2use)=", len(gridsearch_coeff_list2use)
	if verbose>=10: print "l_rc_offset_dict= ", l_rc_offset_dict
	list_half=1
	if l_rc_offset_gridsearch==2:
		list_half=math.floor(len(gridsearch_coeff_list2use)/2.0)
	

	if l_exclude_atoms==1 and l_shiftclass in l_rci_exclude_dic and l_aa in l_rci_exclude_dic[l_shiftclass]:
		list_of_excluded_atoms=l_rci_exclude_dic[l_shiftclass][l_aa]
		if verbose>=10: print "List of excluded atoms has been found ", list_of_excluded_atoms
		#sys.exit()


	if l_rc_offset_gridsearch==1 and l_exclude_atoms==1:
		list_of_excluded_atoms+=rci_exclude_atom_list

	for atom in l_aa_atom_list:
		atom_type=""
		if verbose>=10: print "Processing atom %s" % atom
		if atom in shiftclass_list_of_atoms and atom not in list_of_excluded_atoms:
			if atom not in l_eligible_atoms:
				l_eligible_atoms+=[atom]
			else:
				print "Warning!!! Atom %s is present twice" % atom

			l_list=[]
			if l_rc_offset_gridsearch:

				l_offset_ids=[]
				if verbose>=1: print "Atom %s has been FOUND" % atom



				if atom in l_shift_class_lookup["Protons"] or atom in l_shift_class_lookup["Protons_ave"]:
					atom_type="proton"
				elif atom in l_shift_class_lookup["heavy_atoms"] or atom in l_shift_class_lookup["heavy_atoms_ave"]:
					if "C" in atom:
						atom_type="carbon"
					elif  "N" in atom:
						atom_type="nitrogen"
				if atom_type=="":
					if verbose>=10: print "Warning!!! Atom type has not been found  in  l_shift_class_lookup()"
					if "C" in atom:
						atom_type="carbon"
					elif  "N" in atom:
						atom_type="nitrogen"			
					elif  "H" in atom:
						atom_type="proton"
					else:
						print "ERROR!!! Atom name is misformatted and is ", [atom_type]
				if verbose>=1: print "Atom_type=", [atom_type]
				if atom_type in l_rc_offset_dict:
					l_rc_offset_range=l_rc_offset_dict[atom_type]
					rc_increment=l_rc_offset_range/list_half
					
					if verbose>=10: print "RC offset range is found for atom %s and is " % atom , l_rc_offset_range
					if verbose>=10: print "rc_increment for atom %s and list_half %s and is " % (atom ,list_half), rc_increment

					if l_rc_offset_gridsearch==1:
						l_rc_offset_list=[0.0,rc_increment,-rc_increment]
					else:
						l_rc_offset_list=[]
						for t in range(1,int(list_half)+1):
							if verbose>=10: print "T=", t
							l_rc_offset_list+=[t*rc_increment*l_rc_increment_scale,-rc_increment*t*l_rc_increment_scale]

				else:
					print "Error!!! RC offset range was not found for atom %s " % atom

				if verbose>=10: print "l_offset_list= ", l_rc_offset_list

			for coeff in gridsearch_coeff_list2use:
				l_coeff_dict[counter]=[atom,coeff]
				l_list+=[counter]
				counter+=1
			l_gridsearch_list+=[l_list]

			if l_rc_offset_gridsearch:
				if len(l_rc_offset_list)>0:
					for rc_offset in  l_rc_offset_list:
						l_rc_offsets_dict[rc_counter]=[atom,rc_offset]
						if verbose>=0: print "rc_counter=", rc_counter, "atom=", atom, "rc_offset=", rc_offset
						l_offset_ids+=[rc_counter]
						rc_counter+=1
					l_offset_list+=[l_offset_ids]
				else:
					print "Error!!!! l_rc_offset_list is empty."
					#sys.exit()

		else:
			if verbose>=1: print "Atom %s was NOT found in  shiftclass_list_of_atoms" % atom


	if verbose>=10: print "l_gridsearch_list=", l_gridsearch_list
	if l_rc_offset_gridsearch:	
		if verbose>=10: print "l_rc_offset_list=", l_offset_list
		if verbose>=10: print "l_rc_offsets_dict=", l_rc_offsets_dict
	if verbose>=1:print "Before creating a grid"
	#if l_res_name=="F":
	#	list_of_keys=F_grid(l_aa_atom_list,gridsearch_coeff_list2use)
	#else:
	list_of_keys=list(itertools.product(*l_gridsearch_list))
	if verbose>=1:print "After creating a grid"
	if verbose>=10: print list_of_keys

	l_final_grid_list=[]
	if verbose>=1:print "Creating final l_final_grid_list"
	for i in list_of_keys:
		if verbose>=10: print "i= ", i
		i_list=[]
		l_diff_detected=0
		l_old_coef=""
		for j in i:
			if verbose>=10: print "j= ", j
			j_list=l_coeff_dict[j]	
			if verbose>=10: print "j_list= ", j_list
			atom_name,coeff=j_list[0], j_list[1]
			i_list+=[j_list]

			if l_old_coef!="" and coeff!=l_old_coef:
				l_diff_detected=1	
			l_old_coef=coeff
		if (l_residue_sensitivity==1 or (l_residue_sensitivity==0 and l_diff_detected==1)) or l_old_coef=="":
			l_final_grid_list+=[i_list]
		else:
			if verbose>=10: print "1) Skipping for ", i, "i_list=", i_list

	list_of_keys=[]
	l_final_rc_grid_list=[]

	if verbose>=10: print "l_offset_list=", l_offset_list
	if l_rc_offset_gridsearch>0:
		rc_list_of_keys=list(itertools.product(*l_offset_list))
		if verbose>=10: print rc_list_of_keys

		for i in rc_list_of_keys:
			if verbose>=10: print "i= ", i
			i_list=[]
			l_diff_detected=0
			l_old_rc_offset=""
			for j in i:
				if verbose>=10: print "j= ", j
				j_list=l_rc_offsets_dict[j]	
				if verbose>=10: print "j_list= ", j_list
				atom_name,rc_offset=j_list[0], j_list[1]
				i_list+=[j_list]
				if verbose>=10: print "i_list=",i_list, "rc_offset=", rc_offset, "l_old_rc_offset=", [l_old_rc_offset]

				if (l_old_rc_offset!="" and rc_offset!=l_old_rc_offset) or len(i)==1:
					l_diff_detected=1
					if verbose>=10: print "Differtence detected"	
				l_old_rc_offset=rc_offset

			if l_diff_detected==1 or l_old_rc_offset=="":
				if i_list not in l_final_rc_grid_list:
					l_final_rc_grid_list+=[i_list]
				else:
					print "Warning!!!! l_final_rc_grid_list already has list ", i_list, "Skipping...."
			else:
				if verbose>=10: print "2) Skipping for ", i, "i_list=", i_list
				if verbose>=10: print "l_diff_detected=", l_diff_detected
				if verbose>=10: print "l_old_rc_offset=", l_old_rc_offset

	if verbose>=10: print "l_final_grid_list=", l_final_grid_list

	debug=0
	if verbose>=1:print "DONE"
	if debug==1:
		print "l_aa_atom_list=", l_aa_atom_list
		print "shiftclass_list_of_atoms=", shiftclass_list_of_atoms
		print "l_rc_offset_list=", l_rc_offset_list
		print "Length of l_final_rc_grid_list", len(l_final_rc_grid_list)
		#print "l_final_grid_list=", l_final_grid_list
		print "Length of l_final_grid_list=", len(l_final_grid_list)
		print "Number of optimized atoms is ", len(l_gridsearch_list)
		print "gridsearch_coeff_list2use=", gridsearch_coeff_list2use
		print "Expected number of combinations=", len(gridsearch_coeff_list2use)**len(l_gridsearch_list)
		print "l_eligible_atoms=", l_eligible_atoms
		print "Exiting after function generate_grid()"
		sys.exit()
	return l_final_grid_list,l_eligible_atoms, l_final_rc_grid_list



def clean_lookup_dictionary(forbidden_atoms,exp_assignment_dict,verify_dict,l_strict):
	"""
	Remove atoms from the lookup table that are not present in the BMRB file
	exp_assignment_dict= {1: {'ne_list': [], 'hz_list': [], 'hg_list': [2.55, 2.55], 'ce_list': [], 'cd_list': [], 'cz_list': [], 'res_name': 'MET', 'shifts': {'C': 172.14, 'CGave': 30.78, 'HBave': 2.1, 'CB': 32.83, 'CA': 54.91, 'HG2': 2.55, 'HG3': 2.55, 'HAave': 4.05, 'CG': 30.78, 'HA': 4.05, 'HB3': 2.1, 'HB2': 2.1}, 'hh_list': [], 'he_list': [], 'hd_list': [], 'cg_list': [30.78], 'ha_list': [4.05], 'nd_list': [], 'hb_list': [2.1, 2.1]}, 2:

bmrb_atom_dictionary["D"]=[]
bmrb_atom_dictionary["D"]		+=["H"] #		H	HN	HN	HN	H		HN
bmrb_atom_dictionary["D"]		+=["HA"] #		HA	HA	HA	HA	HA		HA
bmrb_atom_dictionary["D"]		+=["HB2"] #	pro-S	1HB	HB1	HB1	HB2	HB2		HB2
bmrb_atom_dictionary["D"]		+=["HB3"] #	pro-R	2HB	HB2	HB2	HB1	HB1		HB3
bmrb_atom_dictionary["D"]		+=["HD2"] #				HD2				HD2
bmrb_atom_dictionary["D"]		+=["C"] #		C		C	C	C	C	C
bmrb_atom_dictionary["D"]		+=["CA"] #		CA		CA	CA	CA	CA	CA
bmrb_atom_dictionary["D"]		+=["CB"] #		CB		CB	CB	CB	CB	CB
bmrb_atom_dictionary["D"]		+=["CG"] #		CG		CG	CG	CG	CG	CG
bmrb_atom_dictionary["D"]		+=["N"] #		N		N	N	N	N	N
bmrb_atom_dictionary["D"]		+=["O"] #		O		O	O	O	O	O
bmrb_atom_dictionary["D"]		+=["OD1"] #		OD1		OD1	OD1	OD1	OD1	OD1
bmrb_atom_dictionary["D"]		+=["OD2"] #		OD2		OD2	OD2	OD2	OD2	OD2

	"""
	verbose=0
	if verbose>=10: print "Running function clean_lookup_dictionary()"



	new_lookup_dictionary={}
	for residue_name  in verify_dict:
		if verbose>=10: print "Processing residue name ",  residue_name
		atom_list=verify_dict[residue_name]
		residue_found=0
		for atom in atom_list:
			atom_found=0
			if verbose>=10: print "Processing atom ",  atom
			for residue_number in exp_assignment_dict:
				residue_name_3_let=exp_assignment_dict[residue_number]['res_name']
				residue_name_1_let=aa_names_full_all_CAP[residue_name_3_let]
				if residue_name==residue_name_1_let and (residue_name not in forbidden_atoms or (residue_name  in forbidden_atoms and atom not in forbidden_atoms[residue_name])):
					residue_found=1
					if verbose>10: print "Residue %s and atom %s were found " % (residue_name,atom)
					if residue_name not in forbidden_atoms:
						if verbose>=10: print "Residue %s is not in forbidden_atoms" % (residue_name)
					else:
						if residue_name  in forbidden_atoms and atom not in forbidden_atoms[residue_name]:
							if verbose>=10: print "Atom %s of %s is not in forbidden_atoms" % (atom ,residue_name), forbidden_atoms[residue_name]
	

					shift_list=exp_assignment_dict[residue_number]['shifts']
					for shift in shift_list:
						if verbose>=10: print shift 
						if atom==shift or (l_strict==0 and "ave" in shift and shift[0:2]==atom[0:2]):
							atom_found=1
							if residue_name not in new_lookup_dictionary:
								new_lookup_dictionary[residue_name]=[]
							if l_strict==1:
								if atom not in new_lookup_dictionary[residue_name]:
									new_lookup_dictionary[residue_name]+=[atom]		
							elif  l_strict==0:	
								if shift not in new_lookup_dictionary[residue_name]:
									if verbose>=10: print "Adding shift ", shift, shift[0:2],atom[0:2]
									new_lookup_dictionary[residue_name]+=[shift]						

			if atom_found==0:
				if verbose>=10: print "Warning!!! Atom %s of residue %s was not found! EXCLUDING.... " % (atom,residue_name)

		if residue_found==0:
			print "Warning !!!! Residue %s was not found in BMRB file " % residue_name
	#print residue_name,atom_list

	for residue_number in exp_assignment_dict:	
		residue_name_3_let=exp_assignment_dict[residue_number]['res_name']
		residue_name_1_let=aa_names_full_all_CAP[residue_name_3_let]
		shift_dict=exp_assignment_dict[residue_number]['shifts']
		if verbose>=10: print residue_name_3_let, residue_number, shift_dict
		residue_found=0
		#print residue_name_1_let, residue_number
		for residue_name  in verify_dict:
			if residue_name==residue_name_1_let:
				residue_found=1
				atom_list=verify_dict[residue_name]
				for shift in shift_dict:
					shifts_found=0
					for atom in atom_list:
						if atom==shift:
							shifts_found=1

					if shifts_found==0 and "ave" not in shift and shift!="HA":
						if verbose>=0: print "Warning!!! Shifts %s of BMRB residue %s%s was not found! " % (shift,residue_name,residue_number)

		if residue_found==0:
			print "Warning !!!! BMRB residue %s%s was not found in the lookup dictionary " % (residue_name,residue_number)
	#print residue_name,atom_list

	debug=0
	if debug==1:
		print "forbidden_atoms=", forbidden_atoms
		print new_lookup_dictionary["W"]
	

		print "Exiting after function clean_lookup_dictionary()"
		sys.exit()
	return new_lookup_dictionary


def find_total_gridsearch_time(l_exclude_atoms,l_rc_increment_scale,l_residue_type_sensitivity,l_rc_offset_gridsearch,l_remove_stereo,gridsearch_coeff_list,shiftclass4gridsearch_list,shift_class_lookup,verify_dict,res4gridsearch_list_dict,res4gridsearch_list):
	"""

	Find total grid  search time
aa_names_full_all_CAP={"GLN":"Q","TRP":"W","GLU":"E","ARG":"R","THR":"T","TYR":"Y","ILE":"I","PRO":"P","ALA":"A","SER":"S","ASP":"D","PHE":"F","GLY":"G","HIS":"H","LYS":"K","LEU":"L","CYS":"C","VAL":"V","ASN":"N","MET":"M"}	
aa_names_1let_2_full_all_CAP={"Q":"GLN","W":"TRP","E":"GLU","R":"ARG","T":"THR","Y":"TYR","I":"ILE","P":"PRO","A":"ALA","S":"SER","D":"ASP","F":"PHE","G":"GLY","H":"HIS","K":"LYS","L":"LEU","C":"CYS","V":"VAL","N":"ASN","M":"MET","B":"CYS"}

	

	"""
	verbose=1
	if verbose>=1: print "Running function find_total_time()"

	min_time=0.200
	max_time=0.500

	min_time_min=min_time/60.0
	max_time_min=max_time/60.0


	min_time_hours=min_time_min/60.0
	max_time_hours=max_time_min/60.0

	total_time_hours_min=0
	total_time_hours_max=0

	total_time_days_min=0
	total_time_days_max=0
	#for aa in aa_names_1let_2_full_all_CAP:

	#ges4gridsearch_list_dict["F"]={"coeff_set":gridsearch_coeff_list2, "stereo":1}

	for aa in res4gridsearch_list:
		if verbose>=1: print "Processing  residue name ",  aa
		min_time=0.200
		max_time=0.500
		if verbose>=1: print "Processing  residue name ",  aa



		if aa in res4gridsearch_list_dict:
			gridsearch_coeff_list2use=res4gridsearch_list_dict[aa]['coeff_set']
			stereo=res4gridsearch_list_dict[aa]['stereo']

		else:
			gridsearch_coeff_list2use=gridsearch_coeff_list
			stereo=1



		if gridsearch_coeff_list==[]:
			if aa in res4gridsearch_list_dict:
				gridsearch_coeff_list2use=res4gridsearch_list_dict[aa]["coeff_set"]
				stereo=res4gridsearch_list_dict[aa]["stereo"]
				if verbose>=1: print " Residue %s was found in res4gridsearch_list_dict" % aa 
				if verbose>=10: print "stereo=", stereo
				if verbose>=1: print "gridsearch_coeff_list2use=", gridsearch_coeff_list2use
						#sys.exit()
			else:
				if verbose>=1: print "1) Residue %s was NOT found in res4gridsearch_list_dict" % aa , res4gridsearch_list_dict
		else:
			if l_remove_stereo==0:
				stereo=1
			else:
				stereo=0
			gridsearch_coeff_list2use=gridsearch_coeff_list



		if gridsearch_coeff_list2use==[]:
			print "Error!!! gridsearch_coeff_list is empty! The program will exit now"
			sys.exit()			


		for shiftclass in shiftclass4gridsearch_list:
			if verbose>=1: print "Processing  shiftclass ",  shiftclass
			if stereo==0:
				shiftclass1="%s_ave" % shiftclass
				shiftclass2="%save" % shiftclass
				if shiftclass1 in shift_class_lookup:
					shiftclass=shiftclass1
					if verbose>=1: print "Converting to  shiftclass %s because stereo==0 " % shiftclass
				elif shiftclass2 in shift_class_lookup:
					shiftclass=shiftclass2
					if verbose>=1: print "Converting to  shiftclass %s because stereo==0 " %  shiftclass
				else:
					print "Warning!!! Neither %s nor %s is found in shift_class_lookup. Conversion to non-stereospecific class has failed. Using the old shift class %s" % (shiftclass1,shiftclass2,shiftclass) 

			if shiftclass in shift_class_lookup:
				if verbose>=1: print "Processing shiftclass  ",  shiftclass 
				shiftclass_list_of_atoms=shift_class_lookup[shiftclass]
				runname_dict[shiftclass]={}

				if aa in verify_dict:
					aa_atom_list=verify_dict[aa]
					#l_final_grid_list,l_eligible_atoms=generate_grid(aa,aa_atom_list,shiftclass_list_of_atoms,gridsearch_coeff_list2use)
					l_final_grid_list,l_eligible_atoms,l_final_rc_grid_list=generate_grid(l_exclude_atoms,l_rc_increment_scale,l_residue_type_sensitivity,l_rc_offset_gridsearch,rc_offset_dict,shift_class_lookup,aa,rci_exclude_dic,shiftclass,aa_atom_list,shiftclass_list_of_atoms,gridsearch_coeff_list2use,res4gridsearch_list_dict)
					#l_final_grid_list=generate_grid2(aa,aa_atom_list,shiftclass_list_of_atoms,gridsearch_coeff_list2use,res4gridsearch_list_dict)

					total_number_of_combinations=len(l_final_grid_list)
					min_time_ms=total_number_of_combinations * min_time
					max_time_ms=total_number_of_combinations * max_time

					min_time_min=min_time_ms/60.0
					max_time_min=max_time_ms/60.0


					min_time_hours=min_time_ms/60.0
					max_time_hours=max_time_ms/60.0


					min_time_days=min_time_hours/24.0
					max_time_days=max_time_hours/24.0

					total_time_hours_min+=min_time_hours
					total_time_hours_max+=max_time_hours

					total_time_days_min+=min_time_days
					total_time_days_max+=max_time_days
		
					print "Residue %s will take %s hours or %s days minimally and %s hours or %s days maximally for %s combinations" % (aa,min_time_hours,min_time_days,max_time_hours,max_time_days,total_number_of_combinations)
	
				else:
					print "Error!!! Residue %s is not in verify_dict" %  aa

	print "Total minimimal time is %s hours or %s days" % (total_time_hours_min,total_time_days_min)
	print "Total maximal time is  %s hours or %s days " % (total_time_hours_max,total_time_days_max)

	debug=0
	if debug==1:
		print "Exiting after function find_total_gridsearch_time()"
		sys.exit()
	return

def extract_rc_offsets(l_rci_parameter_dict):
	"""
	Extract dictionaries with rc offsets

Side_chain Y {'shifts': {'CD2': {'abs_diff_offset': -2.0}, 'HE2': {'abs_diff_offset': -0.5}, 'HD1': {'abs_diff_offset': -0.5}, 'CB': {'abs_diff_offset': -2.0}, 'HE1': {'abs_diff_offset': -0.5}, 'CE2': {'abs_diff_offset': -2.0}, 'CD1': {'abs_diff_offset': -2.0}, 'CE1': {'abs_diff_offset': -2.0}, 'HB2': {'abs_diff_offset': -0.5}, 'HB3': {'abs_diff_offset': -0.5}, 'HD2': {'abs_diff_offset': -0.5}}, 'early_floor_value': 0.1}

Side_chain Y shifts {'CD2': {'abs_diff_offset': -2.0}, 'HE2': {'abs_diff_offset': -0.5}, 'HD1': {'abs_diff_offset': -0.5}, 'CB': {'abs_diff_offset': -2.0}, 'HE1': {'abs_diff_offset': -0.5}, 'CE2': {'abs_diff_offset': -2.0}, 'CD1': {'abs_diff_offset': -2.0}, 'CE1': {'abs_diff_offset': -2.0}, 'HB2': {'abs_diff_offset': -0.5}, 'HB3': {'abs_diff_offset': -0.5}, 'HD2': {'abs_diff_offset': -0.5}}
Side_chain Y early_floor_value 0.1

	"""
	verbose=0
	if verbose>=1: print "Running function extract_rc_offsets()"
	#l_key="abs_diff_offset" # Temporary
	l_key="rc_offset" 
	l_rcoil_offsets_dict={}
	l_rc_offset_found=0
	for side_chain_class in l_rci_parameter_dict:
		if verbose>=10: print side_chain_class , l_rci_parameter_dict[side_chain_class ]
	 	for residue_name in  l_rci_parameter_dict[side_chain_class]:
			if verbose>=10: print side_chain_class,residue_name,  l_rci_parameter_dict[side_chain_class][residue_name]
			if residue_name  not in l_rcoil_offsets_dict:
				l_rcoil_offsets_dict[residue_name]={}

			else:
				print "Warning!!!! Residue %s is already in extract_rc_offsets_dict. Something is wring with the l_rci_parameter_dict " % residue_name, l_rci_parameter_dict
			if 'shifts' in l_rci_parameter_dict[side_chain_class][residue_name]:
				for shift in l_rci_parameter_dict[side_chain_class][residue_name]['shifts']:
					if l_key in l_rci_parameter_dict[side_chain_class][residue_name]['shifts'][shift]:
						rc_offset=l_rci_parameter_dict[side_chain_class][residue_name]['shifts'][shift][l_key]
						if shift not in l_rcoil_offsets_dict[residue_name]:
							l_rcoil_offsets_dict[residue_name][shift]=rc_offset
							l_rc_offset_found=1
						else:
							print "Warning!!!! RC offset of shift %s  is already in l_rcoil_offsets_dict for residue %s is already in extract_rc_offsets_dict. Something is wring with the l_rci_parameter_dict " % (shift,residue_name), l_rci_parameter_dict	
			else:
				print "Warning!!!! Shifts are absent for residue %s is already in extract_rc_offsets_dict. Something is wring with the l_rci_parameter_dict " % residue_name, l_rci_parameter_dict			

	if l_rc_offset_found==0:
		l_rcoil_offsets_dict={}
	debug=0
	if debug==1:
		print "l_rcoil_offsets_dict=", l_rcoil_offsets_dict
		print "Exiting after function extract_rc_offsets()"
		sys.exit()
	return l_rcoil_offsets_dict


def grid_search_1(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		rc_residue_list,\
		early_floor_value,\
		shift_class_description_dict,\
		shift_class_lookup,\
		value_search,\
		database_file,\
		rci_dict,\
		parameter_dict,\
		averaging,\
		bb_averaging,\
		coef_mean,\
		miss_ass,\
		H_Hertz_corr,\
		C_Hertz_corr,\
		N_Hertz_corr,\
		floor_value1,\
		scale,\
		ceiling_value,\
		rci_method,\
		find_value_mode,\
		no_outliers,\
		power1,\
		scaling1,\
		rci_out_offset,\
		value_search2,\
		full_project_dir,\
		rci_results_dir,\
		gnuplot_program,\
		make_rci_pics,\
		make_rci_images,\
		make_rci_reports,\
		make_secCS_reports,\
		rci_report_residues_dict,\
		gridsearch_dir,\
		residue_numbers_list,\
		aa_names_1let_2_full_all_CAP,\
		verify_dict_original,\
		shiftclass4gridsearch_list,\
		gridsearch_coeff_list,\
		res4gridsearch_list,\
		res4gridsearch_list_dict,\
		exp_assignment_dict,\
		l_remove_stereo,\
		l_do_gridsearch1,\
		l_files4training,\
		l_rci_parameter_dict_file,\
		l_run_rci,\
		l_forbidden_atoms,\
		l_coef_only,\
		l_residue_type_sensitivity,\
		l_sc_rci_weight,\
		l_bb_rci_weight,\
		l_sc_bb_combine,\
		l_detect_missing_atoms,\
		l_rc_offset_gridsearch,\
		l_rc_increment_scale,\
		l_backbone_random_coil_values,\
		l_i_plus_minus_two_residue_correction,\
		l_i_plus_minus_one_residue_correction,\
		l_backbone_random_coil_values_dict,\
		l_side_chain_random_coil_values,\
		l_i_plus_minus_two_residue_correction_dict,\
		l_i_plus_minus_one_residue_correction_dict,\
		l_B_Cys_list,\
		l_find_rci,\
		l_protein_sequence_list,\
		l_rc_values,\
		l_place_dict,\
		l_strong_shift_limit,\
		l_total_length_strong_offset,\
		l_stereo_matching_mode,\
		l_mean_cs_mode,\
		l_fill_dict,\
		l_total_module_text_on,\
		l_total_report_text_on,\
		l_module_text_on,\
		l_report_text_on,\
		l_per_residue_on,\
		l_do_pearson,\
		l_do_spearman,\
		l_do_kendall,\
		l_report_dir,\
		l_outliers,\
		l_stereospecific_dictionary,\
		l_rc_max_offset,\
		l_rc_offset_dict,\
		l_rc_assign_dic,\
		l_bb_end_correct,\
		l_sc_end_correct,\
		l_exclude_atoms,\
		l_bb_rci_opt,\
		l_param_overwrite,\
		):
	"""
	Running grid search 1
	"""
	verbose=0
	if verbose>=1: print "Running function grid_search_1()"
	if verbose>=10: print "Length of shiftclass4gridsearch_list is ", len(shiftclass4gridsearch_list)
	if verbose>=10: print "l_remove_stereo=", l_remove_stereo
	l_rc_assign_dic2use=l_rc_assign_dic
	#sys.exit()
	l_rc_residue_list=rc_residue_list
	runname_list=[]
	runname_dict={}
	#gridsearch_dir="gridsearch"
	measure_time=0
	from datetime import datetime
	total_counter=0
	#if l_remove_stereo==0:
	#	l_remove_stereo=1
	clean_lookup=1
	strict_cleanup=0
	rci_result_dict_text=""
	if clean_lookup==1:
		verify_dict=clean_lookup_dictionary(l_forbidden_atoms,exp_assignment_dict,verify_dict_original,strict_cleanup)
	else:
		verify_dict=verify_dict_original

	RCI_reports_dir_full="%s/RCI_reports" % (full_project_dir)
	make_dir_no_remove(RCI_reports_dir_full)

	#gridsearch_result_file="%s/gridsearch_results.py" % (RCI_reports_dir_full)

	gridsearch_result_file="%s/rci_results.py" % (RCI_reports_dir_full)
	gridsearch_mem_file="%s/rci_memory_usage.txt" % (RCI_reports_dir_full)

	if file_exists(gridsearch_result_file):
		os.system("rm -f %s" % gridsearch_result_file)
	if file_exists(gridsearch_mem_file):
		os.system("rm -f %s" % gridsearch_mem_file)
	

	calculate_total_gridsearch_time=1
	if calculate_total_gridsearch_time==1:
		find_total_gridsearch_time(l_exclude_atoms,l_rc_increment_scale,l_residue_type_sensitivity,l_rc_offset_gridsearch,l_remove_stereo,gridsearch_coeff_list,shiftclass4gridsearch_list,shift_class_lookup,verify_dict,res4gridsearch_list_dict,res4gridsearch_list)


	for shiftclass in shiftclass4gridsearch_list:
		if verbose>=1: print "Processing  shiftclass ",  shiftclass

		if shiftclass in shift_class_lookup:
			shiftclass_old=shiftclass
			runname_dict[shiftclass]={}
			for aa in res4gridsearch_list:
				if gridsearch_coeff_list==[]:
					if aa in res4gridsearch_list_dict:
						gridsearch_coeff_list2use=res4gridsearch_list_dict[aa]["coeff_set"]
						if l_bb_rci_opt==1:
							gridsearch_coeff_list2use=gridsearch_coeff_list1
						stereo=res4gridsearch_list_dict[aa]["stereo"]
						if verbose>=1: print " Residue %s was found in res4gridsearch_list_dict" % aa 
						if verbose>=10: print "stereo=", stereo
						if verbose>=1: print "gridsearch_coeff_list2use=", gridsearch_coeff_list2use
						#sys.exit()
					else:
						if verbose>=1: print "2) Residue %s was NOT found in res4gridsearch_list_dict" % aa , res4gridsearch_list_dict

				else:
					if l_remove_stereo==0:
						stereo=1
					else:
						stereo=0
					gridsearch_coeff_list2use=gridsearch_coeff_list



				if gridsearch_coeff_list2use==[]:
					print "Error!!! gridsearch_coeff_list is empty! The program will exit now"
					sys.exit()			


				if stereo==0:

					shiftclass1="%s_ave" % shiftclass
					shiftclass2="%save" % shiftclass

					if shiftclass1 in shift_class_lookup:
						shiftclass=shiftclass1
						if verbose>=1: print "Converting to  shiftclass %s because stereo==0 ",  shiftclass1
						shiftclass_list_of_atoms=shift_class_lookup[shiftclass]
					elif shiftclass2 in shift_class_lookup:
						shiftclass=shiftclass2
						if verbose>=1: print "Converting to  shiftclass %s because stereo==0 ",  shiftclass2
						shiftclass_list_of_atoms=shift_class_lookup[shiftclass]
					else:
						print "Warning!!! New shiftclass %s is not in shift_class_lookup" % shiftclass
						print "Switching to the old shiftclass %s " % shiftclass_old
						shiftclass=shiftclass_old


				shiftclass_list_of_atoms=shift_class_lookup[shiftclass]
				if 	l_bb_rci_opt==1:
					shiftclass_list_of_atoms=shift_class_lookup["RCI"]
				#if verbose>=0: print "shiftclass_list_of_atoms for shiftclass %s is " % shiftclass ,  shiftclass_list_of_atoms
				#sys.exit()


					

				#{"coeff_set":gridsearch_coeff_list2, "stereo":1}
				if verbose>=1: print "Processing  residue name ",  aa
				


				if aa in verify_dict:
					completeness_found=0
					aa_atom_list=verify_dict[aa]
					l_final_grid_list,l_eligible_atoms,l_final_rc_grid_list=generate_grid(l_exclude_atoms,l_rc_increment_scale,l_residue_type_sensitivity,l_rc_offset_gridsearch,rc_offset_dict,shift_class_lookup,aa,rci_exclude_dic,shiftclass,aa_atom_list,shiftclass_list_of_atoms,gridsearch_coeff_list2use,res4gridsearch_list_dict)
					#l_final_grid_list=generate_grid2(aa,aa_atom_list,shiftclass_list_of_atoms,gridsearch_coeff_list2use,res4gridsearch_list_dict)

					old="""
					l_parameter_dict={}
					l_parameter_dict[shiftclass]={}
					l_parameter_dict[shiftclass]["missing_asignment"]=0
					l_parameter_dict[shiftclass]["early_floor_value"]=early_floor_value
					l_parameter_dict[shiftclass]["floor_value1"]=floor_value1
					l_parameter_dict[shiftclass]["ceiling_value"]=ceiling_value
					l_parameter_dict[shiftclass]["power1"]=power1
					l_parameter_dict[shiftclass]["scaling1"]=scaling1
					l_parameter_dict[shiftclass]["res_type"]=aa
					"""
					if l_bb_rci_opt==0:
						l_parameter_dict={}
					else:
						l_parameter_dict=copy.deepcopy(parameter_dict)

					if shiftclass not in l_parameter_dict:
						l_parameter_dict[shiftclass]={}
					if aa not in l_parameter_dict[shiftclass]:
						l_parameter_dict[shiftclass][aa]={}
					if "missing_asignment" not in l_parameter_dict[shiftclass][aa] or l_param_overwrite:
						l_parameter_dict[shiftclass][aa]["missing_asignment"]=0

					if "early_floor_value" not in l_parameter_dict[shiftclass][aa] or l_param_overwrite:
						l_parameter_dict[shiftclass][aa]["early_floor_value"]=early_floor_value

					if "floor_value1" not in l_parameter_dict[shiftclass][aa] or l_param_overwrite:
						l_parameter_dict[shiftclass][aa]["floor_value1"]=floor_value1

					if "ceiling_value" not in l_parameter_dict[shiftclass][aa] or l_param_overwrite:
						l_parameter_dict[shiftclass][aa]["ceiling_value"]=ceiling_value

					if "power1" not in l_parameter_dict[shiftclass][aa] or l_param_overwrite:
						l_parameter_dict[shiftclass][aa]["power1"]=power1

					if "scaling1" not in l_parameter_dict[shiftclass][aa] or l_param_overwrite:
						l_parameter_dict[shiftclass][aa]["scaling1"]=scaling1

					if "res_type" not in l_parameter_dict[shiftclass][aa] or l_param_overwrite:
						l_parameter_dict[shiftclass][aa]["res_type"]=aa

					rc_offsets={aa:{}}

					if l_rc_offset_gridsearch==0:
						l_total_number_of_combinations=len(l_final_grid_list)
						grid_list2use=l_final_grid_list
					else:
						l_total_number_of_combinations=len(l_final_rc_grid_list)	
						grid_list2use=l_final_rc_grid_list

					if verbose>=0: print "Total number of combinations is %s for atom list " % l_total_number_of_combinations, l_eligible_atoms
					#sys.exit()
					combo_counter=0
					rejected_combinations=0
					accepted_combinations=0
					for combination in grid_list2use:
						combo_counter+=1
						if verbose>=0: print "Processing combination %s of total %s " % (combo_counter,l_total_number_of_combinations)#,combination
						#l_parameter_dict[shiftclass]["shifts"]={}

						if "shifts" not in l_parameter_dict[shiftclass][aa] or l_param_overwrite:
							l_parameter_dict[shiftclass][aa]["shifts"]={}
						total_coeff=0
						runname_suffx=""
						one_found=0
						for i in  combination:
							if l_rc_offset_gridsearch==0:
								atomname,atom_coef=i[0],i[1]
								abs_diff_offset=0.0
							else:
								atomname,abs_diff_offset=i[0],i[1]
								atom_coef=1.0


							if verbose>=10: print "i= ", i, "atom_coef=", atom_coef
							if atom_coef==1.0:
								one_found=1

							if shiftclass in parameter_dict and aa in parameter_dict[shiftclass] and "shifts" in parameter_dict[shiftclass][aa] and atomname in parameter_dict[shiftclass][aa]["shifts"] and 'weight' in parameter_dict[shiftclass][aa]["shifts"][atomname]:
								atom_coef=parameter_dict[shiftclass][aa]["shifts"][atomname]['weight']
								if verbose==10: print "Weight %s has been found " % atom_coef
			


							#l_parameter_dict[shiftclass]["shifts"][atomname]={"weight":atom_coef,"res_name":aa}

							if atomname not in l_parameter_dict[shiftclass][aa]["shifts"] or l_param_overwrite:
								l_parameter_dict[shiftclass][aa]["shifts"][atomname]={}

							if "weight" not in l_parameter_dict[shiftclass][aa]["shifts"][atomname] or l_param_overwrite:
								l_parameter_dict[shiftclass][aa]["shifts"][atomname]["weight"]=atom_coef

							if "res_name" not in l_parameter_dict[shiftclass][aa]["shifts"][atomname] or l_param_overwrite:
								l_parameter_dict[shiftclass][aa]["shifts"][atomname]["res_name"]=aa

							if "abs_diff_offset" not in l_parameter_dict[shiftclass][aa]["shifts"][atomname] or l_param_overwrite:
								l_parameter_dict[shiftclass][aa]["shifts"][atomname]["abs_diff_offset"]=abs_diff_offset

							if verbose==10:
								print """Adding to l_parameter_dict[%s][%s]["shifts"][%s]={"weight":%s,"abs_diff_offset":%s}""" % (shiftclass,aa,atomname,atom_coef,abs_diff_offset)
								#sys.exit()
							rc_offsets[aa][atomname]=abs_diff_offset

							total_coeff+=atom_coef
							runname_suffx+="_%s_%s_%.2f" % (atomname,atom_coef,float(abs_diff_offset))
							total_counter+=1

						#sys.exit()
						if verbose>=10: print "l_residue_type_sensitivity=", l_residue_type_sensitivity
						if ( (l_residue_type_sensitivity==0 and one_found==1 and total_coeff!=0) or (l_residue_type_sensitivity==1 and total_coeff!=0) ) :
							runname="%s_shifts%s_floor1_%s_ceil1_%s_power1_%s_scale1_%s" % (aa,runname_suffx,floor_value1,ceiling_value,power1,scaling1)
							runname_list+=[runname]
							runname_dict[shiftclass_old][runname]={}
							runname_dict[shiftclass_old][runname]=copy.deepcopy(l_parameter_dict[shiftclass])
							accepted_combinations+=1
							if verbose>=10: print "Processing ACCEPTED combination %s of %s  (total = %s ): " % (accepted_combinations,l_total_number_of_combinations,total_counter) , combination

							#sys.exit()
							runname_result_dict={}
							#runname_result_dict[shiftclass_old]={}
							#runname_result_dict[shiftclass_old][runname]={}
							if measure_time==1:
								t1 = datetime.now()



							if rc_offset_gridsearch==1:



								######################################################
								# Find Random Coil Values
								l_rc_assign_dic2use,rc_list_4_offset,l_rc_residue_list,pdb_list=caclulate_random_coil_values(\
									l_backbone_random_coil_values,\
									l_i_plus_minus_two_residue_correction,\
									l_i_plus_minus_one_residue_correction,\
									l_backbone_random_coil_values_dict,\
									l_side_chain_random_coil_values,\
									l_i_plus_minus_two_residue_correction_dict,\
									l_i_plus_minus_one_residue_correction_dict,\
									l_B_Cys_list,\
									l_find_rci,\
									l_protein_sequence_list,\
									exp_assignment_dict,\
									pdb_dict,\
									l_rc_values,\
									l_place_dict,\
									rc_offsets,\
									l_stereospecific_dictionary,
									l_rc_offset_dict,\
									)
								debug=0
								if debug==1:
									print "rc_assign_dic=", rc_assign_dic
									for i in rc_assign_dic:
										print i, rc_assign_dic[i]
									#print "calc_offset=", calc_offset
									#print "l_rc_max_offset=", l_rc_max_offset
									print "Exiting after caclulate_random_coil_values() in the grid_search_1()"
									sys.exit()

								if calc_offset==1 and l_rc_max_offset=="":
									if verbose>=0: print "Running function calc_offsets() for Random Coil Values"
									rc_offset_dic=calc_offsets(rc_list_4_offset,l_protein_sequence_list)
									if verbose>=0: print "Running function max_offset for rci"
									l_rc_max_offset,rc_strong_shift,rc_second_offset_number,rc_second_offset,rc_total_length,max_len=calc_max_offset(rc_offset_dic,l_strong_shift_limit,l_total_length_strong_offset)
									debug_exit(0," Exiting after calculating offset in residue numbering between Random Coil Values predictions and exp. shift files, l_rc_max_offset = %s ..." % l_rc_max_offset)	
									if l_rc_max_offset=="": l_rc_max_offset=0	

								rc_first_residue,rc_last_residue,rc_common_residue_numbers=calc_cs_diff(l_stereo_matching_mode,l_mean_cs_mode,zodb,database_file,pdb_list,pdb_dict,exp_assignment_dict,l_rc_max_offset,"rc","rc_assign_dic")
								debug_exit(0," Exiting after calc_cs_diff() function for rc...")	
								rc_average_dict=calc_cs_diff_stats(zodb,database_file,pdb_list,l_fill_dict,l_total_module_text_on,l_total_report_text_on,l_module_text_on,l_report_text_on,l_per_residue_on,l_do_pearson, l_do_spearman, l_do_kendall,pdb_dict,full_project_dir,l_report_dir,l_mean_cs_mode,rc_common_residue_numbers,"rc",l_outliers)
								debug_exit(0," Exiting after calc_cs_diff_stats() function for rc...")	

							if l_run_rci:
								if verbose>=10: 
									for i in l_parameter_dict:
										for j in  l_parameter_dict[i]:
											print i,j,  l_parameter_dict[i][j]
					
								#sys.exit()
								calculate_rci(\
								pdb_list,\
								pdb_dict,\
								zodb,\
								l_rc_residue_list,\
								early_floor_value,\
								shift_class_description_dict,\
								shift_class_lookup,\
								value_search,\
								database_file,\
								rci_dict,\
								l_parameter_dict,\
								averaging,\
								bb_averaging,\
								coef_mean,\
								miss_ass,\
								H_Hertz_corr,\
								C_Hertz_corr,\
								N_Hertz_corr,\
								floor_value1,\
								scale,\
								ceiling_value,\
								rci_method,\
								find_value_mode,\
								no_outliers,\
								power1,\
								scaling1,\
								rci_out_offset,\
								value_search2,\
								full_project_dir,\
								rci_results_dir,\
								gnuplot_program,\
								make_rci_pics,\
								make_rci_images,\
								make_rci_reports,\
								make_secCS_reports,\
								rci_report_residues_dict,\
								runname,\
								gridsearch_dir,\
								residue_numbers_list,\
								res4gridsearch_list,\
								res4gridsearch_list_dict,\
								l_remove_stereo,\
								shiftclass4gridsearch_list,\
								l_do_gridsearch1,\
								l_files4training,\
								l_rci_parameter_dict_file,\
								runname_result_dict,\
								l_coef_only,\
								completeness_found,\
								l_forbidden_atoms,\
								l_sc_rci_weight,\
								l_bb_rci_weight,\
								l_sc_bb_combine,\
								l_detect_missing_atoms,\
								l_rc_offset_gridsearch,\
								l_rc_assign_dic2use,\
								l_bb_end_correct,\
								l_sc_end_correct,\
								find_rc_order_parameter,\
								find_MD_RMSF,\
								find_NMR_RMSF,\
								find_ASA,\
								find_B_factor,\
								l_exclude_atoms,\
								l_bb_rci_opt,\
								)

								write_gridsearch_dict(l_rc_offset_gridsearch,runname_dict,database_file,pdb_list,zodb,gridsearch_result_file,runname_result_dict,runname,rci_result_dict_text,pdb_dict,shiftclass4gridsearch_list,do_gridsearch1)

							measure_size=1
							if measure_size==1:
								pdb_dict_size=sys.getsizeof(pdb_dict)
								runname_dict_size=sys.getsizeof(runname_dict)
								runname_result_dict_size=sys.getsizeof(runname_result_dict)

								pdb_dict_length=len(pdb_dict)
								runname_dict_length=len(runname_dict)
								runname_result_dict_length=len(runname_result_dict)

								if verbose>=10: print "%s) pdb_dict_size=%s, runname_dict_size=%s, runname_result_dict_size=%s" % (accepted_combinations,pdb_dict_size,runname_dict_size,runname_result_dict_size)
								if verbose>=10: print "%s) pdb_dict_length=%s, runname_dict_length=%s, runname_result_dict_length=%s" % (accepted_combinations,pdb_dict_length,runname_dict_length,runname_result_dict_length), linesep, linesep
								gridsearch_mem_usage_txt="%s of total %s, pdb_dict_size=%s, runname_dict_size=%s, runname_result_dict_size=%s" % (accepted_combinations,l_total_number_of_combinations,pdb_dict_size,runname_dict_size,runname_result_dict_size)+linesep
								gridsearch_mem_usage_txt+="%s of total %s, pdb_dict_length=%s, runname_dict_length=%s, runname_result_dict_length=%s" % (accepted_combinations,l_total_number_of_combinations,pdb_dict_length,runname_dict_length,runname_result_dict_length)+ linesep+ linesep
								append_file(gridsearch_mem_usage_txt,gridsearch_mem_file)


							if measure_time==1:
								t2 = datetime.now()
								time_diff=t2-t1
								milliseconds=(time_diff.microseconds/1000.0)
								seconds=(time_diff.microseconds/1000000.0)
								total_time_seconds=l_total_number_of_combinations*seconds
								total_time_minutes=total_time_seconds/60.0
								total_time_hours=total_time_minutes/60.0
								print "Time per one RCI calc is %s milliseconds " % milliseconds
								print "total time for %s combinations is %s seconds or %s minutes  or %s hours" % (l_total_number_of_combinations, total_time_seconds, total_time_minutes,total_time_hours)


							debug=0
							if debug==1:
								print "Exiting after function combination() for runname ", runname
								sys.exit()
						else:
							if total_coeff==0:
								if verbose>=10: print "Combination is rejected because  total_coeff==0:", combination
							if one_found==0 and l_residue_type_sensitivity==0:
								if verbose>=10: print "Combination is rejected because  one_found==0:", combination
							rejected_combinations+=1
					if verbose>=0:print "Total number of rejected combination is %s from %s " % ( rejected_combinations,l_total_number_of_combinations)
					if verbose>=0:print "Total number of acepted combination is %s from %s " % ( accepted_combinations, l_total_number_of_combinations)

					debug=0
					if debug==1:
						print "Exiting after function combination() for residue type ", aa
						sys.exit()	


				else:
					if verbose>=1: print "Warning!!! Residue type %s was not found in verify_dict" % aa
					#print "Program will exit now...."
					#sys.exit()
		else:
			print "Error!!!! shiftclass % s is not  in shift_class_lookup" % shiftclass
			print "Program will exit now...."
			sys.exit()
		debug=0
		if debug==1:
			print "Exiting after function combination() for shiftclass ", shiftclass
			sys.exit()		

	debug=0
	if debug==1:
		print "Exiting after function grid_search_1()"
		sys.exit()
	return runname_list,runname_dict



def generate_grid2(shiftclass,aa,aa_atom_list,shiftclass_list_of_atoms,gridsearch_coeff_list2use,res4gridsearch_list_dict):
	"""
	Generate grid points
	"""
	verbose=1
	if verbose>=1: print "Running function  generate_grid2()"
	if verbose>=1: print "aa=", aa
	if verbose>=1: print "aa_atom_list=", aa_atom_list
	if verbose>=1: print "shiftclass_list_of_atoms=", shiftclass_list_of_atoms
	if verbose>=1: print "gridsearch_coeff_list2use=", gridsearch_coeff_list2use
	if verbose>=1: print "res4gridsearch_list_dict=", res4gridsearch_list_dict

	debug=0
	if debug==1:
		print "Exiting after function generate_grid2()"
		sys.exit()
	return

def gnuplot_macro_2(l_dyna_data,l_title,l_Y_label,l_bmrb,l_image_type,l_apache_flag,l_base,l_display_error_bars,l_rci_output_dir):
	"""
	Create RCI plots with Gnuplot 
	"""
	#l_image_type="ps"
	#l_apache_flag=1
	verbose=0
	image_file="%s/%s_%s.%s" % (l_rci_output_dir,l_bmrb,l_base,l_image_type)
	image_file_base="%s_%s" % (l_bmrb,l_base)
	f=os.popen('%s' % gnuplot_program,'w')
	#print >>f, "set data style linespoints"
	print >>f, "set style data linespoints"

	print >>f, "set boxwidth 0.9"
	print >>f, "set terminal postscript color"
	print >>f, "set terminal postscript solid"
	#print >>f, "set terminal png  tiny size 640,480"	
	print >>f, "set title '%s'" % l_title 
	print >>f, "set ylabel '%s'" % l_Y_label
	print >>f, "set xlabel '%s'" % "Residue number"
	print >>f, "set output '%s'" % (image_file)
	if l_display_error_bars==0:
		print >>f, "plot [] '%s' title '%s (%s) ' with boxes lt 3 lw 4" % (l_dyna_data,l_Y_label,l_bmrb)
	else:
		print >>f, "plot [] '%s' title '%s (%s) ' with boxerror lt 3 lw 4" % (l_dyna_data,l_Y_label,l_bmrb)
	print >>f, "exit"
	f.flush()
	os.wait()

	#print l_apache_flag
	if verbose>=1: print "Image type is ", l_image_type
	if l_image_type=="jpg" or l_image_type=="gif" or l_image_type=="png":
		rotate_command='convert -rotate 90  %s %s' % (image_file,image_file)
		if verbose>=1:print "Rotating...", rotate_command
		os.system(rotate_command)
	if  l_image_type=="ps":# and l_apache_flag==1:
		os.system('convert -rotate 90  %s %s/%s.gif' % (image_file,l_rci_output_dir,image_file_base))
	return()


def output_rci(l_full_project_dir,gridsearch_dir,project,l_bmrb_file_loc,l_pic_type,l_apache_flag,l_make_pics,files2copy,l_make_error_pics,l_rci_output_dir):
	"""
	Create RCI output
	"""
	verbose=0
	if verbose>=1: print "Running function output_rci()"
	make_dir_no_remove(l_rci_output_dir)
	curdir=os.getcwd()
	gridsearch_dir_full="%s/rci_results/%s" % (l_full_project_dir,gridsearch_dir)
	if verbose>=1: 	print "gridsearch_dir_full=", gridsearch_dir_full
	rci_result_dir="%s/rci_results" %  (l_full_project_dir)
	if verbose>=1: print "rci_result_dir=", rci_result_dir
	bb_rci_full_filename=""
	sc_rci_full_filename=""

	bb_rmsd_full_filename=""
	sc_rmsd_full_filename=""

	error_dict={}
	for filename in files2copy:
		units=""
		if verbose>=10: print "Processing filename ", filename, files2copy[filename]

		
		full_filename="%s/pics/data/%s" % (gridsearch_dir_full,filename)
		mean_error_file="%s.%s_typical_mean_errors.txt" % (l_bmrb_file_loc,filename)
		median_error_file="%s.%s_typical_median_errors.txt" % (l_bmrb_file_loc,filename)
		success=0
		if "error_file" in files2copy[filename] and l_make_error_pics==1:
			error_file=files2copy[filename]["error_file"]
			error_dict=parse_error_file(error_file)
			success=create_error_files(full_filename,error_dict,mean_error_file,median_error_file)



		copy_command="cp %s %s/%s.%s" % (full_filename,l_rci_output_dir,l_bmrb_file_loc,filename)
		if "output_name" in files2copy[filename]:
			output_name="%s.txt" % files2copy[filename]["output_name"]
			copy_command="cp %s %s/%s.%s" % (full_filename,l_rci_output_dir,l_bmrb_file_loc,output_name)
		#if "units" in files2copy[filename]:
		#	units=files2copy[filename]
	
		if verbose>=1: print copy_command
		os.system(copy_command)
		l_title=files2copy[filename]["title"]
		l_Y_label=files2copy[filename]["y_label"]
		base=files2copy[filename]["base"]


		if l_make_pics==1:
			if l_make_error_pics==0 or success==0:
				file2use=full_filename
			else:
				file2use=median_error_file
			gnuplot_macro_2(file2use,l_title,l_Y_label,l_bmrb_file_loc,l_pic_type,l_apache_flag,base,l_make_error_pics,l_rci_output_dir)
					
		
		if filename=="Side_chain_Backbone_RCI.txt":
			bb_rci_full_filename=full_filename

		if filename=="Side_chain_RCI.txt":
			sc_rci_full_filename=full_filename


		if filename=="Side_chain_Backbone_RMSD.txt":
			bb_rmsd_full_filename=full_filename

		if filename=="Side_chain_MD_RMSD.txt":
			sc_rmsd_full_filename=full_filename



	if bb_rci_full_filename!="" and sc_rci_full_filename!="":
		if l_make_pics==1:
			l_base="Side-chain_RCI_vs_backbone_RCI_component"
			l_title="Side-chain RCI vs. its backbone RCI component"
			l_sc_rci_title="Side-chain RCI"
			l_bb_rci_title="Backbone RCI component"
			l_Y_label="RCI"
			compare_side_chain_and_backbone_rci(l_Y_label,bb_rci_full_filename,sc_rci_full_filename,l_bmrb_file_loc,l_pic_type,l_apache_flag,l_base,l_title,l_sc_rci_title,l_bb_rci_title,l_rci_output_dir)

	if bb_rmsd_full_filename!="" and sc_rmsd_full_filename!="":
		if l_make_pics==1:
			l_base="Side-chain_MD_RMSD_vs_MD_RMSD_from_backbone_RCI_component"
			l_title="Side-chain MD RMSD vs. MD RMSD predicted from backbone component of side-chain RCI"
			l_sc_rci_title="Predicted side-chain MD RMSD"
			l_bb_rci_title="MD RMSD predicted from backbone component of side-chain RCI"
			l_Y_label="MD RMSD (A)"
			compare_side_chain_and_backbone_rci(l_Y_label,bb_rmsd_full_filename,sc_rmsd_full_filename,l_bmrb_file_loc,l_pic_type,l_apache_flag,l_base,l_title,l_sc_rci_title,l_bb_rci_title,l_rci_output_dir)
			




	remove_command="rm -r %s " % (l_full_project_dir)
	os.system(remove_command)


	debug=0
	if debug==1:
		print "Exiting after function cleanup()"
		sys.exit()
	return

def create_error_files(full_filename,error_dict,l_mean_error_file,l_median_error_file):
	"""
	Create error files for plots
	"""
	verbose=0
	if verbose>=1: print "Running function create_error_files()"
	l_file_lines=read_file(full_filename)
	l_mean_text=l_median_text=""
	success=0
	for i in l_file_lines:
		j=i.split()
		if len(j)>2:
			residue_number, l_value, residue_name=j[0], j[1], j[2] 
			if residue_name in error_dict:
				if verbose>=10: print "Residue type %s is found in error_dict()" % residue_name
				#mean=error_dict[residue_name]["mean"]
				median=error_dict[residue_name]["median"]
				#l_mean_text+="%s %s %s %s"  % (residue_number,l_value, mean,residue_name) + linesep
				l_median_text+="%s %s %s %s" % (residue_number,l_value, median,residue_name) + linesep
				success=1
			else:
				print "Warning!!! Residue type %s was not found in  error_dict()" % residue_name	

	#write_file(l_mean_text,l_mean_error_file)
	write_file(l_median_text,l_median_error_file)		

	debug=0
	if debug==1:
		print "Exiting after function create_error_files()"
		sys.exit()
 
	return success



def parse_error_file(l_error_file):
	"""
	Parse error file
	"""
	verbose=0
	if verbose>=1: print "Running function parse_error_file()"
	file_lines=read_file(l_error_file)
	l_dict={}
	for i in file_lines:
		j=i.split()
		if len(j)>0 and "Mean" not in i:
			residue_type, mean, median,stdev=j[0],j[1],j[2],j[3]
			l_dict[residue_type]={}
			l_dict[residue_type]["mean"]=mean
			l_dict[residue_type]["median"]=median
			l_dict[residue_type]["stdev"]=stdev
	debug=0
	if debug==1:
		print "Exiting after function parse_error_file()"
		sys.exit()
	return l_dict



def compare_side_chain_and_backbone_rci(l_Y_label, bb_rci_full_filename,sc_rci_full_filename,l_bmrb,l_image_type,l_apache_flag,l_base,l_title,l_sc_rci_title,l_bb_rci_title,l_rci_output_dir ):
	"""
	Compare side-chain and backbone RCIs
	"""
	verbose=0
	if verbose>=1: print "Running function  compare_side_chain_and_backbone_rci()"
	#l_Y_label="Side-chain_RCI_vs_backbone_RCI_component"
	#l_title="Side-chain RCI vs. its backbone RCI component"
	image_file="%s/%s_%s.%s" % (l_rci_output_dir,l_bmrb,l_base,l_image_type)
	image_file_base="%s_%s" % (l_bmrb,l_base)
	f=os.popen('%s' % gnuplot_program,'w')
	#print >>f, "set data style linespoints"
	print >>f, "set style data linespoints"
	#print >>f, "set data style lines"
	print >>f, "set terminal postscript color"
	print >>f, "set terminal postscript solid"
	print >>f, "set title '%s'" % (l_title) 
	print >>f, "set ylabel '%s'" % l_Y_label
	print >>f, "set xlabel '%s'" % "Residue number"
	print >>f, "set output '%s'" % image_file
	print >>f, "plot [][]'%s' title '%s' with linespoints lt 1 pt 3, '%s' title '%s' with linespoints lt 3 pt 6 " % (sc_rci_full_filename,l_sc_rci_title,bb_rci_full_filename,l_bb_rci_title)
	print >>f, "exit"
	f.flush()
	os.wait()	

	#print l_apache_flag
	if verbose>=1: print "Image type is ", l_image_type
	if l_image_type=="jpg" or l_image_type=="gif":
		rotate_command='convert -rotate 90  %s %s' % (image_file,image_file)
		if verbose>=1: print "Rotating...", rotate_command
		os.system(rotate_command)
	if  l_image_type=="ps" and l_apache_flag==1:
		os.system('convert -rotate 90 %s %s.gif' % (image_file,image_file_base))

	return

######################################################
#To-do functions
######################################################



###################################################### End of functions ###########################################


######################################################
# Function-based dictionaries, must go after functions
######################################################	

pict_dict={}
pict_dict["Per_Residue_mean_all"]={}
pict_dict["Per_Residue_mean_all"]["data_files"]={}
pict_dict["Per_Residue_mean_all"]["data_files"][1]="gnuplot_Per_Residue_mean_text_all"
pict_dict["Per_Residue_mean_all"]["title"]="Absolute difference between Shiftx-predicted and experimental chemical shifts for %s"
pict_dict["Per_Residue_mean_all"]["description"]="Chemical shift difference (ppm)"
pict_dict["Per_Residue_mean_all"]["Y_label"]="Chemical shift difference (ppm)"
pict_dict["Per_Residue_mean_all"]["X_label"]="Residue number"
pict_dict["Per_Residue_mean_all"]["plotting_function"]=gnuplot_bars_mean_std

######################################################	




######################################################
# Parsing arguments 
######################################################	

arguments="python "

a=0

if len(sys.argv)>1:
	for item in sys.argv:
		arguments+=" %s " % item
		if item=="-l":
			l_location=sys.argv[a+1]
			location=os.path.abspath(l_location)
		if item=="-p":
			project=sys.argv[a+1]
		if item=="-pdb":
			pdb=sys.argv[a+1]
			pdb_abspath=os.path.abspath(pdb)
			pdb_loc=os.path.basename(pdb)
		if item=="-chain":
			chain=sys.argv[a+1]
		if item=="-pdb_dir":
			pdb_dir=sys.argv[a+1]
			pdb_dir_abspath=os.path.abspath(pdb_dir)
			pdb_dir_loc=os.path.basename(pdb_dir)
		if item in ["-cs","-b2","-b3", "-sh"]:
			shifts=sys.argv[a+1]
			shifts_abspath=os.path.abspath(shifts)
			shifts_loc=os.path.basename(shifts)
			if item=="-b2":
				shift_format=0
			elif item=="-sh":
				shift_format=1
			elif item=="-b3":
				shift_format=2	
		if item in ["-sf", "-shift_format"]:
			shift_format=int(sys.argv[a+1]) # 0 - BMRB, 1 - Shifty

		if item in ["-import"]:
			import_settings_file=os.path.abspath(sys.argv[a+1]) # Optional: File with CSQ settings to import

		if item in ["-gnuplot"]:
			gnuplot_program=os.path.abspath(sys.argv[a+1]) # Optional:  Path to gnuplot binary

		if item in ["-cs_common"]:
			dict_with_common_atoms_file=os.path.abspath(sys.argv[a+1]) # Optional:  Path to gnuplot binary
		if item in ["-cs_common_offset"]:
			dict_with_common_atoms_offset=int(sys.argv[a+1])
		if item in [" -allow_mutations"]:
			allow_mutations=int(sys.argv[a+1])

		if item in ["-fill_dict"]:
			fill_dict=int(sys.argv[a+1]) 
		if item in ["-zodb"]:
			zodb=int(sys.argv[a+1]) 

		if item in ["-pause"]:
			pause=float(sys.argv[a+1]) 

		if item in ["-stereomode"]:
			stereo_matching_mode=int(sys.argv[a+1]) 


		if item in ["-debug"]:
			g_debug=int(sys.argv[a+1]) 


		if item in ["-cluster"]:
			cluster=sys.argv[a+1] 

		if item=="-verify":
			from bmrb_atom_dictionary import bmrb_atom_dictionary as verify_dict
		if item=="-pop":
			population_file=os.path.abspath(sys.argv[a+1])
		if item == "-pop_type":
			pop_type=int(sys.argv[a+1]) # If 1, original population, if 2, original non-zero population, if 3, non-scaled population after filtering, 4, scaled population after filtering,


		if item in ["-pics"]:
			make_pics=int(sys.argv[a+1])
			#if make_pics==0:
			#	fill_dict=0
		if item in ["-v2", "-verbose2"]:
			verbose2=int(sys.argv[a+1])
		if item in [ "-calc_offset"]:
			calc_offset=int(sys.argv[a+1])

		if item in [ "-run_reduce"]: # Run reduce for Shifts
			run_reduce1=int(sys.argv[a+1])

		if item in [ "-reduce_program"]: # Reduce program
			reduce_program=os.path.abspath(sys.argv[a+1])

		if item in [ "-predict_bmrb"]: # Produce BMRB with predicted chemical shifts
			predict_bmrb=int(sys.argv[a+1])

		if item in [ "-ensemble_training"]: # Make training files for ensemble building
			make_training_files=int(sys.argv[a+1])

		if item in [ "-author_cs"]: # Use author CS residue numbering
			author_cs=int(sys.argv[a+1])



		####################################
		# Statistics option
		####################################
		if item in ["-o","-outliers"]:
			outliers=int(sys.argv[a+1])
		if item in ["-per_residue"]:
			per_residue_on=int(sys.argv[a+1]) 
		if item in ["-pearson"]:
			do_pearson=int(sys.argv[a+1])
		if item in ["-spearman"]:
			do_spearman=int(sys.argv[a+1])
		if item in ["-kendall"]:
			do_kendall=int(sys.argv[a+1])
		####################################

		####################################
		# Statistics option
		####################################
		if item in ["-g_always"]:
			global_always=int(sys.argv[a+1]) 
		if item in ["-g_actually"]:
			global_actually=int(sys.argv[a+1]) 

			

		####################################
		# Output option
		####################################
		if item in ["-module_text"]:
			module_text_on=int(sys.argv[a+1]) 
		if item in ["-report_text"]:
			report_text_on=int(sys.argv[a+1]) 
		if item in ["-total_module_text"]:
			total_module_text_on=int(sys.argv[a+1]) 
		if item in ["-total_report_text"]:
			total_report_text_on=int(sys.argv[a+1]) 



		####################################
		# RCI settings settings 
		####################################
		if item=="-rci_results":
			rci_result_file=os.path.abspath(sys.argv[a+1]) #  File with RCI results
		if item=="-rci_program":
			rci_program=sys.argv[a+1] #  RCI program
			csq_options+=" -rci_program %s " % rci_program
		if item=="-rci_dir":
			rci_dir=os.path.abspath(sys.argv[a+1]) #  Dir with RCI program
		if item=="-bb_rci":
			redo_bb_rci=int(sys.argv[a+1]) # if 1, recalculate backbone RCI in this script
		if item=="-sc_rci":
			sc_rci=int(sys.argv[a+1]) # if 1, calculate side-chain RCI in this script
		if item=="-rc":
			rc_values=int(sys.argv[a+1]) # If 1, Wishart RC values will be used; if 2 - Dyson/Write
 		if item=="-run_rci":
			run_rci=int(sys.argv[a+1]) # if 1, run RCI, if 0, do not run
		if item=="-rci_score":
			rci_score=int(sys.argv[a+1]) # if 1, calculate CSI score
		if item=="-S2_program": #???????????????????????????
			S2_program=os.path.abspath(sys.argv[a+1]) #  Dir with RCI program
		if item=="-rc_max_offset":
			rc_max_offset=int(sys.argv[a+1]) # Offset in residue numbering between Random Coil predictions and experimental shifts
		if item in [ "-rci_out_offset"]: # Offset in residue numbering for RCI output
			rci_out_offset=int(sys.argv[a+1])
		if item in [ "-report_residues"]: # Residues to report average RCI and sec CS chemical shifts for
			rci_report_residues=sys.argv[a+1]
		if item in [ "-rci_reports"]: # Output dictionary with RCI values + per segment (user-difined) averages
			make_rci_reports=int(sys.argv[a+1])
		if item in [ "-secCS_reports"]: # Output dictionary with secondary CS values + per segment (user-difined) averages
			make_secCS_reports=int(sys.argv[a+1])
		if item=="-miss_ass":
			miss_ass=int(sys.argv[a+1]) # if 1, use missing assignment coefficients
		if item=="-gap":
			value_search=int(sys.argv[a+1])  # Value search for gap,  if 1, search +1 and -1, if 2, search +1,+2,-1,-2
		if item=="-gap2":
			value_search2=int(sys.argv[a+1])  # Value search for gap,  if 1, search +1 and -1, if 2, search +1,+2,-1,-2
		if item=="-smooth":
			averaging=int(sys.argv[a+1])  # 

		if item=="-bb_smooth":
			bb_averaging=int(sys.argv[a+1])  # 

		if item=="-rci_method":
			rci_method=int(sys.argv[a+1])  # If 1, use original method (do inversion after combining); if 2, do inversion before combining 
		if item=="-value_mode":
			find_value_mode=int(sys.argv[a+1])  # If 1, use mean to calculate RCI
		if item=="-no_outliers":
			no_outliers=int(sys.argv[a+1])  # If 1, remove sec cs outliers
		if item=="-floor1":
			 floor_value1=float(sys.argv[a+1])  # Floor 1 value
		if item=="-ceil1":
			 ceiling_value=float(sys.argv[a+1])  #  Ceiling 1 value
		if item=="-power1":
			 power1=float(sys.argv[a+1])  #  Power 1
		if item=="-scaling1":
			scaling1=float(sys.argv[a+1])  # Scaling1
		if item=="-early_floor":
			early_floor_value=float(sys.argv[a+1])  # Early floor value
		if item=="-rci_param":
			rci_parameter_dict_file=os.path.abspath(sys.argv[a+1])  
		if item=="-rc_offsets":
			rc_offsets_dict_file=os.path.abspath(sys.argv[a+1])  
		if item=="-nostereo":
			remove_stereo=int(sys.argv[a+1]) # if 0, do not remove stereoassignment, if 1, remove all, if 2, remove only for residues on the gridsearch list
		if item=="-files4train":
			files4training=int(sys.argv[a+1])
		if item=="-make_rci_pics":
			make_rci_pics=int(sys.argv[a+1]) # if 2, make pictures even for a grid search
		if item=="-bb_rc": # 1 - Wishart's, 2 - Wang's, 3 - Lukhin's, 4 - Schwarzinger's, 5 - mean of 1 and 4; 6 - average of 1, 2 & 3; #7 -  Average of Wang and Shwartz RC; #8 - Average of Lukin and Shwartz RC; #9 - elif Random_coil_flag==9
			backbone_random_coil_values=int(sys.argv[a+1]) # If 1, Wishart RC values will be used; if 2 - Dyson/Write
		if item=="-i1": 
			i_plus_minus_one_residue_correction=int(sys.argv[a+1])
		if item=="-i2": 
			i_plus_minus_two_residue_correction=int(sys.argv[a+1])

		if item=="-h_sc_corr": 
			side_chain_H_correction=int(sys.argv[a+1])
		if item=="-c_sc_corr": 
			side_chain_C_correction=int(sys.argv[a+1])
		if item=="-n_sc_corr": 
			side_chain_N_correction=int(sys.argv[a+1])

		if item=="-coef_only": 
			coef_only=int(sys.argv[a+1])

		if item=="-sc_coef": 
			sc_rci_weight=float(sys.argv[a+1])
		if item=="-bb_coef": 
			bb_rci_weight=float(sys.argv[a+1])
		if item in ["-bb_sc_combo","-sc_bb_combo"]: 
			sc_bb_combine=int(sys.argv[a+1])

		if item in ["-detect_missing"]: 
			detect_missing_atoms=int(sys.argv[a+1])
		if item in ["-hydro"]: 
			hydro=int(sys.argv[a+1])
		if item in ["-hcoef"]: 
			hcoef=float(sys.argv[a+1])
		if item in ["-hdiv"]: 
			hydro_div=float(sys.argv[a+1])
		if item in ["-exclude"]: 
			exclude_atoms=int(sys.argv[a+1])
		if item in ["-rc_offset_grid"]: 
			rc_offset_gridsearch=int(sys.argv[a+1])
		if item in ["-rc_increment_scale"]: 
			rc_increment_scale=float(sys.argv[a+1])
		if item in ["-bb_end_correct"]: 
			bb_end_correct=int(sys.argv[a+1])
		if item in ["-sc_end_correct"]: 
			sc_end_correct=int(sys.argv[a+1])

		if item in ["-rcs2"]: 
			find_rc_order_parameter=int(sys.argv[a+1])
		if item in ["-md_rmsf"]: 
			find_MD_RMSF=int(sys.argv[a+1])
		if item in ["-nmr_rmsf"]: 
			find_NMR_RMSF=int(sys.argv[a+1])
		if item in ["-asa"]: 
			find_ASA=int(sys.argv[a+1])
		if item in ["-b_factor"]: 
			find_B_factor=int(sys.argv[a+1])

		if item in ["-rci_easy"]: 
			rci_easy=int(sys.argv[a+1])



		if item in ["-find_max"]: 
			find_max=int(sys.argv[a+1])
		if item in ["-find_min"]: 
			find_min=int(sys.argv[a+1])

		if item in ["-norm"]: 
			normalize=int(sys.argv[a+1])


		if item in ["-errors", "-e"]: 
			make_error_pics=int(sys.argv[a+1])

		if item == "-rp":
			rci_project=sys.argv[a+1]
		if item == "-rl":
			rci_location=sys.argv[a+1]

		####################################
		# RCI grid search
		####################################
		if item=="-gridsearch1":
			do_gridsearch1=int(sys.argv[a+1])

		if item=="-shifts4grid":
			shiftclass4gridsearch_list_init=sys.argv[a+1]  # List of chem shift classes for grid search
			if "," not in shiftclass4gridsearch_list_init:
				shiftclass4gridsearch_list=[shiftclass4gridsearch_list_init]
			else:
				shiftclass4gridsearch_list=shiftclass4gridsearch_list_init.split(",")

		if item=="-coeff4grid":
			gridsearch_coeff_list_init=sys.argv[a+1]  # List of coefficients for grid search
			if "," not in gridsearch_coeff_list_init:
				gridsearch_coeff_list=[gridsearch_coeff_list_init]
			else:
				gridsearch_coeff_list=gridsearch_coeff_list_init.split(",")

		if item=="-res4grid":
			res4gridsearch_list_init=sys.argv[a+1]  # List of res for grid search
			if "," not in res4gridsearch_list_init:
				res4gridsearch_list=[res4gridsearch_list_init]
			else:
				res4gridsearch_list=res4gridsearch_list_init.split(",")

		if item=="-res_sense":
			residue_type_sensitivity=int(sys.argv[a+1])
		if item=="-bb_rci_opt":
			bb_rci_opt=int(sys.argv[a+1]) # Optimize only backbone RCI component of side-chain RCI

		if item=="-param_overwrite":
			param_overwrite=int(sys.argv[a+1]) # Overwrite parameters from an external dictionary

		####################################


		############# To-do, reserved options ###########################################

		####################################
		# Decoys-predictor settings To-DO
		####################################
		if item=="-do_decoys":
			do_decoys=int(sys.argv[a+1]) # if 1,  run decoys
		if item in ["-decoys_dir"]:
			decoys_program_dir=os.path.abspath(sys.argv[a+1]) # Optional: Path to a directory with decoys
		if item=="-run_decoys":
			actually_run_decoys=int(sys.argv[a+1]) # if 0, do not run mark, use existing results 
		if item=="-decoys4meta":
			decoys4meta=int(sys.argv[a+1]) # if 0, do not use shiftx in meta_predictor
		####################################


		####################################
		# KS settings To-DO
		####################################
		if item=="-calc_ks":
			calc_ks=int(sys.argv[a+1]) # if 1,  run calculate KS scores
		####################################


		####################################
		# Zscore settings To-DO
		####################################
		if item=="-calc_zscores":
			calc_zscores=int(sys.argv[a+1]) # if 1,  calculate Zscores
		if item=="-zscore_dict":
			zscore_dict=os.path.abspath(sys.argv[a+1]) # Dictionary with mean and std for Z-scores
		if item=="-calc_shaic":
			calc_shaic=int(sys.argv[a+1]) # if 1,  Calculate SHaic scores
		####################################


		####################################
		# SHAIC score settings To-DO
		####################################
		if item=="-shaic_dict":
			shaic_dict=os.path.abspath(sys.argv[a+1]) #  Dictionary with mean and std for Shaic score
		####################################


		####################################
		# Vendruscilo score settings To-DO
		####################################
		if item=="-calc_potential":
			calc_potential=int(sys.argv[a+1]) # Calculate Vendrusculo potential
		if item=="-pot_dict":
			potential_dict=os.path.abspath(sys.argv[a+1]) #  Dictionary with mean and std for Vendrusculo Potential
		####################################



		####################################
		# Weka score settings To-DO
		####################################
		if item=="-weka_dir":
			weka_dir=os.path.abspath(sys.argv[a+1]) #   path to Weka directory
		if item=="-weka_model":
			weka_model=sys.argv[a+1] #  Dictionary with mean and std for Shaic score
			csq_options+=" -weka_model %s " % weka_model
		####################################



		####################################
		# Flexibility settings settings To-DO
		####################################
		if item=="-no_flex":
			no_flex=int(sys.argv[a+1]) #  # 1 - no loops, 2 - no loops and termini
		if item=="-flex_source":
			flex_source=int(sys.argv[a+1]) # if 0, use DSSP, if 1, use RCI
		####################################



		####################################
		# CSI settings settings To-DO
		####################################
		if item=="-csi_results":
			csi_result_file=os.path.abspath(sys.argv[a+1]) #  File with CSI results
		if item=="-csi_program":
			csi_program=sys.argv[a+1] #  CSI program
		if item=="-run_csi":
			run_csi=int(sys.argv[a+1]) # if 1, run CSI, if 0, do not run
		if item=="-csi_score":
			csi_score=int(sys.argv[a+1]) # if 1, calculate CSI score
		####################################


		####################################
		# Preditor settings settings To-DO
		####################################
		if item=="-preditor_results":
			preditor_result_file=os.path.abspath(sys.argv[a+1]) #  File with preditor results
		if item=="-preditor_program":
			preditor_program=sys.argv[a+1] #  preditor program
		if item=="-run_preditor":
			run_preditor=int(sys.argv[a+1]) # if 1, run preditor, if 0, do not run
		if item=="-preditor_score":
			preditor_score=int(sys.argv[a+1]) # if 1, calculate preditor score
		####################################


		if item=="-g_outliers":
			global_outliers=int(sys.argv[a+1]) #

		if item=="-mean_std1":
			outliers1_dict_file=os.path.abspath(sys.argv[a+1]) #  File with mean and standard deviations for protein-overall level 

		if item=="-mean_std2":
			outliers2_dict_file=os.path.abspath(sys.argv[a+1]) #  

		#############


		a+=1
		


######################################################
# Processing arguments 
######################################################			
if verbose2==1:
	print linesep,linesep,linesep,"You ran the following command:", linesep,linesep,arguments,linesep,linesep 

this_script_full_path=os.path.abspath(sys.argv[0])
this_script_name=os.path.basename(sys.argv[0])
script_location=os.path.dirname(this_script_full_path)
if verbose>3:
	print "this_script_full_path=", this_script_full_path
	print "this_script_name=", this_script_name
	print "script_location=", script_location



if rci_location=="" and rci_project!="":
	rci_output_dir="%s/%s" % (script_location,rci_project)
elif rci_location!="" and rci_project!="":
	rci_output_dir="%s/%s" % (rci_location,rci_project)
else:
	rci_output_dir="%s" % (script_location)


if redo_bb_rci==1 or sc_rci==1:
	find_rci=1
	if verify_dict=={}:
		from bmrb_atom_dictionary import bmrb_atom_dictionary as verify_dict


total_usage=""
if shifts_abspath=="":
	total_usage+=usage_1
if pdb_dir_abspath=="" and pdb_abspath=="" and find_rci==0:
	total_usage+=usage_2

if shifts_abspath=="" or (pdb_dir_abspath=="" and pdb_abspath=="" and find_rci==0):
	print total_usage,usage,linesep, linesep, "..............."
	sys.exit(-1)	
if find_rci and pdb_dir_abspath=="" and pdb_abspath=="":
	do_shiftx=do_shiftx2=do_sparta=do_shifts=do_FourDspot=do_camshift=do_ch3shift=do_arshift=meta=0

# Creating project directory
full_project_dir=create_project_dir(project,location)

if shift_format==0:
	bmrb_file_abspath=shifts_abspath
	bmrb_file_loc=shifts_loc
elif shift_format==1:
	shifty_file_abspath=shifts_abspath
	shifty_file_loc=shifts_loc
	shifty_list=read_file(shifty_file_abspath)
	#print shifty_list
	atom_place_dic=atom_place(shifty_list)
	#print atom_place_dic
	bmrb_file_abspath,bmrb_file_loc=write_bmrb(shifty_list,shifty_file_abspath,atom_place_dic,full_project_dir,shifty_file_loc)
elif shift_format==2:
	bmrb_file_abspath=shifts_abspath
	bmrb_file_loc=shifts_loc


# RCI options
rci_report_residues_dict={}
if rci_report_residues!="":
	rci_report_residues_dict=build_rci_report_residues(rci_report_residues)	


if do_gridsearch1==1:
	gridsearch_coeff_list=gridsearch_coeff_list1 #[0.0,0.33,0.66,1.0]
elif do_gridsearch1==2:
	gridsearch_coeff_list=gridsearch_coeff_list2 #[0.0,0.5,1.0]
elif do_gridsearch1==3:
	gridsearch_coeff_list=gridsearch_coeff_list3 #[0.0,0.2,0.4,0.6,0.8,1.0]
elif do_gridsearch1==4:
	gridsearch_coeff_list=gridsearch_coeff_list4 #[0.5,1.0]
elif do_gridsearch1==5:
	gridsearch_coeff_list=gridsearch_coeff_list5 #[0.0,1.0]
elif do_gridsearch1==6:
	gridsearch_coeff_list=gridsearch_coeff_list6 #[0.3,1.0]
elif do_gridsearch1==7:
	gridsearch_coeff_list=gridsearch_coeff_list7 #[0.3,1.0]
elif do_gridsearch1==8:
	gridsearch_coeff_list=gridsearch_coeff_list8 #[1.0]
elif do_gridsearch1==9:
	gridsearch_coeff_list=gridsearch_coeff_list9 #[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.8,1.0]




# Import populations from NNLS
if population_file!="":
	population_dict=import_local_modules(population_file,script_location,prefix)


# Import parameters for RCI
if rci_parameter_dict_file!="":
	rci_parameter_dict=import_local_modules(rci_parameter_dict_file,script_location,prefix)
	rcoil_offset_dict=extract_rc_offsets(rci_parameter_dict)

if rc_offsets_dict_file!="":
	rci_parameter_dict1=import_local_modules(rc_offsets_dict_file,script_location,prefix)
	rcoil_offset_dict=extract_rc_offsets(rci_parameter_dict1)
	if rci_parameter_dict_file=="":
		rci_parameter_dict=rci_parameter_dict1
	debug=0
	if debug==1:
		#print "rci_parameter_dict=", rci_parameter_dict
		print "rcoil_offset_dict=", rcoil_offset_dict
		print "Exiting...."
		sys.exit()	


if global_always!="":
	always_run_shiftx=always_run_shiftx2=always_run_shifts=always_run_camshift=always_run_sparta=always_run_ch3shift=always_run_arshift=always_run_FourDspot=global_always

if global_actually!="":
	actually_run_shiftx=actually_run_shiftx2=actually_run_shifts=actually_run_camshift=actually_run_sparta=actually_run_ch3shift=actually_run_arshift=actually_run_FourDspot=global_actually
#print "meta=", meta


# Training matrix option
#print "matrix_atom_list=", matrix_atom_list
if  "" in matrix_atom_list or "Backbone" in  matrix_atom_list:
	#print "Place 1"
	atom_list_for_ensemble=atom_list_for_ensemble_init
elif "Protons" in matrix_atom_list or  "protons" in matrix_atom_list:
		atom_list_for_ensemble=all_protons
elif "Carbons" in matrix_atom_list or  "carbons" in matrix_atom_list:
		atom_list_for_ensemble=all_carbons
elif "Nitrogens" in matrix_atom_list or "nitrogens" in matrix_atom_list:
		atom_list_for_ensemble=all_nitrogens
elif "ALL" in matrix_atom_list:
		atom_list_for_ensemble=all_protons+all_nitrogens+all_carbons
elif len(matrix_atom_list)>0:
	atom_list_for_ensemble=matrix_atom_list


# ZOPE DB settings
database_file= "%s/database.dat" % full_project_dir	
if zodb:
	import ZODB
	from ZODB.FileStorage import FileStorage
	from ZODB.DB import DB
	import transaction
	import persistent
	import logging
	from BTrees.OOBTree import OOBTree
	from ZODB.PersistentMapping import PersistentMapping
	from persistent import Persistent
	from persistent.dict import PersistentDict
	from persistent.list import PersistentList
	import transaction
	logging.basicConfig()

# Import dictionary with common atoms
if dict_with_common_atoms_file!="":
	dict_with_common_atoms=import_local_modules(dict_with_common_atoms_file,script_location,prefix)
	debug=0
	if debug==1:
		print "dict_with_common_atoms=", dict_with_common_atoms
		print "Exiting after importing dict_with_common_atoms"
		sys.exit()
# End of Import dictionary with common atoms

rci_exclude_atom_list=[]
if rc_offset_gridsearch>0:
	res4gridsearch_list_dict["Y"]["stereo"]=0
	res4gridsearch_list_dict["K"]["stereo"]=0
	res4gridsearch_list_dict["R"]["stereo"]=0
	#rci_exclude_atom_list+=['CB']
	forbidden_atoms["W"]=["NE1","NEave"]


if find_ASA==0:
	files2copy["Side_chain_RCI.txt"]={"title":"Side-Chain Random Coil Index", "y_label":"Side Chain RCI","base":"Side_Chain_RCI"}
	files2copy["Side_chain_NMR_RMSD.txt"]={"title":"Side-Chain Total NMR RMSD predicted from RCI", "y_label":"NMR Side Chain RMSD (A)", "base":"NMR_Side_Chain_RMSD","units":"A"}
	files2copy["Side_chain_NMR_RMSD.txt"]["error_file"]="RCI2_testing_set_ONLY_NMR_RMSD_Scale_5.59_short_C_end_correction_per_aa_stats.dat"
	files2copy["Side_chain_MD_RMSD.txt"]={"title":"Side-Chain Total MD RMSD predicted from RCI", "y_label":"MD Side Chain RMSD (A)","base":"MD_Side_Chain_RMSD","units":"A"}
	files2copy["Side_chain_MD_RMSD.txt"]["error_file"]="RCI2_testing_set_ONLY_MD_RMSD_Scale_9_short_C_end_correction_per_aa_stats.dat"
	files2copy["Side_chain_Backbone_RCI.txt"]={"title":"Backbone RCI component of side-chain RCI without Gly", "y_label":"Backbone RCI component","base":"Backbone_RCI_component_no_Gly","output_name":"Backbone_RCI_component_no_Gly"}
	files2copy["Side_chain_Backbone_RMSD.txt"]={"title":"Backbone MD RMSD predicted from backbone component of side-chain RCI (no Gly)", "y_label":"MD RMSD ","base":"Backbone_RMSD_component_no_Gly","output_name":"Backbone_RMSD_component_no_Gly"}
	files2copy["Side_chain_normalized_mean_B_factor.txt"]={"title":"Normalized mean side-chain B-factor predicted from side-chain RCI", "y_label":"Normalized side-chain B-factor","base":"Normalized_mean_side_chain_B_factor","output_name":"Normalized_mean_side_chain_B_factor"}
	files2copy["Side_chain_normalized_mean_B_factor.txt"]["error_file"]="RCI2_vs_B_factorNorm2_short_C_end_correction_aa_stats_100.dat"
else:
	files2copy["ASAf.txt"]={"title":"Fractional ASA predicted from side-chain RCI", "y_label":"ASA","base":"ASA"}
	files2copy["ASAf.txt"]["error_file"]="RCI2_testing_set_ONLY_SC_ASA_Scale_150_short_C_end_correction_per_aa_stats.dat"


#print "rci_method=", rci_method
#print "Exiting...."
#sys.exit()
######################################################			




######################################################
# Main program 
######################################################
pdb_dict={}
pdb_list=[]
if zodb:
	if file_exists(database_file):
		os.system('rm %s' % database_file)
	storage = FileStorage(database_file)
	db = DB(storage)
	connection = db.open()
	main_database = connection.root()
	#main_database["pdb_dict"]= {}
	main_database["pdb_dict"]= OOBTree()
	main_database["average_dictionary"]= OOBTree()
	main_database["assign_dic"]= OOBTree()
	main_database["average_shift_dict"]= OOBTree()
	main_database["per_residue_dict"]= OOBTree()
	main_database["total_dict"]= OOBTree()

	#main_database["pdb_dict"]= PersistentDict()
	pdb_dict=main_database["pdb_dict"]
	#average_dict=main_database["average_dictionary"]
	#pdb_dict = OOBTree()


######################################################
#6) Parse chemical shift file
protein_sequence_list,final_assignment,exp_assignement,assignment,exp_assignment_dict,residue_numbers_list,B_Cys_list=parse_chemical_shifts(verify_dict,bmrb_file_abspath,author_cs,dict_with_common_atoms,dict_with_common_atoms_offset,allow_mutations)
#print "final_assignment=", final_assignment
#print "exp_assignement=", exp_assignement
#print "assignment= ", assignment
#print "exp_assignment_dict=", exp_assignment_dict
#print protein_sequence_list
#print residue_numbers_list
#sys.exit()
######################################################



######################################################
#6) Find Random Coil Values
if rc_offset_gridsearch in [0,2] or do_gridsearch1==0:
	rc_assign_dic,rc_list_4_offset,rc_residue_list,pdb_list=caclulate_random_coil_values(\
			backbone_random_coil_values,\
			i_plus_minus_two_residue_correction,\
			i_plus_minus_one_residue_correction,\
			backbone_random_coil_values_dict,\
			side_chain_random_coil_values,\
			i_plus_minus_two_residue_correction_dict,\
			i_plus_minus_one_residue_correction_dict,\
			B_Cys_list,find_rci,\
			protein_sequence_list,\
			exp_assignment_dict,\
			pdb_dict,\
			rc_values,\
			place_dict,\
			rc_offset_dict,\
			stereospecific_dictionary,\
			rcoil_offset_dict,\
			)
#print pdb_list
#print rc_assign_dic
#sys.exit()



######################################################




######################################################
if find_rci==1 and calc_offset==1 and rc_max_offset=="" and (rc_offset_gridsearch in [0,2] or do_gridsearch1==0):
	if verbose>=0: print "Running function calc_offsets() for Random Coil Values"
	rc_offset_dic=calc_offsets(rc_list_4_offset,protein_sequence_list)
	if verbose>=0: print "Running function max_offset for rci"
	rc_max_offset,rc_strong_shift,rc_second_offset_number,rc_second_offset,rc_total_length,max_len=calc_max_offset(rc_offset_dic,strong_shift_limit,total_length_strong_offset)
	debug_exit(0," Exiting after calculating offset in residue numbering between Random Coil Values predictions and exp. shift files ...")	
	if rc_max_offset=="": rc_max_offset=0	
######################################################

if zodb:
	pdb_dict._p_changed = True
	transaction.commit()
	db.close()

######################################################
#14) Calculate chemical shift differences and statistics for individual models	for Random Coil Values

if find_rci==1:
	mean_cs_mode=0
	if rc_offset_gridsearch in [0,2] or do_gridsearch1==0:
		rc_first_residue,rc_last_residue,rc_common_residue_numbers=calc_cs_diff(stereo_matching_mode,mean_cs_mode,zodb,database_file,pdb_list,pdb_dict,exp_assignment_dict,rc_max_offset,"rc","rc_assign_dic")
		debug_exit(0," Exiting after calc_cs_diff() function for rc...")	
		rc_average_dict=calc_cs_diff_stats(zodb,database_file,pdb_list,fill_dict,total_module_text_on,total_report_text_on,module_text_on,report_text_on,per_residue_on,do_pearson, do_spearman, do_kendall,pdb_dict,full_project_dir,report_dir,mean_cs_mode,rc_common_residue_numbers,"rc",outliers)
		#print "rc_average_dict=", rc_average_dict
		#debug_exit(0," Exiting after calc_cs_diff() function for rc...")	



	coeff_dict=coef_dic(file_with_coefficients)
	coef_mean=mean_coef(coeff_dict)
	#shift_class_lookup=build_shift_class_lookup(shift_class_dict,shift_class_description_dict)

	#l_rci_out_offset,
	runname_dict={}
	runname_result_dict={}
	completeness_found=0

	if do_gridsearch1==0 and run_rci==1:
		runname=gridsearch_dir="nogridsearch"
		runname_list+=[runname]
		calculate_rci(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		rc_residue_list,\
		early_floor_value,\
		shift_class_description_dict,\
		shift_class_lookup,\
		value_search,\
		database_file,\
		rci_dict,\
		rci_parameter_dict,\
		averaging,\
		bb_averaging,\
		coef_mean,\
		miss_ass,\
		H_Hertz_corr,\
		C_Hertz_corr,\
		N_Hertz_corr,\
		floor_value1,\
		scale,\
		ceiling_value,\
		rci_method,\
		find_value_mode,\
		no_outliers,\
		power1,\
		scaling1,\
		rci_out_offset,\
		value_search2,\
		full_project_dir,\
		rci_results_dir,\
		gnuplot_program,\
		make_rci_pics,\
		make_rci_images,\
		make_rci_reports,\
		make_secCS_reports,\
		rci_report_residues_dict,\
		runname,\
		gridsearch_dir,\
		residue_numbers_list,\
		res4gridsearch_list,\
		res4gridsearch_list_dict,\
		remove_stereo,\
		shiftclass4gridsearch_list,\
		do_gridsearch1,\
		files4training,\
		rci_parameter_dict_file,\
		runname_result_dict,\
		coef_only,\
		completeness_found,\
		forbidden_atoms,\
		sc_rci_weight,\
		bb_rci_weight,\
		sc_bb_combine,\
		detect_missing_atoms,\
		rc_offset_gridsearch,\
		rc_assign_dic,\
		bb_end_correct,\
		sc_end_correct,\
		find_rc_order_parameter,\
		find_MD_RMSF,\
		find_NMR_RMSF,\
		find_ASA,\
		find_B_factor,\
		exclude_atoms,\
		bb_rci_opt,\
		)


		write_rci_reports(\
		pdb_list,\
		pdb_dict,\
		zodb,\
		database_file,\
		full_project_dir,\
		make_rci_reports,\
		make_secCS_reports,\
		rci_report_residues_dict,\
		runname_list,\
		shiftclass4gridsearch_list,\
		do_gridsearch1,\
		runname_dict,\
		rc_offset_gridsearch,\
		)

	else:
		gridsearch_dir="gridsearch_%s" % project
		import itertools
		runname_list,runname_dict=grid_search_1(\
						pdb_list,\
						pdb_dict,\
						zodb,\
						rc_residue_list,\
						early_floor_value,\
						shift_class_description_dict,\
						shift_class_lookup,\
						value_search,\
						database_file,\
						rci_dict,\
						rci_parameter_dict,\
						averaging,\
						bb_averaging,\
						coef_mean,\
						miss_ass,\
						H_Hertz_corr,\
						C_Hertz_corr,\
						N_Hertz_corr,\
						floor_value1,\
						scale,\
						ceiling_value,\
						rci_method,\
						find_value_mode,\
						no_outliers,\
						power1,\
						scaling1,\
						rci_out_offset,\
						value_search2,\
						full_project_dir,\
						rci_results_dir,\
						gnuplot_program,\
						make_rci_pics,\
						make_rci_images,\
						make_rci_reports,\
						make_secCS_reports,\
						rci_report_residues_dict,\
						gridsearch_dir,\
						residue_numbers_list,\
						aa_names_1let_2_full_all_CAP,\
						verify_dict,\
						shiftclass4gridsearch_list,\
						gridsearch_coeff_list,\
						res4gridsearch_list,\
						res4gridsearch_list_dict,\
						exp_assignment_dict,\
						remove_stereo,\
						do_gridsearch1,\
						files4training,\
						rci_parameter_dict_file,\
						run_rci,\
						forbidden_atoms,\
						coef_only,\
						residue_type_sensitivity,\
						sc_rci_weight,\
						bb_rci_weight,\
						sc_bb_combine,\
						detect_missing_atoms,\
						rc_offset_gridsearch,\
						rc_increment_scale,\
						backbone_random_coil_values,\
						i_plus_minus_two_residue_correction,\
						i_plus_minus_one_residue_correction,\
						backbone_random_coil_values_dict,\
						side_chain_random_coil_values,\
						i_plus_minus_two_residue_correction_dict,\
						i_plus_minus_one_residue_correction_dict,\
						B_Cys_list,\
						find_rci,\
						protein_sequence_list,\
						rc_values,\
						place_dict,\
						strong_shift_limit,\
						total_length_strong_offset,\
						stereo_matching_mode,\
						mean_cs_mode,\
						fill_dict,\
						total_module_text_on,\
						total_report_text_on,\
						module_text_on,\
						report_text_on,\
						per_residue_on,\
						do_pearson,\
						do_spearman,\
						do_kendall,\
						report_dir,\
						outliers,\
						stereospecific_dictionary,\
						rc_max_offset,\
						rcoil_offset_dict,\
						rc_assign_dic,\
						bb_end_correct,\
						sc_end_correct,\
						exclude_atoms,\
						bb_rci_opt,\
						param_overwrite,\
						)





if do_gridsearch1>0:
	cleanup(full_project_dir,gridsearch_dir,project)
	time.sleep(pause)
else:
	if rci_easy==1:
		output_rci(full_project_dir,gridsearch_dir,project,bmrb_file_loc,pic_type,apache_flag,make_pics,files2copy,make_error_pics,rci_output_dir)

######################################################



if verbose2==0: print "End of side-chain RCI"



