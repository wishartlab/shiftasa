dic={}
dic["A"]={}
dic["A"]["H"]=8.24
dic["A"]["HA"]=4.32
dic["A"]["C"]=177.8 
dic["A"]["CA"]=52.5 
dic["A"]["CB"]=19.1
dic["A"]["N"]=125
dic["A"]["HB"]=1.38
dic["A"]["HB2"]=1.38
dic["A"]["HB3"]=1.39

dic["B"]={}
dic["B"]["H"]=8.43
dic["B"]["HA"]=4.71
dic["B"]["C"]=174.6 
dic["B"]["CA"]=55.4 
dic["B"]["CB"]=41.1
dic["B"]["N"]=118.6
dic["B"]["HB2"]=3.02
dic["B"]["HB3"]=2.93

dic["C"]={}
dic["C"]["H"]=8.43
dic["C"]["HA"]=4.71
dic["C"]["C"]=174.6 
dic["C"]["CA"]=58.2 
dic["C"]["CB"]=28.0
dic["C"]["N"]=118.9 
dic["C"]["HB2"]=2.85
dic["C"]["HB3"]=2.99

dic["D"]={}
dic["D"]["H"]=8.34
dic["D"]["HA"]=4.64
dic["D"]["C"]=176.3 
dic["D"]["CA"]=54.2 
dic["D"]["CB"]=41.1
dic["D"]["N"]=121.4
dic["D"]["CG"]=179.79
dic["D"]["HB2"]=2.77
dic["D"]["HB3"]=2.65

dic["E"]={}
dic["E"]["H"]=8.42
dic["E"]["HA"]=4.35
dic["E"]["C"]=176.6 
dic["E"]["CA"]=56.6 
dic["E"]["CB"]=29.9 
dic["E"]["N"]=121.7 
dic["E"]["CD"]=183.15
dic["E"]["CG"]=36.32
dic["E"]["HB2"]=2.06
dic["E"]["HB3"]=1.96
dic["E"]["HG2"]=2.3
dic["E"]["HG3"]=2.25

dic["F"]={}
dic["F"]["H"]=8.3
dic["F"]["HA"]=4.62
dic["F"]["C"]=175.8 
dic["F"]["CA"]=57.7
dic["F"]["CB"]=39.6 
dic["F"]["N"]=120.9 
dic["F"]["CD1"]=131.8
dic["F"]["CD2"]=131.77
dic["F"]["CE1"]=130.5
dic["F"]["CE2"]=130.5
dic["F"]["CG"]=139.63
dic["F"]["CZ"]=129.3
dic["F"]["HB2"]=3.04
dic["F"]["HB3"]=3.04
dic["F"]["HD1"]=7.09
dic["F"]["HD2"]=7.09
dic["F"]["HE1"]=7.15
dic["F"]["HE2"]=7.15
dic["F"]["HZ"]=7.1


dic["G"]={}
dic["G"]["H"]=8.33
dic["G"]["HA"]=3.96
dic["G"]["HA2"]=4.11
dic["G"]["HA3"]=3.65
dic["G"]["C"]=174.9
dic["G"]["CA"]=45.1
dic["G"]["N"]=109.1


dic["H"]={} 
dic["H"]["H"]=8.42
dic["H"]["HA"]=4.73
dic["H"]["C"]=174.1
dic["H"]["CA"]=55
dic["H"]["CB"]=29.0
dic["H"]["N"]=118.2 
dic["H"]["CD2"]=119.38
dic["H"]["CE1"]=136.81
dic["H"]["CG"]=133.68
dic["H"]["HB2"]=3.14
dic["H"]["HB3"]=3.16
dic["H"]["HD2"]=8.58
dic["H"]["HE1"]=7.94


dic["I"]={}
dic["I"]["H"]=8.0
dic["I"]["HA"]=4.17
dic["I"]["C"]=176.4
dic["I"]["CA"]=61.1
dic["I"]["CB"]=38.8
dic["I"]["N"]=121.7 
dic["I"]["CD1"]=13.63
dic["I"]["CG1"]=27.79
dic["I"]["CG2"]=17.69
dic["I"]["HB"]=1.79
dic["I"]["HD1"]=0.69
dic["I"]["HG12"]=1.45
dic["I"]["HG13"]=1.18
dic["I"]["HG2"]=0.82


dic["K"]={}
dic["K"]["H"]=8.29
dic["K"]["HA"]=4.32
dic["K"]["C"]=176.6 
dic["K"]["CA"]=56.2
dic["K"]["CB"]=33.1
dic["K"]["N"]=121.6 
dic["K"]["CD"]=29.11
dic["K"]["CE"]=41.87
dic["K"]["CG"]=24.93
dic["K"]["HB2"]=1.8
dic["K"]["HB3"]=1.75
dic["K"]["HD2"]=1.61
dic["K"]["HD3"]=1.61
dic["K"]["HE2"]=2.92
dic["K"]["HE3"]=2.9
dic["K"]["HG2"]=1.38
dic["K"]["HG3"]=1.36
dic["K"]["HZ"]=7.6

dic["L"]={}
dic["L"]["H"]=8.16
dic["L"]["HA"]=4.34
dic["L"]["C"]=177.6 
dic["L"]["CA"]=55.1
dic["L"]["CB"]=42.4
dic["L"]["N"]=122.6 
dic["L"]["CD1"]=24.97
dic["L"]["CD2"]=24.18
dic["L"]["CG"]=26.87
dic["L"]["HB2"]=1.63
dic["L"]["HB3"]=1.62
dic["L"]["HD1"]=0.77
dic["L"]["HD2"]=0.73
dic["L"]["HG"]=1.5


dic["M"]={}
dic["M"]["H"]=8.28
dic["M"]["HA"]=4.48
dic["M"]["C"]=176.3 
dic["M"]["CA"]=55.4 
dic["M"]["CB"]=32.9 
dic["M"]["N"]=120.7 
dic["M"]["CE"]=17.34
dic["M"]["CG"]=32.33
dic["M"]["HB2"]=2.01
dic["M"]["HB3"]=2.01
dic["M"]["HE"]=1.91
dic["M"]["HG2"]=2.46
dic["M"]["HG3"]=2.36

dic["N"]={}
dic["N"]["H"]=8.4 
dic["N"]["HA"]=4.74
dic["N"]["C"]=175.2 
dic["N"]["CA"]=53.1
dic["N"]["CB"]=38.9
dic["N"]["N"]=119.0 
dic["N"]["CG"]=176.79
dic["N"]["HB2"]=2.88
dic["N"]["HB3"]=2.75
dic["N"]["HD21"]=7.51
dic["N"]["HD22"]=6.91
dic["N"]["ND2"]=112.73 # Average from BMRB

dic["P"]={}
dic["P"]["HA"]=4.42
dic["P"]["C"]=177.3 
dic["P"]["CA"]=63.3
dic["P"]["CB"]=32.1 
dic["P"]["CD"]=50.47
dic["P"]["CG"]=27.29
dic["P"]["HB2"]=2.15
dic["P"]["HB3"]=1.94
dic["P"]["HD2"]=3.67
dic["P"]["HD3"]=3.61
dic["P"]["HG2"]=1.98
dic["P"]["HG3"]=1.9

dic["Q"]={}
dic["Q"]["H"]=8.32
dic["Q"]["HA"]=4.34
dic["Q"]["C"]=176.0
dic["Q"]["CA"]=55.7
dic["Q"]["CB"]=29.4
dic["Q"]["N"]=120.6 
dic["Q"]["CD"]=179.73
dic["Q"]["CG"]=33.97
dic["Q"]["HB2"]=2.08
dic["Q"]["HB3"]=1.99
dic["Q"]["HE21"]=7.31
dic["Q"]["HE22"]=6.91
dic["Q"]["HG2"]=2.34
dic["Q"]["HG3"]=2.3

dic["R"]={}
dic["R"]["H"]=8.23
dic["R"]["HA"]=4.34
dic["R"]["C"]=176.3 
dic["R"]["CA"]=56.0
dic["R"]["CB"]=30.9
dic["R"]["N"]=121.3 
dic["R"]["CD"]=43.29
dic["R"]["CG"]=27.3
dic["R"]["CZ"]=159.69
dic["R"]["HB2"]=1.83
dic["R"]["HB3"]=1.76
dic["R"]["HD2"]=3.15
dic["R"]["HD3"]=3.1
dic["R"]["HE"]=7.51
dic["R"]["HG2"]=1.59
dic["R"]["HG3"]=1.53
#dic["R"]["NE"]=89.71 # Average BMRB value
#dic["R"]["NEave"]=89.71 # Average BMRB value

dic["S"]={}
dic["S"]["H"]=8.31
dic["S"]["HA"]=4.47
dic["S"]["C"]=174.6 
dic["S"]["CA"]=58.3
dic["S"]["CB"]=63.8
dic["S"]["N"]=116.6
dic["S"]["HB2"]=3.9
dic["S"]["HB3"]=3.87

dic["T"]={}
dic["T"]["H"]=8.15
dic["T"]["HA"]=4.35
dic["T"]["C"]=174.7
dic["T"]["CA"]=61.8
dic["T"]["CB"]=69.8 
dic["T"]["N"]=116
dic["T"]["CG2"]=21.59
dic["T"]["HB"]=4.18
dic["T"]["HG2"]=1.16


dic["V"]={}
dic["V"]["H"]=8.03
dic["V"]["HA"]=4.12
dic["V"]["C"]=176.3
dic["V"]["CA"]=62.2
dic["V"]["CB"]=32.9 
dic["V"]["N"]=120.5 
dic["V"]["CG1"]=21.65
dic["V"]["CG2"]=21.4
dic["V"]["HB"]=1.99
dic["V"]["HG1"]=0.86
dic["V"]["HG2"]=0.83


dic["W"]={}
dic["W"]["H"]=8.25
dic["W"]["HA"]=4.66
dic["W"]["C"]=176.1 
dic["W"]["CA"]=57.5
dic["W"]["CB"]=29.6 
dic["W"]["N"]=122.2 
dic["W"]["CD1"]=126.81
dic["W"]["CD2"]=128.63
dic["W"]["CE2"]=137.5
dic["W"]["CE3"]=120.5
dic["W"]["CH2"]=123.8
dic["W"]["CG"]=111.37
dic["W"]["CZ2"]=114.4
dic["W"]["CZ3"]=121.5
dic["W"]["HB2"]=3.24
dic["W"]["HB3"]=3.27
dic["W"]["HD1"]=7.13
dic["W"]["HE1"]=10.09
dic["W"]["HE3"]=7.31
dic["W"]["HH2"]=6.97
dic["W"]["HZ2"]=7.37
dic["W"]["HZ3"]=6.96

dic["Y"]={}
dic["Y"]["H"]=8.12
dic["Y"]["HA"]=4.55
dic["Y"]["C"]=175.9
dic["Y"]["CA"]=57.9
dic["Y"]["CB"]=38.8 
dic["Y"]["N"]=120.8 
dic["Y"]["CD1"]=132.99
dic["Y"]["CD2"]=133.1
dic["Y"]["CE1"]=118.18
dic["Y"]["CE2"]=118.22
dic["Y"]["CG"]=129.85
dic["Y"]["CZ"]=158.3
dic["Y"]["HB2"]=2.93
dic["Y"]["HB3"]=2.98
dic["Y"]["HD1"]=6.97
dic["Y"]["HD2"]=6.98
dic["Y"]["HE1"]=6.72
dic["Y"]["HE2"]=6.72
dic["Y"]["HH"]=8.82



shiftx_rc_dic={}
shiftx_rc_dic["H"]={}; shiftx_rc_dic["H"]["ALA"]= 8.24;  shiftx_rc_dic["H"]["CYS"]=8.32;  shiftx_rc_dic["H"]["ASP"]=8.34;  shiftx_rc_dic["H"]["GLU"]=8.42;  shiftx_rc_dic["H"]["PHE"]=8.3;  shiftx_rc_dic["H"]["GLY"]=8.33;  shiftx_rc_dic["H"]["HIS"]=8.42;  shiftx_rc_dic["H"]["ILE"]=8;  shiftx_rc_dic["H"]["LYS"]=8.29;  shiftx_rc_dic["H"]["LEU"]=8.16;  shiftx_rc_dic["H"]["MET"]=8.28;  shiftx_rc_dic["H"]["ASN"]=8.4;  shiftx_rc_dic["H"]["PRO"]=0;  shiftx_rc_dic["H"]["GLN"]=8.32;  shiftx_rc_dic["H"]["ARG"]=8.23;  shiftx_rc_dic["H"]["SER"]=8.31;  shiftx_rc_dic["H"]["THR"]=8.15;  shiftx_rc_dic["H"]["VAL"]=8.03;  shiftx_rc_dic["H"]["TRP"]=8.25;  shiftx_rc_dic["H"]["TYR"]=8.12
shiftx_rc_dic["NH"]={}; shiftx_rc_dic["NH"]["ALA"]= 8.24;  shiftx_rc_dic["NH"]["CYS"]=8.32;  shiftx_rc_dic["NH"]["ASP"]=8.34;  shiftx_rc_dic["NH"]["GLU"]=8.42;  shiftx_rc_dic["NH"]["PHE"]=8.3;  shiftx_rc_dic["NH"]["GLY"]=8.33;  shiftx_rc_dic["NH"]["HIS"]=8.42;  shiftx_rc_dic["NH"]["ILE"]=8;  shiftx_rc_dic["NH"]["LYS"]=8.29;  shiftx_rc_dic["NH"]["LEU"]=8.16;  shiftx_rc_dic["NH"]["MET"]=8.28;  shiftx_rc_dic["NH"]["ASN"]=8.4;  shiftx_rc_dic["NH"]["PRO"]=0;  shiftx_rc_dic["NH"]["GLN"]=8.32;  shiftx_rc_dic["NH"]["ARG"]=8.23;  shiftx_rc_dic["NH"]["SER"]=8.31;  shiftx_rc_dic["NH"]["THR"]=8.15;  shiftx_rc_dic["NH"]["VAL"]=8.03;  shiftx_rc_dic["NH"]["TRP"]=8.25;  shiftx_rc_dic["NH"]["TYR"]=8.12
shiftx_rc_dic["HA"]={}; shiftx_rc_dic["HA"]["ALA"]= 4.32;  shiftx_rc_dic["HA"]["CYS"]=4.55;  shiftx_rc_dic["HA"]["ASP"]=4.64;  shiftx_rc_dic["HA"]["GLU"]=4.35;  shiftx_rc_dic["HA"]["PHE"]=4.62;  shiftx_rc_dic["HA"]["GLY"]=0;  shiftx_rc_dic["HA"]["HIS"]=4.73;  shiftx_rc_dic["HA"]["ILE"]=4.17;  shiftx_rc_dic["HA"]["LYS"]=4.32;  shiftx_rc_dic["HA"]["LEU"]=4.34;  shiftx_rc_dic["HA"]["MET"]=4.48;  shiftx_rc_dic["HA"]["ASN"]=4.74;  shiftx_rc_dic["HA"]["PRO"]=4.42;  shiftx_rc_dic["HA"]["GLN"]=4.34;  shiftx_rc_dic["HA"]["ARG"]=4.34;  shiftx_rc_dic["HA"]["SER"]=4.47;  shiftx_rc_dic["HA"]["THR"]=4.35;  shiftx_rc_dic["HA"]["VAL"]=4.12;  shiftx_rc_dic["HA"]["TRP"]=4.66;  shiftx_rc_dic["HA"]["TYR"]=4.55
shiftx_rc_dic["HA2"]={}; shiftx_rc_dic["HA2"]["ALA"]=   0;  shiftx_rc_dic["HA2"]["CYS"]=0;  shiftx_rc_dic["HA2"]["ASP"]=0;  shiftx_rc_dic["HA2"]["GLU"]=0;  shiftx_rc_dic["HA2"]["PHE"]=0;  shiftx_rc_dic["HA2"]["GLY"]=3.96;  shiftx_rc_dic["HA2"]["HIS"]=0;  shiftx_rc_dic["HA2"]["ILE"]=0;  shiftx_rc_dic["HA2"]["LYS"]=0;  shiftx_rc_dic["HA2"]["LEU"]=0;  shiftx_rc_dic["HA2"]["MET"]=0;  shiftx_rc_dic["HA2"]["ASN"]=0;  shiftx_rc_dic["HA2"]["PRO"]=0;  shiftx_rc_dic["HA2"]["GLN"]=0;  shiftx_rc_dic["HA2"]["ARG"]=0;  shiftx_rc_dic["HA2"]["SER"]=0;  shiftx_rc_dic["HA2"]["THR"]=0;  shiftx_rc_dic["HA2"]["VAL"]=0;  shiftx_rc_dic["HA2"]["TRP"]=0;  shiftx_rc_dic["HA2"]["TYR"]=0
shiftx_rc_dic["HA3"]={}; shiftx_rc_dic["HA3"]["ALA"]=   0;  shiftx_rc_dic["HA3"]["CYS"]=0;  shiftx_rc_dic["HA3"]["ASP"]=0;  shiftx_rc_dic["HA3"]["GLU"]=0;  shiftx_rc_dic["HA3"]["PHE"]=0;  shiftx_rc_dic["HA3"]["GLY"]=3.96;  shiftx_rc_dic["HA3"]["HIS"]=0;  shiftx_rc_dic["HA3"]["ILE"]=0;  shiftx_rc_dic["HA3"]["LYS"]=0;  shiftx_rc_dic["HA3"]["LEU"]=0;  shiftx_rc_dic["HA3"]["MET"]=0;  shiftx_rc_dic["HA3"]["ASN"]=0;  shiftx_rc_dic["HA3"]["PRO"]=0;  shiftx_rc_dic["HA3"]["GLN"]=0;  shiftx_rc_dic["HA3"]["ARG"]=0;  shiftx_rc_dic["HA3"]["SER"]=0;  shiftx_rc_dic["HA3"]["THR"]=0;  shiftx_rc_dic["HA3"]["VAL"]=0;  shiftx_rc_dic["HA3"]["TRP"]=0;  shiftx_rc_dic["HA3"]["TYR"]=0
shiftx_rc_dic["HB"]={}; shiftx_rc_dic["HB"]["ALA"]=    0;  shiftx_rc_dic["HB"]["CYS"]=0;  shiftx_rc_dic["HB"]["ASP"]=0;  shiftx_rc_dic["HB"]["GLU"]=0;  shiftx_rc_dic["HB"]["PHE"]=0;  shiftx_rc_dic["HB"]["GLY"]=0;  shiftx_rc_dic["HB"]["HIS"]=0;  shiftx_rc_dic["HB"]["ILE"]=1.87;  shiftx_rc_dic["HB"]["LYS"]=0;  shiftx_rc_dic["HB"]["LEU"]=0;  shiftx_rc_dic["HB"]["MET"]=0;  shiftx_rc_dic["HB"]["ASN"]=0;  shiftx_rc_dic["HB"]["PRO"]=0;  shiftx_rc_dic["HB"]["GLN"]=0;  shiftx_rc_dic["HB"]["ARG"]=0;  shiftx_rc_dic["HB"]["SER"]=0;  shiftx_rc_dic["HB"]["THR"]=4.24;  shiftx_rc_dic["HB"]["VAL"]=2.08;  shiftx_rc_dic["HB"]["TRP"]=0;  shiftx_rc_dic["HB"]["TYR"]=0
shiftx_rc_dic["HB1"]={}; shiftx_rc_dic["HB1"]["ALA"]=1.39;  shiftx_rc_dic["HB1"]["CYS"]=0;  shiftx_rc_dic["HB1"]["ASP"]=0;  shiftx_rc_dic["HB1"]["GLU"]=0;  shiftx_rc_dic["HB1"]["PHE"]=0;  shiftx_rc_dic["HB1"]["GLY"]=0;  shiftx_rc_dic["HB1"]["HIS"]=0;  shiftx_rc_dic["HB1"]["ILE"]=0;  shiftx_rc_dic["HB1"]["LYS"]=0;  shiftx_rc_dic["HB1"]["LEU"]=0;  shiftx_rc_dic["HB1"]["MET"]=0;  shiftx_rc_dic["HB1"]["ASN"]=0;  shiftx_rc_dic["HB1"]["PRO"]=0;  shiftx_rc_dic["HB1"]["GLN"]=0;  shiftx_rc_dic["HB1"]["ARG"]=0;  shiftx_rc_dic["HB1"]["SER"]=0;  shiftx_rc_dic["HB1"]["THR"]=0;  shiftx_rc_dic["HB1"]["VAL"]=0;  shiftx_rc_dic["HB1"]["TRP"]=0;  shiftx_rc_dic["HB1"]["TYR"]=0
shiftx_rc_dic["HB2"]={}; shiftx_rc_dic["HB2"]["ALA"]=1.39;  shiftx_rc_dic["HB2"]["CYS"]=2.93;  shiftx_rc_dic["HB2"]["ASP"]=2.72;  shiftx_rc_dic["HB2"]["GLU"]=2.06;  shiftx_rc_dic["HB2"]["PHE"]=3.14;  shiftx_rc_dic["HB2"]["GLY"]=0;  shiftx_rc_dic["HB2"]["HIS"]=3.29;  shiftx_rc_dic["HB2"]["ILE"]=0;  shiftx_rc_dic["HB2"]["LYS"]=1.84;  shiftx_rc_dic["HB2"]["LEU"]=1.62;  shiftx_rc_dic["HB2"]["MET"]=2.11;  shiftx_rc_dic["HB2"]["ASN"]=2.83;  shiftx_rc_dic["HB2"]["PRO"]=2.29;  shiftx_rc_dic["HB2"]["GLN"]=2.12;  shiftx_rc_dic["HB2"]["ARG"]=1.86;  shiftx_rc_dic["HB2"]["SER"]=3.89;  shiftx_rc_dic["HB2"]["THR"]=0;  shiftx_rc_dic["HB2"]["VAL"]=0;  shiftx_rc_dic["HB2"]["TRP"]=3.29;  shiftx_rc_dic["HB2"]["TYR"]=3.03
shiftx_rc_dic["HB3"]={}; shiftx_rc_dic["HB3"]["ALA"]=1.39;  shiftx_rc_dic["HB3"]["CYS"]=2.93;  shiftx_rc_dic["HB3"]["ASP"]=2.65;  shiftx_rc_dic["HB3"]["GLU"]=1.96;  shiftx_rc_dic["HB3"]["PHE"]=3.04;  shiftx_rc_dic["HB3"]["GLY"]=0;  shiftx_rc_dic["HB3"]["HIS"]=3.16;  shiftx_rc_dic["HB3"]["ILE"]=0;  shiftx_rc_dic["HB3"]["LYS"]=1.75;  shiftx_rc_dic["HB3"]["LEU"]=1.62;  shiftx_rc_dic["HB3"]["MET"]=2.01;  shiftx_rc_dic["HB3"]["ASN"]=2.75;  shiftx_rc_dic["HB3"]["PRO"]=1.94;  shiftx_rc_dic["HB3"]["GLN"]=1.99;  shiftx_rc_dic["HB3"]["ARG"]=1.76;  shiftx_rc_dic["HB3"]["SER"]=3.87;  shiftx_rc_dic["HB3"]["THR"]=0;  shiftx_rc_dic["HB3"]["VAL"]=0;  shiftx_rc_dic["HB3"]["TRP"]=3.27;  shiftx_rc_dic["HB3"]["TYR"]=2.98
shiftx_rc_dic["HD1"]={}; shiftx_rc_dic["HD1"]["ALA"]=   0;  shiftx_rc_dic["HD1"]["CYS"]=0;  shiftx_rc_dic["HD1"]["ASP"]=0;  shiftx_rc_dic["HD1"]["GLU"]=0;  shiftx_rc_dic["HD1"]["PHE"]=7.28;  shiftx_rc_dic["HD1"]["GLY"]=0;  shiftx_rc_dic["HD1"]["HIS"]=0;  shiftx_rc_dic["HD1"]["ILE"]=0;  shiftx_rc_dic["HD1"]["LYS"]=0;  shiftx_rc_dic["HD1"]["LEU"]=0;  shiftx_rc_dic["HD1"]["MET"]=0;  shiftx_rc_dic["HD1"]["ASN"]=0;  shiftx_rc_dic["HD1"]["PRO"]=0;  shiftx_rc_dic["HD1"]["GLN"]=0;  shiftx_rc_dic["HD1"]["ARG"]=0;  shiftx_rc_dic["HD1"]["SER"]=0;  shiftx_rc_dic["HD1"]["THR"]=0;  shiftx_rc_dic["HD1"]["VAL"]=0;  shiftx_rc_dic["HD1"]["TRP"]=7.27;  shiftx_rc_dic["HD1"]["TYR"]=7.14
shiftx_rc_dic["HD11"]={}; shiftx_rc_dic["HD11"]["ALA"]=  0;  shiftx_rc_dic["HD11"]["CYS"]=0;  shiftx_rc_dic["HD11"]["ASP"]=0;  shiftx_rc_dic["HD11"]["GLU"]=0;  shiftx_rc_dic["HD11"]["PHE"]=0;  shiftx_rc_dic["HD11"]["GLY"]=0;  shiftx_rc_dic["HD11"]["HIS"]=0;  shiftx_rc_dic["HD11"]["ILE"]=0.86;  shiftx_rc_dic["HD11"]["LYS"]=0;  shiftx_rc_dic["HD11"]["LEU"]=0.92;  shiftx_rc_dic["HD11"]["MET"]=0;  shiftx_rc_dic["HD11"]["ASN"]=0;  shiftx_rc_dic["HD11"]["PRO"]=0;  shiftx_rc_dic["HD11"]["GLN"]=0;  shiftx_rc_dic["HD11"]["ARG"]=0;  shiftx_rc_dic["HD11"]["SER"]=0;  shiftx_rc_dic["HD11"]["THR"]=0;  shiftx_rc_dic["HD11"]["VAL"]=0;  shiftx_rc_dic["HD11"]["TRP"]=0;  shiftx_rc_dic["HD11"]["TYR"]=0
shiftx_rc_dic["HD12"]={}; shiftx_rc_dic["HD12"]["ALA"]=  0;  shiftx_rc_dic["HD12"]["CYS"]=0;  shiftx_rc_dic["HD12"]["ASP"]=0;  shiftx_rc_dic["HD12"]["GLU"]=0;  shiftx_rc_dic["HD12"]["PHE"]=0;  shiftx_rc_dic["HD12"]["GLY"]=0;  shiftx_rc_dic["HD12"]["HIS"]=0;  shiftx_rc_dic["HD12"]["ILE"]=0.86;  shiftx_rc_dic["HD12"]["LYS"]=0;  shiftx_rc_dic["HD12"]["LEU"]=0.92;  shiftx_rc_dic["HD12"]["MET"]=0;  shiftx_rc_dic["HD12"]["ASN"]=0;  shiftx_rc_dic["HD12"]["PRO"]=0;  shiftx_rc_dic["HD12"]["GLN"]=0;  shiftx_rc_dic["HD12"]["ARG"]=0;  shiftx_rc_dic["HD12"]["SER"]=0;  shiftx_rc_dic["HD12"]["THR"]=0;  shiftx_rc_dic["HD12"]["VAL"]=0;  shiftx_rc_dic["HD12"]["TRP"]=0;  shiftx_rc_dic["HD12"]["TYR"]=0
shiftx_rc_dic["HD13"]={}; shiftx_rc_dic["HD13"]["ALA"]=  0;  shiftx_rc_dic["HD13"]["CYS"]=0;  shiftx_rc_dic["HD13"]["ASP"]=0;  shiftx_rc_dic["HD13"]["GLU"]=0;  shiftx_rc_dic["HD13"]["PHE"]=0;  shiftx_rc_dic["HD13"]["GLY"]=0;  shiftx_rc_dic["HD13"]["HIS"]=0;  shiftx_rc_dic["HD13"]["ILE"]=0.86;  shiftx_rc_dic["HD13"]["LYS"]=0;  shiftx_rc_dic["HD13"]["LEU"]=0.92;  shiftx_rc_dic["HD13"]["MET"]=0;  shiftx_rc_dic["HD13"]["ASN"]=0;  shiftx_rc_dic["HD13"]["PRO"]=0;  shiftx_rc_dic["HD13"]["GLN"]=0;  shiftx_rc_dic["HD13"]["ARG"]=0;  shiftx_rc_dic["HD13"]["SER"]=0;  shiftx_rc_dic["HD13"]["THR"]=0;  shiftx_rc_dic["HD13"]["VAL"]=0;  shiftx_rc_dic["HD13"]["TRP"]=0;  shiftx_rc_dic["HD13"]["TYR"]=0
shiftx_rc_dic["HD2"]={}; shiftx_rc_dic["HD2"]["ALA"]=   0;  shiftx_rc_dic["HD2"]["CYS"]=0;  shiftx_rc_dic["HD2"]["ASP"]=0;  shiftx_rc_dic["HD2"]["GLU"]=0;  shiftx_rc_dic["HD2"]["PHE"]=7.28;  shiftx_rc_dic["HD2"]["GLY"]=0;  shiftx_rc_dic["HD2"]["HIS"]=8.58;  shiftx_rc_dic["HD2"]["ILE"]=0;  shiftx_rc_dic["HD2"]["LYS"]=1.68;  shiftx_rc_dic["HD2"]["LEU"]=0;  shiftx_rc_dic["HD2"]["MET"]=0;  shiftx_rc_dic["HD2"]["ASN"]=0;  shiftx_rc_dic["HD2"]["PRO"]=3.63;  shiftx_rc_dic["HD2"]["GLN"]=0;  shiftx_rc_dic["HD2"]["ARG"]=3.2;  shiftx_rc_dic["HD2"]["SER"]=0;  shiftx_rc_dic["HD2"]["THR"]=0;  shiftx_rc_dic["HD2"]["VAL"]=0;  shiftx_rc_dic["HD2"]["TRP"]=0;  shiftx_rc_dic["HD2"]["TYR"]=7.14
shiftx_rc_dic["HD21"]={}; shiftx_rc_dic["HD21"]["ALA"]=  0;  shiftx_rc_dic["HD21"]["CYS"]=0;  shiftx_rc_dic["HD21"]["ASP"]=0;  shiftx_rc_dic["HD21"]["GLU"]=0;  shiftx_rc_dic["HD21"]["PHE"]=0;  shiftx_rc_dic["HD21"]["GLY"]=0;  shiftx_rc_dic["HD21"]["HIS"]=0;  shiftx_rc_dic["HD21"]["ILE"]=0;  shiftx_rc_dic["HD21"]["LYS"]=0;  shiftx_rc_dic["HD21"]["LEU"]=0.87;  shiftx_rc_dic["HD21"]["MET"]=0;  shiftx_rc_dic["HD21"]["ASN"]=7.59;  shiftx_rc_dic["HD21"]["PRO"]=0;  shiftx_rc_dic["HD21"]["GLN"]=0;  shiftx_rc_dic["HD21"]["ARG"]=0;  shiftx_rc_dic["HD21"]["SER"]=0;  shiftx_rc_dic["HD21"]["THR"]=0;  shiftx_rc_dic["HD21"]["VAL"]=0;  shiftx_rc_dic["HD21"]["TRP"]=0;  shiftx_rc_dic["HD21"]["TYR"]=0
shiftx_rc_dic["HD22"]={}; shiftx_rc_dic["HD22"]["ALA"]=  0;  shiftx_rc_dic["HD22"]["CYS"]=0;  shiftx_rc_dic["HD22"]["ASP"]=0;  shiftx_rc_dic["HD22"]["GLU"]=0;  shiftx_rc_dic["HD22"]["PHE"]=0;  shiftx_rc_dic["HD22"]["GLY"]=0;  shiftx_rc_dic["HD22"]["HIS"]=0;  shiftx_rc_dic["HD22"]["ILE"]=0;  shiftx_rc_dic["HD22"]["LYS"]=0;  shiftx_rc_dic["HD22"]["LEU"]=0.87;  shiftx_rc_dic["HD22"]["MET"]=0;  shiftx_rc_dic["HD22"]["ASN"]=6.91;  shiftx_rc_dic["HD22"]["PRO"]=0;  shiftx_rc_dic["HD22"]["GLN"]=0;  shiftx_rc_dic["HD22"]["ARG"]=0;  shiftx_rc_dic["HD22"]["SER"]=0;  shiftx_rc_dic["HD22"]["THR"]=0;  shiftx_rc_dic["HD22"]["VAL"]=0;  shiftx_rc_dic["HD22"]["TRP"]=0;  shiftx_rc_dic["HD22"]["TYR"]=0
shiftx_rc_dic["HD23"]={}; shiftx_rc_dic["HD23"]["ALA"]=  0;  shiftx_rc_dic["HD23"]["CYS"]=0;  shiftx_rc_dic["HD23"]["ASP"]=0;  shiftx_rc_dic["HD23"]["GLU"]=0;  shiftx_rc_dic["HD23"]["PHE"]=0;  shiftx_rc_dic["HD23"]["GLY"]=0;  shiftx_rc_dic["HD23"]["HIS"]=0;  shiftx_rc_dic["HD23"]["ILE"]=0;  shiftx_rc_dic["HD23"]["LYS"]=0;  shiftx_rc_dic["HD23"]["LEU"]=0.87;  shiftx_rc_dic["HD23"]["MET"]=0;  shiftx_rc_dic["HD23"]["ASN"]=0;  shiftx_rc_dic["HD23"]["PRO"]=0;  shiftx_rc_dic["HD23"]["GLN"]=0;  shiftx_rc_dic["HD23"]["ARG"]=0;  shiftx_rc_dic["HD23"]["SER"]=0;  shiftx_rc_dic["HD23"]["THR"]=0;  shiftx_rc_dic["HD23"]["VAL"]=0;  shiftx_rc_dic["HD23"]["TRP"]=0;  shiftx_rc_dic["HD23"]["TYR"]=0
shiftx_rc_dic["HD3"]={}; shiftx_rc_dic["HD3"]["ALA"]=   0;  shiftx_rc_dic["HD3"]["CYS"]=0;  shiftx_rc_dic["HD3"]["ASP"]=0;  shiftx_rc_dic["HD3"]["GLU"]=0;  shiftx_rc_dic["HD3"]["PHE"]=0;  shiftx_rc_dic["HD3"]["GLY"]=0;  shiftx_rc_dic["HD3"]["HIS"]=0;  shiftx_rc_dic["HD3"]["ILE"]=0;  shiftx_rc_dic["HD3"]["LYS"]=1.68;  shiftx_rc_dic["HD3"]["LEU"]=0;  shiftx_rc_dic["HD3"]["MET"]=0;  shiftx_rc_dic["HD3"]["ASN"]=0;  shiftx_rc_dic["HD3"]["PRO"]=3.63;  shiftx_rc_dic["HD3"]["GLN"]=0;  shiftx_rc_dic["HD3"]["ARG"]=3.2;  shiftx_rc_dic["HD3"]["SER"]=0;  shiftx_rc_dic["HD3"]["THR"]=0;  shiftx_rc_dic["HD3"]["VAL"]=0;  shiftx_rc_dic["HD3"]["TRP"]=0;  shiftx_rc_dic["HD3"]["TYR"]=0
shiftx_rc_dic["HE"]={}; shiftx_rc_dic["HE"]["ALA"]=    0;  shiftx_rc_dic["HE"]["CYS"]=0;  shiftx_rc_dic["HE"]["ASP"]=0;  shiftx_rc_dic["HE"]["GLU"]=0;  shiftx_rc_dic["HE"]["PHE"]=0;  shiftx_rc_dic["HE"]["GLY"]=0;  shiftx_rc_dic["HE"]["HIS"]=0;  shiftx_rc_dic["HE"]["ILE"]=0;  shiftx_rc_dic["HE"]["LYS"]=0;  shiftx_rc_dic["HE"]["LEU"]=0;  shiftx_rc_dic["HE"]["MET"]=0;  shiftx_rc_dic["HE"]["ASN"]=0;  shiftx_rc_dic["HE"]["PRO"]=0;  shiftx_rc_dic["HE"]["GLN"]=0;  shiftx_rc_dic["HE"]["ARG"]=8.07;  shiftx_rc_dic["HE"]["SER"]=0;  shiftx_rc_dic["HE"]["THR"]=0;  shiftx_rc_dic["HE"]["VAL"]=0;  shiftx_rc_dic["HE"]["TRP"]=0;  shiftx_rc_dic["HE"]["TYR"]=0
shiftx_rc_dic["HE1"]={}; shiftx_rc_dic["HE1"]["ALA"]=   0;  shiftx_rc_dic["HE1"]["CYS"]=0;  shiftx_rc_dic["HE1"]["ASP"]=0;  shiftx_rc_dic["HE1"]["GLU"]=0;  shiftx_rc_dic["HE1"]["PHE"]=7.38;  shiftx_rc_dic["HE1"]["GLY"]=0;  shiftx_rc_dic["HE1"]["HIS"]=7.29;  shiftx_rc_dic["HE1"]["ILE"]=0;  shiftx_rc_dic["HE1"]["LYS"]=0;  shiftx_rc_dic["HE1"]["LEU"]=0;  shiftx_rc_dic["HE1"]["MET"]=2.1;  shiftx_rc_dic["HE1"]["ASN"]=0;  shiftx_rc_dic["HE1"]["PRO"]=0;  shiftx_rc_dic["HE1"]["GLN"]=0;  shiftx_rc_dic["HE1"]["ARG"]=0;  shiftx_rc_dic["HE1"]["SER"]=0;  shiftx_rc_dic["HE1"]["THR"]=0;  shiftx_rc_dic["HE1"]["VAL"]=0;  shiftx_rc_dic["HE1"]["TRP"]=10.09;  shiftx_rc_dic["HE1"]["TYR"]=6.84
shiftx_rc_dic["HE2"]={}; shiftx_rc_dic["HE2"]["ALA"]=   0;  shiftx_rc_dic["HE2"]["CYS"]=0;  shiftx_rc_dic["HE2"]["ASP"]=0;  shiftx_rc_dic["HE2"]["GLU"]=0;  shiftx_rc_dic["HE2"]["PHE"]=7.38;  shiftx_rc_dic["HE2"]["GLY"]=0;  shiftx_rc_dic["HE2"]["HIS"]=0;  shiftx_rc_dic["HE2"]["ILE"]=0;  shiftx_rc_dic["HE2"]["LYS"]=2.99;  shiftx_rc_dic["HE2"]["LEU"]=0;  shiftx_rc_dic["HE2"]["MET"]=2.1;  shiftx_rc_dic["HE2"]["ASN"]=0;  shiftx_rc_dic["HE2"]["PRO"]=0;  shiftx_rc_dic["HE2"]["GLN"]=0;  shiftx_rc_dic["HE2"]["ARG"]=0;  shiftx_rc_dic["HE2"]["SER"]=0;  shiftx_rc_dic["HE2"]["THR"]=0;  shiftx_rc_dic["HE2"]["VAL"]=0;  shiftx_rc_dic["HE2"]["TRP"]=0;  shiftx_rc_dic["HE2"]["TYR"]=6.84
shiftx_rc_dic["HE21"]={}; shiftx_rc_dic["HE21"]["ALA"]=  0;  shiftx_rc_dic["HE21"]["CYS"]=0;  shiftx_rc_dic["HE21"]["ASP"]=0;  shiftx_rc_dic["HE21"]["GLU"]=0;  shiftx_rc_dic["HE21"]["PHE"]=0;  shiftx_rc_dic["HE21"]["GLY"]=0;  shiftx_rc_dic["HE21"]["HIS"]=0;  shiftx_rc_dic["HE21"]["ILE"]=0;  shiftx_rc_dic["HE21"]["LYS"]=0;  shiftx_rc_dic["HE21"]["LEU"]=0;  shiftx_rc_dic["HE21"]["MET"]=0;  shiftx_rc_dic["HE21"]["ASN"]=0;  shiftx_rc_dic["HE21"]["PRO"]=0;  shiftx_rc_dic["HE21"]["GLN"]=7.52;  shiftx_rc_dic["HE21"]["ARG"]=0;  shiftx_rc_dic["HE21"]["SER"]=0;  shiftx_rc_dic["HE21"]["THR"]=0;  shiftx_rc_dic["HE21"]["VAL"]=0;  shiftx_rc_dic["HE21"]["TRP"]=0;  shiftx_rc_dic["HE21"]["TYR"]=0
shiftx_rc_dic["HE22"]={}; shiftx_rc_dic["HE22"]["ALA"]=  0;  shiftx_rc_dic["HE22"]["CYS"]=0;  shiftx_rc_dic["HE22"]["ASP"]=0;  shiftx_rc_dic["HE22"]["GLU"]=0;  shiftx_rc_dic["HE22"]["PHE"]=0;  shiftx_rc_dic["HE22"]["GLY"]=0;  shiftx_rc_dic["HE22"]["HIS"]=0;  shiftx_rc_dic["HE22"]["ILE"]=0;  shiftx_rc_dic["HE22"]["LYS"]=0;  shiftx_rc_dic["HE22"]["LEU"]=0;  shiftx_rc_dic["HE22"]["MET"]=0;  shiftx_rc_dic["HE22"]["ASN"]=0;  shiftx_rc_dic["HE22"]["PRO"]=0;  shiftx_rc_dic["HE22"]["GLN"]=6.85;  shiftx_rc_dic["HE22"]["ARG"]=0;  shiftx_rc_dic["HE22"]["SER"]=0;  shiftx_rc_dic["HE22"]["THR"]=0;  shiftx_rc_dic["HE22"]["VAL"]=0;  shiftx_rc_dic["HE22"]["TRP"]=0;  shiftx_rc_dic["HE22"]["TYR"]=0
shiftx_rc_dic["HE3"]={}; shiftx_rc_dic["HE3"]["ALA"]=   0;  shiftx_rc_dic["HE3"]["CYS"]=0;  shiftx_rc_dic["HE3"]["ASP"]=0;  shiftx_rc_dic["HE3"]["GLU"]=0;  shiftx_rc_dic["HE3"]["PHE"]=0;  shiftx_rc_dic["HE3"]["GLY"]=0;  shiftx_rc_dic["HE3"]["HIS"]=0;  shiftx_rc_dic["HE3"]["ILE"]=0;  shiftx_rc_dic["HE3"]["LYS"]=2.99;  shiftx_rc_dic["HE3"]["LEU"]=0;  shiftx_rc_dic["HE3"]["MET"]=2.1;  shiftx_rc_dic["HE3"]["ASN"]=0;  shiftx_rc_dic["HE3"]["PRO"]=0;  shiftx_rc_dic["HE3"]["GLN"]=0;  shiftx_rc_dic["HE3"]["ARG"]=0;  shiftx_rc_dic["HE3"]["SER"]=0;  shiftx_rc_dic["HE3"]["THR"]=0;  shiftx_rc_dic["HE3"]["VAL"]=0;  shiftx_rc_dic["HE3"]["TRP"]=7.5;  shiftx_rc_dic["HE3"]["TYR"]=0
shiftx_rc_dic["HG"]={}; shiftx_rc_dic["HG"]["ALA"]=    0;  shiftx_rc_dic["HG"]["CYS"]=0;  shiftx_rc_dic["HG"]["ASP"]=0;  shiftx_rc_dic["HG"]["GLU"]=0;  shiftx_rc_dic["HG"]["PHE"]=0;  shiftx_rc_dic["HG"]["GLY"]=0;  shiftx_rc_dic["HG"]["HIS"]=0;  shiftx_rc_dic["HG"]["ILE"]=0;  shiftx_rc_dic["HG"]["LYS"]=0;  shiftx_rc_dic["HG"]["LEU"]=1.59;  shiftx_rc_dic["HG"]["MET"]=0;  shiftx_rc_dic["HG"]["ASN"]=0;  shiftx_rc_dic["HG"]["PRO"]=0;  shiftx_rc_dic["HG"]["GLN"]=0;  shiftx_rc_dic["HG"]["ARG"]=0;  shiftx_rc_dic["HG"]["SER"]=0;  shiftx_rc_dic["HG"]["THR"]=0;  shiftx_rc_dic["HG"]["VAL"]=0;  shiftx_rc_dic["HG"]["TRP"]=0;  shiftx_rc_dic["HG"]["TYR"]=0
shiftx_rc_dic["HG1"]={}; shiftx_rc_dic["HG1"]["ALA"]=   0;  shiftx_rc_dic["HG1"]["CYS"]=0;  shiftx_rc_dic["HG1"]["ASP"]=0;  shiftx_rc_dic["HG1"]["GLU"]=0;  shiftx_rc_dic["HG1"]["PHE"]=0;  shiftx_rc_dic["HG1"]["GLY"]=0;  shiftx_rc_dic["HG1"]["HIS"]=0;  shiftx_rc_dic["HG1"]["ILE"]=0;  shiftx_rc_dic["HG1"]["LYS"]=0;  shiftx_rc_dic["HG1"]["LEU"]=0;  shiftx_rc_dic["HG1"]["MET"]=0;  shiftx_rc_dic["HG1"]["ASN"]=0;  shiftx_rc_dic["HG1"]["PRO"]=0;  shiftx_rc_dic["HG1"]["GLN"]=0;  shiftx_rc_dic["HG1"]["ARG"]=0;  shiftx_rc_dic["HG1"]["SER"]=0;  shiftx_rc_dic["HG1"]["THR"]=0;  shiftx_rc_dic["HG1"]["VAL"]=0;  shiftx_rc_dic["HG1"]["TRP"]=0;  shiftx_rc_dic["HG1"]["TYR"]=0
shiftx_rc_dic["HG11"]={}; shiftx_rc_dic["HG11"]["ALA"]=  0;  shiftx_rc_dic["HG11"]["CYS"]=0;  shiftx_rc_dic["HG11"]["ASP"]=0;  shiftx_rc_dic["HG11"]["GLU"]=0;  shiftx_rc_dic["HG11"]["PHE"]=0;  shiftx_rc_dic["HG11"]["GLY"]=0;  shiftx_rc_dic["HG11"]["HIS"]=0;  shiftx_rc_dic["HG11"]["ILE"]=0;  shiftx_rc_dic["HG11"]["LYS"]=0;  shiftx_rc_dic["HG11"]["LEU"]=0;  shiftx_rc_dic["HG11"]["MET"]=0;  shiftx_rc_dic["HG11"]["ASN"]=0;  shiftx_rc_dic["HG11"]["PRO"]=0;  shiftx_rc_dic["HG11"]["GLN"]=0;  shiftx_rc_dic["HG11"]["ARG"]=0;  shiftx_rc_dic["HG11"]["SER"]=0;  shiftx_rc_dic["HG11"]["THR"]=0;  shiftx_rc_dic["HG11"]["VAL"]=0.94;  shiftx_rc_dic["HG11"]["TRP"]=0;  shiftx_rc_dic["HG11"]["TYR"]=0
shiftx_rc_dic["HG12"]={}; shiftx_rc_dic["HG12"]["ALA"]=  0;  shiftx_rc_dic["HG12"]["CYS"]=0;  shiftx_rc_dic["HG12"]["ASP"]=0;  shiftx_rc_dic["HG12"]["GLU"]=0;  shiftx_rc_dic["HG12"]["PHE"]=0;  shiftx_rc_dic["HG12"]["GLY"]=0;  shiftx_rc_dic["HG12"]["HIS"]=0;  shiftx_rc_dic["HG12"]["ILE"]=1.45;  shiftx_rc_dic["HG12"]["LYS"]=0;  shiftx_rc_dic["HG12"]["LEU"]=0;  shiftx_rc_dic["HG12"]["MET"]=0;  shiftx_rc_dic["HG12"]["ASN"]=0;  shiftx_rc_dic["HG12"]["PRO"]=0;  shiftx_rc_dic["HG12"]["GLN"]=0;  shiftx_rc_dic["HG12"]["ARG"]=0;  shiftx_rc_dic["HG12"]["SER"]=0;  shiftx_rc_dic["HG12"]["THR"]=0;  shiftx_rc_dic["HG12"]["VAL"]=0.94;  shiftx_rc_dic["HG12"]["TRP"]=0;  shiftx_rc_dic["HG12"]["TYR"]=0
shiftx_rc_dic["HG13"]={}; shiftx_rc_dic["HG13"]["ALA"]=  0;  shiftx_rc_dic["HG13"]["CYS"]=0;  shiftx_rc_dic["HG13"]["ASP"]=0;  shiftx_rc_dic["HG13"]["GLU"]=0;  shiftx_rc_dic["HG13"]["PHE"]=0;  shiftx_rc_dic["HG13"]["GLY"]=0;  shiftx_rc_dic["HG13"]["HIS"]=0;  shiftx_rc_dic["HG13"]["ILE"]=1.16;  shiftx_rc_dic["HG13"]["LYS"]=0;  shiftx_rc_dic["HG13"]["LEU"]=0;  shiftx_rc_dic["HG13"]["MET"]=0;  shiftx_rc_dic["HG13"]["ASN"]=0;  shiftx_rc_dic["HG13"]["PRO"]=0;  shiftx_rc_dic["HG13"]["GLN"]=0;  shiftx_rc_dic["HG13"]["ARG"]=0;  shiftx_rc_dic["HG13"]["SER"]=0;  shiftx_rc_dic["HG13"]["THR"]=0;  shiftx_rc_dic["HG13"]["VAL"]=0.94;  shiftx_rc_dic["HG13"]["TRP"]=0;  shiftx_rc_dic["HG13"]["TYR"]=0
shiftx_rc_dic["HG2"]={}; shiftx_rc_dic["HG2"]["ALA"]=   0;  shiftx_rc_dic["HG2"]["CYS"]=0;  shiftx_rc_dic["HG2"]["ASP"]=0;  shiftx_rc_dic["HG2"]["GLU"]=2.31;  shiftx_rc_dic["HG2"]["PHE"]=0;  shiftx_rc_dic["HG2"]["GLY"]=0;  shiftx_rc_dic["HG2"]["HIS"]=0;  shiftx_rc_dic["HG2"]["ILE"]=0;  shiftx_rc_dic["HG2"]["LYS"]=1.44;  shiftx_rc_dic["HG2"]["LEU"]=0;  shiftx_rc_dic["HG2"]["MET"]=2.6;  shiftx_rc_dic["HG2"]["ASN"]=0;  shiftx_rc_dic["HG2"]["PRO"]=2.02;  shiftx_rc_dic["HG2"]["GLN"]=2.36;  shiftx_rc_dic["HG2"]["ARG"]=1.63;  shiftx_rc_dic["HG2"]["SER"]=0;  shiftx_rc_dic["HG2"]["THR"]=0;  shiftx_rc_dic["HG2"]["VAL"]=0;  shiftx_rc_dic["HG2"]["TRP"]=0;  shiftx_rc_dic["HG2"]["TYR"]=0
shiftx_rc_dic["HG21"]={}; shiftx_rc_dic["HG21"]["ALA"]=  0;  shiftx_rc_dic["HG21"]["CYS"]=0;  shiftx_rc_dic["HG21"]["ASP"]=0;  shiftx_rc_dic["HG21"]["GLU"]=0;  shiftx_rc_dic["HG21"]["PHE"]=0;  shiftx_rc_dic["HG21"]["GLY"]=0;  shiftx_rc_dic["HG21"]["HIS"]=0;  shiftx_rc_dic["HG21"]["ILE"]=0.91;  shiftx_rc_dic["HG21"]["LYS"]=0;  shiftx_rc_dic["HG21"]["LEU"]=0;  shiftx_rc_dic["HG21"]["MET"]=0;  shiftx_rc_dic["HG21"]["ASN"]=0;  shiftx_rc_dic["HG21"]["PRO"]=0;  shiftx_rc_dic["HG21"]["GLN"]=0;  shiftx_rc_dic["HG21"]["ARG"]=0;  shiftx_rc_dic["HG21"]["SER"]=0;  shiftx_rc_dic["HG21"]["THR"]=1.21;  shiftx_rc_dic["HG21"]["VAL"]=0.93;  shiftx_rc_dic["HG21"]["TRP"]=0;  shiftx_rc_dic["HG21"]["TYR"]=0
shiftx_rc_dic["HG22"]={}; shiftx_rc_dic["HG22"]["ALA"]=  0;  shiftx_rc_dic["HG22"]["CYS"]=0;  shiftx_rc_dic["HG22"]["ASP"]=0;  shiftx_rc_dic["HG22"]["GLU"]=0;  shiftx_rc_dic["HG22"]["PHE"]=0;  shiftx_rc_dic["HG22"]["GLY"]=0;  shiftx_rc_dic["HG22"]["HIS"]=0;  shiftx_rc_dic["HG22"]["ILE"]=0.91;  shiftx_rc_dic["HG22"]["LYS"]=0;  shiftx_rc_dic["HG22"]["LEU"]=0;  shiftx_rc_dic["HG22"]["MET"]=0;  shiftx_rc_dic["HG22"]["ASN"]=0;  shiftx_rc_dic["HG22"]["PRO"]=0;  shiftx_rc_dic["HG22"]["GLN"]=0;  shiftx_rc_dic["HG22"]["ARG"]=0;  shiftx_rc_dic["HG22"]["SER"]=0;  shiftx_rc_dic["HG22"]["THR"]=1.21;  shiftx_rc_dic["HG22"]["VAL"]=0.93;  shiftx_rc_dic["HG22"]["TRP"]=0;  shiftx_rc_dic["HG22"]["TYR"]=0
shiftx_rc_dic["HG23"]={}; shiftx_rc_dic["HG23"]["ALA"]=  0;  shiftx_rc_dic["HG23"]["CYS"]=0;  shiftx_rc_dic["HG23"]["ASP"]=0;  shiftx_rc_dic["HG23"]["GLU"]=0;  shiftx_rc_dic["HG23"]["PHE"]=0;  shiftx_rc_dic["HG23"]["GLY"]=0;  shiftx_rc_dic["HG23"]["HIS"]=0;  shiftx_rc_dic["HG23"]["ILE"]=0.91;  shiftx_rc_dic["HG23"]["LYS"]=0;  shiftx_rc_dic["HG23"]["LEU"]=0;  shiftx_rc_dic["HG23"]["MET"]=0;  shiftx_rc_dic["HG23"]["ASN"]=0;  shiftx_rc_dic["HG23"]["PRO"]=0;  shiftx_rc_dic["HG23"]["GLN"]=0;  shiftx_rc_dic["HG23"]["ARG"]=0;  shiftx_rc_dic["HG23"]["SER"]=0;  shiftx_rc_dic["HG23"]["THR"]=1.21;  shiftx_rc_dic["HG23"]["VAL"]=0.93;  shiftx_rc_dic["HG23"]["TRP"]=0;  shiftx_rc_dic["HG23"]["TYR"]=0
shiftx_rc_dic["HG3"]={}; shiftx_rc_dic["HG3"]["ALA"]=   0;  shiftx_rc_dic["HG3"]["CYS"]=0;  shiftx_rc_dic["HG3"]["ASP"]=0;  shiftx_rc_dic["HG3"]["GLU"]=2.31;  shiftx_rc_dic["HG3"]["PHE"]=0;  shiftx_rc_dic["HG3"]["GLY"]=0;  shiftx_rc_dic["HG3"]["HIS"]=0;  shiftx_rc_dic["HG3"]["ILE"]=0;  shiftx_rc_dic["HG3"]["LYS"]=1.44;  shiftx_rc_dic["HG3"]["LEU"]=0;  shiftx_rc_dic["HG3"]["MET"]=2.54;  shiftx_rc_dic["HG3"]["ASN"]=0;  shiftx_rc_dic["HG3"]["PRO"]=2.02;  shiftx_rc_dic["HG3"]["GLN"]=2.36;  shiftx_rc_dic["HG3"]["ARG"]=1.63;  shiftx_rc_dic["HG3"]["SER"]=0;  shiftx_rc_dic["HG3"]["THR"]=0;  shiftx_rc_dic["HG3"]["VAL"]=0;  shiftx_rc_dic["HG3"]["TRP"]=0;  shiftx_rc_dic["HG3"]["TYR"]=0
shiftx_rc_dic["HH"]={}; shiftx_rc_dic["HH"]["ALA"]=    0;  shiftx_rc_dic["HH"]["CYS"]=0;  shiftx_rc_dic["HH"]["ASP"]=0;  shiftx_rc_dic["HH"]["GLU"]=0;  shiftx_rc_dic["HH"]["PHE"]=0;  shiftx_rc_dic["HH"]["GLY"]=0;  shiftx_rc_dic["HH"]["HIS"]=0;  shiftx_rc_dic["HH"]["ILE"]=0;  shiftx_rc_dic["HH"]["LYS"]=0;  shiftx_rc_dic["HH"]["LEU"]=0;  shiftx_rc_dic["HH"]["MET"]=0;  shiftx_rc_dic["HH"]["ASN"]=0;  shiftx_rc_dic["HH"]["PRO"]=0;  shiftx_rc_dic["HH"]["GLN"]=0;  shiftx_rc_dic["HH"]["ARG"]=0;  shiftx_rc_dic["HH"]["SER"]=0;  shiftx_rc_dic["HH"]["THR"]=0;  shiftx_rc_dic["HH"]["VAL"]=0;  shiftx_rc_dic["HH"]["TRP"]=0;  shiftx_rc_dic["HH"]["TYR"]=0
shiftx_rc_dic["HH11"]={}; shiftx_rc_dic["HH11"]["ALA"]=  0;  shiftx_rc_dic["HH11"]["CYS"]=0;  shiftx_rc_dic["HH11"]["ASP"]=0;  shiftx_rc_dic["HH11"]["GLU"]=0;  shiftx_rc_dic["HH11"]["PHE"]=0;  shiftx_rc_dic["HH11"]["GLY"]=0;  shiftx_rc_dic["HH11"]["HIS"]=0;  shiftx_rc_dic["HH11"]["ILE"]=0;  shiftx_rc_dic["HH11"]["LYS"]=0;  shiftx_rc_dic["HH11"]["LEU"]=0;  shiftx_rc_dic["HH11"]["MET"]=0;  shiftx_rc_dic["HH11"]["ASN"]=0;  shiftx_rc_dic["HH11"]["PRO"]=0;  shiftx_rc_dic["HH11"]["GLN"]=0;  shiftx_rc_dic["HH11"]["ARG"]=0;  shiftx_rc_dic["HH11"]["SER"]=0;  shiftx_rc_dic["HH11"]["THR"]=0;  shiftx_rc_dic["HH11"]["VAL"]=0;  shiftx_rc_dic["HH11"]["TRP"]=0;  shiftx_rc_dic["HH11"]["TYR"]=0
shiftx_rc_dic["HH12"]={}; shiftx_rc_dic["HH12"]["ALA"]=  0;  shiftx_rc_dic["HH12"]["CYS"]=0;  shiftx_rc_dic["HH12"]["ASP"]=0;  shiftx_rc_dic["HH12"]["GLU"]=0;  shiftx_rc_dic["HH12"]["PHE"]=0;  shiftx_rc_dic["HH12"]["GLY"]=0;  shiftx_rc_dic["HH12"]["HIS"]=0;  shiftx_rc_dic["HH12"]["ILE"]=0;  shiftx_rc_dic["HH12"]["LYS"]=0;  shiftx_rc_dic["HH12"]["LEU"]=0;  shiftx_rc_dic["HH12"]["MET"]=0;  shiftx_rc_dic["HH12"]["ASN"]=0;  shiftx_rc_dic["HH12"]["PRO"]=0;  shiftx_rc_dic["HH12"]["GLN"]=0;  shiftx_rc_dic["HH12"]["ARG"]=0;  shiftx_rc_dic["HH12"]["SER"]=0;  shiftx_rc_dic["HH12"]["THR"]=0;  shiftx_rc_dic["HH12"]["VAL"]=0;  shiftx_rc_dic["HH12"]["TRP"]=0;  shiftx_rc_dic["HH12"]["TYR"]=0
shiftx_rc_dic["HH2"]={}; shiftx_rc_dic["HH2"]["ALA"]=   0;  shiftx_rc_dic["HH2"]["CYS"]=0;  shiftx_rc_dic["HH2"]["ASP"]=0;  shiftx_rc_dic["HH2"]["GLU"]=0;  shiftx_rc_dic["HH2"]["PHE"]=0;  shiftx_rc_dic["HH2"]["GLY"]=0;  shiftx_rc_dic["HH2"]["HIS"]=0;  shiftx_rc_dic["HH2"]["ILE"]=0;  shiftx_rc_dic["HH2"]["LYS"]=0;  shiftx_rc_dic["HH2"]["LEU"]=0;  shiftx_rc_dic["HH2"]["MET"]=0;  shiftx_rc_dic["HH2"]["ASN"]=0;  shiftx_rc_dic["HH2"]["PRO"]=0;  shiftx_rc_dic["HH2"]["GLN"]=0;  shiftx_rc_dic["HH2"]["ARG"]=0;  shiftx_rc_dic["HH2"]["SER"]=0;  shiftx_rc_dic["HH2"]["THR"]=0;  shiftx_rc_dic["HH2"]["VAL"]=0;  shiftx_rc_dic["HH2"]["TRP"]=7.18;  shiftx_rc_dic["HH2"]["TYR"]=0
shiftx_rc_dic["HH21"]={}; shiftx_rc_dic["HH21"]["ALA"]=  0;  shiftx_rc_dic["HH21"]["CYS"]=0;  shiftx_rc_dic["HH21"]["ASP"]=0;  shiftx_rc_dic["HH21"]["GLU"]=0;  shiftx_rc_dic["HH21"]["PHE"]=0;  shiftx_rc_dic["HH21"]["GLY"]=0;  shiftx_rc_dic["HH21"]["HIS"]=0;  shiftx_rc_dic["HH21"]["ILE"]=0;  shiftx_rc_dic["HH21"]["LYS"]=0;  shiftx_rc_dic["HH21"]["LEU"]=0;  shiftx_rc_dic["HH21"]["MET"]=0;  shiftx_rc_dic["HH21"]["ASN"]=0;  shiftx_rc_dic["HH21"]["PRO"]=0;  shiftx_rc_dic["HH21"]["GLN"]=0;  shiftx_rc_dic["HH21"]["ARG"]=0;  shiftx_rc_dic["HH21"]["SER"]=0;  shiftx_rc_dic["HH21"]["THR"]=0;  shiftx_rc_dic["HH21"]["VAL"]=0;  shiftx_rc_dic["HH21"]["TRP"]=0;  shiftx_rc_dic["HH21"]["TYR"]=0
shiftx_rc_dic["HH22"]={}; shiftx_rc_dic["HH22"]["ALA"]=  0;  shiftx_rc_dic["HH22"]["CYS"]=0;  shiftx_rc_dic["HH22"]["ASP"]=0;  shiftx_rc_dic["HH22"]["GLU"]=0;  shiftx_rc_dic["HH22"]["PHE"]=0;  shiftx_rc_dic["HH22"]["GLY"]=0;  shiftx_rc_dic["HH22"]["HIS"]=0;  shiftx_rc_dic["HH22"]["ILE"]=0;  shiftx_rc_dic["HH22"]["LYS"]=0;  shiftx_rc_dic["HH22"]["LEU"]=0;  shiftx_rc_dic["HH22"]["MET"]=0;  shiftx_rc_dic["HH22"]["ASN"]=0;  shiftx_rc_dic["HH22"]["PRO"]=0;  shiftx_rc_dic["HH22"]["GLN"]=0;  shiftx_rc_dic["HH22"]["ARG"]=0;  shiftx_rc_dic["HH22"]["SER"]=0;  shiftx_rc_dic["HH22"]["THR"]=0;  shiftx_rc_dic["HH22"]["VAL"]=0;  shiftx_rc_dic["HH22"]["TRP"]=0;  shiftx_rc_dic["HH22"]["TYR"]=0
shiftx_rc_dic["HZ"]={}; shiftx_rc_dic["HZ"]["ALA"]=    0;  shiftx_rc_dic["HZ"]["CYS"]=0;  shiftx_rc_dic["HZ"]["ASP"]=0;  shiftx_rc_dic["HZ"]["GLU"]=0;  shiftx_rc_dic["HZ"]["PHE"]=7.32;  shiftx_rc_dic["HZ"]["GLY"]=0;  shiftx_rc_dic["HZ"]["HIS"]=0;  shiftx_rc_dic["HZ"]["ILE"]=0;  shiftx_rc_dic["HZ"]["LYS"]=0;  shiftx_rc_dic["HZ"]["LEU"]=0;  shiftx_rc_dic["HZ"]["MET"]=0;  shiftx_rc_dic["HZ"]["ASN"]=0;  shiftx_rc_dic["HZ"]["PRO"]=0;  shiftx_rc_dic["HZ"]["GLN"]=0;  shiftx_rc_dic["HZ"]["ARG"]=0;  shiftx_rc_dic["HZ"]["SER"]=0;  shiftx_rc_dic["HZ"]["THR"]=0;  shiftx_rc_dic["HZ"]["VAL"]=0;  shiftx_rc_dic["HZ"]["TRP"]=0;  shiftx_rc_dic["HZ"]["TYR"]=0
shiftx_rc_dic["HZ1"]={}; shiftx_rc_dic["HZ1"]["ALA"]=   0;  shiftx_rc_dic["HZ1"]["CYS"]=0;  shiftx_rc_dic["HZ1"]["ASP"]=0;  shiftx_rc_dic["HZ1"]["GLU"]=0;  shiftx_rc_dic["HZ1"]["PHE"]=0;  shiftx_rc_dic["HZ1"]["GLY"]=0;  shiftx_rc_dic["HZ1"]["HIS"]=0;  shiftx_rc_dic["HZ1"]["ILE"]=0;  shiftx_rc_dic["HZ1"]["LYS"]=7.81;  shiftx_rc_dic["HZ1"]["LEU"]=0;  shiftx_rc_dic["HZ1"]["MET"]=0;  shiftx_rc_dic["HZ1"]["ASN"]=0;  shiftx_rc_dic["HZ1"]["PRO"]=0;  shiftx_rc_dic["HZ1"]["GLN"]=0;  shiftx_rc_dic["HZ1"]["ARG"]=0;  shiftx_rc_dic["HZ1"]["SER"]=0;  shiftx_rc_dic["HZ1"]["THR"]=0;  shiftx_rc_dic["HZ1"]["VAL"]=0;  shiftx_rc_dic["HZ1"]["TRP"]=0;  shiftx_rc_dic["HZ1"]["TYR"]=0
shiftx_rc_dic["HZ2"]={}; shiftx_rc_dic["HZ2"]["ALA"]=   0;  shiftx_rc_dic["HZ2"]["CYS"]=0;  shiftx_rc_dic["HZ2"]["ASP"]=0;  shiftx_rc_dic["HZ2"]["GLU"]=0;  shiftx_rc_dic["HZ2"]["PHE"]=0;  shiftx_rc_dic["HZ2"]["GLY"]=0;  shiftx_rc_dic["HZ2"]["HIS"]=0;  shiftx_rc_dic["HZ2"]["ILE"]=0;  shiftx_rc_dic["HZ2"]["LYS"]=7.81;  shiftx_rc_dic["HZ2"]["LEU"]=0;  shiftx_rc_dic["HZ2"]["MET"]=0;  shiftx_rc_dic["HZ2"]["ASN"]=0;  shiftx_rc_dic["HZ2"]["PRO"]=0;  shiftx_rc_dic["HZ2"]["GLN"]=0;  shiftx_rc_dic["HZ2"]["ARG"]=0;  shiftx_rc_dic["HZ2"]["SER"]=0;  shiftx_rc_dic["HZ2"]["THR"]=0;  shiftx_rc_dic["HZ2"]["VAL"]=0;  shiftx_rc_dic["HZ2"]["TRP"]=7.65;  shiftx_rc_dic["HZ2"]["TYR"]=0
shiftx_rc_dic["HZ3"]={}; shiftx_rc_dic["HZ3"]["ALA"]=   0;  shiftx_rc_dic["HZ3"]["CYS"]=0;  shiftx_rc_dic["HZ3"]["ASP"]=0;  shiftx_rc_dic["HZ3"]["GLU"]=0;  shiftx_rc_dic["HZ3"]["PHE"]=0;  shiftx_rc_dic["HZ3"]["GLY"]=0;  shiftx_rc_dic["HZ3"]["HIS"]=0;  shiftx_rc_dic["HZ3"]["ILE"]=0;  shiftx_rc_dic["HZ3"]["LYS"]=7.81;  shiftx_rc_dic["HZ3"]["LEU"]=0;  shiftx_rc_dic["HZ3"]["MET"]=0;  shiftx_rc_dic["HZ3"]["ASN"]=0;  shiftx_rc_dic["HZ3"]["PRO"]=0;  shiftx_rc_dic["HZ3"]["GLN"]=0;  shiftx_rc_dic["HZ3"]["ARG"]=0;  shiftx_rc_dic["HZ3"]["SER"]=0;  shiftx_rc_dic["HZ3"]["THR"]=0;  shiftx_rc_dic["HZ3"]["VAL"]=0;  shiftx_rc_dic["HZ3"]["TRP"]=7.25;  shiftx_rc_dic["HZ3"]["TYR"]=0


