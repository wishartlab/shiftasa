dic={}
# Done
dic["A"]={}
dic["A"]={}
dic["A"]["H"]=8.35
dic["A"]["HA"]=4.35
dic["A"]["HB"]=1.42
dic["A"]["HB1"]=1.42
dic["A"]["HB2"]=1.42
dic["A"]["HB3"]=1.42
dic["A"]["C"]=178.5
dic["A"]["CA"]=52.82
dic["A"]["CB"]=19.26
dic["A"]["N"]=125

# Done
dic["B"]={}
dic["B"]["H"]=8.54
dic["B"]["HA"]=4.76
dic["B"]["HB2"]=3.29
dic["B"]["HB3"]=3.02
dic["B"]["C"]=175.5
dic["B"]["CA"]=55.63
dic["B"]["CB"]=41.22
dic["B"]["N"]=118.7

# Done
dic["C"]={}
dic["C"]["H"]=8.44
dic["C"]["HA"]=4.59
dic["C"]["HB2"]=2.98
dic["C"]["HB3"]=2.98
dic["C"]["C"]=175.3
dic["C"]["CA"]=58.63
dic["C"]["CB"]=28.34
dic["C"]["N"]=118.8

# Done
dic["D"]={}
dic["D"]["H"]=8.56
dic["D"]["HA"]=4.82
dic["D"]["HB2"]=2.98
dic["D"]["HB3"]=2.91
dic["D"]["C"]=175.9
dic["D"]["CA"]=52.99
dic["D"]["CB"]=38.33
dic["D"]["CG"]=177.4
dic["D"]["N"]=119.1


dic["E"]={}
dic["E"]["H"]=8.4
dic["E"]["HA"]=4.42
dic["E"]["HB2"]=2.18
dic["E"]["HB3"]=2.01
dic["E"]["HG2"]=2.5
dic["E"]["HG3"]=2.5
dic["E"]["C"]=176.8
dic["E"]["CA"]=56.09
dic["E"]["CB"]=28.88
dic["E"]["CG"]=32.88
dic["E"]["CD"]=180
dic["E"]["N"]=120.2


dic["F"]={}
dic["F"]["H"]=8.31
dic["F"]["HA"]=4.65
dic["F"]["HB2"]=3.19
dic["F"]["HB3"]=3.04
dic["F"]["HD1"]=7.28
dic["F"]["HE1"]=7.38
dic["F"]["HZ"]=7.33
dic["F"]["HE2"]=7.38
dic["F"]["HD2"]=7.28
dic["F"]["C"]=176.6
dic["F"]["CA"]=58.09
dic["F"]["CB"]=39.75
dic["F"]["CG"]=139.2
dic["F"]["CD1"]=132
dic["F"]["CE1"]=131.5
dic["F"]["CZ"]=130
dic["F"]["CE2"]=131.5
dic["F"]["CD2"]=132
dic["F"]["N"]=120.7


dic["G"]={}
dic["G"]["H"]=8.41
dic["G"]["HA"]=4.02
dic["G"]["HA2"]=4.02
dic["G"]["HA3"]=4.02
dic["G"]["C"]=174.9
dic["G"]["CA"]=45.39
dic["G"]["N"]=107.5

dic["H"]={} 
dic["H"]["H"]=8.56
dic["H"]["HA"]=4.79
dic["H"]["HB2"]=3.35
dic["H"]["HB3"]=3.19
dic["H"]["HE1"]=8.61
dic["H"]["HD2"]=7.31
dic["H"]["C"]=175.1
dic["H"]["CA"]=55.39
dic["H"]["CB"]=29.12
dic["H"]["CE1"]=136.4
dic["H"]["CD2"]=120.2
dic["H"]["CG"]=131.4
dic["H"]["N"]=118.1

dic["I"]={}
dic["I"]["H"]=8.17
dic["I"]["HA"]=4.21
dic["I"]["HB"]=1.89
dic["I"]["HG12"]=1.48
dic["I"]["HG13"]=1.19
dic["I"]["HG2"]=0.93
dic["I"]["HD1"]=0.88
dic["I"]["C"]=177.1
dic["I"]["CA"]=61.62
dic["I"]["CB"]=38.91
dic["I"]["CG1"]=27.46
dic["I"]["CG2"]=17.47
dic["I"]["CD1"]=13.16
dic["I"]["N"]=120.4

dic["K"]={}
dic["K"]["H"]=8.36
dic["K"]["HA"]=4.36
dic["K"]["HB2"]=1.89
dic["K"]["HB3"]=1.77
dic["K"]["HG2"]=1.47
dic["K"]["HG3"]=1.42
dic["K"]["HD2"]=1.68
dic["K"]["HD3"]=1.68
dic["K"]["HE2"]=2.92 # From Wishart
dic["K"]["HE3"]=2.9 # From Wishart
dic["K"]["HZ"]=7.6 # From Wishart
dic["K"]["C"]=177.4
dic["K"]["CA"]=56.71
dic["K"]["CB"]=33.21
dic["K"]["CG"]=25.01
dic["K"]["CD"]=29.33
dic["K"]["CE"]=42.35
dic["K"]["N"]=121.6

dic["L"]={}
dic["L"]["H"]=8.28
dic["L"]["HA"]=4.38
dic["L"]["HB2"]=1.67
dic["L"]["HB3"]=1.62
dic["L"]["HG"]=1.62
dic["L"]["HD1"]=0.93
dic["L"]["HD2"]=0.88
dic["L"]["C"]=178.2
dic["L"]["CA"]=55.47
dic["L"]["CB"]=42.46
dic["L"]["CG"]=27.11
dic["L"]["CD1"]=24.99
dic["L"]["CD2"]=23.32
dic["L"]["N"]=122.4


dic["M"]={}
dic["M"]["H"]=8.42
dic["M"]["HA"]=4.52
dic["M"]["HB2"]=2.15
dic["M"]["HB3"]=2.03
dic["M"]["HG2"]=2.63
dic["M"]["HG3"]=2.64
dic["M"]["HE"]=2.11
dic["M"]["C"]=177.1
dic["M"]["CA"]=55.77
dic["M"]["CB"]=32.94
dic["M"]["CG"]=32.25
dic["M"]["CE"]=16.96
dic["M"]["N"]=120.3


dic["N"]={}
dic["N"]["H"]=8.51
dic["N"]["HA"]=4.79
dic["N"]["HB2"]=2.88
dic["N"]["HB3"]=2.81
dic["N"]["HD21"]=7.59
dic["N"]["HD22"]=7.01
dic["N"]["C"]=176.1
dic["N"]["CA"]=53.33
dic["N"]["CB"]=39.09
dic["N"]["CG"]=177.3
dic["N"]["N"]=119
dic["N"]["ND2"]=112.73 # Average from BMRB



dic["P"]={}
dic["P"]["HA"]=4.45
dic["P"]["HB2"]=2.29
dic["P"]["HB3"]=1.99
dic["P"]["HG2"]=2.04
dic["P"]["HG3"]=2.04
dic["P"]["HD2"]=3.67
dic["P"]["HD3"]=3.61
dic["P"]["C"]=177.8
dic["P"]["CA"]=63.7
dic["P"]["CB"]=32.22
dic["P"]["CG"]=27.32
dic["P"]["CD"]=49.81

dic["Z"]={}
dic["Z"]["HA"]=4.6
dic["Z"]["HB2"]=2.39
dic["Z"]["HB3"]=2.18
dic["Z"]["HG2"]=1.95
dic["Z"]["HG3"]=1.88
dic["Z"]["HD2"]=3.6
dic["Z"]["HD3"]=3.54
dic["Z"]["CA"]=63
dic["Z"]["CB"]=34.8
dic["Z"]["CG"]=24.9
dic["Z"]["CD"]=50.4

dic["Q"]={}
dic["Q"]["H"]=8.44
dic["Q"]["HA"]=4.38
dic["Q"]["HB2"]=2.17
dic["Q"]["HB3"]=2.01
dic["Q"]["HG2"]=2.39
dic["Q"]["HG3"]=2.39
dic["Q"]["HE21"]=7.5
dic["Q"]["HE22"]=6.91
dic["Q"]["C"]=176.8
dic["Q"]["CA"]=56.22
dic["Q"]["CB"]=29.53
dic["Q"]["CG"]=33.96
dic["Q"]["CD"]=180.5
dic["Q"]["N"]=120.5

dic["R"]={}
dic["R"]["H"]=8.39
dic["R"]["HA"]=4.38
dic["R"]["HB2"]=1.91
dic["R"]["HB3"]=1.79
dic["R"]["HG2"]=1.68
dic["R"]["HG3"]=1.64
dic["R"]["HD2"]=3.2
dic["R"]["HD3"]=3.2
dic["R"]["HE"]=7.2
dic["R"]["C"]=177.1
dic["R"]["CA"]=56.48
dic["R"]["CB"]=30.93
dic["R"]["CG"]=27.33
dic["R"]["CD"]=43.55
dic["R"]["CZ"]=159.7
dic["R"]["N"]=121.2
dic["R"]["NE"]=89.71 # Average BMRB value
#dic["R"]["NEave"]=89.71 # Average BMRB value

dic["S"]={}
dic["S"]["H"]=8.43
dic["S"]["HA"]=4.51
dic["S"]["HB2"]=3.95
dic["S"]["HB3"]=3.9
dic["S"]["C"]=175.4
dic["S"]["CA"]=58.67
dic["S"]["CB"]=64.06
dic["S"]["N"]=115.5

dic["T"]={}
dic["T"]["H"]=8.25
dic["T"]["HA"]=4.43
dic["T"]["HB"]=4.33
dic["T"]["HG2"]=1.22
dic["T"]["C"]=175.6
dic["T"]["CA"]=62.01
dic["T"]["CB"]=70.01
dic["T"]["CG2"]=21.6
dic["T"]["N"]=112

dic["V"]={}
dic["V"]["H"]=8.16
dic["V"]["HA"]=4.16
dic["V"]["HB"]=2.11
dic["V"]["HG1"]=0.96
dic["V"]["HG2"]=0.96
dic["V"]["C"]=177
dic["V"]["CA"]=62.61
dic["V"]["CB"]=32.82
dic["V"]["CG1"]=21.11
dic["V"]["CG2"]=20.34
dic["V"]["N"]=119.3

dic["W"]={}
dic["W"]["H"]=8.22
dic["W"]["HA"]=4.7
dic["W"]["HB2"]=3.34
dic["W"]["HB3"]=3.25
dic["W"]["HE1"]=10.63
dic["W"]["HD1"]=7.28
dic["W"]["HE3"]=7.65
dic["W"]["HZ3"]=7.18
dic["W"]["HH2"]=7.26
dic["W"]["HZ2"]=7.51
dic["W"]["C"]=177.1
dic["W"]["CA"]=57.6
dic["W"]["CB"]=29.75
dic["W"]["CD1"]=127.4
dic["W"]["CG"]=111.7
dic["W"]["CE3"]=122.2
dic["W"]["CZ3"]=124.8
dic["W"]["CH2"]=121.1
dic["W"]["CZ2"]=114.8
dic["W"]["CE2"]=139
dic["W"]["CD2"]=129.6
dic["W"]["N"]=122.1

dic["Y"]={}
dic["Y"]["H"]=8.26
dic["Y"]["HA"]=4.58
dic["Y"]["HB2"]=3.09
dic["Y"]["HB3"]=2.97
dic["Y"]["HD1"]=7.15
dic["Y"]["HE1"]=6.86
dic["Y"]["HE2"]=6.86
dic["Y"]["HD2"]=7.15
dic["Y"]["C"]=176.7
dic["Y"]["CA"]=58.28
dic["Y"]["CB"]=38.94
dic["Y"]["CG"]=130.8
dic["Y"]["CD1"]=133.3
dic["Y"]["CE1"]=118.3
dic["Y"]["CZ"]=157.5
dic["Y"]["CE2"]=118.3
dic["Y"]["CD2"]=133.3
dic["Y"]["N"]=120.9

