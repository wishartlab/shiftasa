#/usr/bin/python
from os.path import join
import sys, os


def createSSPredHTMLText(input_file_prefix):
    UPLOAD_DIR = os.getcwd()
    ASA_pred="%s.asafeat.pred" % (input_file_prefix)
    file_open = open(join(UPLOAD_DIR,ASA_pred), "r")
    pred_asa_lines = file_open.readlines()
    file_open.close()
    last_resnum = pred_asa_lines[-1].split()[0]
    
    if len(last_resnum) == 2:
        num_spaces = 2
    elif len(last_resnum) == 3:
        num_spaces = 3
    elif len(last_resnum) == 4:
        num_spaces = 4
        
    for i in range(0, len(pred_asa_lines)):
        line = pred_asa_lines[i]
        if not line.startswith("#"):
            (resnum, res, fASA, cls) = line.split()
            suffix= line[1:].strip()
            if len(resnum)== 1:
                line=resnum+ "&nbsp"*num_spaces+suffix
            elif len(resnum) == 2 :
                line=resnum+ "&nbsp"*(num_spaces-1)+suffix 
            elif len(resnum) == 3:
                line=resnum+ "&nbsp"*(num_spaces-2)+suffix
            elif len(resnum) == 4:
                line=resnum+ "&nbsp"*(num_spaces-3)+suffix
        pred_asa_lines[i] = line        
    pred_asa_lines="<br>".join(pred_asa_lines)
    

    with open(input_file_prefix+".asapred.html", "w") as fout:
            fout.write ("<link href='txtstyle.css' rel='stylesheet' type='text/css' /> %s" % pred_asa_lines )
    with open("txtstyle.css", "w") as f:
            f.write ("html, body {font-family:Courier}")
            
            
def  main():

    input_file_prefix = sys.argv[1]
    createSSPredHTMLText(input_file_prefix)
    
main()    
