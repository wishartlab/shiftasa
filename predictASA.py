#!/usr/local/bin/python3
#********************************************
# fractional ASA prediction main program                             
# - accepts commanl-line options
# Available options:
# a. use Re-referencing
# b. Neighbor residue correction
# 
#  
#- output:
#  a. text file with predicted fractional 
#  ASA values
#  b. graphical image  
#*********************************************
import os, sys, getopt, re, fnmatch
#from optparse import OptionParser
#from argparse import ArgumentParser
from os import popen
import getopt
import os.path
from os.path import join, isfile
import subprocess



# global variables
#PANAV = "/var/www/html/shiftasa/Standalone_ShiftASA/PANAV-v2b.jar"
#BLAST = "/var/www/html/shiftasa/blast-2.2.26"
#SABLE = "/var/www/html/shiftasa/Standalone_ShiftASA/sable_distr"
#RCI = "/var/www/html/shiftasa/Standalone_ShiftASA/rci-latest"
#SC_RCI = "/var/www/html/shiftasa/Standalone_ShiftASA/Side_chain_RCI"
#RSCRIPT_EXEC = "/usr/bin/Rscript"
#SCON_SRC = "/var/www/html/shiftasa/Standalone_ShiftASA/conservation_code"
#SRC_DIR = "/var/www/html/shiftasa/Standalone_ShiftASA/src"

PANAV = "/var/www/html/shiftasa/Standalone_ShiftASA/PANAV-v2b.jar"
BLAST = "/var/www/html/shiftasa/blast-2.2.26"
SABLE = "/var/www/html/shiftasa/Standalone_ShiftASA/sable_distr"
RCI = "/var/www/html/shiftasa/Standalone_ShiftASA/rci-latest"
SC_RCI = "/var/www/html/shiftasa/Standalone_ShiftASA/Side_chain_RCI"
RSCRIPT_EXEC = "/usr/bin/Rscript"
SCON_SRC = "/var/www/html/shiftasa/Standalone_ShiftASA/conservation_code"
SRC_DIR = "/var/www/html/shiftasa/Standalone_ShiftASA/src"

def run_shiftasa (options):

    
    

    print("# ShiftASA Relative Accessible Surface Area Prediction")
    
    
    # setting the option flag
    for option, value in options:
        if option == "-i":
            infile = value
            os.system ("echo %s >> shiftasa_log" %infile)
        elif option == "-n":
            NRC_FLAG = value 
            os.system ("echo %s >> shiftasa_log" %NRC_FLAG)
        elif option == "-m":
            FILE_FORMAT = value
        elif option == "-g":
            GRAPHICS = value
        elif option == "-x":
            RCI_FLAG = value
        elif option == "-r":
            REREF = value    
        elif option == "-h":
            HOMOLOGY = value
        """
        elif option == "-s":
            SCRCI = value
        Note: SCRCI option is taken off from the option list
        """ 
    WORKDIR = os.getcwd()
    
    
    
        
    input_bmrb_id = infile.split(".")[0]
    
    #======================================================================
    # pre-processing the data
    #======================================================================

    if FILE_FORMAT == "bmrb3" or FILE_FORMAT == "shifty" :
            REREF = "no"
    
    if REREF == "yes" or REREF == "y" :
        #call PANAV-v2b for chemical-shift rereferencing in batch mode
        os.system("%s/run_PANAV.sh" %SRC_DIR)    
        os.system("echo 'running panav2talos...' >> shiftasa_log")    
        os.system("python3 %s/panav2talos.py %s >> shiftasa_log" %(SRC_DIR, input_bmrb_id))
            
    else:
        if FILE_FORMAT == "bmrb2" :#bmrb 2.1 format
            os.system("echo 'running bmrb2talos...' >> shiftasa_log")   
            os.system("%s/bmrb2talosV2.0.com %s > %s.tab" %(SRC_DIR, infile,input_bmrb_id))
        elif FILE_FORMAT == "bmrb3" :#bmrb 3.1 format
            os.system("echo 'running bmrb3toTalos...' >> shiftasa_log")   
            os.system("python3 %s/bmrb3toTalos.py %s > %s.tab" %(SRC_DIR, infile,input_bmrb_id))    
        elif FILE_FORMAT == "shifty": #shifty format
            # shifty2talos conversion and sequence extraction in fasta format
            os.system("echo 'running shifty2talos...' >> shiftasa_log")
            os.system("python %s/shifty2talos.py %s" %(SRC_DIR, infile))
     
    #to indicate the talos format conversion is done
    os.system("touch donetalos")


    # check the percent completeness of side-chain shifts and set the SCRCI flag "yes"/ "no"
    if FILE_FORMAT == "bmrb3" or FILE_FORMAT == "bmrb2" :
            # call check side-chain completeness code here
            command = "python3 %s/check_side_chain_shift_completeness.py %s %s" %(SRC_DIR, infile, FILE_FORMAT)
            percent_scshifts_assigned = float(popen(command).read())
            if percent_scshifts_assigned >= 0.0:
                SCRCI = "yes"
            else:
                SCRCI = "no"	
    elif FILE_FORMAT == "shifty" :
            SCRCI = "no"        
    
    #sequence extraction in fasta format
    os.system("echo 'running parse_bmrb_seq ...\n' >> shiftasa_log")   
    os.system("python3 %s/parse_bmrb_seq.py %s.tab >> shiftasa_log "%(SRC_DIR, input_bmrb_id))
                    
    # secondary chemical shift calculation with neighbor residue correction
    if NRC_FLAG == "yes":
        os.system("echo 'running neighbor residue correction...' >> shiftasa_log")
        os.system("perl %s/seccs_calculation_with_nrc.pl %s.tab >> shiftasa_log" %(SRC_DIR, input_bmrb_id))
        os.system("echo 'mv *.nrc *.tab...' >> shiftasa_log")
        os.system("mv %s.nrc %s.tab >> shiftasa_log" %(input_bmrb_id, input_bmrb_id))

   # filling up the missing chemical shift entries
    os.system("echo 'running matching triplet and filling the gap in assignment...' >> shiftasa_log")
    os.system("python3 %s/match_triplet_V2.py %s.tab >> shiftasa_log " %(SRC_DIR, input_bmrb_id))
    
    #======================================================================
    # feature calculation
    #======================================================================

    # ss_probability
    os.system("echo 'running secondary structure probability...' >> shiftasa_log")
    os.system("perl %s/ssp_calculation.pl %s.tab >> shiftasa_log" %(SRC_DIR,input_bmrb_id))

    # Random Coil Index value
    os.chdir(RCI)
    os.system("echo 'in the rci folder...%s' >> %s/shiftasa_log" %(os.getcwd(),WORKDIR))
    if FILE_FORMAT == "bmrb2": 
        command ="python rci-prog-updated.py -b2 %s/%s" %(WORKDIR, infile)
    if FILE_FORMAT == "bmrb3": 
        command ="python rci-prog-updated.py -b3 %s/%s" %(WORKDIR, infile)    
    elif FILE_FORMAT == "shifty" :#flag = -sh
        command ="python rci-prog-updated.py -sh %s/%s" %(WORKDIR, infile)   
    os.system(command)
    os.chdir(WORKDIR)
    

    if SCRCI == "yes": 
    # Side-cahin Random Coil Index value
    # call side-chain rci program here
        os.chdir(SC_RCI)
        os.system("echo 'in the side-chain rci folder...%s' >> %s/shiftasa_log" %(os.getcwd(),WORKDIR))
        if FILE_FORMAT == "bmrb2": 
            command ="python side_chain_rci_updated.py -b2 %s/%s" %(WORKDIR, infile)
        if FILE_FORMAT == "bmrb3": 
            command ="python  side_chain_rci_updated.py -b3 %s/%s" %(WORKDIR, infile)    
        elif FILE_FORMAT == "shifty" :#flag = -sh
            command ="python  side_chain_rci_updated.py -sh %s/%s" %(WORKDIR, infile)   
        os.system(command)
        os.system("mv %s.Side_chain_RCI.txt %s/" %(infile, WORKDIR))
        os.system("rm %s*" %(input_bmrb_id))
        os.chdir(WORKDIR)
    
    if HOMOLOGY == "yes":
    # run SABLE and parse the output here
        if not os.path.exists(join(WORKDIR,"%s.sable" %input_bmrb_id)):
            os.chdir(SABLE)
            os.system("echo 'in the SABLE folder...%s' >> %s/shiftasa_log" %(os.getcwd(),WORKDIR))
            if os.path.exists("OUT_SABLE_RES") or os.path.exists("OUT_SABLE_graph"):
                os.system("rm -r OUT_SABLE*")
            os.system("cp %s/%s.fasta data.seq" %(WORKDIR, input_bmrb_id))
            command = "./run.sable"
            os.system(command)
            os.system("mv OUT_SABLE_RES %s/%s.sable" %(WORKDIR, input_bmrb_id))
            os.system("cp OUT_SABLE/align.out %s/%s.aln" %(WORKDIR, input_bmrb_id))
            
            os.chdir(WORKDIR)

    # conservation score
    if not os.path.exists("%s.jsscon"%input_bmrb_id):
        os.chdir(SCON_SRC)
        os.system("echo 'running conservation score...' >> %s/shiftasa_log"%WORKDIR)
    
        os.system("perl %s/run_conservation_score.pl %s %s %s >> %s/shiftasa_log" %(SRC_DIR, input_bmrb_id, BLAST, WORKDIR, WORKDIR))
        os.chdir(WORKDIR)
    
    ## finding the final flag value to choose an appropriate prediction model
    
    if HOMOLOGY=="yes" and SCRCI=="no":
        
        prediction_model_to_choose_flag = "sable"
        
    elif HOMOLOGY=="yes" and SCRCI=="yes":
        
        prediction_model_to_choose_flag = "sable_scrci"
    #elif HOMOLOGY=="no" and SCRCI=="yes":
        
    #    prediction_model_to_choose_flag = "scrci"
    else:
        prediction_model_to_choose_flag = "backbone"
    
    """
    if HOMOLOGY=="yes":
        prediction_model_to_choose_flag = "sable_scrci"
    else:
        prediction_model_to_choose_flag = "backbone"

    """

     
    #======================================================================
    # final ASA prediction
    #======================================================================
    os.system("echo 'predicting ASA...' >> %s/shiftasa_log" %WORKDIR)
    command = "%s/predictASAf.sh %s %s %s" %(SRC_DIR, input_bmrb_id, prediction_model_to_choose_flag, SCRCI)
    os.system("echo '" + command + "' >> %s/shiftasa_log" %WORKDIR)
    os.system(command)
   
            
    #======================================================================
    # Output Graphics
    #======================================================================    
    if GRAPHICS == "y" or GRAPHICS == "yes": 
        # output graphics
        os.system("echo 'producing graphics...' >> %s/shiftasa_log"%WORKDIR)
        os.system("cp %s.asapred.vert asapred" %input_bmrb_id)
        command = "/usr/local/bin/gnuplot %s/singlePlot.gp " %(SRC_DIR)
        os.system(command)
        
    final_prediction_file = input_bmrb_id+'.asapred.vert'     
    if os.path.exists(final_prediction_file):    
        os.system (" echo 'SHIFTASA is successfully done! The predicted fractional ASA is in %s' > %s.success" %(final_prediction_file,input_bmrb_id))
    
def main():

    usage = "usage %prog -i input_bmrb [options]"
    
    try:
        #Example: python3 predictASA.py -i bmrbfilename -r no -n yes -m bmrb -h yes -s yes -p yes -f no -g yes -x no

	#parsing arguments when running from web server
        #opts, args = getopt.getopt(sys.argv[1].split(), 'i:r:n:m:h:s:p:f:g:x:')
        opts, args = getopt.getopt(sys.argv[1].split(), 'i:r:n:m:h:p:f:g:x:')

        # parsing arguments for command-line execution
        #opts, args = getopt.getopt(sys.argv[1:], 'i:r:n:m:h:s:p:f:g:x:')
        #os.system ("echo %s >> shiftasa_log" %sys.argv[1:])
        
        
        
        
    except getopt.GetoptError as err:
        os.system("echo %s >> shiftasa_log" %str(err))
        os.system("echo %s >> shiftasa_log" %usage)
        sys.exit(2)
   
    run_shiftasa(opts)
 	 
    	
main()
