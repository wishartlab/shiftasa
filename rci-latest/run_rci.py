#!/usr/bin/python3
import os
from os import listdir
from os.path import isfile, join, exists

#inputDir="/Users/Noor/Documents/Research/RefDB-dataset/Mark_30_proteins_cs_files"
inputDir = "/Users/Noor/Documents/Research/FeatureGenerateMay08/FeatureGenerate/CS-CompleteShift-features";
rciDir = "/Users/Noor/Documents/Research/FeatureGenerateMay08/FeatureGenerate/RCI_flexibility"
#files = [file for file in listdir(inputDir) if (file.endswith(".corr"))]
files = ["R096_bmr5352.str.corr"]
for file in files:
	# -smooth = 0 (unsmoothed)
	# -rm = 0 (don't remove intermediate secondary shift file)
	#os.system("python rci_v_1n_12_20_13_A.py -b %s -smooth 0 -fsmooth 0 -rm 0" %(join(inputDir, file)))
	#prefix = file.split("_")[0]
	#nrcfile = "".join([f for f in listdir(inputDir) if f.startswith(prefix) and f.endswith(".nrc.complete")])
	if not exists(file+".RCI.txt"):
		print(file)
		rciFile = file+".RCI.txt"
		os.system("python rci-prog-updated.py -b %s" %(join(inputDir, file)))
 		##move the rci output files from RCI directory to the current directory
 		#os.system("cp %s/%s %s/" %(rciDir, rciFile, inputDir))

