@Echo off
@Cls


rem Where sable has been installed?
set SABLE_DIR=C:\sable_distr
rem Where the blastpgp file is?
set BLAST_DIR=C:\dist_sable\blast
rem Where Nr database is installed?
set NR_DIR=C:\dist_sable\nr\nr
rem Where swissprot databse is installed?
set SWISS_DIR=C:\dist_sable\swiss\swissprot

set OSTYPE=windows

rem version of sable you want to there are two possible values:
rem sable1 or sable2(sable2 is using solvent accessibility for secondary structure prediction and usually
rem is giving better results)

set SABLE_VERSION=sable2

rem name of the temporary directory where all files needed by sable will be created
rem also it is the root name for final output files (PBS_JOBID.res and PBS_JOBID_graph)

set PBS_JOBID=OUT_SABLE

rem There are three possibilities for this option: SS (secondary structure prediction), 
rem SA (solvent accessibility prediction), SS-SA (both secondary structure and solvent accessibility)

set SABLE_ACTION=SS-SA


rem type of approximator, for solvent accessibility prediction, that will be used, there are following possible values:
rem wApproximator, Thermometer, Approximator, SVR. The default value is wApproximator. 
rem Be aware that solvent accessibility prediction is also used in secondary structure prediction for sable2 version.
set SA_ACTION=wApproximator

rem what networks use to predict solevnt accessibility trained on data with complexes (complex)
rem or trained on sigle chains (single) this option is valid onlu for wApproximator
set SABLE_SA=complex	    


rem SS consist of three steps, one is making prediction based on rem mutiple alignment on NR database, the second step 
rem based on multiple alignment on swissprot database, and third is prediction
rem is combining those results (option Nr). There is also possible to
rem obtain results only by using swissprot (option swissprot).
			 
set BLAST_DATABASE=Nr


mkdir %PBS_JOBID%
copy data.seq %PBS_JOBID%
cd %PBS_JOBID%

perl %SABLE_DIR%\sable.pl


set file=%PBS_JOBID%_graph
copy %file% ..
set file=%PBS_JOBID%_res
copy %file% ..

cd ..
