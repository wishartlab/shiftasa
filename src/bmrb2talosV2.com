
awk '   BEGIN { start0 = 0; start1 = 0; start2 = 0; cnt = 0; start3 =0;stop = 0;
		ONE   = "AcCDEFGHIKLMNPQRSTVWY";
		THREE = "ALA.cys.CYS.ASP.GLU.PHE.GLY.HIS.ILE.LYS.LEU.MET.ASN.PRO.GLN.ARG.SER.THR.VAL.TRP.TYR"
	}
	NF~10 && ($10 == "1"||$10 == "2"||$10 == "." || $10 == "0" ){
		pos = index( THREE,toupper($5) );
		if( pos>0 && ($6=="CA"||$6=="CB"||$6=="C"||$6=="N"||$6=="H"||$6=="HN"||index($6,"HA")>0) ){
			atomName = $6;
			if( atomName == "H" ) atomName = "HN"
			#printf("%4d %1s %4s %8.3f\n",$3,substr(ONE,pos/4+1,1),atomName,$7);
			printf("%4d %4d %4s %4s %8.3f\n",$2, $4, $5,atomName,$8);
		}
	}
	NF~9 && ($9 == "1"||$9 == "2"||$9 == "." || $9 == "0"){
		pos = index( THREE,toupper($4) );
		if( pos>0 && ($5=="CA"||$5=="CB"||$5=="C"||$5=="N"||$5=="H"||$5=="HN"||index($5,"HA")>0) ){
			atomName = $5;
			if( atomName == "H" ) atomName = "HN"
			#printf("%4d %1s %4s %8.3f\n",$3,substr(ONE,pos/4+1,1),atomName,$7);
			printf("%4d %4d %1s %4s %8.3f\n",$2,$3,$4,atomName,$7);
		}
	}
	NF~8 && ($8 == "1"||$8 == "2"||$8 == "." || $8 == "0"){
		pos = index( THREE,toupper($3) );
		if( pos>0 && ($4=="CA"||$4=="CB"||$4=="C"||$4=="N"||$4=="H"||$4=="HN"||index($4,"HA")>0) ){
			atomName = $4;
			if( atomName == "H" ) atomName = "HN"
			#printf("%4d %1s %4s %8.3f\n",$2,substr(ONE,pos/4+1,1),atomName,$6);
			printf("%4d %1s %4s %8.3f\n",$2,$3,atomName,$6);
		}
	}
	NF~1{
		if($1 == "_Residue_label" && start3 == 0){
			start3 = 1;
			printf("\n\n");
		}
	}
	NF~1{
		if($1 == "stop_" && start3 == 1){
			stop = 1;
			
		}
	}
        NF >= 1 {
		if ($1 != "stop_" && (start3 == 1 && stop!= 1 && $1~ /^[0-9]+$/ && $2~ /^[A-Z]+$/)){
				
			printf("SEQ %d%s %d%s %d%s %d%s %d%s\n" , $1,$2,$3,$4,$5,$6,$7,$8,$9, $10);
		
		}
	}	
	
	NF >= 1{
		if( $1 != "stop_" && (start3 == 1 && stop!= 1 && $1~ /^[0-9]+$/ && $3 ~ /^[A-Z]+$/)){
			printf("SEQ %d%s %d%s %d%s %d%s %d%s\n" , $1,$3,$4,$6,$7,$9,$10,$12,$13, $15);
		
		}

	}		
	NF~ 1 && ($1 == "_Chem_shift_ambiguity_code" && start3 == 1){
		
		print"\n\nVARS   PDBRESID BMRBRESID RESNAME ATOMNAME SHIFT "; 
		print"FORMAT %4d %4d %4s %4s %8.3f\n";
		start3 = 0;			
				
	}
	' $1

# CS-ROSETTA: System for Chemical Shifts based protein structure prediction using ROSETTA
# (C) Shen and Bax 2007-2008, Lab of Chemical Physics, NIDDK, NIH
# Version 1.01(build 2009.1117.15)
#
# bmrb2talos.com: converts BMRB format chemical shift file to talos/mfr/csrosetta format
#         syntax: bmrb2talos.com bmrb.str > talos.tab
# This script is a modified version of original script bmrb2talos.com which converts a bmrb formatted chemical shift file into talos format and includes both bmrb and pdb residue numbers into tab file 

