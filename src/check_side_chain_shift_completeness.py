#!/usr/bin/python

from __future__ import division
from collections import defaultdict
import sys

#side chain atom dictionary of 20 amino acids 
SideChainAtomDict={"ALA":[ "HB"], 
            "GLU": [ "CD", "CG","HB2", "HB3", "HG2", "HG3"], 
            "GLY":[], 
            "ILE": ["CD1", "CG1", "CG2", "HB", "HG2", "HD1", "HG12", "HG13" ], 
            "THR":["CG2", "HB", "HG1", "HG2" ], 
            "LEU": ["CD1", "CD2", "CG", "HB2","HB3", "HG", "HD1", "HD2"], 
            "PRO":["CD","CG", "HB2","HB3","HG2","HG3","HD2","HD3"], 
            "ASP":["CG", "HB2", "HB3"], 
            "SER":[ "HB2", "HB3", "HG"], 
            "PHE": ["CD1", "CD2", "CE1", "CE2","CG", "CZ", "HB2", "HB3","HD1", "HD2", "HE1", "HE2", "HZ"], 
            "HIS": ["CD2", "CE1", "CG", "HB2", "HB3","HD1", "HD2", "HE1"], 
            "LYS":["CD","CE","CG", "HB2","HB3","HG2","HG3","HD2","HD3", "HE2","HE3", "HZ" ], 
            "ARG":[ "CD", "CG", "CZ", "HB2","HB3","HG2","HG3","HD2","HD3", "HE", "HH11", "HH12", "HH21", "HH22", "NE"], 
            "TYR":[ "CD1", "CD2", "CE1", "CE2", "CG", "CZ", "HB2", "HB3","HD1", "HD2", "HE1", "HE2", "HH"], 
            "CYS":[ "HB2","HB3","HG"],
            "CSP":[ "HB2","HB3","HG"], # synonym for CYS residue
            "ASN": ["CG", "HB2", "HB3","HD21", "HD22", "ND2"],
            "VAL": ["CG1", "CG2", "HB", "HG1","HG2"],
            "GLN": ["CD","CG", "HB2","HB3","HG2","HG3", "HE21", "HE22"],
            "MET": ["CE", "CG", "HB2","HB3","HG2","HG3", "HE"],
            "TRP": ["CD1", "CD2", "CE2", "CE3", "CH2", "CG", "CZ2", "CZ3", "HB2","HB3", "HD1", "HE1", "HE3", "HZ2", "HZ3", "HH2"]
    }

#side-chain proton dictionary of 20 amino acids 
SideChainProtonDict={"ALA":["HB"], 
            "GLU": ["HB2", "HB3", "HG2", "HG3"], 
            "GLY":[], 
            "ILE": [ "HB", "HG2", "HD1", "HG12", "HG13" ], 
            "THR":[ "HG1", "HG2" ], 
            "LEU": [ "HB2","HB3", "HG", "HD1", "HD2"], 
            "PRO":["HB2","HB3","HG2","HG3","HD2","HD3"], 
            "ASP":["HB2", "HB3"], 
            "SER":[ "HB2", "HB3", "HG"], 
            "PHE": ["HB2", "HB3","HD1", "HD2", "HE1", "HE2", "HZ"], 
            "HIS": [ "HB2", "HB3","HD1", "HD2", "HE1"], 
            "LYS":["HB2","HB3","HG2","HG3","HD2","HD3", "HE2","HE3", "HZ" ], 
            "ARG":[ "HB2","HB3","HG2","HG3","HD2","HD3", "HE", "HH11", "HH12", "HH21", "HH22"], 
            "TYR":[ "HB2", "HB3","HD1", "HD2", "HE1", "HE2", "HH"], 
            "CYS":[ "HB2","HB3","HG"],
            "CSP":[ "HB2","HB3","HG"], # synonym for CYS residue
            "ASN": [ "HB2", "HB3","HD21", "HD22"],
            "VAL": [ "HB", "HG1","HG2"],
            "GLN": [ "HB2","HB3","HG2","HG3", "HE21", "HE22"],
            "MET": [ "HB2","HB3","HG2","HG3", "HE" ],
            "TRP": [ "HB2","HB3", "HD1", "HE1", "HE3", "HZ2", "HZ3", "HH2"]
    }


atomSynonymDict={'VAL_HG11':'HG1', 'VAL_HG12':'HG1', 'VAL_HG13':'HG1', 'VAL_HG21':'HG2', 'VAL_HG22':'HG2', 'VAL_HG23':'HG2', 'LEU_HD11':'HD1', 'LEU_HD12':'HD1', 'LEU_HD13':'HD1', 'LEU_HD21':'HD2', 'LEU_HD22':'HD2', 'LEU_HD23':'HD2', 'ALA_HB1': 'HB', 'ALA_HB2': 'HB', 'ALA_HB3': 'HB', 'MET_HE1': 'HE', 'MET_HE2': 'HE', 'MET_HE3': 'HE','THR_HG21': 'HG2', 'THR_HG22': 'HG2', 'THR_HG23': 'HG2', 'ILE_HG21':'HG2', 'ILE_HG22':'HG2', 'ILE_HG23':'HG2', 'ILE_HD11':'HD1', 'ILE_HD12':'HD1', 'ILE_HD13':'HD1', 'LYS_HZ1': 'HZ', 'LYS_HZ2': 'HZ', 'LYS_HZ3': 'HZ'}

backboneAtoms = ['HA', 'H', 'HA2', 'HA3', 'C', 'CA', 'CB', 'N']

def side_chain_atoms_stats_bmrb3(atomLines):
    resnList = []
    residueDict = dict()
    sideChainAtoms = defaultdict(list)
    sideChainProtons= defaultdict(list)
    availableAtoms = 0
    totalSideChainAtoms = 0
    availableProtons = 0
    totalSideChainProtons = 0
    
    for atomLine in atomLines:
        elems = atomLine.split()
        resn = elems[5]
        if resn not in resnList: 
            resnList.append(resn)
        residue = elems[6]
        residueDict[resn]=residue
        atom = elems[7]
        if residue+'_'+atom in atomSynonymDict:
            atom = atomSynonymDict[residue+'_'+atom]
            
        if atom not in backboneAtoms and atom not in sideChainAtoms[resn]:
            sideChainAtoms[resn].append(atom)
            if atom.startswith("H"):
                sideChainProtons[resn].append(atom)
            
    for resn in resnList:
        availableAtoms +=  len(sideChainAtoms[resn])
        totalSideChainAtoms+= len(SideChainAtomDict[residueDict[resn]])
        
        availableProtons += len(sideChainProtons[resn])
        totalSideChainProtons+= len(SideChainProtonDict[residueDict[resn]])
        
    percent_complete_sc_shifts = availableAtoms/totalSideChainAtoms * 100
    percent_complete_sc_proton_shifts = availableProtons/totalSideChainProtons * 100
    return percent_complete_sc_shifts, percent_complete_sc_proton_shifts
    
    
def side_chain_atoms_stats_bmrb2(atomLines):
    resnList = []
    residueDict = dict()
    sideChainAtoms = defaultdict(list)
    sideChainProtons = defaultdict(list)
    availableAtoms = 0
    totalSideChainAtoms = 0
    availableProtons = 0
    totalSideChainProtons = 0
    
    for atomLine in atomLines:
        
        elems = atomLine.split()
        if len(elems) == 9:
            resn = elems[2]
            if resn not in resnList: 
                resnList.append(resn)
            residue = elems[3]
            residueDict[resn]=residue
            atom = elems[4]
        if len(elems) == 8:
            resn = elems[1]
            if resn not in resnList: 
                resnList.append(resn)
            residue = elems[2]
            residueDict[resn]=residue
            atom = elems[3]    
        if residue+'_'+atom in atomSynonymDict:
            atom = atomSynonymDict[residue+'_'+atom]
            
            
        if atom not in backboneAtoms and atom not in sideChainAtoms[resn]:
            sideChainAtoms[resn].append(atom)
            if atom.startswith("H"):
                sideChainProtons[resn].append(atom)
    for resn in resnList:
        availableAtoms +=  len(sideChainAtoms[resn])
        totalSideChainAtoms += len(SideChainAtomDict[residueDict[resn]])
        
        
        availableProtons += len(sideChainProtons[resn])
        totalSideChainProtons+= len(SideChainProtonDict[residueDict[resn]])

        
    percent_complete_sc_shifts = availableAtoms/totalSideChainAtoms * 100
    percent_complete_sc_proton_shifts = availableProtons/totalSideChainProtons * 100
    return percent_complete_sc_shifts, percent_complete_sc_proton_shifts

def backbone_atoms_stats_bmrb2(atomLines):
    resnList = []
    residueDict = dict()
    backboneAtomsDict = defaultdict(list)
    backboneAtomsResn = defaultdict(list)
    availableAtoms = 0
    totalBBAtoms = 0
    
    for atomLine in atomLines:
        elems = atomLine.split()
        
        if len(elems) == 9:
            resn = elems[2]
            if resn not in resnList: 
                resnList.append(resn)
            residue = elems[3]
            residueDict[resn]=residue
            atom = elems[4]
        if len(elems) == 8 or len(elems) == 6:
            resn = elems[1]
            if resn not in resnList: 
                resnList.append(resn)
            residue = elems[2]
            residueDict[resn]=residue
            atom = elems[3]
          
       
        if atom in backboneAtoms and atom not in backboneAtomsDict[resn]:
            backboneAtomsDict[resn].append(atom)
            backboneAtomsResn[atom].append(resn)
    num_atoms_missing= 0        
    
    for atom in backboneAtoms :
        if atom not in ['HA2', 'HA3'] and not backboneAtomsResn[atom]:
            num_atoms_missing+=1
            
    for resn in resnList:
        availableAtoms +=  len( backboneAtomsDict[resn])
        if residueDict[resn] not in ['PRO']:
            totalBBAtoms += 6
        
        elif residueDict[resn] == "PRO":
            totalBBAtoms += 4    
        
                
    print (availableAtoms/totalBBAtoms * 100 )  
    
def backbone_atoms_stats_bmrb3(atomLines):
    resnList = []
    residueDict = dict()
    backboneAtomsDict = defaultdict(list)
    backboneAtomsResn = defaultdict(list)
    availableAtoms = 0
    totalBBAtoms = 0
    
    for atomLine in atomLines:
        elems = atomLine.split()
        resn = elems[5]
        if resn not in resnList: 
            resnList.append(resn)
        residue = elems[6]
        residueDict[resn]=residue
        atom = elems[7]
       
        if atom in backboneAtoms and atom not in backboneAtomsDict[resn]:
            backboneAtomsDict[resn].append(atom)
            backboneAtomsResn[atom].append(resn)
    num_atoms_missing= 0        
    
    for atom in backboneAtoms:
        if atom not in ['HA2', 'HA3'] and not backboneAtomsResn[atom]:
            num_atoms_missing+=1
            
    for resn in resnList:
        availableAtoms +=  len( backboneAtomsDict[resn])
        if residueDict[resn] not in ['PRO']:
            totalBBAtoms += 6
        
        elif residueDict[resn] == "PRO":
            totalBBAtoms += 4        
                
     
    print (availableAtoms/totalBBAtoms * 100 )
def main():
    bmrbFile = sys.argv[1]
    fileFormat = sys.argv[2]
    fin = open(bmrbFile, "r")
    atomLines = []
    
    if bmrbFile.endswith(".shifty.corr"):
        for line in fin:
            line = line.strip()
            if line.strip() and not line.startswith("#") and line[0].isdigit():
                atomLines.append(line.strip())
        
    else:    
        if fileFormat == "bmrb2":
            ChemShiftStartTag = "_Chem_shift_ambiguity_code"
            num_elems = 8
        elif fileFormat == "bmrb3":
            ChemShiftStartTag = "_Atom_chem_shift.Assigned_chem_shift_list_ID"
            num_elems = 24
        ChemShiftEndTag = "stop_"
            
        ChemShiftStartFlag = 0
        ChemShiftEndFlag = 0
        
        for line in fin:
            if not line.strip(): continue
            line = line.strip()
            if not ChemShiftStartFlag:
                if line.startswith(ChemShiftStartTag):
                    ChemShiftStartFlag = 1
            if ChemShiftStartFlag and not ChemShiftEndFlag:
                
                if line[0].isdigit() and len(line.split()) >= num_elems:
                    atomLines.append(line)
            if ChemShiftStartFlag and not ChemShiftEndFlag == 1 and line.startswith(ChemShiftEndTag):    
                ChemShiftEndFlag = 1
                break
     
    if fileFormat == "bmrb2":
        
        percent_complete_sc_shifts, percent_complete_sc_proton_shifts=side_chain_atoms_stats_bmrb2(atomLines)
        #backbone_atoms_stats_bmrb2(atomLines)
    elif fileFormat == "bmrb3":    
        percent_complete_sc_shifts, percent_complete_sc_proton_shifts=side_chain_atoms_stats_bmrb3(atomLines)
        #backbone_atoms_stats_bmrb3(atomLines)
    print(percent_complete_sc_shifts)
main()
