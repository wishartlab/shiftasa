#!/usr/bin/python3

import operator
from collections import *
import os
from os import getcwd, listdir
from os.path import isfile, join
from sys import argv

resmap = {'A' : 'ALA','R' : 'ARG','N' : 'ASN','D' : 'ASP','B' : 'ASX','C' : 'CYS', 'a':'CYS', 'b':'CYS', 'c':'CYS', 'Q' : 'GLN','E' : 'GLU','Z' : 'GLX','G' : 'GLY','H' : 'HIS','I' : 'ILE','L' : 'LEU','K' : 'LYS','M' : 'MET','F' : 'PHE','P' : 'PRO','S' : 'SER','T' : 'THR','W' : 'TRP','Y' : 'TYR','V' : 'VAL','X' : 'PCA'}

kyte_doolittle_hydro_scale={'ILE':4.5, 'PHE':2.8, 'VAL':4.2, 'LEU':3.8, 'TRP':-0.9, 'MET':1.9, 'ALA':1.8, 'GLY': -0.4, 'CYS':2.5, 'TYR': -1.3, 'PRO': -1.6, 'THR':-0.7, 'SER':-0.8, 'HIS':-3.2, 'GLU':-3.5, 'ASN':-3.5, 'GLN':3.5, 'ASP':-3.5, 'LYS':-3.9, 'ARG':-4.5}

eisenberg_weiss_hydro_scale={'ILE':0.73, 'PHE':0.61, 'VAL':0.54, 'LEU':0.53, 'TRP':0.37, 'MET':0.26, 'ALA':0.25, 'GLY': 0.16, 'CYS':0.04, 'TYR': 0.02, 'PRO': -0.07, 'THR':-0.18, 'SER':-0.26, 'HIS':-0.40, 'GLU':-0.62, 'ASN':-0.64, 'GLN':-0.69, 'ASP':-0.72, 'LYS':-1.10, 'ARG':-1.80}

engleman_hydro_scale={'ILE':3.1, 'PHE':3.7, 'VAL':2.6, 'LEU':2.8, 'TRP':1.9, 'MET':3.4, 'ALA':1.6, 'GLY': 1.0, 'CYS':2.0, 'TYR': -0.7, 'PRO': -0.2, 'THR':1.2, 'SER':0.6, 'HIS':-3.0, 'GLU':-8.2, 'ASN':-4.8, 'GLN':-4.1, 'ASP':-9.2, 'LYS':-8.8, 'ARG':-12.3}

hoop_woods_hydro_scale={'ILE':-1.8, 'PHE':-2.5, 'VAL':-1.5, 'LEU':-1.8, 'TRP':-3.4, 'MET':-1.3, 'ALA':-0.5, 'GLY': 0.0, 'CYS':-1.0, 'TYR': -2.3, 'PRO': 0.0, 'THR':-0.4, 'SER':0.3, 'HIS':-0.5, 'GLU':3.0, 'ASN':0.2, 'GLN':0.2, 'ASP':3.0, 'LYS':3.0, 'ARG':3.0}

janin_hydro_scale={'ILE':0.7, 'PHE':0.5, 'VAL':0.6, 'LEU':0.5, 'TRP':0.3, 'MET':0.4, 'ALA':-0.3, 'GLY': 0.3, 'CYS':0.9, 'TYR': -0.4, 'PRO': -0.3, 'THR':-0.2, 'SER':-0.1, 'HIS':-0.1, 'GLU':-0.7, 'ASN':-0.5, 'GLN':-0.7, 'ASP':-0.6, 'LYS':-1.8, 'ARG':-1.4}

#Reference: Manavalan P. and Ponnuswamy P.K. Hydrophobic character of amino acid residues in globular proteins. Nature 1978,  275:673-674 ~~ Surrounding hydrophobicity scale 
ponnuswamy_scaleA= {'ILE':15.67, 'PHE':14.0, 'VAL':15.71, 'LEU':14.90, 'TRP':13.93, 'MET':14.39, 'ALA':12.97, 'GLY': 12.43, 'CYS':14.63, 'TYR': 13.42, 'PRO': 11.37, 'THR':11.69, 'SER':11.23, 'HIS':12.16, 'GLU':11.89, 'ASN':11.42, 'GLN':11.76, 'ASP':10.85, 'LYS':11.36, 'ARG':11.72}

ponnuswamy_aminoacid_scale= {'ILE':3.05, 'PHE':2.77, 'VAL':1.77, 'LEU':2.07, 'TRP':3.67, 'MET':1.57, 'ALA':0.77, 'GLY': 0.0, 'CYS':1.42, 'TYR': 2.57, 'PRO': 2.67, 'THR':-0.03, 'SER':-0.03, 'HIS':0.70, 'GLU':0.57, 'ASN':-0.01, 'GLN':-0.10, 'ASP':0.56, 'LYS':1.54, 'ARG':0.75}

# modified Surrounding hydrophobicity scale 
# Reference: P.K. PONNUSWAMY and M. MICHAEL GROMIHA, Prediction of transmembrane helices from hydrophobic characteristics of proteins, Int. J. Peplide Prorein Res. 42. 1993. 326-341
ponnuswamy_scaleB= {'ILE':15.34, 'PHE':13.89, 'VAL':14.73, 'LEU':14.19, 'TRP':13.96, 'MET':13.62, 'ALA':13.05, 'GLY': 12.20, 'CYS':14.30, 'TYR': 13.57, 'PRO': 11.06, 'THR':12.12, 'SER':11.68, 'HIS':12.42, 'GLU':11.41, 'ASN':11.72, 'GLN':11.78, 'ASP':11.10, 'LYS':11.01, 'ARG':12.40}


'''
# training set mean and std. dev of features 
mean= {'RCI(i+1)': 0.05621029, 'coilCSProb(i+1)': 0.36248352, 'RCI(i)': 0.05529781, 'hydro(i-1)': -0.21354353, 'helixCSProb(i)': 0.29771849, 'helixCSProb(i+1)': 0.29866462, 'hydro(i)': -0.21705482, 'coilCSProb(i-1)': 0.36235141, 'coilCSProb(i)': 0.36252843, 'strandCSProb(i)': 0.33975307, 'helixCSProb(i-1)': 0.29743289, 'strandCSProb(i+1)': 0.33885185, 'RCI(i-1)': 0.0547349, 'strandCSProb(i-1)': 0.34021569, 'hydro(i+1)': -0.21744894}
stdev={'RCI(i+1)': 0.07534252, 'coilCSProb(i+1)': 0.31680383, 'RCI(i)': 0.07282048, 'hydro(i-1)': 0.71084988, 'helixCSProb(i)': 0.39603871, 'helixCSProb(i+1)': 0.39591505, 'hydro(i)': 0.71269296, 'coilCSProb(i-1)': 0.3174993, 'coilCSProb(i)': 0.31706449, 'strandCSProb(i)': 0.36609803, 'helixCSProb(i-1)': 0.39631364, 'strandCSProb(i+1)': 0.36610553, 'RCI(i-1)': 0.07157887, 'strandCSProb(i-1)': 0.36637018, 'hydro(i+1)': 0.71310825}
'''
           
# training set mean and std. dev of features 
mean= {'RCI(i+1)': 0.05740456, 'coilCSProb(i+1)': 0.36615536, 'RCI(i)': 0.05648017, 'hydro(i-1)': -0.20903674, 'helixCSProb(i)': 0.28579420, 'helixCSProb(i+1)': 0.28685674, 'hydro(i)': -0.21343926, 'coilCSProb(i-1)': 0.36616302, 'coilCSProb(i)': 0.36642401, 'strandCSProb(i)': 0.34778179, 'helixCSProb(i-1)': 0.28562472, 'strandCSProb(i+1)': 0.34698789, 'SideChainRCI(i-1)': 0.21537067, 'SideChainRCI(i)': 0.21597451, 'SideChainRCI(i+1)': 0.21716012, 'RCI(i-1)': 0.05593949, 'strandCSProb(i-1)': 0.34821226, 'hydro(i+1)': -0.21373717, 'scon(i-1)':0.46916838, 'scon(i)': 0.46926395, 'scon(i+1)':0.46882342, 'sable(i-1)':0.22955975, 'sable(i)': 0.22965905, 'sable(i+1)': 0.22939424}
          
stdev={'RCI(i+1)': 0.07740708, 'coilCSProb(i+1)': 0.31659212, 'RCI(i)': 0.07474682, 'hydro(i-1)': 0.71280235, 'helixCSProb(i)': 0.39098935, 'helixCSProb(i+1)': 0.39086484, 'hydro(i)': 0.71481817, 'coilCSProb(i-1)': 0.31735203, 'coilCSProb(i)': 0.31704942, 'strandCSProb(i)': 0.36675251, 'helixCSProb(i-1)': 0.39123409, 'strandCSProb(i+1)': 0.36677854, 'SideChainRCI(i-1)': 0.16072919, 'SideChainRCI(i)': 0.16233807 , 'SideChainRCI(i+1)': 0.16577299,'RCI(i-1)': 0.07344419, 'strandCSProb(i-1)': 0.36699853, 'hydro(i+1)':  0.71484262,'scon(i-1)':0.15723403, 'scon(i)': 0.15710835, 'scon(i+1)':0.15771190, 'sable(i-1)': 0.16868238, 'sable(i)': 0.16916483, 'sable(i+1)': 0.16938708}

#reading the command line arguments
bmrbid = argv[1]
flag = argv[2]
scrci_flag = argv[3]

tabFile = bmrbid+'.tab'
ssProbFile= bmrbid+'.ss_prob_seccs'
rciFile = "".join(f for f in listdir(getcwd()) if f.startswith(bmrbid) and f.endswith(".RCI.txt"))
sidechainRCIFile = "".join(f for f in listdir(getcwd()) if f.startswith(bmrbid) and f.endswith("Side_chain_RCI.txt"))
sconFile = bmrbid+'.jsscon'
sableFile = bmrbid+'.sable'

#print(rciFile)
scoutFile= bmrbid+'.scrcifeat'
bboutFile= bmrbid+'.bbrcifeat'
bbout1resFile= bmrbid+'.bbrcifeat1res'
sableoutFile = bmrbid+'.sablefeat'
sablescrciFile = bmrbid+'.sablescrcifeat'

resnList = []
AAList = dict()
rciDict=dict()
sconDict = dict()
helixmomentDict = dict()
betamomentDict = dict()
avgSurroundingHydroIndex = dict()
strandCSProbDict=dict()
helixCSProbDict=dict()
coilCSProbDict=dict()
sidechainrciDict=dict()
sableDict=dict()

fbb1res = open(bbout1resFile, 'w')
fbb1res.write("resn AA hydro(i) strandCSProb(i) helixCSProb(i) coilCSProb(i) RCI(i) scon(i)\n")

fbb = open(bboutFile, 'w')
    

fbb.write("resn AA hydro(i-1) hydro(i) hydro(i+1) strandCSProb(i-1) strandCSProb(i) strandCSProb(i+1) helixCSProb(i-1) helixCSProb(i) helixCSProb(i+1) coilCSProb(i-1) coilCSProb(i) coilCSProb(i+1) RCI(i-1) RCI(i) RCI(i+1) scon(i-1) scon(i) scon(i+1)\n")

    
if scrci_flag == "yes":
    fsc = open(scoutFile, 'w')
    fsc.write("resn AA hydro(i-1) hydro(i) hydro(i+1) strandCSProb(i-1) strandCSProb(i) strandCSProb(i+1) helixCSProb(i-1) helixCSProb(i) helixCSProb(i+1) coilCSProb(i-1) coilCSProb(i) coilCSProb(i+1) RCI(i-1) RCI(i) RCI(i+1) sidechainRCI(i-1) sidechainRCI(i) sidechainRCI(i+1) scon(i-1) scon(i) scon(i+1)\n")

if flag == "sable":
    fsable = open(sableoutFile, 'w')
    fsable.write("resn AA hydro(i-1) hydro(i) hydro(i+1) strandCSProb(i-1) strandCSProb(i) strandCSProb(i+1) helixCSProb(i-1) helixCSProb(i) helixCSProb(i+1) coilCSProb(i-1) coilCSProb(i) coilCSProb(i+1) RCI(i-1) RCI(i) RCI(i+1) sable(i-1) sable(i) sable(i+1) scon(i-1) scon(i) scon(i+1)\n")
    
elif flag == "sable_scrci":
    fscsable = open(sablescrciFile, 'w')
    fscsable.write("resn AA hydro(i-1) hydro(i) hydro(i+1) strandCSProb(i-1) strandCSProb(i) strandCSProb(i+1) helixCSProb(i-1) helixCSProb(i) helixCSProb(i+1) coilCSProb(i-1) coilCSProb(i) coilCSProb(i+1) RCI(i-1) RCI(i) RCI(i+1) sidechainRCI(i-1) sidechainRCI(i) sidechainRCI(i+1) sable(i-1) sable(i) sable(i+1) scon(i-1) scon(i) scon(i+1)\n")


with open(tabFile, "r") as fin:
    for line in fin:
        if line.startswith("SEQ"):
            elems = line.split()
            for elem in elems:
                if elem.isalnum() and (elem != "0" and elem!="SEQ"):
                   resnList.append(int(elem[:-3]))

if flag == "sable" or flag=="sable_scrci":
    #reading sable ASA prediction
    with open(sableFile, "r") as fin:
        for line in fin:
            if not line.strip(): continue
            line = line.strip()
        
        
            if line.startswith("Query"):
                
                sa_start = 0
                sapred_start = 0
                sa_end = 0
                rsaList = []
                aaList = []
                
            if not sa_end and line.startswith("SECTION_SA"):
                sa_start = 1
                
            if sa_start and line.startswith(">"):
                sapred_start = 1
                
            if sapred_start and line[0].isalpha():
                aaList+=[resmap[i] for i in list(line)]
                
            if sapred_start and line[0].isdigit():
                rsaList+=[float(i)/10 for i in list(line)]
                sapred_start = 0
                
            if sa_start and line.startswith("END_SECTION"):
                
                sa_start = 0
                sapred_start = 0
                sa_end = 1

    for i in range(len(resnList)):
        sableDict[resnList[i]] = rsaList[i]
    #print(sableDict)    


fin1 = open(ssProbFile, "r")        

resnList = []
# retrieving seconday structure probability calculated from chemical shift    
for line in fin1:
    tmp = line.split()
    resn = int(tmp[0]) 
    AA = tmp[1] 
    strandCSProb = float(tmp[2])
    helixCSProb = float(tmp[3])
    coilCSProb = float(tmp[4])
    resnList.append(int(resn))
    AAList[resn] = AA
    strandCSProbDict[resn]= float(strandCSProb)
    helixCSProbDict[resn]= float(helixCSProb)
    coilCSProbDict[resn]= float(coilCSProb)
    

				
# calculating the running average hydrophobicity in 3-residue window
for i in range(len(resnList)):
    if i == 0:
        
        avgSurroundingHydroIndex[resnList[i]] = (ponnuswamy_scaleB[AAList[resnList[i]]] + ponnuswamy_scaleB[AAList[resnList[i+1]]])/2
    elif i == len(resnList)-1:
        
        avgSurroundingHydroIndex[resnList[i]] = (ponnuswamy_scaleB[AAList[resnList[i-1]]] + ponnuswamy_scaleB[AAList[resnList[i]]])/2    
    else:    
        avgSurroundingHydroIndex[resnList[i]] = (ponnuswamy_scaleB[AAList[resnList[i-1]]] + ponnuswamy_scaleB[AAList[resnList[i]]] + ponnuswamy_scaleB[AAList[resnList[i+1]]] )/3
    
    

if os.path.exists(rciFile):
    
    print(rciFile)
    fin2 = open(rciFile, "r")
    # retrieving backbone RCI value
    for line in fin2:
        (resn, rci, AA) = line.split()
        rciDict[int(resn)] = float(rci)

if os.path.exists(sidechainRCIFile):    
    fin3 = open(sidechainRCIFile, "r")
    # retrieving side-chain RCI value
    for line in fin3:
        (resn, rci, AA) = line.split()
        sidechainrciDict[int(resn)] = float(rci) 
        
            # filling up the GLY residue prediction
    for key in rciDict.keys():
        if (key in rciDict) and (key not in sidechainrciDict):
            sidechainrciDict[key] = rciDict[key] 
            
    
fin4 = open(sconFile, "r")
# retrieving per-residue conservation score
for line in fin4:
    if line.strip() and not line.startswith("#"):
        (resn, AA, score) = line.split()
        sconDict[int(resn)] = float(score)
       
# ordering the residue number in the rci dictionary    
rciDictOrdered= OrderedDict(sorted(rciDict.items(), key=operator.itemgetter(0)))    
    
#print(len(rciDictOrdered.keys()),'...........')
for key in rciDictOrdered.keys():
        currentRes = key
        previousRes = key - 1
        nextRes = key + 1
        
        
        if currentRes in rciDictOrdered:
            hydroCurrent = janin_hydro_scale[AAList[currentRes]]
            strandCSProbCurrent = strandCSProbDict[currentRes]
            helixCSProbCurrent = helixCSProbDict[currentRes]
            coilCSProbCurrent= coilCSProbDict[currentRes]
            rciCurrent= rciDict[currentRes]
            sconCurrent= sconDict[currentRes]
            
            fbb1res.write("%d %s %f %f %f %f %f %f\n"%(currentRes, AAList[currentRes], hydroCurrent,strandCSProbCurrent, helixCSProbCurrent,coilCSProbCurrent, rciCurrent, sconCurrent))
            
            
        if (previousRes in rciDictOrdered and currentRes in rciDictOrdered and nextRes in rciDictOrdered) :
            # feature z-score normalization
            hydroPrev = janin_hydro_scale[AAList[previousRes]]
            hydroCurrent = janin_hydro_scale[AAList[currentRes]]
            hydroNext = janin_hydro_scale[AAList[nextRes]]
            
            strandCSProbPrev = strandCSProbDict[previousRes]
            strandCSProbCurrent = strandCSProbDict[currentRes]
            strandCSProbNext = strandCSProbDict[nextRes]
            
            helixCSProbPrev = helixCSProbDict[previousRes]
            helixCSProbCurrent = helixCSProbDict[currentRes]
            helixCSProbNext = helixCSProbDict[nextRes]
            
            coilCSProbPrev= coilCSProbDict[previousRes] 
            coilCSProbCurrent= coilCSProbDict[currentRes]
            coilCSProbNext=coilCSProbDict[nextRes]
            
            
            rciPrev= rciDict[previousRes]
            rciCurrent= rciDict[currentRes]
            rciNext= rciDict[nextRes]

            
            # conservation score
            sconPrev= sconDict[previousRes]
            sconCurrent= sconDict[currentRes]
            sconNext= sconDict[nextRes]
           
            
            #fout.write("%d %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n"%(currentRes, AAList[currentRes], hydroPrev, hydroCurrent, hydroNext, strandCSProbPrev , strandCSProbCurrent, strandCSProbNext, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext, rciPrev, rciCurrent, rciNext, sconPrev, sconCurrent, sconNext, helixmomentPrev, helixmomentCurrent, helixmomentNext, betamomentPrev, betamomentCurrent, betamomentNext))
            
            fbb.write("%d %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n"%(currentRes, AAList[currentRes], hydroPrev, hydroCurrent, hydroNext, strandCSProbPrev , strandCSProbCurrent, strandCSProbNext, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext, rciPrev, rciCurrent, rciNext, sconPrev, sconCurrent, sconNext))
            
            if  scrci_flag == "yes":    
                if (previousRes in sidechainrciDict and currentRes in sidechainrciDict and nextRes in sidechainrciDict):
                    #print(currentRes)
                    sidechainrciPrev= sidechainrciDict[previousRes]
                    sidechainrciCurrent= sidechainrciDict[currentRes]
                    sidechainrciNext= sidechainrciDict[nextRes]
                    
        
                    fsc.write("%d %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n"%(currentRes, AAList[currentRes], hydroPrev, hydroCurrent, hydroNext, strandCSProbPrev , strandCSProbCurrent, strandCSProbNext, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext, rciPrev, rciCurrent, rciNext, sidechainrciPrev, sidechainrciCurrent, sidechainrciNext, sconPrev, sconCurrent, sconNext))
                
            if flag == "sable": #backbone + sable    
                if (previousRes in sableDict and currentRes in sableDict and nextRes in sableDict):
                
                    sablePrev= sableDict[previousRes]
                    sableCurrent= sableDict[currentRes]
                    sableNext= sableDict[nextRes]
                    
        
                    fsable.write("%d %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n"%(currentRes, AAList[currentRes], hydroPrev, hydroCurrent, hydroNext, strandCSProbPrev , strandCSProbCurrent, strandCSProbNext, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext, rciPrev, rciCurrent, rciNext, sablePrev, sableCurrent, sableNext, sconPrev, sconCurrent, sconNext))     
        
            elif flag == "sable_scrci": # backbone + side-chain + sable   
                if ((previousRes in sidechainrciDict and currentRes in sidechainrciDict and nextRes in sidechainrciDict) and (previousRes in sableDict and currentRes in sableDict and nextRes in sableDict)):
                
                    sidechainrciPrev= sidechainrciDict[previousRes]
                    sidechainrciCurrent= sidechainrciDict[currentRes]
                    sidechainrciNext= sidechainrciDict[nextRes]
                    
                    sablePrev= sableDict[previousRes]
                    sableCurrent= sableDict[currentRes]
                    sableNext= sableDict[nextRes]
                    
        
                    fscsable.write("%d %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n"%(currentRes, AAList[currentRes], hydroPrev, hydroCurrent, hydroNext, strandCSProbPrev , strandCSProbCurrent, strandCSProbNext, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext, rciPrev, rciCurrent, rciNext, sidechainrciPrev, sidechainrciCurrent, sidechainrciNext, sablePrev, sableCurrent, sableNext, sconPrev, sconCurrent, sconNext))  
    


