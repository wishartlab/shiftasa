function hypertext_1() {
canvas = document.getElementById("hypertext_1");
ctx = canvas.getContext("2d");
// Suppress refresh on mouseover if this was the plot we just left
if ((gnuplot.active_plot == hypertext_1 && gnuplot.display_is_uptodate)) return;
else gnuplot.display_is_uptodate = true;
// Reinitialize mouse tracking and zoom for this particular plot
if ((typeof(gnuplot.active_plot) == "undefined" || gnuplot.active_plot != hypertext_1)  &&  typeof(gnuplot.mouse_update) != "undefined") {
  gnuplot.active_plot_name = "hypertext_1";
  gnuplot.active_plot = hypertext_1;
  canvas.onmousemove = gnuplot.mouse_update;
  canvas.onmouseup = gnuplot.zoom_in;
  canvas.onmousedown = gnuplot.saveclick;
  canvas.onkeypress = gnuplot.do_hotkey;
  if (canvas.attachEvent) {canvas.attachEvent('mouseover', hypertext_1);}
  else if (canvas.addEventListener) {canvas.addEventListener('mouseover', hypertext_1, false);} 
  gnuplot.zoomed = false;
  gnuplot.zoom_axis_width = 0;
  gnuplot.zoom_in_progress = false;
  gnuplot.polar_mode = false;
  ctx.clearRect(0,0,600,400);
}
// Gnuplot version 5.0.0
// short forms of commands provided by gnuplot_common.js
function DT  (dt)  {gnuplot.dashtype(dt);};
function DS  (x,y) {gnuplot.dashstart(x,y);};
function DL  (x,y) {gnuplot.dashstep(x,y);};
function M   (x,y) {if (gnuplot.pattern.length > 0) DS(x,y); else gnuplot.M(x,y);};
function L   (x,y) {if (gnuplot.pattern.length > 0) DL(x,y); else gnuplot.L(x,y);};
function Dot (x,y) {gnuplot.Dot(x/10.,y/10.);};
function Pt  (N,x,y,w) {gnuplot.Pt(N,x/10.,y/10.,w/10.);};
function R   (x,y,w,h) {gnuplot.R(x,y,w,h);};
function T   (x,y,fontsize,justify,string) {gnuplot.T(x,y,fontsize,justify,string);};
function TR  (x,y,angle,fontsize,justify,string) {gnuplot.TR(x,y,angle,fontsize,justify,string);};
function bp  (x,y) {gnuplot.bp(x,y);};
function cfp () {gnuplot.cfp();};
function cfsp() {gnuplot.cfsp();};

gnuplot.hypertext_list = [];
gnuplot.on_hypertext = -1;
function Hypertext(x,y,w,text) {
    newtext = {x:x, y:y, w:w, text:text};
    gnuplot.hypertext_list.push(newtext);
}
gnuplot.dashlength = 400;
ctx.lineCap = "butt"; ctx.lineJoin = "miter";
CanvasTextFunctions.enable(ctx);
ctx.strokeStyle = "rgb(215,215,215)";
ctx.lineWidth = 1;

ctx.lineWidth = 1;
ctx.strokeStyle = "rgb(000,000,000)";
ctx.fillStyle = "rgb(000,000,000)";
T(2979,201,10.0,"Center","Hypertext is shown when the mouse is over a point");
if (typeof(gnuplot.hide_hypertext_1_plot_1) == "undefined"|| !gnuplot.hide_hypertext_1_plot_1) {
ctx.strokeStyle = "rgb(255,238,153)";
ctx.fillStyle = "rgb(255,238,153)";
Pt(6,3184,1094,183.0);
Hypertext(3184,1094,183.0,"Parispop: 2110420");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4059,3568,144.5);
Hypertext(4059,3568,144.5,"Marseillepop: 820729");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3903,2476,123.9);
Hypertext(3903,2476,123.9,"Lyonpop: 443859");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2928,3430,121.6);
Hypertext(2928,3430,121.6,"Toulousepop: 411768");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4608,3394,115.2);
Hypertext(4608,3394,115.2,"Nicepop: 331958");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2056,1821,110.6);
Hypertext(2056,1821,110.6,"Nantespop: 282251");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4749,1219,109.7);
Hypertext(4749,1219,109.7,"Strasbourgpop: 272599");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3626,3434,105.2);
Hypertext(3626,3434,105.2,"Montpellierpop: 230663");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2342,2886,103.6);
Hypertext(2342,2886,103.6,"Bordeauxpop: 216978");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2024,1429,103.1);
Hypertext(2024,1429,103.1,"Rennespop: 212616");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2544,809,100.1);
Hypertext(2544,809,100.1,"Le Havrepop: 189329");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3672,921,99.5);
Hypertext(3672,921,99.5,"Reimspop: 184594");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3395,301,98.9);
Hypertext(3395,301,98.9,"Lillepop: 180547");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3776,2623,97.6);
Hypertext(3776,2623,97.6,"Saint-Étiennepop: 170734");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4218,3648,96.2);
Hypertext(4218,3648,96.2,"Toulonpop: 161160");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4160,2730,95.3);
Hypertext(4160,2730,95.3,"Grenoblepop: 155621");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2353,1709,95.0);
Hypertext(2353,1709,95.0,"Angerspop: 153340");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,1210,1304,94.7);
Hypertext(1210,1304,94.7,"Brestpop: 151526");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2567,1478,94.3);
Hypertext(2567,1478,94.3,"Le Manspop: 148654");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3961,1776,94.0);
Hypertext(3961,1776,94.0,"Dijonpop: 147153");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4079,3470,92.5);
Hypertext(4079,3470,92.5,"Aix-en-Provencepop: 138060");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3398,2467,92.4);
Hypertext(3398,2467,92.4,"Clermont-Ferrandpop: 137274");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3765,3332,92.2);
Hypertext(3765,3332,92.2,"N̨îmespop: 136424");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3173,631,92.1);
Hypertext(3173,631,92.1,"Amienspop: 135768");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2708,1754,91.8);
Hypertext(2708,1754,91.8,"Tourspop: 133538");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2870,2445,91.5);
Hypertext(2870,2445,91.5,"Limogespop: 132299");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4293,978,90.3);
Hypertext(4293,978,90.3,"Metzpop: 125010");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3918,2472,89.7);
Hypertext(3918,2472,89.7,"Villeurbannepop: 121852");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4247,1816,89.0);
Hypertext(4247,1816,89.0,"Besançonpop: 118152");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2405,947,88.6);
Hypertext(2405,947,88.6,"Caenpop: 116259");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3057,1522,87.9);
Hypertext(3057,1522,87.9,"Orléanspop: 112327");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4628,1585,87.7);
Hypertext(4628,1585,87.7,"Mulhousepop: 111307");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3343,3840,87.4);
Hypertext(3343,3840,87.4,"Perpignanpop: 110149");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3156,1108,87.0);
Hypertext(3156,1108,87.0,"Boulogne-Billancourtpop: 108079");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2821,836,86.5);
Hypertext(2821,836,86.5,"Rouenpop: 105310");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,4290,1170,86.0);
Hypertext(4290,1170,86.0,"Nancypop: 103196");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3897,3278,82.4);
Hypertext(3897,3278,82.4,"Avignonpop: 86865");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3144,1077,82.1);
Hypertext(3144,1077,82.1,"Nanterrepop: 85753");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,2607,2111,82.1);
Hypertext(2607,2111,82.1,"Poitierspop: 85406");
ctx.strokeStyle = "rgb(000,000,000)";
ctx.strokeStyle = "rgb(255,238,153)";
Pt(6,3127,1117,81.3);
Hypertext(3127,1117,81.3,"Versaillespop: 82101");
ctx.strokeStyle = "rgb(000,000,000)";
} // End hypertext_1_plot_1 
if (typeof(gnuplot.hide_hypertext_1_plot_2) == "undefined"|| !gnuplot.hide_hypertext_1_plot_2) {
ctx.lineWidth = 1;
ctx.strokeStyle = "rgb(000,000,000)";
DT(gnuplot.solid);
ctx.lineWidth = 0.1;
Pt(5,3184,1094,183.0);
Hypertext(3184,1094,183.0,"Parispop: 2110420");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4059,3568,144.5);
Hypertext(4059,3568,144.5,"Marseillepop: 820729");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3903,2476,123.9);
Hypertext(3903,2476,123.9,"Lyonpop: 443859");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2928,3430,121.6);
Hypertext(2928,3430,121.6,"Toulousepop: 411768");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4608,3394,115.2);
Hypertext(4608,3394,115.2,"Nicepop: 331958");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2056,1821,110.6);
Hypertext(2056,1821,110.6,"Nantespop: 282251");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4749,1219,109.7);
Hypertext(4749,1219,109.7,"Strasbourgpop: 272599");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3626,3434,105.2);
Hypertext(3626,3434,105.2,"Montpellierpop: 230663");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2342,2886,103.6);
Hypertext(2342,2886,103.6,"Bordeauxpop: 216978");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2024,1429,103.1);
Hypertext(2024,1429,103.1,"Rennespop: 212616");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2544,809,100.1);
Hypertext(2544,809,100.1,"Le Havrepop: 189329");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3672,921,99.5);
Hypertext(3672,921,99.5,"Reimspop: 184594");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3395,301,98.9);
Hypertext(3395,301,98.9,"Lillepop: 180547");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3776,2623,97.6);
Hypertext(3776,2623,97.6,"Saint-Étiennepop: 170734");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4218,3648,96.2);
Hypertext(4218,3648,96.2,"Toulonpop: 161160");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4160,2730,95.3);
Hypertext(4160,2730,95.3,"Grenoblepop: 155621");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2353,1709,95.0);
Hypertext(2353,1709,95.0,"Angerspop: 153340");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,1210,1304,94.7);
Hypertext(1210,1304,94.7,"Brestpop: 151526");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2567,1478,94.3);
Hypertext(2567,1478,94.3,"Le Manspop: 148654");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3961,1776,94.0);
Hypertext(3961,1776,94.0,"Dijonpop: 147153");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4079,3470,92.5);
Hypertext(4079,3470,92.5,"Aix-en-Provencepop: 138060");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3398,2467,92.4);
Hypertext(3398,2467,92.4,"Clermont-Ferrandpop: 137274");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3765,3332,92.2);
Hypertext(3765,3332,92.2,"N̨îmespop: 136424");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3173,631,92.1);
Hypertext(3173,631,92.1,"Amienspop: 135768");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2708,1754,91.8);
Hypertext(2708,1754,91.8,"Tourspop: 133538");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2870,2445,91.5);
Hypertext(2870,2445,91.5,"Limogespop: 132299");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4293,978,90.3);
Hypertext(4293,978,90.3,"Metzpop: 125010");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3918,2472,89.7);
Hypertext(3918,2472,89.7,"Villeurbannepop: 121852");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4247,1816,89.0);
Hypertext(4247,1816,89.0,"Besançonpop: 118152");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2405,947,88.6);
Hypertext(2405,947,88.6,"Caenpop: 116259");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3057,1522,87.9);
Hypertext(3057,1522,87.9,"Orléanspop: 112327");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4628,1585,87.7);
Hypertext(4628,1585,87.7,"Mulhousepop: 111307");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3343,3840,87.4);
Hypertext(3343,3840,87.4,"Perpignanpop: 110149");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3156,1108,87.0);
Hypertext(3156,1108,87.0,"Boulogne-Billancourtpop: 108079");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2821,836,86.5);
Hypertext(2821,836,86.5,"Rouenpop: 105310");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,4290,1170,86.0);
Hypertext(4290,1170,86.0,"Nancypop: 103196");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3897,3278,82.4);
Hypertext(3897,3278,82.4,"Avignonpop: 86865");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3144,1077,82.1);
Hypertext(3144,1077,82.1,"Nanterrepop: 85753");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,2607,2111,82.1);
Hypertext(2607,2111,82.1,"Poitierspop: 85406");
ctx.lineWidth = 1;
ctx.lineWidth = 0.1;
Pt(5,3127,1117,81.3);
Hypertext(3127,1117,81.3,"Versaillespop: 82101");
ctx.lineWidth = 1;
} // End hypertext_1_plot_2 
ctx.lineWidth = 2;
ctx.strokeStyle = "rgb(000,000,000)";
DT(gnuplot.solid);
ctx.lineWidth = 1;

// plot boundaries and axis scaling information for mousing 
gnuplot.plot_term_xmax = 600;
gnuplot.plot_term_ymax = 400;
gnuplot.plot_xmin = 121.0;
gnuplot.plot_xmax = 474.9;
gnuplot.plot_ybot = 384.0;
gnuplot.plot_ytop = 30.1;
gnuplot.plot_width = 353.9;
gnuplot.plot_height = 353.9;
gnuplot.plot_axis_xmin = -4.5;
gnuplot.plot_axis_xmax = 7.76;
gnuplot.plot_axis_ymin = 42.7;
gnuplot.plot_axis_ymax = 50.64;
gnuplot.plot_axis_x2min = "none"
gnuplot.plot_axis_y2min = "none"
gnuplot.plot_logaxis_x = 0;
gnuplot.plot_logaxis_y = 0;
gnuplot.plot_axis_width = gnuplot.plot_axis_xmax - gnuplot.plot_axis_xmin;
gnuplot.plot_axis_height = gnuplot.plot_axis_ymax - gnuplot.plot_axis_ymin;
gnuplot.plot_timeaxis_x = "";
gnuplot.plot_timeaxis_y = "";
}
