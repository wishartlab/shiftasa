#!/usr/local/bin/python3
from collections import defaultdict
from collections import *
from os import listdir
from os.path import isfile, join
import operator
import os.path
import sys

three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C', 
             'GLN' : 'Q','GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K','MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y','VAL' : 'V','PCA' : 'X'}
RandomCoil = {'GLY_HN' :  8.33,'GLY_N' :  107.5,'GLY_HA' :  4.02,'GLY_CA' :  45.4,'GLY_CB' :  0.0,'GLY_C' :  174.9,'GLY_HN' : 8.41,'ALA_HN' :  8.35,'ALA_N' :  125.0,'ALA_HA' :  4.35,'ALA_CA' :  52.8,'ALA_CB' :  19.3,'ALA_C' :  178.5, 'CYS_HN' :  8.44,'CYS_N' :  118.8,'CYS_HA' :  4.59,'CYS_CA' :  58.6,'CYS_CB' :  28.3,'CYS_C' :  175.3,
		  'ASP_HN' :  8.56,'ASP_N' :  119.1,'ASP_HA' :  4.82,'ASP_CA' :  53.0,'ASP_CB' :  38.3,'ASP_C' :  175.9,
		  'GLU_HN' :  8.40,'GLU_N' :  120.2,'GLU_HA' :  4.42,'GLU_CA' :  56.1,'GLU_CB' :  29.9,'GLU_C' :  176.8,
		  'PHE_HN' :  8.31,'PHE_N' :  120.7,'PHE_HA' :  4.65,'PHE_CA' :  58.1,'PHE_CB' :  39.8,'PHE_C' :  176.6,
		  'HIS_HN' :  8.56,'HIS_N' :  118.1,'HIS_HA' :  4.79,'HIS_CA' :  55.4,'HIS_CB' :  29.1,'HIS_C' :  175.1,
		  'ILE_HN' :  8.17,'ILE_N' :  120.4,'ILE_HA' :  4.21,'ILE_CA' :  61.6,'ILE_CB' :  38.9,'ILE_C' :  177.1,
		  'LYS_HN' :  8.36,'LYS_N' :  121.6,'LYS_HA' :  4.36,'LYS_CA' :  56.7,'LYS_CB' :  33.2,'LYS_C' :  177.4,
		  'LEU_HN' :  8.28,'LEU_N' :  122.4,'LEU_HA' :  4.38,'LEU_CA' :  55.5,'LEU_CB' :  42.5,'LEU_C' :  178.2,
		  'MET_HN' :  8.42,'MET_N' :  120.3,'MET_HA' :  4.52,'MET_CA' :  55.8,'MET_CB' :  32.9,'MET_C' :  177.1,
		  'ASN_HN' :  8.51,'ASN_N' :  119.0,'ASN_HA' :  4.79,'ASN_CA' :  53.3,'ASN_CB' :  39.1,'ASN_C' :  176.1,
		  'PRO_HN' :  0.0, 'PRO_N' :  0.0,  'PRO_HA' :  4.45,'PRO_CA' :  63.7,'PRO_CB' :  32.2,'PRO_C' :  177.8,
		  'GLN_HN' :  8.44,'GLN_N' :  120.5,'GLN_HA' :  4.38,'GLN_CA' :  56.2,'GLN_CB' :  29.5,'GLN_C' :  176.8,
		  'ARG_HN' :  8.39,'ARG_N' :  121.2,'ARG_HA' :  4.38,'ARG_CA' :  56.5,'ARG_CB' :  30.9,'ARG_C' :  177.1,
		  'SER_HN' :  8.43,'SER_N' :  115.5,'SER_HA' :  4.51,'SER_CA' :  58.7,'SER_CB' :  64.1,'SER_C' :  175.4,
		  'THR_HN' :  8.25,'THR_N' :  112.0,'THR_HA' :  4.43,'THR_CA' :  62.0,'THR_CB' :  70.0,'THR_C' :  175.6,
		  'VAL_HN' :  8.16,'VAL_N' :  119.3,'VAL_HA' :  4.16,'VAL_CA' :  62.6,'VAL_CB' :  31.8,'VAL_C' :  177.0,
		  'TRP_HN' :  8.22,'TRP_N' :  122.1,'TRP_HA' :  4.70,'TRP_CA' :  57.6,'TRP_CB' :  29.8,'TRP_C' :  177.1,
		  'TYR_HN' :  8.26,'TYR_N' :  120.9,'TYR_HA' :  4.58,'TYR_CA' :  58.3,'TYR_CB' :  38.9,'TYR_C' :  176.7}

nrc={'ALA_B_CA' : -0.17,'ALA_C_CA' : 0.06,'ALA_B_HA' : -0.03,'ALA_C_HA' : -0.03,'ALA_B_HN' : -0.05,'ALA_C_HN' : 0.07,'ALA_B_N' : -0.33,'ALA_C_N' : -0.57,
'ALA_B_C' : -0.77,'ALA_C_C' : -0.07,'ARG_B_CA' : -0.07,'ARG_C_CA' : -0.01,'ARG_B_HN' : -0.02,'ARG_C_HN' : 0.15,'ARG_B_HA' : -0.02,'ARG_C_HA' : -0.02,
'ARG_B_N' : -0.14,'ARG_C_N' : 1.62,'ARG_B_C' : -0.49,'ARG_C_C' : -0.19,'ASN_B_CA' : -0.03,'ASN_C_CA' : 0.23,'ASN_B_HN' : -0.03,'ASN_C_HN' : 0.13,
'ASN_B_HA' : -0.01,'ASN_C_HA' : -0.02,'ASN_B_N' : -0.26,'ASN_C_N' : 0.87,'ASN_B_C' : -0.66,'ASN_C_C' : -0.10,'ASP_B_CA' : 0.00,'ASP_C_CA' : 0.25,
'ASP_B_HN' : -0.03,'ASP_C_HN' : 0.14,'ASP_B_HA' : -0.01,'ASP_C_HA' : -0.02,'ASP_B_N' : -0.20,'ASP_C_N' : 0.86,'ASP_B_C' : -0.58,'ASP_C_C' : -0.13,
'CYS_B_CA' : -0.07,'CYS_C_CA' : 0.10,'CYS_B_HN' : -0.02,'CYS_C_HN' : 0.20,'CYS_B_HA' : 0.02,'CYS_C_HA' : 0.00,'CYS_B_N' : -0.26,'CYS_C_N' : 3.07,
'CYS_B_C' : -0.51,'CYS_C_C' : -0.28,'GLN_B_CA' : -0.06,'GLN_C_CA' : 0.04,'GLN_B_HN' : -0.02,'GLN_C_HN' : 0.15,'GLN_B_HA' : -0.02,'GLN_C_HA' : -0.01,
'GLN_B_N' : -0.14,'GLN_C_N' : 1.62,'GLN_B_C' : -0.48,'GLN_C_C' : -0.18,'GLU_B_CA' : -0.08,'GLU_C_CA' : 0.05,'GLU_B_HN' : -0.03,'GLU_C_HN' : 0.15,
'GLU_B_HA' : -0.02,'GLU_C_HA' : -0.02,'GLU_B_N' : -0.20,'GLU_C_N' : 1.51,'GLU_B_C' : -0.48,'GLU_C_C' : -0.20,'GLY_B_CA' : 0,'GLY_C_CA' : 0,'GLY_B_HN' : 0,
'GLY_C_HN' : 0,'GLY_B_HA' : 0,'GLY_C_HA' : 0,'GLY_B_N' : 0,'GLY_C_N' : 0,'GLY_B_C' : 0,'GLY_C_C' : 0,'HIS_B_CA' : -0.09,'HIS_C_CA' : 0.02,'HIS_B_HN' : -0.04,
'HIS_C_HN' : 0.20,'HIS_B_HA' : -0.06,'HIS_C_HA' : 0.01,'HIS_B_N' : -0.55,'HIS_C_N' : 1.68,'HIS_B_C' : -0.65,'HIS_C_C' : -0.22,'ILE_B_CA' : -0.20,'ILE_C_CA' : -0.01,
'ILE_B_HN' : -0.06,'ILE_C_HN' : 0.17,'ILE_B_HA' : -0.02,'ILE_C_HA' : -0.02,'ILE_B_N' : -0.14,'ILE_C_N' : 4.87,'ILE_B_C' : -0.58,'ILE_C_C' : -0.18,
'LEU_B_CA' : -0.10,'LEU_C_CA' : 0.03,'LEU_B_HN' : -0.03,'LEU_C_HN' : 0.14,'LEU_B_HA' : -0.03,'LEU_C_HA' : -0.05,'LEU_B_N' : -0.14,'LEU_C_N' : 1.05,
'LEU_B_C' : -0.50,'LEU_C_C' : -0.13,'LYS_B_CA' : -0.11,'LYS_C_CA' : -0.02,'LYS_B_HN' : -0.03,'LYS_C_HN' : 0.14,'LYS_B_HA' : -0.02,'LYS_C_HA' : -0.01,
'LYS_B_N' : -0.20,'LYS_C_N' : 1.57,'LYS_B_C' : -0.50,'LYS_C_C' : -0.18,'MET_B_CA' : -0.10,'MET_C_CA' : -0.06,'MET_B_HN' : -0.02,'MET_C_HN' : 0.15,
'MET_B_HA' : -0.01,'MET_C_HA' : -0.01,'MET_B_N' : -0.20,'MET_C_N' : 1.57,'MET_B_C' : -0.41,'MET_C_C' : -0.18,'PHE_B_CA' : -0.23,'PHE_C_CA' : 0.06,
'PHE_B_HN' : -0.12,'PHE_C_HN' : 0.10,'PHE_B_HA' : -0.09,'PHE_C_HA' : -0.08,'PHE_B_N' : -0.49,'PHE_C_N' : 2.78,'PHE_B_C' : -0.83,'PHE_C_C' : -0.25,
'PRO_B_CA' : -2.0,'PRO_C_CA' : 0.02,'PRO_B_HN' : -0.18,'PRO_C_HN' : 0.19,'PRO_B_HA' : 0.11,'PRO_C_HA' : -0.03,'PRO_B_N' : -0.32,'PRO_C_N' : 0.87,'PRO_B_C' : -2.84,
'PRO_C_C' : -0.09,'SER_B_CA' : -0.08,'SER_C_CA' : 0.13,'SER_B_HN' : -0.03,'SER_C_HN' : 0.16,'SER_B_HA' : 0.02,'SER_C_HA' : 0.00,'SER_B_N' : -0.03,
'SER_C_N' : 2.55,'SER_B_C' : -0.40,'SER_C_C' : -0.15,'THR_B_CA' : -0.04,'THR_C_CA' : 0.12,'THR_B_HN' : 0.00,'THR_C_HN' : 0.14,'THR_B_HA' : 0.05,
'THR_C_HA' : 0.00,'THR_B_N' : -0.03,'THR_C_N' : 2.78,'THR_B_C' : -0.19,'THR_C_C' : -0.13,'TRP_B_CA' : -0.17,'TRP_C_CA' : 0.03,'TRP_B_HN' : -0.13,
'TRP_C_HN' : 0.04,'TRP_B_HA' : -0.10,'TRP_C_HA' : -0.15,'TRP_B_N' : -0.26,'TRP_C_N' : 3.19,'TRP_B_C' : -0.85,'TRP_C_C' : -0.30,'TYR_B_CA' : -0.22,'TYR_C_CA' : 0.06,
'TYR_B_HN' : -0.11,'TYR_C_HN' : 0.09,'TYR_B_HA' : -0.10,'TYR_C_HA' : -0.08,'TYR_B_N' : -0.43,'TYR_C_N' : 3.01,'TYR_B_C' : -0.85,'TYR_C_C' : -0.24,
'VAL_B_CA' : -0.21,'VAL_C_CA' : -0.02,'VAL_B_HN' : -0.05,'VAL_C_HN' : 0.17,'VAL_B_HA' : -0.01,'VAL_C_HA' : -0.02,'VAL_B_N' : -0.14,'VAL_C_N' : 4.34,
'VAL_B_C' : -0.57,'VAL_C_C' : -0.18}


# atom ordering in triplet db
atoms = [ 'HN', 'N', 'HA', 'C','CA', 'CB' ]


# for residue -1, 0 and 1 positions
seqSimWeights = [0.5,2.5, 1.5]
#preditor weights for six backbone shifts
#atomShiftsWeights = [[37,11,9,5,1,1],[31,14,14,6,1.5,0.3], [37,7,7,4,2,1.5]]
#Talos weights for six backbone shifts
atomShiftsWeights = [[0.16, 0.16, 14.66, 1.15, 0.72, 0.76],[0.03, 0.18, 17.54,1.21, 0.99,0.91], [0.15, 0.20, 15.25, 1.04, 0.72, 0.70]]

seqSimFactors = {'A':{'A':0, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':1, 'G':2, 'H':1, 'I':2, 'L':1, 'K':1,'M':1,'F':2, 'P':3,'S':1,'T':2, 'W':2, 'Y':2, 'V':2 },
                 'R':{'A':1, 'R':0, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':1, 'G':2, 'H':1, 'I':2, 'L':1, 'K':0,'M':1,'F':1, 'P':3,'S':1,'T':2, 'W':1, 'Y':1, 'V':2 },
                 'N':{'A':1, 'R':1, 'N':0, 'D':0, 'C':1, 'Q':1, 'E':1, 'G':2, 'H':1, 'I':2, 'L':1, 'K':1,'M':1,'F':1, 'P':3,'S':1,'T':2, 'W':1, 'Y':1, 'V':2 }, 
                 'D':{'A':1, 'R':1, 'N':0, 'D':0, 'C':1, 'Q':1, 'E':1, 'G':2, 'H':1, 'I':2, 'L':1, 'K':1,'M':1,'F':1, 'P':3,'S':1,'T':2, 'W':1, 'Y':1, 'V':2 }, 
                 'C':{'A':1, 'R':1, 'N':1, 'D':1, 'C':0, 'Q':1, 'E':1, 'G':2, 'H':1, 'I':2, 'L':1, 'K':1,'M':1,'F':1, 'P':3,'S':1,'T':2, 'W':1, 'Y':1, 'V':2 }, 
                 'Q':{'A':1, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':0, 'E':1, 'G':2, 'H':1, 'I':2, 'L':1, 'K':1,'M':1,'F':1, 'P':3,'S':1,'T':2, 'W':1, 'Y':1, 'V':2 }, 
                 'E' : {'A':1, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':0, 'G':2, 'H':1, 'I':2, 'L':2, 'K':2,'M':1,'F':1, 'P':3,'S':1,'T':2, 'W':1, 'Y':1, 'V':2 },
                 'G':{'A':2, 'R':2, 'N':2, 'D':2, 'C':2, 'Q':2, 'E':2, 'G':0, 'H':3, 'I':3, 'L':3, 'K':3,'M':3,'F':3, 'P':3,'S':3,'T':3, 'W':3, 'Y':3, 'V':3 }, 
                 'H':{'A':1, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':1, 'G':3, 'H':0, 'I':2, 'L':1, 'K':2,'M':2,'F':1, 'P':3,'S':2,'T':2, 'W':1, 'Y':1, 'V':2 }, 
                 'I':{'A':2, 'R':2, 'N':2, 'D':2, 'C':2, 'Q':2, 'E':2, 'G':3, 'H':2, 'I':0, 'L':1, 'K':2,'M':2,'F':2, 'P':3,'S':2,'T':1, 'W':2, 'Y':2, 'V':0 }, 
                 'L':{'A':1, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':2, 'G':3, 'H':1, 'I':1, 'L':0, 'K':1,'M':1,'F':1, 'P':3,'S':2,'T':2, 'W':1, 'Y':1, 'V':2 }, 
                 'K':{'A':1, 'R':0, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':2, 'G':3, 'H':2, 'I':2, 'L':1, 'K':0,'M':1,'F':2, 'P':3,'S':1,'T':2, 'W':2, 'Y':2, 'V':2 }, 
                 'M':{'A':1, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':1, 'G':3, 'H':2, 'I':2, 'L':1, 'K':1,'M':0,'F':2, 'P':3,'S':1,'T':2, 'W':2, 'Y':2, 'V':2 },
                 'F':{'A':2, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':1, 'G':3, 'H':1, 'I':2, 'L':1, 'K':2,'M':2,'F':0, 'P':3,'S':2,'T':2, 'W':0, 'Y':0, 'V':1 },
                 'P':{'A':3, 'R':3, 'N':3, 'D':3, 'C':3, 'Q':3, 'E':3, 'G':3, 'H':3, 'I':3, 'L':3, 'K':3,'M':3,'F':3, 'P':0,'S':3,'T':3, 'W':3, 'Y':3, 'V':3 },
                 'S':{'A':1, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':1, 'G':3, 'H':2, 'I':2, 'L':2, 'K':1,'M':1,'F':2, 'P':3,'S':0,'T':1, 'W':2, 'Y':2, 'V':2 },
                 'T':{'A':2, 'R':2, 'N':2, 'D':2, 'C':2, 'Q':2, 'E':2, 'G':3, 'H':2, 'I':1, 'L':2, 'K':2,'M':2,'F':2, 'P':3,'S':1,'T':0, 'W':1, 'Y':1, 'V':1 },
                 'W':{'A':2, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':1, 'G':3, 'H':1, 'I':2, 'L':1, 'K':2,'M':2,'F':0, 'P':3,'S':2,'T':1, 'W':0, 'Y':0, 'V':1 },
                 'Y':{'A':2, 'R':1, 'N':1, 'D':1, 'C':1, 'Q':1, 'E':1, 'G':3, 'H':1, 'I':2, 'L':1, 'K':2,'M':2,'F':0, 'P':3,'S':2,'T':1, 'W':0, 'Y':0, 'V':1 },
                 'V':{'A':2, 'R':2, 'N':2, 'D':2, 'C':2, 'Q':2, 'E':2, 'G':3, 'H':2, 'I':0, 'L':2, 'K':2,'M':2,'F':1, 'P':3,'S':2,'T':1, 'W':1, 'Y':1, 'V':0 } }

tripletAA = defaultdict(list)
tripletCS = defaultdict(list)

def load_triplet_db():
    lines=[]
    
    fin=open("/apps/csi/project/Standalone_CSI2.0/data/triplet.db", "r")
    #fin = open("/Users/Noor/Documents/Research/RefDB-dataset/talos.tab","r")
            
    entry = 0 
    '''
    for i in range(len(lines)):
        
        if "Triplet" in lines[i]:
                AA = []
                CSlist = []
                recordA = lines[nextres].rstrip().split()
                recordB = lines[i+2].rstrip().split()
                recordC = lines[i+3].rstrip().split()
                
                if len(recordA) == 8 and len(recordB) == 8 and len(recordC) == 8:
                    if (not recordA[1][0].isdigit()) and (not recordB[1][0].isdigit()) and (not recordC[1][0].isdigit()):   
                        tripletAA[entry].append(recordA[1])
                        tripletAA[entry].append(recordB[1])
                        tripletAA[entry].append(recordC[1])
                           
                        CS = [float(recordA[k]) for k in range(2, 8)]
                        tripletCS[entry].append(CS)
                        CS = [float(recordB[k]) for k in range(2, 8)]
                        tripletCS[entry].append(CS)
                        CS = [float(recordC[k]) for k in range(2, 8)]
                        tripletCS[entry].append(CS)
                    
                #tripletAA[entry]=AA    
                #tripletCS[entry]=CSlist
                
            
                entry+=1
                i = i+4
        '''
    # triplet.db (talosplus triplet database format) file parsing
    '''
    VARS   RESID_R1 RESNAME_R1 HN_R1 N_R1 HA_R1 C_R1 CA_R1 CB_R1 RESID_R2 RESNAME_R2 HN_R2 N_R2 HA_R2 C_R2 CA_R2 CB_R2 RESID_R3 RESNAME_R3 HN_R3 N_R3 HA_R3 C_R3 CA_R3 CB_R3 PHI PSI CHI ABP_R1 ABP_R2 ABP_R3 SOURCE 
    FORMAT %4d %s %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %4d %s %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %4d %s %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %1s %1s %1s %s

   2 A   -0.640   -5.269   -0.111   -3.079   -0.267    0.800    3 D    0.350    2.144    0.249   -1.108    0.200    0.910    4 T    0.380    2.865    0.919   -1.128    0.600    2.552  -77.412  164.287  -71.548 N B B 4060
   3 D    0.350    2.144    0.249   -1.108    0.200    0.910    4 T    0.380    2.865    0.919   -1.128    0.600    2.552    5 L    1.310    6.671    1.059   -3.498   -1.140    1.991 -115.518  136.314  -67.198 B B B 4060
    '''
    
    for line in fin:
        if line.strip():
            line = line.lstrip()
            #print(line)
            if line[0].isdigit():
                line = line.replace('c','C')
                record = line.split()
                tripletAA[entry] = [record[1], record[9], record[17]]
                CS1 = [float(record[i]) if float(record[i]) != 9999.0 else 0.0 for i in range(2, 8)]
                tripletCS[entry].append(CS1)
                CS2 = [float(record[i]) if float(record[i]) != 9999.0 else 0.0 for i in range(10, 16)]
                tripletCS[entry].append(CS2)
                CS3 = [float(record[i]) if float(record[i]) != 9999.0 else 0.0 for i in range(18, 24)]
                tripletCS[entry].append(CS3)
        entry+=1
    
    fin.close()
    
def find_matched_triplets(nrcfile,outfile):
    
    fin=open(nrcfile, 'r')
    fout=open(outfile, 'w')
    lines = fin.readlines()
    
    start = 0
    #ResN_Atom_CS = defaultdict(list)
    ResN_Atom_CS = dict()
    ResN_Atom_rcShift = dict()
    ResN_Atom_secCS = dict()
    residueList = dict()
    resNumList = []
    #pdbResID = dict()
    
    # parsing the residue and residuenum list
    '''
    1MET 2GLN 3ILE 4PHE 5VAL
    6LYS 7THR 8LEU 9THR 10GLY
    11LYS 12THR 13ILE 14THR 15LEU
    16GLU 17VAL 18GLU 19PRO 20SER
    21ASP 22THR 23ILE 24GLU 25ASN
    ...
    76GLY 0 0 0 0
    '''
    line_tags = ['DATA', 'VARS', 'FORMAT']
    for i in range (len(lines)):
        line = lines[i].strip()
        
        #if line.strip() and (line.split()[0].isalnum()) and (line.split()[0] not in line_tags)  and (len(line.split()) == 5):
        if line.startswith("SEQ"):
            #print(line)
            elems = line.split()
            for elem in elems:
                if elem.isalnum() and (elem != "0" and elem!='SEQ'):
                    residue = elem[-3:]
                    residueNum = elem[:-3]
                    resNumList.append(int(residueNum))
                    if residue in three2one:
                        residueList[residueNum]= residue
                                 	   
    for i in range (len(lines)):
        line = lines[i].strip()  
        #print(line)
        if line.strip() and line[0].isdigit() and len(line.split()) == 6:
            record = line.split()
            
            residueNum = int(record[0])
            #pdbResID[residueNum] = int(record[0])
            residue = record[1]
            
            atom = record[2]
            CS = float(record[3])
            rcShift = float(record[4])
            secCS =  float(record[5])
            
            if start == 0:
                
                startRes = residueNum
                start = 1

                 
            ResN_Atom_CS[str(residueNum)+'_'+atom]=CS
            ResN_Atom_rcShift[str(residueNum)+'_'+atom]=rcShift
            ResN_Atom_secCS[str(residueNum)+'_'+atom]=secCS    
            endRes = residueNum    
        else:
            if line.startswith("VARS"):
                fout.write('\n'+line+'\n')
            elif line.startswith("FORMAT"):
                fout.write(line+'\n\n')
            elif line.startswith("DATA"):
                fout.write(line+'\n')
            elif line.startswith("SEQ"):
                fout.write(line+'\n')
            
            
    #for i in range(startRes+1, endRes):
    for x in range(1,len(resNumList)-1):
        missingFlag = 0
        
        i = resNumList[x]
        
        prevres = resNumList[x-1]
        nextres = resNumList[x+1]
        for j in range(len(atoms)):
            
            if str(i)+'_'+ atoms[j] in ResN_Atom_CS:
                continue
            else:
                missingAtom = atoms[j]
                if str(i) in residueList:
                    residue = residueList[str(i)]
                
                    if((residue == 'GLY' and missingAtom == 'CB') or
                       (residue == 'PRO' and (missingAtom == 'HN' or missingAtom == 'N'))):
                        continue 
                    #print (i, residue, missingAtom)
                    missingFlag = 1
                
                if missingFlag:  
                    missingShift = 0.0
                    if str(prevres)+'_'+atoms[j] in ResN_Atom_secCS and str(nextres)+'_'+atoms[j] in ResN_Atom_secCS:
                            missingShift = (ResN_Atom_secCS[str(prevres)+'_'+atoms[j]] + ResN_Atom_secCS[str(nextres)+'_'+atoms[j]])/2
                            
                    else:        
                        continue
                        threeresAA = [] 
                        threeresCS = []
                        for k in range(prevres,i+2):
                            threeresAA.append(three2one[residueList[str(k)]])
                            
                        
                        seqSimDict = dict()
                        seqSimDictOrdered = dict()
                        shiftSimDict = dict()
                        shiftSimDictOrdered=dict()
                        seqShiftSimDict = dict()
                        
                        #print (threeresAA)
                        for key in tripletAA:
                            
                            seqSim = 0
                            shiftSim = 0
                            AAtriplet = tripletAA[key]# returns a list
                            AAtripletstr = str(AAtriplet)
                            ShiftArraytriplet = tripletCS[key] # ShiftArraytriplet is a two dimensional array, each entry has six backbone shifts 
                            
                            for l in range(3):
                                # TALOS standard database search formula
                                seqSim += seqSimWeights[l]*(seqSimFactors[threeresAA[l]][AAtriplet[l]])** 2
                                # Preditor standard database search formula
                                #seqSim += seqSimWeights[l]*(seqSimFactors[threeresAA[l]][AAtriplet[l]])
                            
                            residArr = []    
                            residArr.append(prevres)
                            residArr.append(i)
                            residArr.append(nextres)
                            
                            for m in range(len(residArr)):    
                                for n in range(6):
                                    if str(residArr[m])+'_'+atoms[n] in ResN_Atom_CS:
                                        # TALOS standard database search formula
                                        shiftSim += atomShiftsWeights[m][n] * (ResN_Atom_CS[str(residArr[m])+'_'+atoms[n]] - ShiftArraytriplet[m][n])**2
                                        # Preditor standard database search formula
                                        #shiftSim += atomShiftsWeights[m][n] * abs(ResN_Atom_secCS[str(residArr[m])+'_'+atoms[n]] - ShiftArraytriplet[m][n])
                            #seqSimDict[AAtripletstr] = seqSim
                            #shiftSimDict[AAtripletstr] = shiftSim
                            seqSimDict[key] = seqSim
                            shiftSimDict[key] = shiftSim
                            seqShiftSimDict[key] = seqSim+ shiftSim
                            
                            
                        seqShiftSimDictOrdered  = OrderedDict(sorted(seqShiftSimDict.items(), key=operator.itemgetter(1)))
                        lowestScoredTripletArray = []
                        n = 0 # counter
                        for key in seqShiftSimDictOrdered.keys():
                            if n  == 0:
                                lowestScoredTriplet = key
                            lowestScoredTripletArray.append(key)
                            n += 1
                            if n==9:
                                break
                            
                        #print ("Lowest scored triplet: %d" %lowestScoredTriplet)
                        
                        
                        sumShifts = 0
                        # if lowestScoredTriplet's shift is available, fill the missing assignment with it
                        # else, fill the missing assignment with the average of top 10 lowest scored triplets
                        '''
                        if tripletCS[lowestScoredTriplet][1][a]:
                            missingShift = tripletCS[lowestScoredTriplet][1][a]
                            print ("missing atom=%s shift :%0.3f" %(atoms[a],tripletCS[lowestScoredTriplet][1][a]))
                        else:
                        '''
                        for triplet in lowestScoredTripletArray: 
                            #print(tripletCS[triplet][1][a])
                            sumShifts += tripletCS[triplet][1][j]
            
                        missingShift = sumShifts/len(lowestScoredTripletArray)
                        #print ("missing shift from average:%0.3f" %missingShift)
                        '''    
                        if missingShift == 0.000:
                            if ResN_Atom_secCS[str(prevres)+'_'+atoms[j]] and ResN_Atom_secCS[str(nextres)+'_'+atoms[j]]:
                                missingShift = (ResN_Atom_secCS[str(prevres)+'_'+atoms[j]] + ResN_Atom_secCS[str(nextres)+'_'+atoms[j]])/2
                            else:
                                missingShift = (ResN_Atom_secCS[str(i-2)+'_'+atoms[j]] + ResN_Atom_secCS[str(prevres)+'_'+atoms[j]]+ResN_Atom_secCS[str(nextres)+'_'+atoms[j]] + ResN_Atom_secCS[str(i+2)+'_'+atoms[j]])/4
                                ResN_Atom_secCS[str(i)+'_'+atoms[j]] =  missingShift     
                        else:
                        '''    
                            #print (str(i),atoms[a], missingShift)
                    ResN_Atom_secCS[str(i)+'_'+atoms[j]] =  missingShift 
                    ResN_Atom_rcShift[str(i)+'_'+atoms[j]] = RandomCoil[residue+'_'+atoms[j]]
                    ResN_Atom_CS[str(i)+'_'+atoms[j]] = missingShift + RandomCoil[residue+'_'+atoms[j]]
        
    ##::::::::::::::::::::::::;;;;::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::               
    #start residue completion if missing 
    #print("start residue from:%d" %startRes)
    #print("start residue completion if missing ....")
    '''
    for a in range(len(atoms)):
        residue = residueList[str(startRes)]
        
        if((residue == 'GLY' and atoms[a] == 'CB') or (residue == 'PRO' and (atoms[a] == 'HN' or atoms[a] == 'N'))):
            continue 
        else: 
            if str(startRes)+'_'+atoms[a] not in ResN_Atom_secCS:
                print(str(startRes)+ atoms[a]+ 'missing\n')
                if str(startRes+1)+'_'+atoms[a] not in ResN_Atom_secCS and str(startRes+2)+'_'+atoms[a] in ResN_Atom_secCS:
                    if str(startRes+3)+'_'+atoms[a] in ResN_Atom_secCS:
                        missingShift = (ResN_Atom_secCS[str(startRes+3)+'_'+atoms[a]] + ResN_Atom_secCS[str(startRes+2)+'_'+atoms[a]])/2
                    else:
                        missingShift = (ResN_Atom_secCS[str(startRes+4)+'_'+atoms[a]] + ResN_Atom_secCS[str(startRes+2)+'_'+atoms[a]])/2 
                elif str(startRes+1)+'_'+atoms[a] in ResN_Atom_secCS and str(startRes+2)+'_'+atoms[a] not in ResN_Atom_secCS:
                    if str(startRes+3)+'_'+atoms[a] in ResN_Atom_secCS:
                        missingShift = (ResN_Atom_secCS[str(startRes+1)+'_'+atoms[a]] + ResN_Atom_secCS[str(startRes+3)+'_'+atoms[a]])/2
                    else:
                        missingShift = (ResN_Atom_secCS[str(startRes+1)+'_'+atoms[a]] + ResN_Atom_secCS[str(startRes+4)+'_'+atoms[a]])/2
                elif str(startRes+1)+'_'+atoms[a] in ResN_Atom_secCS and str(startRes+2)+'_'+atoms[a] in ResN_Atom_secCS:    
                    missingShift = (ResN_Atom_secCS[str(startRes+1)+'_'+atoms[a]] + ResN_Atom_secCS[str(startRes+2)+'_'+atoms[a]])/2
                else:
                    if str(startRes+3)+'_'+atoms[a] in ResN_Atom_secCS:
                        missingShift = (ResN_Atom_secCS[str(startRes+3)+'_'+atoms[a]] + ResN_Atom_secCS[str(startRes+4)+'_'+atoms[a]])/2
                    else:
                        missingShift = (ResN_Atom_secCS[str(startRes+5)+'_'+atoms[a]] + ResN_Atom_secCS[str(startRes+4)+'_'+atoms[a]])/2
                ResN_Atom_secCS[str(startRes)+'_'+atoms[a]] =  missingShift 
                ResN_Atom_rcShift[str(startRes)+'_'+atoms[a]] = RandomCoil[residue+'_'+atoms[a]]
                ResN_Atom_CS[str(startRes)+'_'+atoms[a]] = missingShift + RandomCoil[residue+'_'+atoms[a]]
    '''
    #second to last residue completion if missing 
    #print("second to last residue completion if missing...")
    for a in range(len(atoms)):
        residue = residueList[str(endRes-1)]
        #print(residue)
        if((residue == 'GLY' and atoms[a] == 'CB') or (residue == 'PRO' and (atoms[a] == 'HN' or atoms[a] == 'N'))):
            continue 
        else:
            if str(endRes-1)+'_'+atoms[a] not in ResN_Atom_secCS:
                if str(endRes)+'_'+atoms[a] in ResN_Atom_secCS and str(endRes-2)+'_'+atoms[a] in ResN_Atom_secCS:
                    missingShift = (ResN_Atom_secCS[str(endRes-2)+'_'+atoms[a]] + ResN_Atom_secCS[str(endRes)+'_'+atoms[a]])/2
                else:
                    if str(endRes)+'_'+atoms[a] not in ResN_Atom_secCS and str(endRes-2)+'_'+atoms[a] in ResN_Atom_secCS and str(endRes-3)+'_'+atoms[a] in ResN_Atom_secCS:
                        missingShift = (ResN_Atom_secCS[str(endRes-2)+'_'+atoms[a]] + ResN_Atom_secCS[str(endRes-3)+'_'+atoms[a]])/2
                    elif str(endRes)+'_'+atoms[a] in ResN_Atom_secCS and str(endRes-2)+'_'+atoms[a] not in ResN_Atom_secCS and str(endRes-3)+'_'+atoms[a] in ResN_Atom_secCS:
                    
                        missingShift = (ResN_Atom_secCS[str(endRes)+'_'+atoms[a]] + ResN_Atom_secCS[str(endRes-3)+'_'+atoms[a]])/2
                    else:
                        continue
 
                ResN_Atom_secCS[str(endRes-1)+'_'+atoms[a]] =  missingShift 
                ResN_Atom_rcShift[str(endRes-1)+'_'+atoms[a]] = RandomCoil[residue+'_'+atoms[a]]
                ResN_Atom_CS[str(endRes-1)+'_'+atoms[a]] = missingShift + RandomCoil[residue+'_'+atoms[a]]
    
    #last residue completion if missing 
    #print("last residue completion if missing...")
    for a in range(len(atoms)):
        residue = residueList[str(endRes)]
        #print(residue)
        if((residue == 'GLY' and atoms[a] == 'CB') or (residue == 'PRO' and (atoms[a] == 'HN' or atoms[a] == 'N'))):
            continue 
        else:
            if str(endRes)+'_'+atoms[a] not in ResN_Atom_secCS:
                if str(endRes-1)+'_'+atoms[a] in ResN_Atom_secCS and str(endRes-2)+'_'+atoms[a] in ResN_Atom_secCS:
                    missingShift = (ResN_Atom_secCS[str(endRes-2)+'_'+atoms[a]] + ResN_Atom_secCS[str(endRes-1)+'_'+atoms[a]])/2
                else:
                    if str(endRes-1)+'_'+atoms[a] not in ResN_Atom_secCS and str(endRes-2)+'_'+atoms[a] in ResN_Atom_secCS and str(endRes-3)+'_'+atoms[a] in ResN_Atom_secCS:
                        missingShift = (ResN_Atom_secCS[str(endRes-2)+'_'+atoms[a]] + ResN_Atom_secCS[str(endRes-3)+'_'+atoms[a]])/2
                    elif str(endRes-1)+'_'+atoms[a] in ResN_Atom_secCS and str(endRes-2)+'_'+atoms[a] not in ResN_Atom_secCS and str(endRes-3)+'_'+atoms[a] in ResN_Atom_secCS:
                    
                        missingShift = (ResN_Atom_secCS[str(endRes-1)+'_'+atoms[a]] + ResN_Atom_secCS[str(endRes-3)+'_'+atoms[a]])/2
                    else:
                        continue
 
                ResN_Atom_secCS[str(endRes)+'_'+atoms[a]] =  missingShift 
                ResN_Atom_rcShift[str(endRes)+'_'+atoms[a]] = RandomCoil[residue+'_'+atoms[a]]
                ResN_Atom_CS[str(endRes)+'_'+atoms[a]] = missingShift + RandomCoil[residue+'_'+atoms[a]]
                
                
                prevResAffect = 0.0
                nextResAffect = 0.0
                if str(endRes - 1) in residueList:
                    prevRes = residueList[str(endRes - 1)]
                    if prevRes+'_C_'+residue in nrc :
                        prevResAffect = nrc[prevRes+'_C_'+residue]
                
                if str(endRes + 1) in residueList:    
                    nextRes = residueList[str(endRes + 1)]
                    if nextRes+'_B_'+residue in nrc:
                        nextResAffect = nrc[nextRes+'_B_'+residue]
                
                ResN_Atom_rcShift[str(endRes)+'_'+atoms[a]] = RandomCoil[residue+'_'+atoms[a]] + prevResAffect + nextResAffect 
                # secondary schemical shift
                ResN_Atom_secCS[str(endRes)+'_'+atoms[a]] = ResN_Atom_CS[str(endRes)+'_'+atoms[a]] - ResN_Atom_rcShift[str(endRes)+'_'+atoms[a]]
                
    fin.close()         
    # now writing the complete residue list       
    for i in range (startRes, endRes+1):
        if str(i) in residueList:
            for j in range(len(atoms)):
                key = str(i)+'_'+atoms[j]
                
                if key in ResN_Atom_secCS:
                    fout.write ("%d %s %s %0.3f %0.3f %0.3f\n" %(i, residueList[str(i)], atoms[j], ResN_Atom_CS[str(i)+'_'+atoms[j]], ResN_Atom_rcShift[str(i)+'_'+atoms[j]], ResN_Atom_secCS[str(i)+'_'+atoms[j]]))  
    fout.close()
   
           
def main():
    load_triplet_db()
    file=sys.argv[1]
    outfile=file.split(".")[0]+".nrc.complete"
    find_matched_triplets(file,outfile)
    os.system("mv %s %s" %(outfile, file)) 
main()    
