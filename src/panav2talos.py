#!/usr/local/bin/python3

import sys, os, os.path
import re


resmap = {'A' : 'ALA','R' : 'ARG','N' : 'ASN','D' : 'ASP','B' : 'ASX','C' : 'CYS', 'a':'CYS', 'b':'CYS', 'c':'CYS', 'Q' : 'GLN','E' : 'GLU','Z' : 'GLX','G' : 'GLY','H' : 'HIS','I' : 'ILE','L' : 'LEU','K' : 'LYS','M' : 'MET','F' : 'PHE','P' : 'PRO','S' : 'SER','T' : 'THR','W' : 'TRP','Y' : 'TYR','V' : 'VAL','X' : 'PCA'}

# Schwarzinger et al. (2000) random coil chemical shift table
# proline trans has been used
# cysteine redox has been used 

atomList = ['C','CA', 'CB', 'N', 'HN', 'HA']
bmrb = sys.argv[1]


def panav2talosConversion(fin):
    Seq1letterList = []
    SeqStr=[]
    resnList = []
    resnSeqList = []
    CS_dict = dict()
    for line in fin:
        line = line.lstrip()
        if not line.startswith("Seq"):
            #(seq, C, CA, CB, N, HN, HA, d,d,d,d) = line.split()
            m=re.match(r'^([A-Z]\d+|null)\s+(\d\d\d\.?\d\d|\s+|null)\s+(\d\d\.?\d\d|\s+|null)\s+(\d\d\.?\d\d|\s+|null)\s+(\d\d\d\.?\d\d|\s+|null)\s+([6|7|8|9|1]\d?\.?\d+|\s+|null)\s+([1|2|3|4|5|6]\.?\d+|\s+|null)\s+([0|1]\.\d+)\s+([0|1]\.\d+)\s+([0|1]\.\d+)\s+[A-Z]', line)
            if m:
                seq = m.group(1)
                resnum = int(seq[1:len(seq)])
                resnList.append(resnum)
                Seq1letterList.append(seq[0])
                SeqStr.append(resmap[seq[0]])
                resnSeqList.append(str(resnum)+resmap[seq[0]])
                #print (m.group(1),m.group(2), m.group(3), m.group(4), m.group(5), m.group(6), m.group(7))
                if m.group(2)[0].isdigit():
                    CS_dict[str(resnum)+'_C'] = float(m.group(2))
                if m.group(3)[0].isdigit():
                    CS_dict[str(resnum)+'_CA'] = float(m.group(3))
                if m.group(4)[0].isdigit():
                    CS_dict[str(resnum)+'_CB'] = float(m.group(4))
                if m.group(5)[0].isdigit():
                    CS_dict[str(resnum)+'_N'] = float(m.group(5))
                if m.group(6)[0].isdigit():
                    CS_dict[str(resnum)+'_HN'] = float(m.group(6))
                if m.group(7)[0].isdigit():
                    CS_dict[str(resnum)+'_HA'] = float(m.group(7))
        
    fin.close()     
           
    if CS_dict: # if CS_dict has chemical shift values
        fout= open(bmrb+ ".panav", "w")
        for i in range(0, len(resnSeqList), 5):
            s = "SEQ " + " ".join(resnSeqList[i:i+5])
            alist = s.split()
            if (len(alist[1:]) < 5):
                for j in range(len(alist[1:]), 5):
                    alist.extend("0")
	       
                s=  " ".join(alist) 
            fout.write(s+"\n")
	 
        fout.write("VARS RESID RESNAME ATOMNAME SHIFT\n")
        fout.write("FORMAT %4d %4s %4s %8.3f\n\n")
	
        for i in range(len(resnList)):
            for j in range(len(atomList)):
                if str(resnList[i])+'_'+atomList[j] in CS_dict:
                    fout.write("%d %s %s %0.3f\n" %(resnList[i], SeqStr[i], atomList[j], CS_dict[str(resnList[i])+'_'+atomList[j]]))
        os.system("mv %s.panav %s.tab" %(bmrb, bmrb))
        fout.close()  
    else: # if CS_dict is empty
        os.system("../../Standalone_CSI2.0/src/bmrb2talosV2.0.com %s.str > %s.tab" %(bmrb, bmrb))
        os.system("rm %s.out_calibrated" %bmrb)
    
    	

        
   
     
    #fout2.write(">CSI2.0|CHAIN|SEQUENCE\n")
    #Seq1letterStr="".join(Seq1letterList)
    #fout2.write(Seq1letterStr+"\n")
    
    #fout2.close()
        
def main():
    
    infile= open(bmrb+".out_calibrated", "r")
    panav2talosConversion(infile)
    #os.system("mv %s.panav %s.tab" %(bmrb, bmrb))

main()
