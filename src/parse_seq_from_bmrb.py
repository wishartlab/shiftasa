#!/usr/bin/python

from collections import defaultdict
from collections import *
from os import listdir
from os.path import isfile, join
import operator
import os.path
import sys
three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C','GLN' : 'Q',
	     'GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K',
	     'MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y',
	     'VAL' : 'V','PCA' : 'X'}



#tabdir = "/Users/Noor/Documents/Research/CMPUT-605Dec12/CMPUT-605/testfiles/CS-addPDBresno-residueLabel"
#tabdir="/Users/Noor/Documents/Research/RefDB-dataset/Mark_30_proteins_cs_files"
#tabfiles = [file for file in listdir(tabdir) if isfile(join(tabdir, file)) and file.endswith(".tab")]
bmrbfile= sys.argv[1]
bmrbfiles=[bmrbfile]
#tabfiles = ["R186_bmr6833_1UB4B.tab"]
for file in bmrbfiles:
        tmp = file.split(".")
        #infile = join(tabdir, file)
        #outfile = join(tabdir,tmp[0]+ '.fasta')
        outfile = tmp[0]+ '.fasta'
        fin = open (file, 'r')
        fout = open (outfile, 'w')
        lines = fin.readlines()
        
        AAstr = ''
        residList = dict()
        residueList = dict()
        
        seqEndFlag = 0
        
	# reading the sequence from the resnum+resiude entries before "VARS" line 
	# Example format:
	#1GLY 2SER 3SER 4GLY 5SER
	#6SER 7GLY 8SER 9LEU 10GLN
	#11THR 12SER 13ASP 14VAL 15VAL
	
        for i in range(len(lines)):
                line = lines[i].strip()
                
                               
                #if line.startswith("DATA"):
                 #       elems= line.split()
                  #      AAstr += "".join([elems[i] for i in range (2,len(elems))])
                for i in range (len(lines)):
                        line = lines[i].strip()
                
                        num = 1
                
                        if line.startswith("VARS"):
                                seqEndFlag = 1
                        if line.strip() and not seqEndFlag and line.split()[0].isalnum() and len(line.split()) == 5:
                        #print(line)
                                elems = line.split()
                                for elem in elems:
                                        if elem.isalnum():
                                                residue = elem[-3:]
                                                residueNum = elem[:-3]
                                                if residue in three2one:
                                                         residList[num] = residueNum    
                                                         residueList[residueNum]= residue
                                                         AAstr += three2one[residue]
                                                         num += 1	
        fin.close()
        
        fout.write(">QUERY|CHAIN|SEQUENCE\n")
        fout.write(AAstr)
        fout.close()
        
