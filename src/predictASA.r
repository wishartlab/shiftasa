#!/usr/local/bin/Rscript 

library(caret)
library(gbm)

specify_decimal <- function(x, k) format(round(x, k), nsmall=k)

# loading the 3-residue fetaure GBM Model
load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_jsscon_30proteins.RData")
# loading the single-res feature GBM Model
load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_1res_jsscon_30proteins.RData")


Args <- commandArgs(TRUE)
 
if (length(Args) < 1) {
  cat("\nError: No input file supplied.")
  cat("\n\t Syntax: predictASA.R <input-bmrb> \n\n")
  quit()
}

prediction_model_to_choose= paste(Args[2])

# naming the final output file
outfile = paste(Args[1], "asafeat.pred", sep=".")


## single-residue feature prediction
infile = paste(Args[1], "bbrcifeat1res",sep=".")
test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
testData = test.asa_features[,3:8]
resnList = test.asa_features[,1]
residueList = test.asa_features[,2]
newdata = data.frame(x=testData)


test.data.predict=predict(ASAPredgbmFitGaussian1resjsscon30proteins$finalModel, newdata, type="response", n.trees=120)# the last argument indicates number of iterations needed to predict the response

predict=test.data.predict

len = length(predict)
res1class = ifelse(predict[1]>0.25, 'E', 'B')
resLastclass = ifelse(predict[len]>0.25, 'E', 'B')
lastResNum = paste(resnList[len])
if (lastResNum>= 100){
        space = "  "
}else if (lastResNum< 100){
        space = " "
}
res1str = paste( resnList[1], space, residueList[1], specify_decimal(predict[1],2), res1class, sep=" ")
space = " "
resLaststr = paste( resnList[len], space, residueList[len], specify_decimal(predict[len],2), resLastclass, sep=" ")
#=====================================================================================================

## 3-residue feature prediction
infile = paste(Args[1], "bbrcifeat",sep=".")

test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
testData = test.asa_features[,3:20]
resnList = test.asa_features[,1]
residueList = test.asa_features[,2]
newdata = data.frame(x=testData)

#backbone shifts only prediction
test.data.predict=predict(ASAPredgbmFitGaussian3resjsscon30proteins$finalModel, newdata, type="response", n.trees=240)# the last argument indicates number of iteration trees needed in the model

bbpredict=test.data.predict


predict2 = list()

if (prediction_model_to_choose == "sable"){
  infile = paste(Args[1], "sablefeat",sep=".")
  
  test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  testData = test.asa_features[,3:23]
  resnList = test.asa_features[,1]
  residueList = test.asa_features[,2]
  newdata = data.frame(x=testData)
  
  load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_Sable.RData")
  #Shannon's entropy conservation score
  test.data.predict=predict(ASAPredgbmFitGaussian3resSable$finalModel, newdata, type="response", n.trees=160)# the last argument indicates number of iteration trees needed in the model
  
  predict2=test.data.predict
  
  
}else if (prediction_model_to_choose == "scrci"){
  infile = paste(Args[1], "scrcifeat",sep=".")
  
  test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  testData = test.asa_features[,3:23]
  resnList = test.asa_features[,1]
  residueList = test.asa_features[,2]
  newdata = data.frame(x=testData)
  
  load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_sideChainRCI.RData")
  #Shannon's entropy conservation score
  test.data.predict=predict(ASAPredgbmFitGaussian3resSideChainRCI$finalModel, newdata, type="response", n.trees=160)# the last argument indicates number of iteration trees needed in the model
  
  predict2=test.data.predict
  
  
}else if (prediction_model_to_choose == "sable_scrci"){
  infile = paste(Args[1], "sablescrcifeat",sep=".")
  
  test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  testData = test.asa_features[,3:26]
  resnList = test.asa_features[,1]
  residueList = test.asa_features[,2]
  newdata = data.frame(x=testData)
  
  load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_Sable_scrci.RData")
  #Shannon's entropy conservation score
  test.data.predict=predict(ASAPredgbmFitGaussian3resSableSCrci$finalModel, newdata, type="response", n.trees=160)# the last argument indicates number of iteration trees needed in the model
  
  predict2=test.data.predict
  
  
}

if (length(predict2) != 0){

	coef = cor(bbpredict, predict2, method ="spearman")

	if (coef > 0.95){
  		finalPred = predict2
	}else{
  		finalPred = bbpredict
	}
}else{

	finalPred = bbpredict
}
  

#obs = matrix(observed)
pred = matrix(finalPred)

print ("after prediction......")

con=file(outfile, "w")
oheader = "#Num Res fASA Cls";
cat(file=con, append=TRUE, oheader, "\n")
# writing the 1st residue prediction
cat(file=con, append=TRUE, res1str, "\n")

class = NULL
for (i in 1:nrow(pred)){
  if (pred[i]>0.25){
    class[i] = 'E'
  }else{
    class[i]='B'
  }
  if (lastResNum>= 100){
	if (resnList[i]<10){
		space = "  "
        }else if (resnList[i] >= 10){
		space =" "
	}else if (resnList[i] > 99){
		space =""
	}
  }else if (lastResNum< 100){
	if (resnList[i]< 10 ){
		space = " "
	}else if (resnList[i]> 10 ){
		space = ""
	}
  }
  
  ostr = paste (resnList[i], space, residueList[i], specify_decimal(pred[i],2), class[i], sep=" ")
  #save to a file
  cat( file=con, append=TRUE, ostr,  "\n" )
}
# writing the last residue prediction
cat(file=con, append=TRUE, resLaststr, "\n")

close(con)
