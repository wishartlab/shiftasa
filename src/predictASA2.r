#!/usr/local/bin/Rscript 

library(caret)
library(gbm)
library(hash)

# user-defined functions
specify_decimal <- function(x, k) format(round(x, k), nsmall=k)

assign_num_by_asa <- function(x) {
	if (x>=0.0 && x<=0.09){
	    num = 0 		
        }else if (x>=0.1 && x<=0.19){
            num = 1
        }else if (x>=0.2 && x<=0.29){
            num = 2
        }else if (x>=0.3 && x<=0.39){
            num = 3
        }else if (x>=0.4 && x<=0.49){
            num = 4
        }else if (x>=0.5 && x<=0.59){
            num = 5
        }else if (x>=0.6 && x<=0.69){
            num = 6
        }else if (x>=0.7 && x<=0.79){
            num = 7
        }else if (x>=0.8 && x<=0.89){
            num = 8
        }else if ((x>=0.9 && x<=1.0) || (x> 1.0) ){
            num = 9
        }
    return (num)
}

# user-defined variables 
AAthree2One <- hash()
.set(AAthree2One, 'ALA' = 'A','ARG' = 'R','ASN' = 'N','ASP' = 'D','ASX' = 'B','CYS' = 'C','GLN' = 'Q',
'GLU' = 'E','GLX' = 'Z','GLY' = 'G','HIS' = 'H','ILE' = 'I','LEU' = 'L','LYS' = 'K',
'MET' = 'M','PHE' = 'F','PRO' = 'P','SER' = 'S','THR' = 'T','TRP' = 'W','TYR' = 'Y',
'VAL' = 'V','PCA' = 'X' )


# loading the 3-residue fetaure GBM Model
load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_jsscon_30proteins.RData")
# loading the single-res feature GBM Model
load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_1res_jsscon_30proteins.RData")


Args <- commandArgs(TRUE)
 
if (length(Args) < 1) {
  cat("\nError: No input file supplied.")
  cat("\n\t Syntax: predictASA.R <input-bmrb> \n\n")
  quit()
}

prediction_model_to_choose= paste(Args[2])
scrci_flag = paste(Args[3])

# naming the final output file
outfile1 = paste(Args[1], "asapred.vert", sep=".")
outfile2 = paste(Args[1], "asapred.horiz", sep=".")

## single-residue feature prediction
infile = paste(Args[1], "bbrcifeat1res",sep=".")
test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
testData = test.asa_features[,3:8]
resnList = test.asa_features[,1]
residueList = test.asa_features[,2]
newdata = data.frame(x=testData)


test.data.predict=predict(ASAPredgbmFitGaussian1resjsscon30proteins$finalModel, newdata, type="response", n.trees=120)# the last argument indicates number of iterations needed to predict the response

singleResPredict=test.data.predict

len = length(singleResPredict)
lastResNum = paste(resnList[len])

startResn = resnList[1]
lastResn = resnList[len]
startResidue = residueList[1]
lastResidue = residueList[len]

#=====================================================================================================

## 3-residue feature prediction
infile = paste(Args[1], "bbrcifeat",sep=".")

test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
testData = test.asa_features[,3:20]
newdata = data.frame(x=testData)

#============================================default method for prediction=============================

#backbone shifts only prediction
test.data.predict=predict(ASAPredgbmFitGaussian3resjsscon30proteins$finalModel, newdata, type="response", n.trees=240)# the last argument indicates number of iteration trees needed in the model

bb_predict=test.data.predict


if (scrci_flag == "yes"){
  #backbone+sidechain shifts prediction

  infile = paste(Args[1], "scrcifeat",sep=".")

  test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  testData = test.asa_features[,3:23]
  newdata = data.frame(x=testData)

  load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_sideChainRCI.RData")
  test.data.predict=predict(ASAPredgbmFitGaussian3resSideChainRCI$finalModel, newdata, type="response", n.trees=160)# the last argument indicates number of iteration trees needed in the model

  bb_sc_predict=test.data.predict

  if (length(bb_sc_predict) != 0){

        coef = cor(bb_predict, bb_sc_predict, method ="spearman")
        cat ("bb vs. bb+sc estimation corr: ",coef, " \n")
        if (coef > 0.95){
                predict1 = bb_sc_predict
        }else{
                predict1 = bb_predict
        }
  }else{

        predict1 = bb_predict
  }

}else{
     predict1 = bb_predict
}


#===================================================================================================

#using sequence derived ASA values

predict2 = list()

if (prediction_model_to_choose == "sable_scrci"){
  infile = paste(Args[1], "sablescrcifeat",sep=".")
  
  test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  testData = test.asa_features[,3:26]
  newdata = data.frame(x=testData)
  
  # backbone + side-chain + sable
  load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_Sable_scrci.RData")
 
  test.data.predict=predict(ASAPredgbmFitGaussian3resSableSCrci$finalModel, newdata, type="response", n.trees=160)# the last argument indicates number of iteration trees needed in the model
  
  predict2=test.data.predict
  
  
}else if (prediction_model_to_choose == "sable"){ 
  infile = paste(Args[1], "sablefeat",sep=".")

  test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  testData = test.asa_features[,3:23]
  newdata = data.frame(x=testData)

  # backbone + sable
  load("/apps/shiftasa/project/project/Standalone_ShiftASA/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_Sable.RData")
  
  test.data.predict=predict(ASAPredgbmFitGaussian3resSable$finalModel, newdata, type="response", n.trees=160)# the last argument indicates number of iteration trees needed in the model

  predict2=test.data.predict

}

cat (predict1, "\n\n")
if (length(predict2) != 0){

	coef = cor(predict1, predict2, method ="spearman")
        cat ("chem. shift vs. chem. shift + seq estimation corr: ",coef, " \n")
	if (coef > 0.95){
  		finalPred = predict1
	}else{
  		finalPred = predict2
	}
}else{# in case using only backbone shifts

	finalPred = predict1
}
  

#================================================================================================================
tempResnList = resnList
tempResidueList = paste(residueList)

resnList = NULL
residueList = NULL
oneLetterResidueList = NULL
ASANum = NULL
ASApred = NULL

resnList[1] = startResn
residueList[1] = paste(startResidue)
oneLetterResidueList[1] = AAthree2One [[residueList[1]]]
ASApred[1] = specify_decimal(singleResPredict[1],2)
ASANum[1] = assign_num_by_asa(ASApred[1]) 
j = 2
for (i in 1:length(finalPred)){
    if (finalPred[i] < 0.0){
       finalPred [i] = 0.0
    }
    ASApred[j] = specify_decimal(finalPred[i], 2)
    ASANum[j] = assign_num_by_asa(ASApred[j])
    resnList[j] = tempResnList[j]
    residueList[j] = tempResidueList[j]
    oneLetterResidueList[j] = AAthree2One[[residueList[j]]]
    j = j+1  	
    
}

resnList[j] = lastResn
residueList[j] = paste(lastResidue)
oneLetterResidueList[j] = AAthree2One[[residueList[j]]]
ASApred[j] = specify_decimal(singleResPredict[length(singleResPredict)],2)
ASANum[j] = assign_num_by_asa(ASApred[j])


# horizontal representation of output

con=file(outfile2, "w")

len = length(ASANum)
n = ceiling(len/50)
j = 1

AAvector = NULL
ASAvector = NULL

for (i in 1:n){
    start = j
    end = j+50-1

    if(end > len){
       end = len
    }
    ASAvector = paste(ASANum[start:end], sep='', collapse='')
    AAvector=paste(oneLetterResidueList[start:end], sep='', collapse='')
    ASAstr = paste (resnList[start], ASAvector, resnList[end], sep="   ")
    AAstr = paste(resnList[start], AAvector, resnList[end], sep ="   ")

    #save to a file
    cat( file=con, append=TRUE, AAstr, "\n" )
    cat( file=con, append=TRUE, ASAstr, "\n\n" )

    j = j+ 50
}
close(con)

# vertical representation of output

con=file(outfile1, "w")
oheader = "#Num Res fASA";
cat(file=con, append=TRUE, oheader, "\n")

## ASA class assignment
#class = NULL
#for (i in 1:len(ASApred)){
# if (ASApred[i]>0.25){
#  class[i] = 'E'
#}else{
# class[i]='B'
#}

for (i in 1:length(resnList)){
  if (lastResNum>= 100){
	if (resnList[i]<10){
		space = "  "
        }else if (resnList[i] >= 10){
		space =" "
	}else if (resnList[i] > 99){
		space =""
	}
  }else if (lastResNum< 100){
	if (resnList[i]< 10 ){
		space = " "
	}else if (resnList[i]> 10 ){
		space = ""
	}
  }

  # with 2-class classifucation  
  #ostr = paste (resnList[i], space, residueList[i], specify_decimal(pred[i],2), class[i], sep=" ")
  
  # w/o 2-class classifcation
  ostr = paste (resnList[i], space, residueList[i], ASApred[i], sep=" ")
  #save to a file
  cat( file=con, append=TRUE, ostr,  "\n" )
}

close(con)
