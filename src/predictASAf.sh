#!/bin/bash
# a stand-alone program to predict fractional ASA from experimental shift
# it takes input bmrbid as only input 
# steps to predict fractional ASA from bmrb file
# a. bmrb2talos conversion - a shell script
# b. seccs with nrc calculation - perl script
# c. sec str. probability with incomplete shift - perl script
# d. backbone RCI prediction - a system call
# e. calculate per-residue conservation score
# f. create 3-residue window feature file - python script
# g. invoke GBM model to predict on unknown protein - an R script

#export R_LIBS=/usr/lib64/R/library

# reading the command line arguments
bmrbid=$1
prediction_model_to_choose=$2
scrci_flag=$3
src_dir="/var/www/html/shiftasa/Standalone_ShiftASA/src"
 
R_DEFAULT_PACKAGES='utils,grDevices,graphics,stats'
######################################
# feature file creation and prediction
######################################
python3 $src_dir/createASAPredFeatureFile_UnScaled.py $bmrbid $prediction_model_to_choose $scrci_flag>> shiftasa_log

#echo $R_LIBS
/usr/local/bin/Rscript --vanilla $src_dir/predictASA2.r $bmrbid  $prediction_model_to_choose $scrci_flag>> shiftasa_log
