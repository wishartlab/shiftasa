#!/usr/bin/perl

# input file, ncbi blast and current working directory are provided as command-line arguments
$file = $ARGV[0];
$blast = $ARGV[1];
$work_dir = $ARGV[2];

$blastbin= "$blast/bin";
$blastdb = "$blast/db/uniref90.fasta";
#blastdb = "$blast/db/nr";
$scon_dir =  "/apps/shiftasa/project/Standalone_ShiftASA/conservation_code";


$testcase = $file;
print ("processing $testcase...\n");

        chdir($scon_dir);
        unless (-e "$work_dir/$testcase.aln"){
	# run blastpgp using nr clustered database, alignment format: blast, number of iterations = 1
	system("$blastbin/blastpgp -d $blastdb -i $work_dir/$testcase.fasta -m 0 -j 1 -o $work_dir/$testcase.aln");
	}
        
	# convert blast format into msf format using mview
	system("mview-1.56/bin/mview -in blast -out msf $work_dir/$testcase.aln > $work_dir/$testcase.msf");
	# run clustal omega alignment, input alignment: msf, output alignment: fasta, no. iteration: 3
	system("./clustalo --in $work_dir/$testcase.msf --infmt msf --outfmt fasta --iter 3 --force --out $work_dir/$testcase.clustalo");
		
	# calculate conservation score using shannon entropy
	# input: multiple alignment file in fasta format, output: per-residue conservation score
	system("python score_conservation.py -s js_divergence $work_dir/$testcase.clustalo > $work_dir/$testcase.jsscon");
	system("python score_conservation.py -s shannon_entropy $work_dir/$testcase.clustalo > $work_dir/$testcase.scon");	
