#!/usr/bin/perl

#$parentdir = "/Users/Noor/Documents/Research/CMPUT-605Dec12/CMPUT-605/testfiles";
#$tabdir = "$parentdir/CS-addPDBresno-residueLabel";
#$nrcdir =  "$parentdir/CS-addPDBresno-residueLabel-nrc";

#$parentdir = "/Users/Noor/Documents/Research/RefDB-dataset";
#$tabdir = "$parentdir/Mark_30_protiens_cs_files";
#$nrcdir =  "$parentdir/Mark_30_protiens_cs_files";

#opendir $dir, $tabdir;
#@tabfiles = grep /tab$/, readdir $dir;

my %three2one =('ALA' => 'A','ARG' => 'R','ASN' => 'N','ASP' => 'D','ASX' => 'B','CYS' => 'C','GLN' => 'Q',
	     'GLU' => 'E','GLX' => 'Z','GLY' => 'G','HIS' => 'H','ILE' => 'I','LEU' => 'L','LYS' => 'K',
	     'MET' => 'M','PHE' => 'F','PRO' => 'P','SER' => 'S','THR' => 'T','TRP' => 'W','TYR' => 'Y',
	     'VAL' => 'V','PCA' => 'X');
             
# Reference: Schwarzinger et al. (2000) random coil chemical shift table
# Random Coil chemical shifts in acidic 8M urea: Implementation of  random
# coil shift data in NMR view, Schwarzinger et al. (2000)
#used Schwarzinger et al. random coil chemical shift table 
my %RandomCoil = ('GLY_HN' =>  8.33,'GLY_N' =>  107.5,'GLY_HA' =>  4.02,'GLY_CA' =>  45.4,'GLY_CB' =>  0.0,'GLY_C' =>  174.9,'GLY_HN' => 8.41,
		  'ALA_HN' =>  8.35,'ALA_N' =>  125.0,'ALA_HA' =>  4.35,'ALA_CA' =>  52.8,'ALA_CB' =>  19.3,'ALA_C' =>  178.5,
		  'CYS_HN' =>  8.44,'CYS_N' =>  118.8,'CYS_HA' =>  4.59,'CYS_CA' =>  58.6,'CYS_CB' =>  28.3,'CYS_C' =>  175.3,
		  'ASP_HN' =>  8.56,'ASP_N' =>  119.1,'ASP_HA' =>  4.82,'ASP_CA' =>  53.0,'ASP_CB' =>  38.3,'ASP_C' =>  175.9,
		  'GLU_HN' =>  8.40,'GLU_N' =>  120.2,'GLU_HA' =>  4.42,'GLU_CA' =>  56.1,'GLU_CB' =>  29.9,'GLU_C' =>  176.8,
		  'PHE_HN' =>  8.31,'PHE_N' =>  120.7,'PHE_HA' =>  4.65,'PHE_CA' =>  58.1,'PHE_CB' =>  39.8,'PHE_C' =>  176.6,
		  'HIS_HN' =>  8.56,'HIS_N' =>  118.1,'HIS_HA' =>  4.79,'HIS_CA' =>  55.4,'HIS_CB' =>  29.1,'HIS_C' =>  175.1,
		  'ILE_HN' =>  8.17,'ILE_N' =>  120.4,'ILE_HA' =>  4.21,'ILE_CA' =>  61.6,'ILE_CB' =>  38.9,'ILE_C' =>  177.1,
		  'LYS_HN' =>  8.36,'LYS_N' =>  121.6,'LYS_HA' =>  4.36,'LYS_CA' =>  56.7,'LYS_CB' =>  33.2,'LYS_C' =>  177.4,
		  'LEU_HN' =>  8.28,'LEU_N' =>  122.4,'LEU_HA' =>  4.38,'LEU_CA' =>  55.5,'LEU_CB' =>  42.5,'LEU_C' =>  178.2,
		  'MET_HN' =>  8.42,'MET_N' =>  120.3,'MET_HA' =>  4.52,'MET_CA' =>  55.8,'MET_CB' =>  32.9,'MET_C' =>  177.1,
		  'ASN_HN' =>  8.51,'ASN_N' =>  119.0,'ASN_HA' =>  4.79,'ASN_CA' =>  53.3,'ASN_CB' =>  39.1,'ASN_C' =>  176.1,
		  'PRO_HN' =>  0.0, 'PRO_N' =>  0.0,  'PRO_HA' =>  4.45,'PRO_CA' =>  63.7,'PRO_CB' =>  32.2,'PRO_C' =>  177.8,
		  'GLN_HN' =>  8.44,'GLN_N' =>  120.5,'GLN_HA' =>  4.38,'GLN_CA' =>  56.2,'GLN_CB' =>  29.5,'GLN_C' =>  176.8,
		  'ARG_HN' =>  8.39,'ARG_N' =>  121.2,'ARG_HA' =>  4.38,'ARG_CA' =>  56.5,'ARG_CB' =>  30.9,'ARG_C' =>  177.1,
		  'SER_HN' =>  8.43,'SER_N' =>  115.5,'SER_HA' =>  4.51,'SER_CA' =>  58.7,'SER_CB' =>  64.1,'SER_C' =>  175.4,
		  'THR_HN' =>  8.25,'THR_N' =>  112.0,'THR_HA' =>  4.43,'THR_CA' =>  62.0,'THR_CB' =>  70.0,'THR_C' =>  175.6,
		  'VAL_HN' =>  8.16,'VAL_N' =>  119.3,'VAL_HA' =>  4.16,'VAL_CA' =>  62.6,'VAL_CB' =>  31.8,'VAL_C' =>  177.0,
		  'TRP_HN' =>  8.22,'TRP_N' =>  122.1,'TRP_HA' =>  4.70,'TRP_CA' =>  57.6,'TRP_CB' =>  29.8,'TRP_C' =>  177.1,
		  'TYR_HN' =>  8.26,'TYR_N' =>  120.9,'TYR_HA' =>  4.58,'TYR_CA' =>  58.3,'TYR_CB' =>  38.9,'TYR_C' =>  176.7);

my %nrc=('ALA_B_CA' => -0.17,'ALA_C_CA' => 0.06,'ALA_B_HA' => -0.03,'ALA_C_HA' => -0.03,'ALA_B_HN' => -0.05,'ALA_C_HN' => 0.07,'ALA_B_N' => -0.33,'ALA_C_N' => -0.57,
'ALA_B_C' => -0.77,'ALA_C_C' => -0.07,'ARG_B_CA' => -0.07,'ARG_C_CA' => -0.01,'ARG_B_HN' => -0.02,'ARG_C_HN' => 0.15,'ARG_B_HA' => -0.02,'ARG_C_HA' => -0.02,
'ARG_B_N' => -0.14,'ARG_C_N' => 1.62,'ARG_B_C' => -0.49,'ARG_C_C' => -0.19,'ASN_B_CA' => -0.03,'ASN_C_CA' => 0.23,'ASN_B_HN' => -0.03,'ASN_C_HN' => 0.13,
'ASN_B_HA' => -0.01,'ASN_C_HA' => -0.02,'ASN_B_N' => -0.26,'ASN_C_N' => 0.87,'ASN_B_C' => -0.66,'ASN_C_C' => -0.10,'ASP_B_CA' => 0.00,'ASP_C_CA' => 0.25,
'ASP_B_HN' => -0.03,'ASP_C_HN' => 0.14,'ASP_B_HA' => -0.01,'ASP_C_HA' => -0.02,'ASP_B_N' => -0.20,'ASP_C_N' => 0.86,'ASP_B_C' => -0.58,'ASP_C_C' => -0.13,
'CYS_B_CA' => -0.07,'CYS_C_CA' => 0.10,'CYS_B_HN' => -0.02,'CYS_C_HN' => 0.20,'CYS_B_HA' => 0.02,'CYS_C_HA' => 0.00,'CYS_B_N' => -0.26,'CYS_C_N' => 3.07,
'CYS_B_C' => -0.51,'CYS_C_C' => -0.28,'GLN_B_CA' => -0.06,'GLN_C_CA' => 0.04,'GLN_B_HN' => -0.02,'GLN_C_HN' => 0.15,'GLN_B_HA' => -0.02,'GLN_C_HA' => -0.01,
'GLN_B_N' => -0.14,'GLN_C_N' => 1.62,'GLN_B_C' => -0.48,'GLN_C_C' => -0.18,'GLU_B_CA' => -0.08,'GLU_C_CA' => 0.05,'GLU_B_HN' => -0.03,'GLU_C_HN' => 0.15,
'GLU_B_HA' => -0.02,'GLU_C_HA' => -0.02,'GLU_B_N' => -0.20,'GLU_C_N' => 1.51,'GLU_B_C' => -0.48,'GLU_C_C' => -0.20,'GLY_B_CA' => 0,'GLY_C_CA' => 0,'GLY_B_HN' => 0,
'GLY_C_HN' => 0,'GLY_B_HA' => 0,'GLY_C_HA' => 0,'GLY_B_N' => 0,'GLY_C_N' => 0,'GLY_B_C' => 0,'GLY_C_C' => 0,'HIS_B_CA' => -0.09,'HIS_C_CA' => 0.02,'HIS_B_HN' => -0.04,
'HIS_C_HN' => 0.20,'HIS_B_HA' => -0.06,'HIS_C_HA' => 0.01,'HIS_B_N' => -0.55,'HIS_C_N' => 1.68,'HIS_B_C' => -0.65,'HIS_C_C' => -0.22,'ILE_B_CA' => -0.20,'ILE_C_CA' => -0.01,
'ILE_B_HN' => -0.06,'ILE_C_HN' => 0.17,'ILE_B_HA' => -0.02,'ILE_C_HA' => -0.02,'ILE_B_N' => -0.14,'ILE_C_N' => 4.87,'ILE_B_C' => -0.58,'ILE_C_C' => -0.18,
'LEU_B_CA' => -0.10,'LEU_C_CA' => 0.03,'LEU_B_HN' => -0.03,'LEU_C_HN' => 0.14,'LEU_B_HA' => -0.03,'LEU_C_HA' => -0.05,'LEU_B_N' => -0.14,'LEU_C_N' => 1.05,
'LEU_B_C' => -0.50,'LEU_C_C' => -0.13,'LYS_B_CA' => -0.11,'LYS_C_CA' => -0.02,'LYS_B_HN' => -0.03,'LYS_C_HN' => 0.14,'LYS_B_HA' => -0.02,'LYS_C_HA' => -0.01,
'LYS_B_N' => -0.20,'LYS_C_N' => 1.57,'LYS_B_C' => -0.50,'LYS_C_C' => -0.18,'MET_B_CA' => -0.10,'MET_C_CA' => -0.06,'MET_B_HN' => -0.02,'MET_C_HN' => 0.15,
'MET_B_HA' => -0.01,'MET_C_HA' => -0.01,'MET_B_N' => -0.20,'MET_C_N' => 1.57,'MET_B_C' => -0.41,'MET_C_C' => -0.18,'PHE_B_CA' => -0.23,'PHE_C_CA' => 0.06,
'PHE_B_HN' => -0.12,'PHE_C_HN' => 0.10,'PHE_B_HA' => -0.09,'PHE_C_HA' => -0.08,'PHE_B_N' => -0.49,'PHE_C_N' => 2.78,'PHE_B_C' => -0.83,'PHE_C_C' => -0.25,
'PRO_B_CA' => -2.0,'PRO_C_CA' => 0.02,'PRO_B_HN' => -0.18,'PRO_C_HN' => 0.19,'PRO_B_HA' => 0.11,'PRO_C_HA' => -0.03,'PRO_B_N' => -0.32,'PRO_C_N' => 0.87,'PRO_B_C' => -2.84,
'PRO_C_C' => -0.09,'SER_B_CA' => -0.08,'SER_C_CA' => 0.13,'SER_B_HN' => -0.03,'SER_C_HN' => 0.16,'SER_B_HA' => 0.02,'SER_C_HA' => 0.00,'SER_B_N' => -0.03,
'SER_C_N' => 2.55,'SER_B_C' => -0.40,'SER_C_C' => -0.15,'THR_B_CA' => -0.04,'THR_C_CA' => 0.12,'THR_B_HN' => 0.00,'THR_C_HN' => 0.14,'THR_B_HA' => 0.05,
'THR_C_HA' => 0.00,'THR_B_N' => -0.03,'THR_C_N' => 2.78,'THR_B_C' => -0.19,'THR_C_C' => -0.13,'TRP_B_CA' => -0.17,'TRP_C_CA' => 0.03,'TRP_B_HN' => -0.13,
'TRP_C_HN' => 0.04,'TRP_B_HA' => -0.10,'TRP_C_HA' => -0.15,'TRP_B_N' => -0.26,'TRP_C_N' => 3.19,'TRP_B_C' => -0.85,'TRP_C_C' => -0.30,'TYR_B_CA' => -0.22,'TYR_C_CA' => 0.06,
'TYR_B_HN' => -0.11,'TYR_C_HN' => 0.09,'TYR_B_HA' => -0.10,'TYR_C_HA' => -0.08,'TYR_B_N' => -0.43,'TYR_C_N' => 3.01,'TYR_B_C' => -0.85,'TYR_C_C' => -0.24,
'VAL_B_CA' => -0.21,'VAL_C_CA' => -0.02,'VAL_B_HN' => -0.05,'VAL_C_HN' => 0.17,'VAL_B_HA' => -0.01,'VAL_C_HA' => -0.02,'VAL_B_N' => -0.14,'VAL_C_N' => 4.34,
'VAL_B_C' => -0.57,'VAL_C_C' => -0.18);             

@atoms = ('HA', 'CA', 'CB', 'C', 'N', 'HN');

#for $file(@tabfiles){
    $file = $ARGV[0];
    open $in, "<$file";
    ($name, $ext) = split /\./, $file;
    open $out, ">$name.nrc"; 
    $startResFound = 0;
    %cs = ();
    %aa =();
    while(<$in>){
        chomp($_);
        
        if ($_=~/^\s+\d+/ or $_=~/^\d+\s+/){
            $_ =~ s/^\s+//;  #removing leading white space  
            @tmp = split /\s+/, $_;
	    
	    if (@tmp == 5){
			$resn = $tmp[1];
			if ($startResFound == 0){
		    	$startRes = $resn;
		    	$startResFound = 1;
			}     
			$endRes = $resn;
			$AA = $tmp[2];
			$atom=$tmp[3];
			$absShift =  $tmp[4];
			$pdbresid{$tmp[1]} = $tmp[0];
	    }elsif (@tmp==4){
			$resn = $tmp[0];
			
			if ($startResFound == 0){
		    	$startRes = $resn;
		    	$startResFound = 1;
			}     
			$endRes = $resn;
			$AA = $tmp[1];
			$atom=$tmp[2];
			$absShift =  $tmp[3];
		
	    }	
        if($AA eq 'GLY' and ($atom eq 'HA2' or $atom eq 'HA3')){
                if ($atom eq 'HA2'){
                    $HA2 = $absShift;
                } if ($atom eq 'HA3'){
                    $HA3 = $absShift;
                    $absShift = sprintf("%0.3f",($HA2+$HA3)/2);
                    $cs{$resn.'_HA'} = $absShift;
                    $aa{$resn} = $AA;
                }
                
        }else{
                $cs{$resn.'_'.$atom} = $absShift;
                $aa{$resn} = $AA; 
            } 
        }
		else{
	    	if($_=~ /^VARS/){
				#print $out "VARS PDBRESID RESNAME ATOMNAME SHIFT RCSHIFT SECSHIFT\n";    
				print $out "\n$_\n";
	    	}elsif($_=~ /^FORMAT/){
				#print $out "FORMAT %4d %4s %4s %8.3f %8.3f %8.3f\n";
				print $out "$_\n\n";
	    	}
	    	elsif($_=~ /\d+[A-Z]+/){
				print $out "$_\n";
	    	}elsif($_=~ /^DATA/){
			#print $out "FORMAT %4d %4s %4s %8.3f %8.3f %8.3f\n";
			print $out "$_\n";
	    	}
		}
	
    
    }
    
    
    
    for ($i = $startRes; $i<=$endRes; $i++ ){
	for ($j = 0; $j < @atoms; $j++){
	    if (exists $cs{$i.'_'.$atoms[$j]}){
		$absShift = $cs{$i.'_'.$atoms[$j]};
		$currentAA = $aa{$i};
		
		$randomCoil = $RandomCoil{$currentAA.'_'.$atoms[$j]};
		
		$currentAtom = $atoms[$j]; 
	
		$prevRes = $i - 1;
		$nextRes = $i + 1;
		$prevAA = $aa{$prevRes};
		$nextAA = $aa{$nextRes};
	
	
		$randomCoil_nrc = $randomCoil + $nrc{$prevAA.'_C_'.$currentAtom}+ $nrc{$nextAA.'_B_'.$currentAtom};
		$secShift_nrc = sprintf("%0.3f", $absShift - $randomCoil_nrc);
	    
		#print "$i $currentAA $currentAtom $absShift $randomCoil_nrc $secShift_nrc\n";
		print $out "$i $currentAA $currentAtom $absShift $randomCoil_nrc $secShift_nrc\n";
	    } 	
	}    
    }
    
#}    
    