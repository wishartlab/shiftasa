set terminal gif size 800, 600
set output "asapred.jpg"

set size 1,1
set origin 0, 0
set multiplot layout 1,1
set key left

set ylabel "Fractional ASA"
set xlabel "Residue Number"
set xtics 10# set the x-axis tick mark interval
set ytics nomirror


##1st plot
set style line 1 lw 2 lc rgb "red"
#set style line 2 lw 2 lc rgb "blue"
set title "Estimated Fractional ASA"
plot 'asapred' using 1:3 ls 1 notitle with linespoints

unset multiplot
