
use Math::Complex;

%avg_seccs_by_aa_ss_type=('ALA_B_CA' => -1.11,'ALA_B_CB' => 1.84,'ALA_B_C' => -1.71,'ALA_H_CA' => 2.11,'ALA_H_CB' => -1.04,'ALA_H_C' => 1.63,'ALA_C_CA' => 0.23,'ALA_C_CB' => -0.24,'ALA_C_C' => -0.13,'ARG_B_CA' => -1.18,'ARG_B_CB' => 1.29,'ARG_B_C' => -1.17,'ARG_H_CA' => 2.51,'ARG_H_CB' => -0.76,'ARG_H_C' => 1.84,'ARG_C_CA' => 0.11,'ARG_C_CB' => -0.24,'ARG_C_C' => -0.34,'ASN_B_CA' => -0.41,'ASN_B_CB' => 1.02,'ASN_B_C' => -0.74,'ASN_H_CA' => 2.21,'ASN_H_CB' => -0.49,'ASN_H_C' => 1.52,'ASN_C_CA' => 0.1,'ASN_C_CB' => -0.55,'ASN_C_C' => -0.3,'ASP_B_CA' => 1.04,'ASP_B_CB' => 4,'ASP_B_C' => 0.37,'ASP_H_CA' => 3.78,'ASP_H_CB' => 2.21,'ASP_H_C' => 2.82,'ASP_C_CA' => 1.32,'ASP_C_CB' => 2.55,'ASP_C_C' => 1.03,'CYS_B_CA' => -2.31,'CYS_B_CB' => 8.18,'CYS_B_C' => -1.07,'CYS_H_CA' => 1.43,'CYS_H_CB' => 5.16,'CYS_H_C' => 1.53,'CYS_C_CA' => -1.91,'CYS_C_CB' => 8.59,'CYS_C_C' => 0.29,'GLN_B_CA' => -1.22,'GLN_B_CB' => 1.78,'GLN_B_C' => -1.17,'GLN_H_CA' => 2.34,'GLN_H_CB' => -0.99,'GLN_H_C' => 1.86,'GLN_C_CA' => 0.04,'GLN_C_CB' => -0.36,'GLN_C_C' => -0.24,'GLU_B_CA' => -0.43,'GLU_B_CB' => 2.11,'GLU_B_C' => -0.74,'GLU_H_CA' => 3.08,'GLU_H_CB' => -0.53,'GLU_H_C' => 2.53,'GLU_C_CA' => 0.89,'GLU_C_CB' => 0.3,'GLU_C_C' => 0.29,'HIS_B_CA' => -0.14,'HIS_B_CB' => 2.75,'HIS_B_C' => -0.17,'HIS_H_CA' => 3.69,'HIS_H_CB' => 0.44,'HIS_H_C' => 2.55,'HIS_C_CA' => 0.69,'HIS_C_CB' => 0.87,'HIS_C_C' => 0.42,'ILE_B_CA' => -1.42,'ILE_B_CB' => 0.96,'ILE_B_C' => -1.53,'ILE_H_CA' => 3.06,'ILE_H_CB' => -1.3,'ILE_H_C' => 1.34,'ILE_C_CA' => -0.34,'ILE_C_CB' => -0.25,'ILE_C_C' => -0.78,'LEU_B_CA' => -1.3,'LEU_B_CB' => 1.29,'LEU_B_C' => -1.83,'LEU_H_CA' => 2.07,'LEU_H_CB' => -0.85,'LEU_H_C' => 1,'LEU_C_CA' => -0.39,'LEU_C_CB' => -0.12,'LEU_C_C' => -0.6,'LYS_B_CA' => -1.17,'LYS_B_CB' => 1.43,'LYS_B_C' => -1.38,'LYS_H_CA' => 2.31,'LYS_H_CB' => -0.93,'LYS_H_C' => 1.7,'LYS_C_CA' => 0,'LYS_C_CB' => -0.41,'LYS_C_C' => -0.39,'GLY_B_CA' => -0.04,'GLY_B_CB' => 0,'GLY_B_C' => -1.64,'GLY_H_CA' => 1.64,'GLY_H_CB' => 0,'GLY_H_C' => 1.32,'GLY_C_CA' => 0.19,'GLY_C_CB' => 0,'GLY_C_C' => -0.34,'MET_B_CA' => -1.09,'MET_B_CB' => 2.15,'MET_B_C' => -1.61,'MET_H_CA' => 2.38,'MET_H_CB' => -0.63,'MET_H_C' => 1.55,'MET_C_CA' => -0.01,'MET_C_CB' => 0.46,'MET_C_C' => -1.06,'PHE_B_CA' => -1.33,'PHE_B_CB' => 1.74,'PHE_B_C' => -1.65,'PHE_H_CA' => 2.78,'PHE_H_CB' => -1.02,'PHE_H_C' => 1.23,'PHE_C_CA' => -0.02,'PHE_C_CB' => -0.35,'PHE_C_C' => -0.39,'PRO_B_CA' => -0.93,'PRO_B_CB' => 0.07,'PRO_B_C' => -0.92,'PRO_H_CA' => 1.84,'PRO_H_CB' => -0.74,'PRO_H_C' => 1.22,'PRO_C_CA' => -0.15,'PRO_C_CB' => -0.26,'PRO_C_C' => -0.27,'SER_B_CA' => -1,'SER_B_CB' => 1.06,'SER_B_C' => -1.11,'SER_H_CA' => 2.25,'SER_H_CB' => -1.02,'SER_H_C' => 1.24,'SER_C_CA' => -0.17,'SER_C_CB' => -0.07,'SER_C_C' => -0.25,'THR_B_CA' => -0.8,'THR_B_CB' => 0.75,'THR_B_C' => -1.22,'THR_H_CA' => 3.71,'THR_H_CB' => -1.12,'THR_H_C' => 1.02,'THR_C_CA' => -0.2,'THR_C_CB' => 0.12,'THR_C_C' => -0.21,'TRP_B_CA' => -1.14,'TRP_B_CB' => 1.7,'TRP_B_C' => -1.02,'TRP_H_CA' => 2.56,'TRP_H_CB' => -0.5,'TRP_H_C' => 1.7,'TRP_C_CA' => 0.23,'TRP_C_CB' => -0.13,'TRP_C_C' => -0.32,'TYR_B_CA' => -1.36,'TYR_B_CB' => 2.07,'TYR_B_C' => -1.48,'TYR_H_CA' => 2.74,'TYR_H_CB' => -0.65,'TYR_H_C' => 1.36,'TYR_C_CA' => -0.04,'TYR_C_CB' => 0.05,'TYR_C_C' => -0.54,'VAL_B_CA' => -1.62,'VAL_B_CB' => 2.11,'VAL_B_C' => -1.47,'VAL_H_CA' => 3.62,'VAL_H_CB' => -0.31,'VAL_H_C' => 1.34,'VAL_C_CA' => -0.37,'VAL_C_CB' => 0.91,'VAL_C_C' => -0.66,
                          'ALA_B_N' => -2.41,'ALA_H_N' => -5.02,'ALA_C_N' => -2.85,'ARG_B_N' => -0.87,'ARG_H_N' => -4.11,'ARG_C_N' => -2.21,'ASN_B_N' => 0.51,'ASN_H_N' => -3.41,'ASN_C_N' => -2.34,'ASP_B_N' => 1.18,'ASP_H_N' => -1.53,'ASP_C_N' => -0.6,'CYS_B_N' => 0.39,'CYS_H_N' => -2.73,'CYS_C_N' => -2.26,'GLN_B_N' => -1.18,'GLN_H_N' => -3.44,'GLN_C_N' => -2.5,'GLU_B_N' => 0.05,'GLU_H_N' => -2.65,'GLU_C_N' => -1.12,'HIS_B_N' => 0.43,'HIS_H_N' => -1.76,'HIS_C_N' => -1.1,'ILE_B_N' => 0.51,'ILE_H_N' => -2.12,'ILE_C_N' => -0.85,'LEU_B_N' => -0.18,'LEU_H_N' => -4.3,'LEU_C_N' => -2.31,'LYS_B_N' => -1.19,'LYS_H_N' => -4.01,'LYS_C_N' => -2.55,'GLY_B_N' => -0.06,'GLY_H_N' => -1.68,'GLY_C_N' => 0.19,'MET_B_N' => -0.6,'MET_H_N' => -3.75,'MET_C_N' => -1.71,'PHE_B_N' => -1.42,'PHE_H_N' => -3.05,'PHE_C_N' => -2.38,'PRO_B_N' => 0.0,'PRO_H_N' => 0.0,'PRO_C_N' => 0.0,'SER_B_N' => -0.66,'SER_H_N' => -2.28,'SER_C_N' => -1.33,'THR_B_N' => 2.62,'THR_H_N' => 1,'THR_C_N' => -0.29,'TRP_B_N' => -1.84,'TRP_H_N' => -3.66,'TRP_C_N' => -3.33,'TYR_B_N' => -1.34,'TYR_H_N' => -3.1,'TYR_C_N' => -2.68,'VAL_B_N' => 0.79,'VAL_H_N' => -1.72,'VAL_C_N' => -0.96,
                         'ALA_B_HN' => -0,'ALA_B_HA' => 0.46,'ALA_H_HN' => -0.36,'ALA_H_HA' => -0.28,'ALA_C_HN' => -0.29,'ALA_C_HA' => -0.07,'ARG_B_HN' => 0.09,'ARG_B_HA' => 0.41,'ARG_H_HN' => -0.42,'ARG_H_HA' => -0.34,'ARG_C_HN' => -0.22,'ARG_C_HA' => -0.12,'ASN_B_HN' => 0.01,'ASN_B_HA' => 0.3,'ASN_H_HN' => -0.38,'ASN_H_HA' => -0.26,'ASN_C_HN' => -0.2,'ASN_C_HA' => -0.1,'ASP_B_HN' => -0.12,'ASP_B_HA' => 0.16,'ASP_H_HN' => -0.47,'ASP_H_HA' => -0.35,'ASP_C_HN' => -0.29,'ASP_C_HA' => -0.19,'CYS_B_HN' => 0.27,'CYS_B_HA' => 0.6,'CYS_H_HN' => -0.33,'CYS_H_HA' => -0.39,'CYS_C_HN' => -0.27,'CYS_C_HA' => 0.07,'GLN_B_HN' => -0.04,'GLN_B_HA' => 0.46,'GLN_H_HN' => -0.48,'GLN_H_HA' => -0.34,'GLN_C_HN' => -0.29,'GLN_C_HA' => -0.09,'GLU_B_HN' => 0.05,'GLU_B_HA' => 0.4,'GLU_H_HN' => -0.28,'GLU_H_HA' => -0.36,'GLU_C_HN' => -0.12,'GLU_C_HA' => -0.11,'HIS_B_HN' => -0.03,'HIS_B_HA' => 0.32,'HIS_H_HN' => -0.56,'HIS_H_HA' => -0.42,'HIS_C_HN' => -0.44,'HIS_C_HA' => -0.23,'ILE_B_HN' => 0.42,'ILE_B_HA' => 0.5,'ILE_H_HN' => -0.24,'ILE_H_HA' => -0.51,'ILE_C_HN' => -0.27,'ILE_C_HA' => -0.04,'LEU_B_HN' => 0.23,'LEU_B_HA' => 0.48,'LEU_H_HN' => -0.32,'LEU_H_HA' => -0.34,'LEU_C_HN' => -0.28,'LEU_C_HA' => -0.01,'LYS_B_HN' => 0.04,'LYS_B_HA' => 0.37,'LYS_H_HN' => -0.47,'LYS_H_HA' => -0.33,'LYS_C_HN' => -0.22,'LYS_C_HA' => -0.08,'GLY_B_HN' => -0.15,'GLY_B_HA' => 0.22,'GLY_H_HN' => -0.21,'GLY_H_HA' => -0.17,'GLY_C_HN' => -0.17,'GLY_C_HA' => -0.02,'MET_B_HN' => 0.13,'MET_B_HA' => 0.47,'MET_H_HN' => -0.43,'MET_H_HA' => -0.41,'MET_C_HN' => -0.32,'MET_C_HA' => -0.12,'PHE_B_HN' => 0.35,'PHE_B_HA' => 0.46,'PHE_H_HN' => -0.23,'PHE_H_HA' => -0.45,'PHE_C_HN' => -0.22,'PHE_C_HA' => -0.09,'PRO_B_HN' => 0,'PRO_B_HA' => 0.19,'PRO_H_HN' => 0,'PRO_H_HA' => -0.19,'PRO_C_HN' => 0,'PRO_C_HA' => -0.05,'SER_B_HN' => -0.01,'SER_B_HA' => 0.45,'SER_H_HN' => -0.39,'SER_H_HA' => -0.21,'SER_C_HN' => -0.29,'SER_C_HA' => -0.01,'THR_B_HN' => 0.18,'THR_B_HA' => 0.47,'THR_H_HN' => -0.3,'THR_H_HA' => -0.39,'THR_C_HN' => -0.18,'THR_C_HA' => 0.04,'TRP_B_HN' => 0.28,'TRP_B_HA' => 0.52,'TRP_H_HN' => -0.18,'TRP_H_HA' => -0.28,'TRP_C_HN' => -0.38,'TRP_C_HA' => -0.12,'TYR_B_HN' => 0.34,'TYR_B_HA' => 0.55,'TYR_H_HN' => -0.27,'TYR_H_HA' => -0.44,'TYR_C_HN' => -0.28,'TYR_C_HA' => -0.04,'VAL_B_HN' => 0.37,'VAL_B_HA' => 0.47,'VAL_H_HN' => -0.24,'VAL_H_HA' => -0.53,'VAL_C_HN' => -0.19,'VAL_C_HA' => -0.02);

%stdev_seccs_by_aa_ss_type=('ALA_B_HN' => 0.75,'ALA_B_HA' => 0.56,'ALA_H_HN' => 0.52,'ALA_H_HA' => 0.33,'ALA_C_HN' => 0.71,'ALA_C_HA' => 0.32,'ARG_B_HN' => 0.63,'ARG_B_HA' => 0.51,'ARG_H_HN' => 0.54,'ARG_H_HA' => 0.32,'ARG_C_HN' => 0.67,'ARG_C_HA' => 0.42,'ASN_B_HN' => 0.63,'ASN_B_HA' => 0.5,'ASN_H_HN' => 0.58,'ASN_H_HA' => 0.22,
                            'ASN_C_HN' => 0.78,'ASN_C_HA' => 0.35,'ASP_B_HN' => 0.6,'ASP_B_HA' => 0.4,'ASP_H_HN' => 0.56,'ASP_H_HA' => 0.22,'ASP_C_HN' => 0.62,'ASP_C_HA' => 0.28,'CYS_B_HN' => 0.64,'CYS_B_HA' => 0.52,'CYS_H_HN' => 0.7,'CYS_H_HA' => 0.66,'CYS_C_HN' => 0.72,'CYS_C_HA' => 0.38,'GLN_B_HN' => 0.65,'GLN_B_HA' => 0.5,
                            'GLN_H_HN' => 0.55,'GLN_H_HA' => 0.27,'GLN_C_HN' => 0.65,'GLN_C_HA' => 0.33,'GLU_B_HN' => 0.61,'GLU_B_HA' => 0.5,'GLU_H_HN' => 0.62,'GLU_H_HA' => 0.25,'GLU_C_HN' => 0.67,'GLU_C_HA' => 0.32,'HIS_B_HN' => 0.74,'HIS_B_HA' => 0.48,'HIS_H_HN' => 0.55,'HIS_H_HA' => 0.33,'HIS_C_HN' => 0.79,'HIS_C_HA' => 0.49,
                            'ILE_B_HN' => 0.69,'ILE_B_HA' => 0.48,'ILE_H_HN' => 0.52,'ILE_H_HA' => 0.33,'ILE_C_HN' => 0.83,'ILE_C_HA' => 0.38,'LEU_B_HN' => 0.7,'LEU_B_HA' => 0.47,'LEU_H_HN' => 0.55,'LEU_H_HA' => 0.34,'LEU_C_HN' => 0.76,'LEU_C_HA' => 0.37,'LYS_B_HN' => 0.67,'LYS_B_HA' => 0.52,'LYS_H_HN' => 0.56,'LYS_H_HA' => 0.3,
                            'LYS_C_HN' => 0.72,'LYS_C_HA' => 0.4,'GLY_B_HN' => 0.86,'GLY_B_HA' => 0.59,'GLY_H_HN' => 0.67,'GLY_H_HA' => 0.38,'GLY_C_HN' => 0.77,'GLY_C_HA' => 0.35,'MET_B_HN' => 0.67,'MET_B_HA' => 0.48,'MET_H_HN' => 0.58,'MET_H_HA' => 0.34,'MET_C_HN' => 0.64,'MET_C_HA' => 0.41,'PHE_B_HN' => 0.71,'PHE_B_HA' => 0.46,
                            'PHE_H_HN' => 0.62,'PHE_H_HA' => 0.46,'PHE_C_HN' => 0.81,'PHE_C_HA' => 0.47,'PRO_B_HN' => 0,'PRO_B_HA' => 0.5,'PRO_H_HN' => 0,'PRO_H_HA' => 0.3,'PRO_C_HN' => 0,'PRO_C_HA' => 0.34,'SER_B_HN' => 0.66,'SER_B_HA' => 0.49,'SER_H_HN' => 0.56,'SER_H_HA' => 0.24,'SER_C_HN' => 0.65,'SER_C_HA' => 0.34,'THR_B_HN' => 0.6,
                            'THR_B_HA' => 0.46,'THR_H_HN' => 0.51,'THR_H_HA' => 0.33,'THR_C_HN' => 0.69,'THR_C_HA' => 0.35,'TRP_B_HN' => 0.82,'TRP_B_HA' => 0.5,'TRP_H_HN' => 0.74,'TRP_H_HA' => 0.38,'TRP_C_HN' => 0.89,'TRP_C_HA' => 0.48,'TYR_B_HN' => 0.75,'TYR_B_HA' => 0.54,'TYR_H_HN' => 0.62,'TYR_H_HA' => 0.39,'TYR_C_HN' => 0.77,
                            'TYR_C_HA' => 0.44,'VAL_B_HN' => 0.68,'VAL_B_HA' => 0.48,'VAL_H_HN' => 0.65,'VAL_H_HA' => 0.35,'VAL_C_HN' => 0.65,'VAL_C_HA' => 0.41,'ALA_B_N' => 4.09,'ALA_H_N' => 2.59,'ALA_C_N' => 3.68,'ARG_B_N' => 3.93,'ARG_H_N' => 2.95,'ARG_C_N' => 4.23,'ASN_B_N' => 3.87,'ASN_H_N' => 3.08,'ASN_C_N' => 4.45,'ASP_B_N' => 4.01,
                            'ASP_H_N' => 2.77,'ASP_C_N' => 4.3,'CYS_B_N' => 4.36,'CYS_H_N' => 3.63,'CYS_C_N' => 3.96,'GLN_B_N' => 3.71,'GLN_H_N' => 2.94,'GLN_C_N' => 4.02,'GLU_B_N' => 3.66,'GLU_H_N' => 2.89,'GLU_C_N' => 3.89,'HIS_B_N' => 4.06,'HIS_H_N' => 2.95,'HIS_C_N' => 4.43,'ILE_B_N' => 4.38,'ILE_H_N' => 2.94,'ILE_C_N' => 5.22,
                            'LEU_B_N' => 3.91,'LEU_H_N' => 2.96,'LEU_C_N' => 4.18,'LYS_B_N' => 3.89,'LYS_H_N' => 2.73,'LYS_C_N' => 4,'GLY_B_N' => 3.78,'GLY_H_N' => 2.91,'GLY_C_N' => 3.82,'MET_B_N' => 3.76,'MET_H_N' => 3.03,'MET_C_N' => 3.88,'PHE_B_N' => 4.19,'PHE_H_N' => 3.59,'PHE_C_N' => 4.67,'PRO_B_N' => 0.0,
                            'PRO_H_N' => 0.0,'PRO_C_N' => 0.0,'SER_B_N' => 3.79,'SER_H_N' => 3.01,'SER_C_N' => 3.89,'THR_B_N' => 4.77,'THR_H_N' => 4.09,'THR_C_N' => 4.85,'TRP_B_N' => 4.98,'TRP_H_N' => 3.01,'TRP_C_N' => 5.26,'TYR_B_N' => 4.33,'TYR_H_N' => 3.05,'TYR_C_N' => 4.99,'VAL_B_N' => 4.84,'VAL_H_N' => 3.74,'VAL_C_N' => 5.3,
                            'ALA_B_CA' => 1.44,'ALA_B_CB' => 2.05,'ALA_B_C' => 1.44,'ALA_H_CA' => 1.11,'ALA_H_CB' => 0.88,'ALA_H_C' => 1.3,'ALA_C_CA' => 1.5,'ALA_C_CB' => 1.26,'ALA_C_C' => 1.51,'ARG_B_CA' => 1.63,'ARG_B_CB' => 1.8,'ARG_B_C' => 1.3,'ARG_H_CA' => 1.57,'ARG_H_CB' => 1.14,'ARG_H_C' => 1.42,'ARG_C_CA' => 1.78,'ARG_C_CB' => 1.67,
                            'ARG_C_C' => 1.58,'ASN_B_CA' => 1.43,'ASN_B_CB' => 2.07,'ASN_B_C' => 1.63,'ASN_H_CA' => 1.43,'ASN_H_CB' => 1.31,'ASN_H_C' => 1.58,'ASN_C_CA' => 1.39,'ASN_C_CB' => 1.41,'ASN_C_C' => 1.44,'ASP_B_CA' => 1.55,'ASP_B_CB' => 1.62,'ASP_B_C' => 1.56,'ASP_H_CA' => 1.57,'ASP_H_CB' => 1.33,'ASP_H_C' => 1.31,'ASP_C_CA' => 1.52,
                            'ASP_C_CB' => 1.32,'ASP_C_C' => 1.3,'CYS_B_CA' => 2.17,'CYS_B_CB' => 7.55,'CYS_B_C' => 1.6,'CYS_H_CA' => 3.57,'CYS_H_CB' => 6.35,'CYS_H_C' => 1.75,'CYS_C_CA' => 2.88,'CYS_C_CB' => 6.29,'CYS_C_C' => 1.73,'GLN_B_CA' => 1.35,'GLN_B_CB' => 1.93,'GLN_B_C' => 1.32,'GLN_H_CA' => 1.22,'GLN_H_CB' => 0.92,'GLN_H_C' => 1.32,
                            'GLN_C_CA' => 1.65,'GLN_C_CB' => 1.69,'GLN_C_C' => 1.59,'GLU_B_CA' => 1.61,'GLU_B_CB' => 1.98,'GLU_B_C' => 1.37,'GLU_H_CA' => 1.19,'GLU_H_CB' => 0.99,'GLU_H_C' => 1.21,'GLU_C_CA' => 1.69,'GLU_C_CB' => 1.55,'GLU_C_C' => 1.32,'HIS_B_CA' => 1.74,'HIS_B_CB' => 2.22,'HIS_B_C' => 1.46,'HIS_H_CA' => 1.74,'HIS_H_CB' => 1.46,
                            'HIS_H_C' => 1.29,'HIS_C_CA' => 1.82,'HIS_C_CB' => 2.42,'HIS_C_C' => 1.75,'ILE_B_CA' => 1.51,'ILE_B_CB' => 1.98,'ILE_B_C' => 1.4,'ILE_H_CA' => 1.79,'ILE_H_CB' => 1.15,'ILE_H_C' => 1.26,'ILE_C_CA' => 1.71,'ILE_C_CB' => 1.69,'ILE_C_C' => 1.62,'LEU_B_CA' => 1.25,'LEU_B_CB' => 2,'LEU_B_C' => 1.48,'LEU_H_CA' => 1.27,
                            'LEU_H_CB' => 1.05,'LEU_H_C' => 1.34,'LEU_C_CA' => 1.58,'LEU_C_CB' => 1.64,'LEU_C_C' => 1.63,'LYS_B_CA' => 1.28,'LYS_B_CB' => 1.78,'LYS_B_C' => 1.29,'LYS_H_CA' => 1.44,'LYS_H_CB' => 0.88,'LYS_H_C' => 1.44,'LYS_C_CA' => 1.67,'LYS_C_CB' => 1.67,'LYS_C_C' => 1.38,'GLY_B_CA' => 1.2,'GLY_B_CB' => 0,'GLY_B_C' => 1.56,
                            'GLY_H_CA' => 1.16,'GLY_H_CB' => 0,'GLY_H_C' => 1.26,'GLY_C_CA' => 1.05,'GLY_C_CB' => 0,'GLY_C_C' => 1.39,'MET_B_CA' => 1.31,'MET_B_CB' => 2.29,'MET_B_C' => 1.47,'MET_H_CA' => 1.8,'MET_H_CB' => 1.66,'MET_H_C' => 1.15,'MET_C_CA' => 1.4,'MET_C_CB' => 2.26,'MET_C_C' => 1.89,'PHE_B_CA' => 1.55,'PHE_B_CB' => 1.74,
                            'PHE_B_C' => 1.6,'PHE_H_CA' => 1.95,'PHE_H_CB' => 1.31,'PHE_H_C' => 1.33,'PHE_C_CA' => 1.92,'PHE_C_CB' => 1.98,'PHE_C_C' => 1.63,'PRO_B_CA' => 1.04,'PRO_B_CB' => 1.2,'PRO_B_C' => 1.43,'PRO_H_CA' => 1.09,'PRO_H_CB' => 0.95,'PRO_H_C' => 1.44,'PRO_C_CA' => 1.19,'PRO_C_CB' => 0.95,'PRO_C_C' => 1.3,'SER_B_CA' => 1.36,
                            'SER_B_CB' => 1.51,'SER_B_C' => 1.49,'SER_H_CA' => 1.62,'SER_H_CB' => 1.12,'SER_H_C' => 1.4,'SER_C_CA' => 1.61,'SER_C_CB' => 1.27,'SER_C_C' => 1.3,'THR_B_CA' => 1.56,'THR_B_CB' => 1.51,'THR_B_C' => 1.48,'THR_H_CA' => 2.35,'THR_H_CB' => 1.17,'THR_H_C' => 1.18,'THR_C_CA' => 1.95,'THR_C_CB' => 1.33,'THR_C_C' => 1.42,
                            'TRP_B_CA' => 1.86,'TRP_B_CB' => 1.7,'TRP_B_C' => 1.66,'TRP_H_CA' => 1.82,'TRP_H_CB' => 1.4,'TRP_H_C' => 1.4,'TRP_C_CA' => 1.7,'TRP_C_CB' => 1.74,'TRP_C_C' => 1.14,'TYR_B_CA' => 1.7,'TYR_B_CB' => 1.85,'TYR_B_C' => 1.43,'TYR_H_CA' => 1.77,'TYR_H_CB' => 1.11,'TYR_H_C' => 1.41,'TYR_C_CA' => 1.99,'TYR_C_CB' => 1.84,
                            'TYR_C_C' => 1.64,'VAL_B_CA' => 1.57,'VAL_B_CB' => 1.61,'VAL_B_C' => 1.36,'VAL_H_CA' => 1.57,'VAL_H_CB' => 0.72,'VAL_H_C' => 1.39,'VAL_C_CA' => 2.03,'VAL_C_CB' => 1.37,'VAL_C_C' => 1.46);
#$pi = 3.14159265;


#######################################################################################################################################################################
#secondary structure element ordering: B, H, C
my %aa_conformational_preference = ('ALA'=>[ 0.789761533198793,1.49924563818542,0.878168339669498],'ARG'=>[ 0.974219498873687,1.23712983288999,1.03110602665106],
                                    'ASN'=>[ 0.698757100791386,0.771752799074036,1.6881540140909],'ASP'=>[ 0.58670654927333,0.996003919131311,1.52805133312363],
                                    'CYS'=>[ 1.30790460589582,1.00055550974591,1.06223803156554],'GLN'=>[ 0.862589127341086,1.32191214199515,1.01486357205853],
                                    'GLU'=>[ 0.728539708919817,1.46101518025535,0.956819352372455],'HIS'=>[ 0.975096688331227,0.951456647977694,1.32576354990543],
                                    'ILE'=>[ 1.88709573384393,1.02406803511045,0.667624678562322],'LEU'=>[ 1.21950809243057,1.30028096501514,0.809015603889919],
                                    'LYS'=>[ 0.884257725831421,1.25060640169846,1.07469767891425],'MET'=>[ 0.920067783938393,1.27829379995462,1.02318942384569],
                                    'PHE'=>[ 1.56927802498522,1.0379041217654,0.856528174137633],'PRO'=>[ 0.445946549529034,0.556687171691378,2.07204412859124],
                                    'SER'=>[ 0.925483289428025,0.890745209803576,1.42022466439667],'THR'=>[ 1.28953451670023,0.819248082138054,1.26134868833441],
                                    'TRP'=>[ 1.30715344446676,1.17230045293007,0.885234850527776],'TYR'=>[ 1.58067886126504,0.956572285881172,0.933288237653651],
                                    'VAL'=>[ 1.99766216849704,0.88635422678738,0.739247475095553], 'GLY'=>[ 0.632062862530898,0.49027929602511,1.71384509956936]); 

$pi = 3.14159265;
@secstruct = ('B','H','C');

@atoms = ('CA', 'CB', 'C', 'N', 'HA', 'HN');
# atom ordering: CA, CB, CO, N, HA, HN 
#%atom_index_dic = ('CA'=>0, 'CB'=> 1, 'C'=> 2, 'N'=> 3, 'HA'=>4, 'HN'=> 5); 
%ss_index_dic = ('B'=>0, 'H'=> 1, 'C'=> 2); 


#********************************************************************************************


#$nrcdir = "/Users/Noor/Documents/Research/FeatureGenerateMay08/FeatureGenerate/CS-IncompleteShift-features";
#opendir $dir, $nrcdir;

#@files = grep /nrc$/, readdir $dir;

#foreach $file(@files){
    $file = $ARGV[0]; 
    #$file = "A001_bmr4032_1KF3A.nrc";   
    print "processing $file\n ";
    open $in, "< $file";
    @f = split /\./, $file;
    $filename = $f[0];
    #open $out, "> $nrcdir/$filename.ss_prob_refdb";
    open $out, "> $filename.ss_prob_seccs";
    #@lines = <$in>;
    %resid_atom_cs = ();
    %resid_residue = ();
        
    while ($line = <$in>){
        
        if($line =~ /^\d+\s+[A-Z]+/){    
            #push @b, $line;
            #print "$line\n";
            @elems = split /\s+/, $line;
            $resid = $elems[0];
            $residue = $elems[1];
            $atom = $elems[2];
            $seccs = $elems[5];
            $resid_atom_cs{$resid.'_'.$atom} = $seccs;
            if (not exists  $resid_residue{$resid}){ 
                $resid_residue{$resid} = $residue;
            }    
        }    
    }
        
    foreach $key(sort{$a <=> $b} keys (%resid_residue)){
        #print "$key, $resid_residue{$key}\n";
        @pref_ss_seq = ();
        @prob_ss_cs = ();
        for($i = 0; $i < scalar(@secstruct);$i++){  
          
          $p[$i] = 1;
          for ($a = 0; $a < @atoms; $a++){
    
            $resid = $key;
            $aa = $resid_residue{$resid};
            $atom = $atoms[$a];    
            $resid_atom  = $resid.'_'.$atom;
            if (($aa eq 'PRO' and ($atom eq 'N' or $atom eq 'HN')) or ($aa eq 'GLY' and $atom eq 'CB') ){
                next;
            }
      
            if (exists $resid_atom_cs{$resid_atom}){
                $seccs = $resid_atom_cs{$resid_atom};
                    
              # calculating  the probability of secondary structure type (B,C,H)
              # multiplying over three secondary structure types gives us the join probability distribution as each probability is independent
      
                # printing debugging information
                #print "\n$resid $aa $atom $secstruct[$i] $stdev_seccs_by_aa_ss_type{$aa.'_'.$secstruct[$i].'_'.$atom} \n\n";
                
                #$arg = - (($seccs - $avg_seccs_by_aa_ss_type{$aa.'_'.$secstruct[$i].'_'.$atom}) ** 2) / (2 *$stdev_seccs_by_aa_ss_type{$aa.'_'.$secstruct[$i].'_'.$atom} ** 2 );
                $arg =  -(($seccs - $avg_seccs_by_aa_ss_type{$aa.'_'.$secstruct[$i].'_'.$atom})**2) / (2 *$stdev_seccs_by_aa_ss_type{$aa.'_'.$secstruct[$i].'_'.$atom} **2);
                #print "$arg\n";
                # calculating probability density function
                # gaussian distribution with mean at the averaged chemical shift for (B,H,C) secondary structure types
                $f_value =  1/(sqrt(2*$pi)*$stdev_seccs_by_aa_ss_type{$aa.'_'.$secstruct[$i].'_'.$atom}) * exp ($arg);
                
                # As the probability for one of six atoms being in one of the secondary structure type is independent,
                # we will mutiply the individual probabilities of six backbone atoms to calculate the join probability
                # distribution
                
                $p[$i] = $p[$i] * $f_value;
            }
        }
            
             
        $p[$i] = $p[$i] * 1.0e5; # p[i] turns out to be a very small number, so raise it to a large value
        push @prob_ss_cs, $p[$i];     
        
    
        $ss_index = $ss_index_dic{$secstruct[$i]}; 
        
        push @pref_ss_seq , $aa_conformational_preference{$aa}[$ss_index];
        
    }#forloop2
            
      
      $sum_prob_cs = $prob_ss_cs[0] + $prob_ss_cs[1] + $prob_ss_cs[2];
      $sum_pref_seq = $pref_ss_seq[0] + $pref_ss_seq[1] + $pref_ss_seq[2];
      
      $strand_prob_cs = $prob_ss_cs[0]/$sum_prob_cs;
      $helix_prob_cs = $prob_ss_cs[1]/$sum_prob_cs;
      $coil_prob_cs = $prob_ss_cs[2]/$sum_prob_cs;
      
      $strand_pref_seq = $pref_ss_seq[0]/$sum_pref_seq;
      $helix_pref_seq = $pref_ss_seq[1]/$sum_pref_seq;
      $coil_pref_seq = $pref_ss_seq[2]/$sum_pref_seq;
      
      print $out "$resid $aa $strand_prob_cs $helix_prob_cs $coil_prob_cs $strand_pref_seq $helix_pref_seq $coil_pref_seq\n";
      
      
           
    }#forloop1  
    
#}#main foreach ends here
    


