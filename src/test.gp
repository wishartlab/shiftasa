set terminal canvas  solid butt size 600,400 enhanced fsize 10 lw 1 fontscale 1 name "hypertext_1" jsdir "."
set output 'hypertext.1.js'
unset border
unset key
set encoding utf8
set datafile separator "	"
set size ratio 1 1,1
set noxtics
set noytics
set title "Hypertext is shown when the mouse is over a point" 
Scale(size) = 0.08*sqrt(sqrt(column(size)))
City(String,Size) = sprintf("%s\npop: %d", stringcolumn(String), column(Size))
GPFUN_Scale = "Scale(size) = 0.08*sqrt(sqrt(column(size)))"
GPFUN_City = "City(String,Size) = sprintf(\"%s\\npop: %d\", stringcolumn(String), column(Size))"
plot 'cities.dat' using 5:4:(City(1,3)):(Scale(3))      with labels hypertext point pt 7 ps var lc rgb "#ffee99",      'cities.dat' using 5:4:(City(1,3)):(Scale(3))      with labels hypertext point pt 6 ps var lc rgb "black" lw 0.1
